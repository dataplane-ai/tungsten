#ifndef DRAM_H_
#define DRAM_H_

#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <array>
#include <cassert>
#include <unordered_set>
#include <optional>
#include <random>

#include "MemoryFactory.h"

#include "module.h"
#include "interface.h"
#include "token.h"
#include "statistics.h"

struct DRAMCommand {
  bool                     write {false};
  int64_t                 addr {0}; //BURST LEVEL
  int32_t data;
  DRAMCommand(bool write=false, int64_t addr=0) : write(write), addr(addr) {}
};

struct SparseDRAMCommand {
  bool                     write {false};
  std::array<int32_t, 16>  addr;
  std::array<int32_t, 16>  data;
};


extern template struct CheckedSend<DRAMCommand>;
extern template struct CheckedReceive<DRAMCommand>;
std::ostream& operator<<(std::ostream& stream, SparseDRAMCommand& cmd);
std::ostream& operator<<(std::ostream& stream, DRAMCommand& cmd);

// This wraps a DRAMSim2 controller
class DRAMController : public Module {
  // Rob_entry = {addr, returned, write}
  using Rob_entry = std::tuple<int64_t, bool, bool>;
  using Rob       = std::list<Rob_entry>;

  bool limiter_mode{false};
  uint64_t cycles{0};
  uint64_t bytes{0};
  float bytes_per_ns{100};
  int core_clock{1600};

  // Coalesce the last few bursts for the ideal DRAM to behave similarly to the modeled DRAM.
  const int coalesce_for_ideal{64};
  std::set<uint64_t> inflight_bursts;
  std::deque<uint64_t> inflight_bursts_list;

  std::mt19937 rd;

  const int bank_shift{5};
  int                                       burstSize{32}; // in byte

  std::array<int, 1000> dram_ticks{0};

  struct SpRob_entry {
    array<int32_t,16> addrs;
    array<int32_t,16> data;
    array<bool,16>    valid;
    array<bool,16>    complete{false};
    array<bool,16>    sent{false};
    bool              write;
  };
  struct mem_region{
    uint64_t base;
    uint64_t size;
    vector<optional<int32_t>> mem;
    mem_region(uint64_t _base, uint64_t _size) : base(_base), size(_size) {
      mem.resize(size);
    }
    bool maps(uint64_t addr) const {
      return (base*4 <= addr) && ((base+size)*4 > addr);
    }
    void fill(uint32_t data) {
      for (auto& d : mem)
        d = data;
    }
    void wr(uint64_t addr, int32_t data) {
      //cout << "Mem write " << data << " to " << addr << endl;
      if (!maps(addr))
        throw runtime_error("Access to out-of-range DRAM address!");
      mem.at(addr/4-base) = data;
    }
    int32_t rd(uint64_t addr) const {
      if (!maps(addr))
        throw runtime_error("Access to out-of-range DRAM address!");
      if (!mem.at((addr/4-base)))
        throw runtime_error("Access to uninitialized DRAM address!");
      //cout << "Read addr " << addr << ": " << *mem[(addr/4-base)] << endl;
      return *mem.at((addr/4-base));
    }
    void dump(const string& name, int count, bool uns) {
      int32_t val;
      uint64_t a{base};
      if (name == "-") {
        while (count--) {
          if (uns) 
            cout << (uint32_t)rd((4*a++)) << endl;
          else
            cout << rd((4*a++)) << endl;
        }
      } else {
        ofstream out(name);
        while (count--) {
          if (uns) 
            out << (uint32_t)rd((4*a++)) << endl;
          else
            out << rd((4*a++)) << endl;
        }
      }
    }
    void load(const string& name) {
      int32_t val;
      uint64_t a{base};
      ifstream in(name);
      while (in >> val) {
        //cout << "Init " << a << " to " << val << endl;
        wr(4*(a++), val);
      }
    }
    bool intersects(const mem_region &other) const {
      if (other.maps(4*base) || maps(4*other.base))
        return true;
      return false;
    }
  };

  using SpRob       = std::list<SpRob_entry>;

  uint64_t cycle{0};
  std::vector<uint64_t>                     input_served;
  std::vector<CheckedReceive<DRAMCommand>*> inputs;
  std::vector<CheckedSend<DRAMCommand>*>    outputs;
  std::vector<Rob>                          robs;
  
  std::vector<CheckedReceive<SparseDRAMCommand>*> sparse_inputs;
  std::vector<CheckedSend<SparseDRAMCommand>*>    sparse_outputs;
  std::vector<SpRob>                        sparse_robs;

  vector<mem_region>                        mem_data;

  ramulator::MemoryBase                    *mem {nullptr};
  ramulator::Config                         configs;
  std::string                               memfile;
  int                                       memTicks;
  int                                       simTicks;
  int                                       currentTick{0};
  std::string                               tracename;
  std::unordered_set<uint64_t>              issued_read; // burst address
  std::unordered_set<uint64_t>              sparse_issued_read; // byte address
  std::unordered_set<uint64_t>              issued_write;
  long                                      served_read{0};

  long                                      input_read{0};
  long                                      coalesced_read{0};
  std::unordered_set<uint64_t>              all_issued_read; // burst address
  long                                      missed_coalesced_read{0};

  long                                      served_write{0};

  BandwidthStats read;
  BandwidthStats write;

  DRAMCommand SliceSparseCmd(const SparseDRAMCommand& cmd, int idx);
  int last_sparse_in{0};

  // Only finalize when clocked
  void FinalizeClock();
 public:
  const int                                 robsize {512};
  DRAMController(); 
  ~DRAMController(); 
  DRAMController(const std::string& _name,
                 const std::string& _memfile,
                 std::initializer_list<CheckedReceive<DRAMCommand>*> _inputs,
                 std::initializer_list<CheckedSend<DRAMCommand>*> _outputs);
  void Clock();
  void Eval();
  void Log(std::ostream& log);
  void Finalize();

  void Fill(int32_t n);
  void Add(CheckedReceive<DRAMCommand>* input, CheckedSend<DRAMCommand>* output);
  void AddSparse(CheckedReceive<SparseDRAMCommand>* input, CheckedSend<SparseDRAMCommand>* output);
  void AddMemRegion(uint64_t base, uint64_t size, const string& name="");
  void DumpMemRegion(uint64_t base, uint64_t size, const string& name="-", bool uns=false);
  void ReadCallback(const ramulator::Request &req);
  void WriteCallback(const ramulator::Request &req);

  void Apply(const string& cmd, const vector<string>& vals);
  ramulator::Request GetRequest(const DRAMCommand &cmd);
  double GetElapsedTime(long cycles); // in seconds
  double GetAverageTotalPower(); // in mJ
  double GetAverageReadBW(double time); // in GB/s
  double GetAverageWriteBW(double time); // in GB/s

  void DumpStats();
  const BandwidthStats & ReadBW();

  const BandwidthStats & WriteBW(); 
};

// This is a write interface to a DRAMSim2 controller
class DRAMWriteAG : public Module {
  //const int                 bytes {4};
  CheckedSend<DRAMCommand> *toCtrl;
  CheckedReceive<Token>    *addr;
  CheckedReceive<Token>    *data;

 public:
  explicit DRAMWriteAG(const std::string& _name, CheckedSend<DRAMCommand> *_toCtrl,
      CheckedReceive<Token> *_addr, CheckedReceive<Token> *_data ) :
    Module(_name), toCtrl(_toCtrl), addr(_addr), data(_data) {}
  void Eval();
};

// This is a read interface to a DRAMSim2 controller
class DRAMReadAG : public Module {
  //const int                 bytes {4};
  CheckedSend<DRAMCommand> *toCtrl;
  CheckedReceive<Token>    *addr;
  CheckedReceive<Token>    *en;

 public:
  explicit DRAMReadAG(const std::string& _name, CheckedSend<DRAMCommand> *_toCtrl,
      CheckedReceive<Token> *_addr, CheckedReceive<Token> *_en) :
    Module(_name), toCtrl(_toCtrl), addr(_addr), en(_en) {}
  void Eval();
};

// This is a read interface to a DRAMSim2 controller (TODO)

#endif // DRAM_H_
