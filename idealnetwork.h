#ifndef IDEALNETWORK_H_
#define IDEALNETWORK_H_

#include <regex>
#include <vector>
#include <unordered_map>

#include "module.h"
#include "interface.h"
#include "plasticine/templates/broadcast.h"

#include "debug.h"

using namespace std;

// template<int D>
class IdealNetwork : public Module,
                      public virtual NetworkInterface<Token, int, int> {

 // flow -> network input fifo
 vector<VariableFIFO<Token>*> fifos;
 int max{IDEAL_DEPTH};
 // unordered_map<int, CheckedSend<Token>*> inputs;
 // unordered_map<int, CheckedReceive<Token>*> outputs;
 vector<CheckedSend<Token>*> inputs;
 vector<CheckedReceive<Token>*> outputs;
 vector<Broadcast<Token>*> broadcasts;
 string currName;

 public:
  explicit IdealNetwork(const std::string& _name) : Module(_name) {}

  void SetParameter(const std::string& key, const std::string& val) {
    if (key == "name") {
      currName = val;
    }
    if (key == "src") {
      int src = stoi(val);
      auto* input = new VariableFIFO<Token>("netbuf_" + currName, max);
      AddChild(input, false);
      while (inputs.size() <= src)
        inputs.push_back(nullptr);
      assert(!inputs.at(src));
      inputs[src] = input;

      auto* bc = new Broadcast<Token>("bc_" + currName, {input}, {});
      AddChild(bc, false);
      fifos.push_back(input);
      broadcasts.push_back(bc);
    }
    if (key == "dst") {
      int dst = stoi(val);
      auto* output = new VariableFIFO<Token>("netbuf_" + currName, max);
      AddChild(output, false);
      while (outputs.size() <= dst)
        outputs.push_back(nullptr);
      assert(!outputs.at(dst));
      outputs[dst] = output;
      fifos.push_back(output);
      broadcasts.back()->AddTo(output);
    }
  }
  bool Ready(int src, int vc, int flow) const {
//#ifdef DEBUG
    //if (!inputs.count(src))
      //throw module_error(this, "Accessing src=" + to_string(src) + " not exists");
//#endif
    return inputs[src]->Ready();
  }
  int GetCredit(int src, int vc, int flow) const {
    return 1;
  }
  void Push(shared_ptr<Token> v, int src, int vc, int flow) {
//#ifdef DEBUG
    //if (!inputs.count(src))
      //throw module_error(this, "Accessing src=" + to_string(src) + " not exists");
//#endif
    inputs[src]->Push(*v);
  }
  void Push(const Token& v, int src, int vc, int flow) {
//#ifdef DEBUG
    //if (!inputs.count(src))
      //throw module_error(this, "Accessing src=" + to_string(src) + " not exists");
//#endif
    inputs[src]->Push(v);
  }
  bool Valid(int dst, int vc) const {
//#ifdef DEBUG
    //if (!outputs.count(dst))
      //throw module_error(this, "Accessing dst=" + to_string(dst) + " not exists");
//#endif
    return outputs[dst]->Valid();
  }
  Token Read(int dst, int vc) {
//#ifdef DEBUG
    //if (!outputs.count(dst))
      //throw module_error(this, "Accessing dst=" + to_string(dst) + " not exists");
//#endif
    Token token = outputs[dst]->Read();
    outputs[dst]->Pop();
    return token;
  }
  void LogSrc(std::ostream& log, int src, int flow) {
    dynamic_cast<Module *>(inputs[src])->Log(log);
  }
  void LogDst(std::ostream& log, int dst, int vc) {
    dynamic_cast<Module *>(outputs[dst])->Log(log);
  }
  void Eval() {
    for (auto m: fifos)
      m->Eval();
    for (auto m: broadcasts)
      m->Eval();
  }
  void Clock() {
    for (auto m: fifos)
      m->Clock();
    for (auto m: broadcasts)
      m->Clock();
  }
};

#endif // IDEALNETWORK_H_

