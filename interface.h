#ifndef INTERFACE_H_
#define INTERFACE_H_

#include <memory>
#include "token.h"

using namespace std;

struct CheckedReady {
  virtual bool Ready() const    = 0;
};
template<class T>
struct CheckedSend : CheckedReady {
  virtual void Push(const T& v)      = 0;
  virtual void Push(const T&& v) {
    Push(v);
  }
  virtual void Push(shared_ptr<T> v) {
    Push(*v);
  }
};
extern template struct CheckedSend<Token>;

struct CheckedValid {
  virtual bool Valid() const    = 0;
};
template<class T>
struct CheckedReceive : CheckedValid {
  virtual void Pop()            = 0;
  virtual const T& Read() const = 0;
  virtual const T ReadPop() {
    T t = Read();
    Pop();
    return t;
  }
};
extern template struct CheckedReceive<Token>;

template<class T>
struct ValSend {
  virtual void Push(const T& v) = 0;
  virtual void Push(const T&& v) {
    Push(v);
  }
};

template<class T>
struct ValReceive : CheckedValid {
  virtual T    Read() const     = 0;
  virtual T    ReadMove() {
    return Read();
  }
};

template<class T, class A, class ID>
struct NetworkInterface {
  virtual bool Ready(A src, ID vc, ID flow) const           = 0;
  virtual void Push(const T& v,      A src, ID vc, ID flow) = 0;
  virtual void Push(shared_ptr<T> v, A src, ID vc, ID flow) {
    Push(*v, src, vc, flow);
  }
  virtual bool Valid(A dst, ID vc) const                    = 0;
  virtual T Read(A dst, ID vc)                              = 0;
  // Debug API
  virtual int GetCredit(A src, ID vc, ID flow) const        = 0;
  virtual void LogSrc(std::ostream& log, int src, int flow) = 0;
  virtual void LogDst(std::ostream& log, int dst, int vc)   = 0;
};

struct DelayNetBuild {
  virtual void BuildNet() = 0;
};

#endif  // INTERFACE_H_
