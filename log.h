#ifndef LOG_H_
#define LOG_H_

#include <iostream>
#include <fstream>

#include "interface.h"

template <class T>
class Log : public Module {
 protected:
  std::ofstream file;
  std::vector<CheckedReceive<T>*> receives;
  std::vector<ValReceive<T>*> vreceives;
  int in; // number of inputs
  bool isChecked; // uses checked interface

 public:
  explicit Log(const std::string& _name,
      std::initializer_list<CheckedReceive<T>*> _receives) :
    Module(_name) {
      file.open(_name); // use module name for now
      in = _receives.size();
      receives = _receives;
      isChecked = true;
    }

  explicit Log(const std::string& _name,
      std::initializer_list<ValReceive<T>*> _vreceives) :
    Module(_name) {
      file.open(_name);
      in = _vreceives.size();
      vreceives = _vreceives;
      isChecked = false;
    }

  void Eval() {
    for (int i = 0; i < in; i++) {
      if (isChecked) {
        CheckedReceive<T> *receive = receives[i];
        if (receive->Valid()) {
          file << "Receive " + std::to_string(i) + ": " + receive->Read().ToString() << std::endl;
        }
      } else {
        ValReceive<T> *vreceive = vreceives[i];
        if (vreceive->Valid()) {
          file << "Receive " + std::to_string(i) + ": " + vreceive->Read().ToString() << std::endl;
        }
      }
    }
  }

  void Clock() {
  }
};

#endif // LOG_H_
