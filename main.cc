#include <iostream>
#include <fstream>
#include <istream>

#include "repl.h"

#include "sparsity/test/Random.h"
#include "sparsity/test/RandomRMW.h"

RandomTestRMW<4096,16,int> DUT("DUT");
TestDRAMExec<float> DUT("DUT");

int main(int argc, char **argv) {

  REPL Top(&DUT, std::cout);
  if (argc == 2) {
    std::ifstream in(argv[1]);
    Top.Run(in, true);
  } else {
    Top.Run(std::cin);
  }
  std::ofstream logfile("hello.blog", std::ofstream::out|std::ofstream::binary);
  binlog::consume(logfile);
  return 0;
}
