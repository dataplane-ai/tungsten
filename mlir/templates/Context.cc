#include "mlir/templates/Context.h"
#include "mlir/templates/Host.h"

#include "util.h"
#include "debug.h"

// #define LINE_TRACE_ALL

MLIRRetime::MLIRRetime(const std::string& name, int count, const std::vector<int>& max_toks_in, const std::vector<bool>& restrict_scalar, NI* dynnet, NI* statnet, NI* idealnet) : Module(name), count(count), max_toks(max_toks_in), restrict_scalar(restrict_scalar) {
  toks.resize(count);
  scal_slots.resize(count, 0);
  for (int i=0; i<count; i++) {
    auto _in = new NetworkOutput(fmt::format("in{}", i), dynnet, statnet, idealnet);
    auto _out = new NetworkInput(fmt::format("out{}", i), dynnet, statnet, idealnet);
    AddChild(_in);
    AddChild(_out);
    in.emplace_back(_in);
    out.emplace_back(_out);
    if (!restrict_scalar[i])
      max_toks[i] *= 16;
  }
}

void MLIRRetime::_proc_tok(const Token& t, int i, bool enq) {
  int slots = t.nvalid;
  if (restrict_scalar[i])
    slots = 1;
  if (t.done_vec > 1)
    slots++;
  assert(scal_slots[i] >= 0 && scal_slots[i] <= max_toks[i]);

  assert(slots <= 17);
  assert(slots >= 0);

  if (enq)
    scal_slots[i] += slots;
  else
    scal_slots[i] -= slots;

  assert(scal_slots[i] >= 0); 
  if (scal_slots[i] > max_toks[i])
    fmt::print(stderr, "{}/{}: Scalar slots {} max toks {} i={}\n", path, name, scal_slots[i], max_toks[i], i);
  assert(scal_slots[i] <= max_toks[i]);
}

void MLIRRetime::Eval() {
  for (int i=0; i<count; i++) {
    if (toks[i].size() && out[i]->Ready()) {
      _proc_tok(toks[i].front(), i, false);
      out[i]->Push(toks[i].front());
      toks[i].pop_front();
    }
    if (scal_slots[i]+17 < max_toks[i] && in[i]->Valid()) {
      // This check is broken for the vector-of-bool case
      if (restrict_scalar[i])  {
        if (in[i]->Read().nvalid > 1) {
          for (int j=0; j<in[i]->Read().nvalid; j++) {
            int v = in[i]->Read()[j];
            assert(v == 0 || v == 1);
          }
        }
      }
      _proc_tok(in[i]->Read(), i, true);
      toks[i].push_back(in[i]->ReadPop());
    }
  }
}

MLIRRetimeDRAM::MLIRRetimeDRAM(const std::string& name, NI* dynnet, NI* statnet, NI* idealnet, DRAMController *dram) : DualDRAMAddrGen(name) {
  auto _in = new NetworkOutput("in", dynnet, statnet, idealnet);
  auto _out = new NetworkInput("out", dynnet, statnet, idealnet);
  AddChild(_in);
  AddChild(_out);
  in = _in;
  out = _out;
  SetupDRAM(dram);
  auto buf = new int[1024*32];
  next_read_addr = (long)buf;
  next_write_addr = (long)buf;
}

void MLIRRetimeDRAM::Eval() {
  if (in->Valid() && out->Ready() && !toks.size()) {
    out->Push(in->ReadPop());
    continue_inactive_count = 0;
    return;
  }

  if (in->Valid() && !out->Ready())
    continue_inactive_count++;
  if (continue_inactive_count == threshold)
    fmt::print("{}/{} beginning to buffer to DRAM.\n", path, name);

  if (continue_inactive_count >= threshold && !burst_pend_to_dram && in->Valid()) {
    toks.emplace_back(in->ReadPop());
    burst_pend_to_dram += 2;
  }
  if (burst_pend_to_dram && burstcmd.Ready()) {
    burstcmd.Push(DRAMCommand(true, 32*(next_write_addr++)));
    burst_pend_to_dram--;
  }
  if (burstrsp.Valid()) {
    burstrsp.Pop();
    burst_pend_to_read++;
  }
  if (burst_pend_to_read && burstcmd2.Ready()) {
    burstcmd2.Push(DRAMCommand(false, 32*(next_read_addr++)));
    burst_pend_to_read--;
  }
  if (burstrsp2.Valid() && burst_pend_to_output < 4) {
    burstrsp2.Pop();
    burst_pend_to_output++;
  }
  if (out->Ready() && burst_pend_to_output >= 2) {
    out->Push(toks.front());
    toks.pop_front();
    burst_pend_to_output -= 2;
    if (continue_inactive_count >= threshold)
      fmt::print("{}/{} stop buffering to DRAM.\n", path, name);
    continue_inactive_count = 0;
  }
}

int MLIRContext::_outputs_stall() const {
  int ret{0};
  for (const auto& [o, src, val, inv, while_exit, while_be, loc] : outputs)
    if (!o->Ready())
      ret |= (1<<loc);
  return ret;
}

template<class TP>
void MLIRContext::_set_constant(int idx, TP val) {
  Token t;
  t.nvalid = 16;
  t.type = TokVecType<TP>();
  for (int i=0; i<16; i++) {
    (TP&)t[i] = val;
    t.threadIDs.emplace_back(-1);
  }
  t.special = "constant";
  reg_data.at(idx) = t;
}

void MLIRContext::AddRegister(int idx, const string tp) {
  if (reg_data.size() <= idx)
    reg_data.resize(idx+1, Token((int)0));
  assert(!reg_types.count(idx));
  if (tp == "i32") {
    reg_types[idx] = TokVecType<int>();
  } else if (tp == "f32") {
    reg_types[idx] = TokVecType<float>();
  } else {
    assert(false);
  }
}

void MLIRContext::LabelRegister(int idx, const string label) {
  reg_labels[idx] = label;
  label_regs[label] = idx;
}

void MLIRContext::AddConstant(int idx, int val) {
  AddRegister(idx);
  _set_constant(idx, val);
}

void MLIRContext::AddConstant(int idx, double val) {
  AddRegister(idx);
  _set_constant(idx, (float)val);
}

void MLIRContext::AddSymConstant(int idx, MLIRSymConst* ref) {
  AddRegister(idx);
  ref->Register(this, idx);
}

void MLIRContext::AddInput(int dst_reg, int depth, bool counter, int loc, bool fifo, bool scalar, bool internal, bool token) {
  NetworkOutput *in = new NetworkOutput("in"+std::to_string(loc),
      dynnet, statnet, idealnet);
  assert(!input_locs.count(loc));
  input_locs.insert(loc);
  AddChild(in);
  if (counter) {
    assert(false);
#if 0
    int min_idx = ctrl.AddConstant(0, -1);
    int max_idx = ctrl.AddInput(in, 0, -1, -1, "scalar_counter", false, false);
    int step_idx = ctrl.AddConstant(1, -1);
    input_regs.emplace_back(dst_reg);
    ctrl.AddCounter(min_idx, max_idx, step_idx, -1, dst_reg, depth, false);
#endif
  } else {
    if (dst_reg >= 0 && dst_reg < 10000)
      input_regs.emplace_back(dst_reg);
    ctrl.AddInput(in, depth, loc, dst_reg, fifo, scalar, internal, token);
  }
}

void MLIRContext::AddCounter(int dst_reg, int min_loc, int max_loc, int
    step_loc, int min_const, int max_const, int step_const, int depth, bool flat) {
  if (min_loc < 0)
    min_loc = ctrl.AddConstant(min_const, -1);
  if (max_loc < 0)
    max_loc = ctrl.AddConstant(max_const, -1);
  if (step_loc < 0)
    step_loc = ctrl.AddConstant(step_const, -1);
  if (dst_reg >= 0 && dst_reg < 10000)
    input_regs.emplace_back(dst_reg);
  ctrl.AddCounter(min_loc, max_loc, step_loc, -1, dst_reg, depth, flat);
}

void MLIRContext::AddMerge(int dst_reg, int left, int right, bool is_while) {
  if (dst_reg >= 0 && dst_reg < 10000)
    input_regs.emplace_back(dst_reg);
  ctrl.AddMerge(left, right, dst_reg, is_while);
}

void MLIRContext::AddValid(int dst_reg) {
  AddConstInput(dst_reg, -1, 1);
}

void MLIRContext::AddConstInput(int dst_reg, int loc, int val) {
  if (dst_reg >= 0 && dst_reg < 10000)
    input_regs.emplace_back(dst_reg);
  ctrl.AddConstant(val, loc, dst_reg);
}

void MLIRContext::AddConstFloatInput(int dst_reg, int loc, float val) {
  input_regs.emplace_back(dst_reg);
  ctrl.AddFloatConstant(val, loc, dst_reg);
}

void MLIRContext::AddInfCounter(int dst_reg, int loc, int max) {
  if (dst_reg >= 0 && dst_reg < 10000)
    input_regs.emplace_back(dst_reg);
  ctrl.AddInfCounter(max, loc, dst_reg);
}

void MLIRContext::AddOutput(int src_reg, int loc, int val_reg, bool inv, bool while_exit, bool while_be, bool scalar) {
  assert(loc >= 0 && loc < 32);
  if (loc + 1 > max_out)
    max_out = loc + 1;
  NetworkInput *out_raw = new NetworkInput("out"+std::to_string(loc),
      dynnet, statnet, idealnet);
  AddChild(out_raw);
  CheckedSend<Token> *out = out_raw;
#ifdef COAL_OUT
  MetadataCoal *coal = new MetadataCoal("coal"+std::to_string(loc), out);
  AddChild(coal);
  out = coal;
#endif

  if (scalar) {
    ScalarOut *scal = new ScalarOut("scal"+std::to_string(loc), out);
    AddChild(scal);
    out = scal;
  }

  outputs.emplace_back(out, src_reg, val_reg, inv, while_exit, while_be, loc);
}


void MLIRContext::AddCtrlInput(int loc) {
  NetworkOutput *in = new NetworkOutput("in"+std::to_string(loc),
      dynnet, statnet, idealnet);
  assert(!input_locs.count(loc));
  input_locs.insert(loc);
  AddChild(in);

  ctrl.SetNoHold();
  ctrl.SetCtrlInput(in);
}
void MLIRContext::AddCtrlOutput(int loc) {
  assert(!ctrl_out);
  ctrl.SetNoHold();
  NetworkInput *out = new NetworkInput("out"+std::to_string(loc),
      dynnet, statnet, idealnet);
  AddChild(out);
  ctrl_out = out;
}

void MLIRContext::AddUnOp(const std::string& op, int src, int dst, int stage, int mix_out, int align, int off, int alignR, int offR) {
  if (mix_out >= 0) {
    stage_funcs[stage].emplace_back(buildOp(op, dst, src, mix_out, mix_out, mix_out, 0, 
          align, off, align, off, align, off, 
          alignR, offR));
  } else {
    stage_funcs[stage].emplace_back(buildOp(op, dst, src, src, src, mix_out, 0, 
          align, off, align, off, align, off, 
          alignR, offR));
  }
}

void MLIRContext::AddBiOp(const std::string& op, int srcA, int srcB, int dst, int stage, int mix_out, int alignA, int offA, int alignB, int offB, int alignR, int offR) {
  stage_funcs[stage].emplace_back(buildOp(op, dst, srcA, srcB, srcA, mix_out, 0, alignA, offA, alignB, offB, alignB, offB, alignR, offR));
}

void MLIRContext::AddCmpOp(const std::string& op, int srcA, int srcB, int dst, int stage, int alignA, int offA, int alignB, int offB) {
  stage_funcs[stage].emplace_back(buildOp(op, dst, srcA, srcB, srcA, -1, 0, alignA, offA, alignB, offB));
}

// TODO: special-case this to pass through the original type
void MLIRContext::AddMuxOp(int sel, int srcA, int srcB, int dst, int stage, int mix_out, int alignA, int offA, int alignB, int offB, int alignR, int offR) {
  stage_funcs[stage].emplace_back(buildOp("mux", dst, srcA, srcB, sel, mix_out, 0, alignA, offA, alignB, offB, 32, 0, alignR, offR));
}

void MLIRContext::AddRedOp(const std::string& op, int src, int dst, int stage, int level, int align, int off) {
  stage_funcs[stage].emplace_back(buildOp(op, dst, src, src, src, -1, level, align, off));
}

void MLIRContext::AddMemOp(const std::string& op, int addr, int data, int dst, int stage, int align, bool dense, int alignA, int offA, int alignB, int offB, int alignR, int offR) {
  assert(mem || dram);
  assert(alignR == 8 || alignR == 16 || alignR == 32);
  // if (op == "alloc" || op == "dealloc")
    // ctrl.SetPar(1);
  if (mem) {
    stage_funcs.at(stage).emplace_back(new MemOp(op, mem, dst, addr, data, data, align, dense, alignA, offA, alignB, offB, alignR, offR));
  } else {
    if (op == "read") {
      auto ld = new MLIRDRAMCoal(dram, dram_ctrl, false, align);
      AddChild(ld);
      stage_funcs.at(stage).emplace_back(new DRAMOp(op, ld, dst, addr, data, data, alignA, offA, alignB, offB, alignR, offR));
    } else if (op == "write") {
      auto st = new MLIRDRAMCoal(dram, dram_ctrl, true, align);
      AddChild(st);
      stage_funcs.at(stage).emplace_back(new DRAMOp(op, st, dst, addr, data, data, alignA, offA, alignB, offB, alignR, offR));
    } else {
      assert(false);
    }
  }
}

void MLIRContext::Verify() {
  // TODO: go through every reference and check that nothing's dangling.

}
/*
MLIRContext::MLIRContext(const std::string& name, int stages, NI* dynnet, NI* statnet, NI* idealnet, T* mem) 
  : Module(name), nStage(stages), dynnet(dynnet), statnet(statnet), idealnet(idealnet) {
  auto wrap = new PMUWrapper(mem);
  mem =
  stage_funcs.resize(nStage+1);
  stage_valid.resize(nStage+1, false);
}*/

MLIRContext::MLIRContext(const std::string& name, int stages, NI* dynnet, NI* statnet, NI* idealnet, MLIRDRAM* dram, DRAMController *dram_ctrl) 
  : Module(name), nStage(stages), dynnet(dynnet), statnet(statnet), idealnet(idealnet), dram(dram), dram_ctrl(dram_ctrl) {
  stage_funcs.resize(nStage+1);
  stage_valid.resize(nStage+1, false);
  SetInstrumentation();
}

void MLIRContext::Clock() {

}

void MLIRContext::Eval() {
  if (!trace_enabled)
    EnableLineTrace(ctrl.getMaxIn(), max_out, 0, 0, 0, 16, 16);
  ctrl.SetName(fmt::format("{}/{}", path, name));
  // Now, we need to take a look at pipeline stages from the outputs backward.
  // We generate a list of which stages are back-pressured.
  std::vector<bool> stage_backpressured;
  std::vector<bool> stage_stall;

  stage_backpressured.resize(nStage+1, false);
  stage_stall.resize(nStage+1, false);

  stage_backpressured[nStage] = 
    stage_valid[nStage] && _outputs_stall();
  for (int i = nStage; i >= 0; i--) {
    if (i < nStage) {
      stage_backpressured[i] = stage_valid[i] && 
        (stage_backpressured[i+1] || stage_stall[i+1]);
    }
    // Don't enable a write for this stage if any of the operations have a 
    // stall override pending
    if (i > 0) {
      for (auto op : stage_funcs[i-1]) {
        if (op->override_stall()) {
          stage_backpressured[i-1] = true;
          stage_stall[i] = true;
        }
      }
    }
  }

  // Then, working from the inputs forward, make a list of registers that are 
  // valid to be updated.
  std::vector<bool> stage_write_new;
  stage_write_new.resize(nStage+1, false);
  stage_write_new[0] = !stage_backpressured[0] && ctrl.Valid();
  if (ctrl_out && !ctrl_out->Ready())
    stage_write_new[0] = false;
  for (int i = 1; i <= nStage; i++) {
    stage_write_new[i] = (!stage_backpressured[i] && stage_valid[i-1] && !stage_stall[i]);
  }

  // Also clear/send outputs from the last stage
  if (stage_valid[nStage] && !stage_backpressured[nStage]) {
    for (auto& [out, reg, val_reg, inv, while_exit, while_be, loc] : outputs) {
      assert(val_reg >= 0 || reg >= 0);
      if (reg < 0)
        assert(val_reg >= 0);
      auto all_ones = std::array<int,16>();
      all_ones.fill(1);
      Token send = (val_reg < 0) ? reg_data[reg] : 
                   (reg < 0)     ? filter(Token(all_ones), reg_data[val_reg], inv)
                                 : filter(reg_data[reg], reg_data[val_reg], inv);
      if (while_exit && send.done_vec > 0)
        send.done_vec--;
      if ((send.done_vec || send.nvalid) && !(while_be && send.done_vec > 1)) {
#if DEBUG_PIPE
        fmt::print("{}/{} send ({:2}): @{:6} {}\n", path, name, loc, cyc, send.ToString());
        fmt::print("{}/{}          :     ids {}\n", path, name, send.IDsToString());
#endif
        out->Push(send);
      }
    }
    stage_valid[nStage] = false;
  }

  // Then, run the update function, generating next values for each register
  for (int i = nStage; i > 0; i--) {
    if (stage_write_new[i]) {
      Active();
      assert(stage_valid[i-1]);
      // The value in this stage may have been valid before execution, but it
      // *must* be invalidated before being overwritten.
      assert(!stage_valid[i]);
      
      int n_valid{0};
      // Compute the update functions for this stage
      int n_ret{16};
      std::vector<int> threadIDs;
      int endThreadID;
      int done_vec{0};
      std::set<Token*> dsts;
      for (auto op : stage_funcs[i-1]) {
        Token *dst = NULL;
        if (op->ret < 10000)
          dst = &reg_data[op->ret];
        if (op->mix_out >= 0) {
          *dst = reg_data[op->mix_out];
        } else if (dst) {
          dst->nvalid = reg_data[op->argA].nvalid;
          for (int i=0; i<dst->nvalid; i++)
            (int&)((*dst)[i]) = 0;
        }

        // std::cout << "Eval func at stage: " << i-1 << std::endl;
        bool op_ret = op->eval(reg_data[op->argA], reg_data[op->argB], 
                                 reg_data[op->argC], dst);
        if (dst) {
          dst->type = reg_types.at(op->ret);
          if (n_ret > dst->nvalid) {
            n_ret = dst->nvalid;
            threadIDs = dst->threadIDs;
            endThreadID = dst->endThreadID;
          }
          done_vec = std::max(done_vec, dst->done_vec);
          dsts.insert(dst);
        }

        if (op_ret)
          n_valid++;
      }
      // Promote length of tokens
      for (auto d : dsts) {
        if (d->nvalid != n_ret) {

          assert(d->nvalid == 16);
          for (int i=0; i<16; i++) {
            assert(d->threadIDs.at(i) == -1);
          }

          d->nvalid = n_ret;
          d->threadIDs = threadIDs;
          d->endThreadID = endThreadID;
        }
        d->done_vec = done_vec;
      }

      for (auto op : stage_funcs[i-1]) {
        Token *dst = NULL;
        if (op->ret < 10000)
          dst = &reg_data[op->ret];
        // fmt::print("{}/{}: \targA {}\n", path, name, reg_data[op->argA].ToString());
        // fmt::print("{}/{}: \targB {}\n", path, name, reg_data[op->argB].ToString());
        // fmt::print("{}/{}: \targC {}\n", path, name, reg_data[op->argC].ToString());
        // fmt::print("{}/{}: \tret  {}\n", path, name, reg_data[op->ret ].ToString());
#if DEBUG_PIPE
        if (dst) {
          fmt::print("{}/{}: Eval {} at stage {} to {} (n_valid={})\n", path, name, op->name, i-1, op->ret, n_valid);
          fmt::print("{}/{}: \targA {}\n", path, name, reg_data[op->argA].ToString());
          fmt::print("{}/{}: \targB {}\n", path, name, reg_data[op->argB].ToString());
          fmt::print("{}/{}: \targC {}\n", path, name, reg_data[op->argC].ToString());
          fmt::print("{}/{}: {}\n", path, name, dst->ToString());
          if (reg_labels.count(op->ret) && n_valid) {
            fmt::print("{}/{}: {}\n", path, name, reg_labels[op->ret]);
          }
        }
#endif
        string dbg = op->name;
        if (reg_labels.count(op->ret))
          dbg = reg_labels.at(op->ret);
        if (dst && n_valid && dbg != "copy") {
          auto tok = *dst;
          for (int i=0; i<tok.nvalid; i++) {
            auto s = tok[i];
            if (tok.type == TT_FLOATVEC)
              logValueAtThread(s.getThreadID(), (float)s, dbg, op->name);
            else
              logValueAtThread(s.getThreadID(), (int)s, dbg, op->name);
          }
        }
      }

      // Sanity check that either everyone's valid or no one's valid. This
      // should be taken care of by the MLIR assertion passes though.
      assert(n_valid == 0 || n_valid == stage_funcs[i-1].size());

      // Purge the data from the previous stage.
      stage_valid[i-1] = false;

      // Only push to the next stage if the operation wants to.
      stage_valid[i] = !!n_valid;
    }

    // After evaluating validity for this stage, check for delayed responses
    if (!stage_valid[i]) {
      int n_delayed{0};
      for (auto op : stage_funcs[i-1]) {
        Token *dst = NULL;
        if (op->ret < 10000)
          dst = &reg_data[op->ret];
        bool op_ret = op->get_delayed(dst);
        if (op_ret && dst) {
          if (reg_labels.count(op->ret)) {
            auto tok = *dst;
            for (int i=0; i<tok.nvalid; i++) {
              auto s = tok[i];
              logValueAtThread(s.getThreadID(), (int)s, reg_labels[op->ret], op->name);
            }
          }
          n_delayed++;
        }
      }

      // Sanity check that either everyone's valid or no one's valid. This
      // should be taken care of by the MLIR assertion passes though.
      assert(n_delayed == 0 || n_delayed == stage_funcs[i-1].size());
#if DEBUG_PIPE
      // fmt::print("{}/{}: poll delayed = {}\n", path, name, n_delayed);
#endif

      //if (n_delayed)
        //std::cout << "Got delayed memory operation" << std::endl;

      stage_valid[i] = !!n_delayed;
    }
  }
  // Handle inputs separately, because they pull from input buffers.
  if (!ctrl_out || ctrl_out->Ready()) {
    auto tok = ctrl.Advance();
    if (tok && ctrl_out) {
#if DEBUG_PIPE
      fmt::print("{}/{}: write output control {}\n", path, name, tok->ToString());
#endif
      ctrl_out->Push(*tok);
    }
  }
  int vec_len{0};
  if (stage_write_new[0]) {
    assert(!stage_valid[0]);
    for (const auto& i : input_regs) {
      reg_data[i] = ctrl.Read(i);
      vec_len = reg_data[0].nvalid;
    }
    ctrl.Pop();
    stage_valid[0] = true;
  }
  LineTrace(ctrl.getStarveWord(), _outputs_stall(), 0, 0, 0, vec_len, ctrl.DeqFromRight());

  cyc++;

  for (auto& s : stage_funcs)
    for (auto& op : s)
      op->Tick();

#if LINE_TRACE_PIPE
  fmt::print("{}/{}: {}\n", path, name, ctrl.LineTrace());
#endif

#if 0
  std::cout << path << name << ": ["; //
  for (int i=0; i<=nStage; i++) {
    std::cout << stage_valid[i];
  }
  std::cout << "]" << std::endl;
  for (const auto& r : reg_data)
    std::cout << "\t" << r << std::endl;
#endif
}

void MLIRContext::UpdateSymConst(int idx, int val) {
  _set_constant(idx, val);
}
