#ifndef _MLIR_TEMPLATES_CONTEXT_H
#define _MLIR_TEMPLATES_CONTEXT_H

#include "module.h"
#include "interface.h"
#include "token.h"
#include "state.h"
#include "debug.h"

#include "mlir/templates/Input.h"
#include "mlir/templates/DRAM.h"
#include "sparsity/templates/SparsePMU.h"
#include "sparsity/templates/SparseReorder.h"

#include <deque>
#include <limits>

class MLIRSymConst;

struct MLIRRetime : public Module {
  using NI = NetworkInterface<Token, int, int>;
  std::vector<CheckedReceive<Token>*> in;
  std::vector<CheckedSend<Token>*> out;
  std::vector<int> max_toks;
  std::vector<bool> restrict_scalar;
  int count{1};
  std::vector<std::deque<Token>> toks;
  std::vector<int> scal_slots;
  void _proc_tok(const Token& t, int i, bool enq);
public:
  MLIRRetime(const std::string& name, int count, const std::vector<int>& max_toks_in, const std::vector<bool>& restrict_scalar, NI* dynnet, NI* statnet, NI* idealnet);
  void Eval();
};

struct MLIRRetimeDRAM : public DualDRAMAddrGen {
  using NI = NetworkInterface<Token, int, int>;
  CheckedReceive<Token>* in;
  CheckedSend<Token>* out;
  std::deque<Token> toks;

  int continue_inactive_count{0};
  int threshold{1000};

  int burst_pend_to_dram{0};
  int burst_pend_to_read{0};
  int burst_pend_to_output{0};

  long next_write_addr{0};
  long next_read_addr{0};
public:
  MLIRRetimeDRAM(const std::string& name, NI* dynnet, NI* statnet, NI* idealnet, DRAMController *dram);
  void Eval();
};

// TODO: create a generic class here to erase the PMU types and supply one abstraction

class PMUWrapper : public SparseOuter {
  SparsePMU<int,4096,16> *a{NULL};
  SparsePMU<float,4096,16> *b{NULL};
  using Prt = SparseOuter::SparsePMUPort;
public:
  PMUWrapper(SparsePMU<int,4096,16> *pmu) : a(pmu), SparseOuter("wrap") {}
  PMUWrapper(SparsePMU<float,4096,16> *pmu) : b(pmu), SparseOuter("wrap") {}
  PMUWrapper(int *dummy) : SparseOuter("error") {
    assert(false && "Attempted to null-initialize PMU Wrapper!");
  }
  Prt *GetAllocPort(int k=-1) {
    if (a)
      return a->GetAllocPort(k);
    else if (b)
      return b->GetAllocPort(k);
    assert(false);
  }
  Prt *GetFreePort(int k=-1) {
    if (a)
      return a->GetFreePort(k);
    else if (b)
      return b->GetFreePort(k);
    assert(false);
  }
  // Returns one port (addr input, data output)
  Prt *GetReadPort(int align, bool dense) {
    if (a)
      return a->GetReadPort(-1, "unordered", align, dense);
    else if (b)
      return b->GetReadPort(-1, "unordered", align, dense);
    assert(false);
  }
  // Returns two ports (addr input, done output) and (data input)
  pair<Prt*,Prt*> GetWritePorts(int align, bool dense) {
    if (a)
      return a->GetWritePorts(-1, "unordered", align, dense);
    else if (b)
      return b->GetWritePorts(-1, "unordered", align, dense);
    assert(false);
  }
  pair<Prt*,Prt*> GetRMWPorts(const string& op, int align) {
    if (a)
      return a->GetRMWPorts(op, "unordered", -1, align);
    else if (b)
      return b->GetRMWPorts(op, "unordered", -1, align);
    assert(false);
  }
};

// Context template for auto-generated code from the MLIR backend
class MLIRContext : public Module {
  static const int vD{16};
  static const int sD{16*vD};
  const int nStage;

  long long int cyc{0};
  // using PMU = SparsePMU<int,4096,16>;
  using PMU = PMUWrapper;
  using Prt = SparseOuter::SparsePMUPort;

  Token filter(Token t, Token val, bool inv) {
    std::vector<int> vals;
    Token ret = t;
    ret.threadIDs.clear();
    for (int i=0; i<std::min(t.nvalid,val.nvalid); i++) {
      if ((val.intVec_[i] && !inv)
       || (!val.intVec_[i] && inv)) {
        vals.emplace_back(t.intVec_[i]);
        ret.threadIDs.emplace_back(unionThreadIDs(
            t.threadIDs.at(i), val.threadIDs.at(i)));
      }
    }
    // ret.copyUnionThreadIDsAndVal({t, val});
    ret.nvalid = vals.size();
    ret.endThreadID = unionThreadIDs(t.endThreadID, val.endThreadID);
    for (int i=0; i<vals.size(); i++) {
      ret.intVec_[i] = vals[i];
    }
    ret.done_vec = std::max(t.done_vec, val.done_vec);
    return ret;
  }

  struct Op {
    int cycle{0};

    // Linearly-addressed PCU register values for simplicity. Invalid values
    // are just passed as zero.
    int ret;
    int mix_out; // The value with which to mix a subword result
    int argA;
    int argB;
    int argC;

    int alignA{-1};
    int offA{-1};
    int alignB{-1};
    int offB{-1};
    int alignC{-1};
    int offC{-1};
    int alignR{-1};
    int offR{-1};
    std::string name{"XXX"};

    Token procAlign(const Token& in, int align, int off) {
      auto abs_align = abs(align);
      assert(abs_align == 32 || abs_align == 16 || abs_align == 8);
      assert(off >= 0);
      assert(off % 8 == 0);
      Token ret = in;
      if (abs_align == 32)
        return ret;
      for (int i=0; i<ret.nvalid; i++) {
        int mask =((1<<abs_align)-1);
        int tmp = (((int)ret[i])>>(off))&mask;
        if (align < 0 && (tmp&(1<<(abs_align-1))))
          tmp |= ~mask;
        (int&)ret[i] = tmp;
      }
      return ret;
    }

    void procOutAlign(Token& out, const Token& prev, int align, int off) {
      assert(align == 16 || align == 8);
      assert(off >= 0);
      assert(off % 8 == 0);
      for (int i=0; i<out.nvalid; i++) {
        // fmt::print(stderr, "procOutAlign {}, {}\n", (int)out[i], (int)prev[i]);
        unsigned mask = (1<<align)-1;
        (unsigned&)out[i] = (((unsigned)out[i])&mask) << (off)
                     | (((unsigned)prev[i])&(~(mask<<(off))));
        // fmt::print(stderr, "procOutAlign -> {}\n", (int)out[i]);
      }
    }

    Op(const std::string& opcode, int mix_out, int ret, int argA, int argB, int argC, int alignA, int offA, int alignB, int offB, int alignC, int offC, int alignR, int offR) 
      : name(opcode), argA(argA), argB(argB), argC(argC), ret(ret), mix_out(mix_out), alignA(alignA), offA(offA), alignB(alignB), offB(offB), alignC(alignC), offC(offC), alignR(alignR), offR(offR)  {
      }

    virtual bool override_stall() const = 0;
    virtual bool get_delayed(Token* ret) = 0;
    virtual bool eval(Token A, Token B, Token C, Token* ret) = 0;

    void Tick() {
      cycle++;
    }

    // Factor these out to provide a better implementation for constants
    std::optional<int> getParamTok(const Token& A, int param, bool force=false) {
      if (A.special == "constant" && !force)
        return std::nullopt;
      else if (param == 0)
        return A.nvalid;
      else if (param == 1)
        return A.done_vec;
      else 
        assert(false);
    }
    int getParam(const Token& A, const Token& B, const Token& C, int param) { 
      std::optional<int> ret;
      auto a = getParamTok(A, param);
      auto b = getParamTok(B, param);
      auto c = getParamTok(C, param);
      if (!ret)
        ret = a;
      if (!ret)
        ret = b;
      if (!ret)
        ret = c;
      if (!ret)
        ret = getParamTok(A, param, true);
      assert(ret);
      if (a)
        assert(*a == *ret);
      if (b)
        assert(*b == *ret);
      if (c)
        assert(*c == *ret);
      return *ret;
    }
    int getValid(const Token& A, const Token& B, const Token& C) {
      return getParam(A, B, C, 0);
    }
    int getDoneVec(const Token& A, const Token& B, const Token& C) {
      return getParam(A, B, C, 1);
    }
  };
  // template<class T>
  struct OpImpl : public Op {

    static const int red_delay{5};
    std::deque<std::pair<int,Token>> pending;

    std::function<void(ConstTokenSlice,ConstTokenSlice,ConstTokenSlice, TokenSlice)> op;

    Token red_accum_base{0};
    Token red_accum{0};
    int num_red{0};
    int red_level{0};
    int parent_id{-1};

    OpImpl(const std::string& opcode, int mix_out, int ret, int argA, int argB, int argC, int alignA, int offA, int alignB, int offB, int alignC, int offC, int alignR, int offR) 
      : Op(opcode, mix_out, ret, argA, argB, argC, alignA, offA, alignB, offB, alignC, offC, alignR, offR){
       if (opcode == "copy") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a; };
} else if (opcode == "inc")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a + 1; };
} else if (opcode == "not")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = ~(int)a; };

} else if (opcode == "add")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a + (int)b; };
} else if (opcode == "sub")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a - (int)b; };
} else if (opcode == "mul")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a * (int)b; }; red_accum_base = Token(1);

} else if (opcode == "remui")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a % (unsigned)b; };
} else if (opcode == "remsi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a % (int)b; };
} else if (opcode == "divsi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a / (int)b; };

} else if (opcode == "maxsi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a > (int)b ? (int)a : (int)b; };
} else if (opcode == "minsi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a > (int)b ? (int)b : (int)a; };

// TODO: actually do fp to ConstTokenSlice conversions
} else if (opcode == "addf")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = (float)a + (float)b; };
} else if (opcode == "subf")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = (float)a - (float)b; };
} else if (opcode == "mulf")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = (float)a * (float)b; }; red_accum_base = Token(1.0f);
} else if (opcode == "divf")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = (float)a / (float)b; }; 

} else if (opcode == "itof")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = (int)a; };
} else if (opcode == "ftoi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a; }; 
  
} else if (opcode == "max")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = std::max((int)a, (int)b); }; red_accum_base = Token(std::numeric_limits<int>::min());
} else if (opcode == "min")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = std::min((int)a, (int)b); }; red_accum_base = Token(std::numeric_limits<int>::max());

} else if (opcode == "maxf")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = std::max((float)a, (float)b); }; red_accum_base = Token(std::numeric_limits<float>::min());
} else if (opcode == "minf")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (float&)ret = std::min((float)a, (float)b); }; red_accum_base = Token(std::numeric_limits<float>::max());

} else if (opcode == "or")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a | (unsigned)b; };
} else if (opcode == "and")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a & (unsigned)b; }; red_accum_base = Token(-1);
} else if (opcode == "ori")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a | (unsigned)b; };
} else if (opcode == "mix")   { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a | (unsigned)b; };
} else if (opcode == "andi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a & (unsigned)b; }; red_accum_base = Token(-1);
} else if (opcode == "xori")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (unsigned)a ^ (unsigned)b; };

} else if (opcode == "shli")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)b > 32 ? 0 : (int)a << (int)b; };
} else if (opcode == "shrui")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = (int)b > 32 ? 0 : (unsigned)a >> (unsigned)b; };
} else if (opcode == "shrsi")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a/(1<<(int)b); };
} else if (opcode == "roli")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (unsigned&)ret = ((unsigned)a << (unsigned)b) | ((unsigned)a >> (32-(unsigned)b)); };

} else if (opcode == "convert_32_1_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0x1; };
} else if (opcode == "convert_32_4_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0xF; };
} else if (opcode == "convert_32_8_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0xFF; };
} else if (opcode == "convert_32_16_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0xFFFF; };
} else if (opcode == "convert_32_32_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a; };
} else if (opcode == "convert_4_32_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0xF; };
} else if (opcode == "convert_8_32_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0xFF; };
} else if (opcode == "convert_16_32_u")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&0xFFFF; };
} else if (opcode == "convert_1_32_s")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a&1; };
} else if (opcode == "convert_4_32_s")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { int tmp = (int)a&0xF; if (tmp >= 8) tmp -= 16; (int&)ret = tmp; };
} else if (opcode == "convert_8_32_s")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (char)((int)a&0xFF); };
} else if (opcode == "convert_16_32_s")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (short)((int)a&0xFFFF); };

} else if (opcode == "eq")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a == (int)b; };
} else if (opcode == "ne")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a != (int)b; };
} else if (opcode == "slt") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a <  (int)b; };
} else if (opcode == "sle") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a <= (int)b; };
} else if (opcode == "sgt") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a >  (int)b; };
} else if (opcode == "sge") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)a >= (int)b; };

} else if (opcode == "eqf")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a == (float)b; };
} else if (opcode == "nef")  { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a != (float)b; };
} else if (opcode == "sltf") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a <  (float)b; };
} else if (opcode == "slef") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a <= (float)b; };
} else if (opcode == "sgtf") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a >  (float)b; };
} else if (opcode == "sgef") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (float)a >= (float)b; };

} else if (opcode == "mux") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = (int)c ? (int)a : (int)b; };

} else if (opcode == "void") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { (int&)ret = 1; };
  // Put this one in to create a dummy operation that is not callable.
} else if (opcode == "null") { op = [](ConstTokenSlice a, ConstTokenSlice b, ConstTokenSlice c, TokenSlice ret) { assert(false); (int&)ret = 0; };

} else {
  fmt::print(stderr, "Unknown opcode {}", opcode);
  assert(false);
}
      red_accum = red_accum_base;
    }

    virtual bool override_stall() const {
      return false;
    }

    virtual bool get_delayed(Token* ret) {
      assert(ret);
      if (pending.size() && pending.front().first < cycle) {
        *ret = pending.front().second;
        pending.pop_front();
        return true;
      }
      return false;
    }

    void Tick() {
      cycle++;
    }

    virtual bool eval(Token A, Token B, Token C, Token* ret) {
      // Sanity check on pipeline inputs.
      // assert(A.nvalid == B.nvalid);
      // assert(B.nvalid == C.nvalid);
      // assert(A.done_vec == B.done_vec);
      // assert(B.done_vec == C.done_vec);
      int nvalid = getParam(A, B, C, 0);
      int done_vec = getParam(A, B, C, 1);

      A = procAlign(A, alignA, offA);
      B = procAlign(B, alignB, offB);
      C = procAlign(C, alignC, offC);

      Token ret_orig;
      if (alignR != 32)
        ret_orig = *ret;

      assert(ret);
      if (red_level == 0) {
        // Do this to copy over all metadata
        if (A.special != "constant")
          *ret = A;
        else if (B.special != "constant")
          *ret = B;
        else 
          *ret = C;
        ret->nvalid = nvalid;
        ret->done_vec = done_vec;
        // ret.threadIDs.clear();
        for (int i=0; i<nvalid; i++) {
          // (int&)ret[i] = op(A[i], B[i], C[i]);
          op(A[i], B[i], C[i], (*ret)[i]);
          // ret.threadIDs.emplace_back(unionThreadIDs({
                // A.threadIDs[i], B.threadIDs[i], C.threadIDs[i]}));
        }
        ret->copyUnionThreadIDsAndVal({A, B, C});
        if (alignR != 32)
          procOutAlign(*ret, ret_orig, alignR, offR);
        return true;
      } else {
        // Reduction case. The incoming token can either be a non-flushing or a
        // flushing token. Non-flushing tokens are reduced within themselves
        // and added to the accumulator. Flushing tokens are the same, but they
        // generate an output and reset the accumulator.
        int loc_done_id = unionThreadIDs(A.endThreadID, B.endThreadID);
        if (parent_id < 0)
          parent_id = loc_done_id;
        for (int i=0; i<nvalid; i++) {
          // red_accum = op(red_accum, A[i], B[i]);
          auto pre = red_accum;
          op(pre[0], A[i], B[i], red_accum[0]);
          int loc_parent_id = getAncestorThread(unionThreadIDs(A[i].getThreadID(), B[i].getThreadID()), red_level);
          // if (parent_id < 0)
            // parent_id = loc_parent_id;
          parent_id = Module::unionThreadIDs(parent_id, loc_parent_id);
          assert(Module::threadBroadcastCompatible(parent_id, loc_parent_id));
          num_red++;
        }
        if (done_vec >= red_level) {
          Token tmp = red_accum;
          if (done_vec == red_level || num_red) {
            tmp.nvalid = 1;
            tmp.threadIDs.emplace_back(parent_id);
          } else {
            tmp.nvalid = 0;
          }
          tmp.done_vec = done_vec - red_level;
          red_accum = red_accum_base;
          num_red = 0;
          parent_id = -1;
          pending.emplace_back(cycle+red_delay, tmp);
          return false;
        } else {
          // Non-flushing
          return false;
        }
      }
    }
  };

  Op *buildOp(const std::string& opcode, int ret, int argA, int argB, int argC, int mix_out, int red_level, int alignA=32, int offA=0, int alignB=32, int offB=0, int alignC=32, int offC=0, int alignR=32, int offR=0) {
    OpImpl *o = new OpImpl(opcode, mix_out, ret, argA, argB, argC, alignA, offA, alignB, offB, alignC, offC, alignR, offR);
    o->red_level = red_level;
    return o;
  }

  struct MemOp : public Op {
    Prt *pA{NULL};
    Prt *pB{NULL};
    std::deque<Token> pend_thread_IDs;
    MemOp(const std::string& opcode, PMU* pmu, int ret, int argA, int argB, int argC, int align, bool dense, int alignA, int offA, int alignB, int offB, int alignR, int offR) 
      : Op("null", -1, ret, argA, argB, argC, alignA, offA, alignB, offB, 32, 0, alignR, offR) {
        name = opcode;
        if (opcode == "read") {
          pA = pmu->GetReadPort(align, dense);
        } else if (opcode == "write") {
          // assert(align == 32);
          auto [A,D] = pmu->GetWritePorts(align, dense);
          pA = A;
          pB = D;
        } else if (opcode == "alloc") {
          assert(!dense);
          assert(align == -1);
          pA = pmu->GetAllocPort();
        } else if (opcode == "dealloc") {
          assert(!dense);
          assert(align == -1);
          pA = pmu->GetFreePort();
        } else {
          assert(!dense);
          auto [A,D] = pmu->GetRMWPorts(opcode, align);
          pA = A;
          pB = D;
        }
    }
    virtual bool override_stall() const {
      return !pA->Ready() || (pB && !pB->Ready());
    }
    virtual bool eval(Token A, Token B, Token C, Token* ret) {
      // Sanity check on pipeline inputs.
      auto nvalid = getParam(A, B, C, 0);
      auto done_vec = getParam(A, B, C, 1);

      pend_thread_IDs.emplace_back();
      // for (int i=0; i<nvalid; i++)
        // pend_thread_IDs.back().emplace_back(unionThreadIDs(A.threadIDs[i], B.threadIDs[i]));
      pend_thread_IDs.back().copyUnionThreadIDsAndVal({A, B});

      pA->Push(procAlign(A, alignA, offA));
      if (pB)
        pB->Push(procAlign(B, alignB, offB));

      // std::cout << name << " Memop A: " << A << std::endl;
      // if (pB)
        // std::cout << name << " Memop B: " << B << std::endl;

      // Always return false from the main eval path, which will trigger a call
      // to get_delayed. That call is what polls the memory for returned data
      // as necessary.
      return false;
    }
    virtual bool get_delayed(Token* ret) {
      // std::cout << "Poll for delayed memory operation!" << std::endl;
      if (pA->Valid()) {
        assert(pend_thread_IDs.size());
        if (ret) {
          Token ret_orig = *ret;
          *ret = pA->ReadPop();
          if (alignR != 32)
            procOutAlign(*ret, ret_orig, alignR, offR);
          //ret.threadIDs = pend_thread_IDs.front();
          ret->copyUnionThreadIDsAndVal({pend_thread_IDs.front()});
        } else {
          pA->ReadPop();
        }
        pend_thread_IDs.pop_front();
        //assert(ret.threadIDs.size() == ret.nvalid);
        // std::cout << "Mem ret: " << ret << std::endl;
        return true;
      }
      return false;
    }
  };

  struct DRAMOp : public Op {
    MLIRDRAMCoal *load;
    std::deque<std::vector<int>> pend_thread_IDs;
    DRAMOp(const std::string& opcode, MLIRDRAMCoal *load, int ret, int argA, int argB, int argC, int alignA, int offA, int alignB, int offB, int alignR, int offR) 
      : Op("null", -1, ret, argA, argB, argC, alignA, offA, alignB, offB, 32, 0, alignR, offR), load(load) {
    }
    virtual bool override_stall() const {
      return !load->address()->Ready() && (!load->isWrite() || load->send_data()->Ready());
    }
    virtual bool eval(Token A, Token B, Token C, Token* ret) {
      assert(ret);
      auto nvalid = getParam(A, B, C, 0);
      pend_thread_IDs.emplace_back();
      for (int i=0; i<nvalid; i++)
        pend_thread_IDs.back().emplace_back(unionThreadIDs(A.threadIDs[i], B.threadIDs[i]));

      load->address()->Push(procAlign(A, alignA, offA));
      if (load->isWrite())
        load->send_data()->Push(procAlign(B, alignB, offB));
      return false;
    }
    virtual bool get_delayed(Token* ret) {
      // assert(ret);
      // std::cout << "Poll for delayed memory operation!" << std::endl;
      if (load->data()->Valid()) {
        if (ret) {
          Token ret_orig = *ret;
          *ret = load->data()->ReadPop();
          if (alignR != 32)
            procOutAlign(*ret, ret_orig, alignR, offR);
          ret->threadIDs = pend_thread_IDs.front();
        } else {
          load->data()->ReadPop();
        }
        pend_thread_IDs.pop_front();
        if (ret) {
          assert(ret->threadIDs.size() == ret->nvalid);
        }
        // std::cout << "Mem ret: " << ret << std::endl;
        return true;
      }
      return false;
    }
  };

  int _outputs_stall() const;

  std::vector<Token> reg_data;
  MLIRController ctrl;
  std::vector<int> input_regs;

  class ScalarOut : public CheckedSend<Token>, public Module {

#if IDEAL_SCALAR
    const int write_per_cycle{100};
    const int depth{1000};
#else
    const int write_per_cycle{SCALAR_RETIME_PORTS};
    const int depth{SCALAR_RETIME_DEPTH};
#endif
    int write_lock_out{0};

    CheckedSend<Token> *out;
    std::deque<Token> pend;
  public:
    ScalarOut(const std::string& name, CheckedSend<Token> *out) : Module(name), out(out) {}
    bool Ready() const override {
      if (pend.size() >= depth) {
#if DEBUG_SCALAR
        fmt::print("{}/{} scalar not ready (depth)\n", path, name);
#endif
        return false;
      }
      if (write_lock_out) {
#if DEBUG_SCALAR
        fmt::print("{}/{} scalar not ready (write_lock_out)\n", path, name);
#endif
        return false;
      }
      return true;
    }
    void Push(const Token& v) override {
      assert(Ready());

#if DEBUG_SCALAR
      fmt::print("{}/{} scalar out push: {}\n", path, name, v.ToString());
#endif

      // Implement a queue with limited write ports
      write_lock_out = v.nvalid/write_per_cycle;

      if (!v.nvalid && v.done_vec) {
        pend.push_back(v);
      } else {
        for (int i=0; i<v.nvalid; i++) {
          Token tmp;
          tmp.nvalid = 1;
          tmp.type = v.type;
          tmp.intVec_[0] = v.intVec_[i];
          tmp.threadIDs.push_back(v.threadIDs[i]);
          if (i == v.nvalid-1) {
            tmp.done_vec = v.done_vec;
            tmp.endThreadID = v.endThreadID;
          }
          // fmt::print("\t{}\n", tmp.ToString());
          pend.push_back(tmp);
        }
      }
    }
    void Eval() {
      if (out->Ready() && pend.size()) {
#if DEBUG_SCALAR
        fmt::print("{}/{} scalar out send: {}\n", path, name, pend.front().ToString());
#endif
        out->Push(pend.front());
        pend.pop_front();
      }
    }
    void Clock() {
      if (write_lock_out > 0)
        write_lock_out--;
    }
  };

  class MetadataCoal : public CheckedSend<Token>, public Module {
    bool valid{false};
    int elapsed{0};
    const int max_time{1};
    Token tmp;
    CheckedSend<Token> *out;
    void _flush() {
      fmt::print("{}/{} send: {}\n", path, name, tmp.ToString());
      out->Push(tmp);
      tmp.nvalid = 0;
      tmp.done_vec = 0;
      valid = false;
      elapsed = 0;
    }
    void _reset(const Token& t) {
      valid = true;
      tmp = t;
      elapsed = 0;
    }
    void _merge(const Token& t) {
      assert(valid);
      assert(t.nvalid == 0 && tmp.done_vec == 0);
      tmp.done_vec = t.done_vec;
    }
   public:
    MetadataCoal(const std::string& name, CheckedSend<Token> *out) : Module(name), out(out) {}
    bool Ready() const override {
      return out->Ready();
    }
    void Push(const Token& v) override {
      assert(Ready());
      if (valid && (tmp.done_vec > 0 || v.nvalid > 0)) {
        _flush();
        _reset(v);
      } else if (!valid) {
        _reset(v);
      } else {
        _merge(v);
      }
    }
    void Eval() {
      if (elapsed++ > max_time && valid && out->Ready())
        _flush();
    }
  };

  int max_out{0};
  std::vector<std::tuple<CheckedSend<Token>*,int,int,bool,bool,bool,int>> outputs;

  std::vector<std::vector<Op*>> stage_funcs;

  std::vector<bool> stage_valid;

  PMU *mem{NULL};
  MLIRDRAM *dram{NULL};
  DRAMController *dram_ctrl{NULL};

  using NI = NetworkInterface<Token, int, int>;
  NI *dynnet;
  NI *statnet;
  NI *idealnet;


  CheckedSend<Token>* ctrl_out{NULL};

  map<int,TokenType> reg_types;

  map<int,string> reg_labels;
  map<string,int> label_regs;

  std::set<int> input_locs;

  template<class TP>
  void _set_constant(int idx, TP val);
protected:
  void SetScalar() {
    ctrl.SetPar(1);
  }
  void AddRegister(int idx, const string tp="i32");
  void LabelRegister(int idx, const string label);
  void AddConstant(int idx, int val);
  void AddConstant(int idx, double val);
  void AddSymConstant(int idx, MLIRSymConst* ref);

  void AddInput(int dst_reg, int depth, bool counter, int loc, bool fifo, bool scalar, bool internal, bool token);
  void AddCounter(int dst_reg, int min_loc, int max_loc, int step_loc, int min_const, int max_const, int step_const, int depth, bool flat);
  void AddMerge(int dst_reg, int left, int right, bool is_while);
  void AddValid(int dst_reg);
  void AddConstInput(int dst_reg, int loc, int val);
  void AddConstFloatInput(int dst_reg, int loc, float val);
  void AddInfCounter(int dst_reg, int loc, int max);
  void AddOutput(int src_reg, int loc, int val_reg, bool inv, bool while_exit, bool while_be, bool scalar);

  void AddCtrlInput(int loc);
  void AddCtrlOutput(int loc);

  void AddUnOp(const std::string& op, int src, int dst, int stage, int mix_out=-1, int align=32, int off=0, int alignR=32, int offR=0);
  void AddBiOp(const std::string& op, int srcA, int srcB, int dst, int stage, int mix_out=-1, int alignA=32, int offA=0, int alignB=32, int offB=0, int alignR=32, int offR=0);
  void AddCmpOp(const std::string& op, int srcA, int srcB, int dst, int stage, int alignA=32, int offA=0, int alignB=32, int offB=0);
  void AddMuxOp(int sel, int srcA, int srcB, int dst, int stage, int mix_out=-1, int alignA=32, int offA=0, int alignB=32, int offB=0, int alignR=32, int offR=0);

  void AddRedOp(const std::string& op, int src, int dst, int stage, int level, int align=32, int off=0);
  void AddMemOp(const std::string& op, int addr, int data, int dst, int stage, int align, bool dense, int alignA=32, int offA=0, int alignB=32, int offB=0, int alignR=32, int offR=0);
  void Verify();
public:
  template<class T=int>
  MLIRContext(const std::string& name, int stages, NI* dynnet, NI* statnet, NI* idealnet, T *mem_in=NULL) 
  : Module(name), nStage(stages), dynnet(dynnet), statnet(statnet), idealnet(idealnet) {
    if (mem_in)
      mem = new PMUWrapper(mem_in);
    else
      mem = NULL;
    stage_funcs.resize(nStage+1);
    stage_valid.resize(nStage+1, false);
    SetInstrumentation();
  }
  MLIRContext(const std::string& name, int stages, NI* dynnet, NI* statnet, NI* idealnet, MLIRDRAM *dram, DRAMController *dram_ctrl);
  void Clock(void);
  void Eval(void);
  void UpdateSymConst(int idx, int val);
};

#endif // _MLIR_TEMPLATES_CONTEXT_H
