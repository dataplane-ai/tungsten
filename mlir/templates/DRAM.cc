#include "mlir/templates/DRAM.h"

void MLIRBulkStore::Eval() {
  assert(base);
  if (next_base && data.Valid() && burstcmd.Ready() && inflight.size() < max_infl) {
    auto& t = data.Read();
    auto a = base + *next_base*sizeof(T);
    DRAMCommand c;
    c.write = true;
    c.addr = a/64;
    for (int i=0; i<t.nvalid; i++) {
      auto ptr = (T*)a;
      ptr[i] = (int)t[i];
    }
    if (base_done_at == t.done_vec) {
      // if (enLog) BINLOG_INFO_W(writer, "Clear base");
      next_base = std::nullopt;
    } else {
      *next_base += t.nvalid;
    }
    burstcmd.Push(c);
    inflight.emplace_back(!next_base, t.done_vec);
    // if (enLog) BINLOG_INFO_W(static_writer, "{}/{}: Send token to DRAM addr {} nvalid {} done_vec {}", path, name, c.addr, t.nvalid, t.done_vec);
    data.Pop();
  }
  if (burstrsp.Valid() && tok.Ready()) {
    assert(inflight.size());
    if (inflight.front().first) {
      Token t;
      t.type = TT_INTVEC;
      for (int i=0; i<16; i++)
        (int&)t[i] = 0;
      t.nvalid = 1;
      t.done_vec = inflight.front().second;
      // if (enLog) BINLOG_INFO_W(writer, "Send from DRAM nvalid {} done_vec {}", t.nvalid, t.done_vec);
      tok.Push(t);
    } else {
      // if (enLog) BINLOG_INFO_W(writer, "Accumulate from DRAM");
    }

    burstrsp.Pop();
    inflight.pop_front();
  }
  if (!next_base && offset.Valid()) {
    const auto& t = offset.Read();
    assert(t.nvalid == 1);
    next_base = (int)t;
    base_done_at = t.done_vec+1;
    // if (enLog) BINLOG_INFO_W(writer, "Set new offset {} done_at {}", (int)t, base_done_at);
  }
}
