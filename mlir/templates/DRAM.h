#ifndef _MLIR_TEMPLATES_DRAM_H
#define _MLIR_TEMPLATES_DRAM_H

#include "plasticine/templates/dramag.h"
#include "network.h"

#include "fmt/format.h"

struct MLIRDRAMUser {
  virtual void SetBase(uint64_t base) = 0;
};

class MLIRDRAM : public Module {
  uint64_t _base;
  int *_base_raw{nullptr};
  int sz;
  std::vector<MLIRDRAMUser*> users;
public:
  const int align{32};
  void realloc(int sz_new) {
    sz = sz_new;
    fmt::print("Reallocate {}/{} to {}\n", path, name, sz);
    if (_base_raw)
      delete[] _base_raw;
    int adj_len = sz+1024;
    _base_raw = new int[adj_len];
    for (int i=0; i<adj_len; i++)
      _base_raw[i] = 0;
    _base = ((uint64_t)(_base_raw))+64;
    for (auto u : users)
      u->SetBase(_base);
  }
  MLIRDRAM(const std::string& name, int sz_new, int align) : Module(name), align(align) {
    realloc(sz_new);
  }
  int size() {
    return sz;
  }
  void Register(MLIRDRAMUser* u) {
    u->SetBase(_base);
    users.emplace_back(u);
  }
  void Update(uint64_t base) {
    _base = base;
    for (auto u : users)
      u->SetBase(base);
  }
  uint64_t Get() {
    return _base;
  }
};

class MLIRDRAMCoal : public DualDRAMAddrGen, public MLIRDRAMUser {
  uint64_t base;
  bool write;
  int len{32};

  const int burst_len{32};

  FIFO<Token,2> addr;
  FIFO<Token,2> write_data;
  deque<uint64_t> burst_pend;
  deque<uint64_t> burst_pend2;
  deque<tuple<Token,int,int>> pend;
  FIFO<Token,4> ret;

  MLIRDRAM* buf;

  long int cyc{0};
  bool next0{true};

public:
  MLIRDRAMCoal(MLIRDRAM *buf, DRAMController *dram, bool write=false, int len=32) : DualDRAMAddrGen("ag"), write(write), len(len), buf(buf) {
    assert(len%8 == 0 && "DRAM length should be at least 8.");
    AddChildren({&addr,&write_data,&ret});
    SetupDRAM(dram);
    buf->Register(this);
  }
  bool isWrite() const { return write; }
  CheckedSend<Token>    *address()   { return &addr;      }
  CheckedSend<Token>    *send_data() { return &write_data;}
  CheckedReceive<Token> *data()      { return &ret;       }
  void Eval() {
    if (burst_pend.size() && burstcmd.Ready()) {
#if DBG_DRAM
      fmt::print("{:15} DRAM Burst to DRAM\n", cyc);
#endif
      burstcmd.Push(DRAMCommand(write, burst_pend.front()));
      burst_pend.pop_front();
    }
    if (burst_pend2.size() && burstcmd2.Ready()) {
#if DBG_DRAM
      fmt::print("{:15} DRAM Burst2 to DRAM\n", cyc);
#endif
      burstcmd2.Push(DRAMCommand(write, burst_pend2.front()));
      burst_pend2.pop_front();
    }
    if (burstrsp.Valid() && std::get<1>(pend.front())) {
#if DBG_DRAM
      fmt::print("{:15} DRAM Burst from DRAM\n", cyc);
#endif
      std::get<1>(pend.front())--;
      burstrsp.Pop();
    } else if (burstrsp.Valid()) {
#if DBG_DRAM
      fmt::print("{:15} Stalled burst!\n", cyc);
#endif
    }
    if (burstrsp2.Valid() && std::get<2>(pend.front())) {
#if DBG_DRAM
      fmt::print("{:15} DRAM Burst2 from DRAM\n", cyc);
#endif
      std::get<2>(pend.front())--;
      burstrsp2.Pop();
    } else if (burstrsp2.Valid()) {
#if DBG_DRAM
      fmt::print("{:15} Stalled burst2!\n", cyc);
#endif
    }
    if (pend.size() && !std::get<1>(pend.front()) && !std::get<2>(pend.front()) && ret.Ready()) {
#if DBG_DRAM
      fmt::print("{:15} DRAM vector return\n", cyc);
#endif
      ret.Push(std::get<0>(pend.front()));
      pend.pop_front();
    }
    if (addr.Valid() && (!write || write_data.Valid())) {
      std::set<uint64_t> bursts;
      Token t = addr.ReadPop();
      Token d;
      if (write)
        d = write_data.ReadPop();
      for (int i=0; i<t.nvalid; i++) {
        auto a = (int)t[i];
        if (buf->align == 1)
          assert(a%32 == 0 && "Writes to bool DRAMs must be word-aligned");
        int off = a*(buf->align/8);
        if (buf->align < 8)
          off = (a*buf->align)/8;
        bursts.insert((off/burst_len)*burst_len+base);
        if (write) {
          // auto off_loc = ((((int)d[i])>>16) & 0xFF);
#if DBG_DRAM
          fmt::print("{:15} DRAM Store {} to {} base {} off {}\n", cyc, (int)d[i], a, base, off);
#endif
          if (len == 32) {
            // *(int*)(base + a*4) = (int)d[i];
            *(int*)(base + off) = (int)d[i];
          } else if (len == 16) {
            // *(short*)(base + a*4 + off_loc) = (int)d[i];
            // *(short*)(base + a*2) = (int)d[i];
            *(short*)(base + off) = (int)d[i];
          } else if (len == 8) {
            // *(char*)(base + a*4 + off_loc) = (int)d[i];
            *(char*)(base + off) = (int)d[i];
          }
          (int&)t[i] = 1;
        } else {
          // assert(len == 32);
          int v = 0;
          if (len == 32) {
            // v = *(int*)(base + a*4);
            v = *(int*)(base + off);
          } else if (len == 16) {
            // v = *(short*)(base + a*2);
            v = *(short*)(base + off);
          } else if (len == 8) {
            v = *(char*)(base + off);
          } else {
            assert(false && "unknown length");
          }
#if DBG_DRAM
          fmt::print("{:15} DRAM Load {} from {} base {} off {}\n", cyc, v, a, base, off);
#endif
          (int&)t[i] = v;
        }
      }
#if DBG_DRAM
      fmt::print("{:15} DRAM Pending bursts {}\n", cyc, bursts.size());
#endif
      pend.emplace_back(t, 0, 0);
      for (auto b : bursts) {
        if (next0) {
          burst_pend.emplace_back(b);
          std::get<1>(pend.back())++;
        } else {
          burst_pend2.emplace_back(b);
          std::get<2>(pend.back())++;
        }
        next0 = !next0;
      }
    }
    cyc++;
  }
  void SetBase(uint64_t _base) override {
    base = _base;
  }
};

class MLIRBulkLoad : public Module, public MLIRDRAMUser {
  using NI = NetworkInterface<Token, int, int>;
  StreamLoadAG<16, 64, int, false> ag;

  NetworkOutput offset;
  NetworkOutput size;
  NetworkInput out;
public:
  MLIRBulkLoad(const std::string& name, MLIRDRAM *buf,
      NI *dynnet, NI *statnet, NI *idealnet, DRAMController *dram) 
    : Module(name), ag("ag", &offset, &size, &out, dram),
    offset("in0", dynnet, statnet, idealnet), 
    size("in1", dynnet, statnet, idealnet), 
    out("out0", dynnet, statnet, idealnet) {
      AddChild(&ag);
      AddChild(&offset);
      AddChild(&size);
      AddChild(&out);
      buf->Register(this);
    }
  void SetBase(uint64_t base) override {
    ag.SetBase(base);
  }
};

class MLIRBulkStore : public DRAMAddrGen,  public MLIRDRAMUser {
  using NI = NetworkInterface<Token, int, int>;
  using T = int;
  static const int max_infl{64};
  uint64_t base{0};
  NetworkOutput offset;
  NetworkOutput data;
  NetworkInput tok;

  std::optional<int> next_base;
  int base_done_at;

  // <send_tok, done_vec>
  std::deque<std::pair<bool,int>> inflight;

public:
  MLIRBulkStore(const std::string& name, MLIRDRAM *buf,
      NI *dynnet, NI *statnet, NI *idealnet, DRAMController *dram)
    :DRAMAddrGen(name),
    offset("in0", dynnet, statnet, idealnet),
    data("in1", dynnet, statnet, idealnet),
    tok("out0", dynnet, statnet, idealnet) {
      AddChildren({&offset, &data, &tok});
      buf->Register(this);
      SetupDRAM(dram);
  }
  void Eval() override;
  void SetBase(uint64_t _base) override {
    base = _base;
  }

};

#endif  // _MLIR_TEMPLATES_DRAM_H
