#include "mlir/templates/DRAM.h"
#include "mlir/templates/Host.h"
#include "mlir/templates/Context.h"

#include <cxxopts.hpp>

using namespace std;

void MLIRHostArgIn::Eval() {
  if (pending.size() && ni.Ready()) {
    auto t = pending.front();
    ni.Push(t);
    pending.pop_front();
  }
}

void MLIRHostArgOut::Eval() {
  if (pending.size() && no.Valid()) {
    if (no.Read().nvalid) {
      auto f = pending.front();
      assert(!f->IsResolved());
      f->Resolve(no.ReadPop());
      pending.pop_front();
      // Interface with the existing module completion hierarchy
      Complete(1);
    } else {
      no.Pop();
    }
  }
}

std::shared_ptr<MLIRFuture<Token>> MLIRHostArgOut::Pop() {
  // Create a shared pointer-location that we and the recipient can use to 
  // access the data.
  Expect(1);
  auto f = std::shared_ptr<MLIRFuture<Token>>(new MLIRFuture<Token>());
  pending.push_back(f);
  return f;
}

void MLIRSymConst::Set(int val) {
  assert(!frozen);
  set = true;
  for (auto& [ctx, idx] : users)
    ctx->UpdateSymConst(idx, val);
}

void MLIRSymConst::Register(MLIRContext* ctx, int idx) {
  users.emplace_back(ctx, idx);
}

template<typename T>
T *MLIRRunner::getOnly(const std::string& name) {
  auto mods = main.FindModules(padname(name));
  if (mods.size() == 0)
    throw std::runtime_error("No module found for path: " + name);
  if (mods.size() != 1)
    throw std::runtime_error("Not exactly one module for path!");
  auto ret = dynamic_cast<T*>(mods[0]);
  if (!ret)
    throw std::runtime_error("Module conversion error"+ name);
  return ret;
}

template<typename T>
std::vector<T*> MLIRRunner::getAll(bool check) {
  std::vector<T*> ret;
  for (auto m : main.AllChildren()) {
    auto tmp = dynamic_cast<T*>(m);
    if (tmp) 
      ret.emplace_back(tmp);
  }
  if (check)
    assert(ret.size() && "Should be returning at least one value");
  return ret;
}

void MLIRRunner::SetFloatLUT(const std::string& name, const std::vector<float>& vals) {
  auto srams = getAll<SparsePMU<float,4096,16>>();
  for (auto s : srams)
    if (s->lut_name == name)
      for (int i=0; i<vals.size(); i++)
        s->Set(vals[i], i, 32);
}

void MLIRRunner::SetIntLUT(const std::string& name, const std::vector<int>& vals) {
  auto srams = getAll<SparsePMU<int,4096,16>>();
  for (auto s : srams)
    if (s->lut_name == name)
      for (int i=0; i<vals.size(); i++)
        s->Set(vals[i], i, 32);
}

void MLIRRunner::SetShortLUT(const std::string& name, const std::vector<short>& vals) {
  std::vector<int> vals_adj;
  vals_adj.resize((vals.size()+1)/2, 0);
  for (int i=0; i<vals.size(); i++)
    vals_adj[i/2] |= vals[i] << ((i%2)*16);
  SetIntLUT(name, vals_adj);
}

void MLIRRunner::SetCharLUT(const std::string& name, const std::vector<char>& vals) {
  std::vector<int> vals_adj;
  vals_adj.resize((vals.size()+3)/4, 0);
  for (int i=0; i<vals.size(); i++)
    vals_adj[i/4] |= vals[i] << ((i%4)*8);
  SetIntLUT(name, vals_adj);
}

MLIRRunner::MLIRRunner(Module *DUT, int argc, char **argv) 
  : main(DUT, std::cout), DUT(DUT) {
  if (argc) {
    cxxopts::Options options("MLIRRunner", "A class to interface between C++ host code and MLIR accelerators.");
    options.add_options()
      ("prlinks","Print links")
      ("prmem","Print memory");
    auto result = options.parse(argc, argv);
    // Enable link printing if requested
    if (result["prlinks"].as<bool>()) {
      std::cout << "Enabling link printing!" << std::endl;
      main.Command("lognet all");
    }
    if (result["prmem"].as<bool>()) {
      std::cout << "Enabling memory printing!" << std::endl;
      main.Command("logmem dram");
    }
  }
}

void MLIRRunner::AllocDRAM(const std::string& name, int sz) {
  auto buf = new int[sz+32];
  for (int i=0; i<sz+32; i++) {
    buf[i] = -1;
  }
  getOnly<MLIRDRAM>(name)->Update(((uint64_t)buf)+16*4);
}

void MLIRRunner::PushArgIn(const std::string& name, int i, int last_level) {
  std::vector<int> dat;
  std::vector<Token> toks;
  dat.emplace_back(i);
  GenTokens<>(dat, std::back_inserter(toks));
  assert(toks.size() == 1);
  toks[0].done_vec = last_level;
  toks[0].nvalid = 1;
  toks[0].threadIDs.emplace_back(0);
  PushArgIn(name, toks[0]);
}

void MLIRRunner::PushArgIn(const std::string& name, float f, int last_level) {
  std::vector<float> dat;
  std::vector<Token> toks;
  dat.emplace_back(f);
  GenTokens<>(dat, std::back_inserter(toks));
  assert(toks.size() == 1);
  toks[0].done_vec = last_level;
  toks[0].nvalid = 1;
  toks[0].threadIDs.emplace_back(0);
  PushArgIn(name, toks[0]);
}

void MLIRRunner::PushArgIn(const std::string& name, const Token& t) {
  getOnly<MLIRHostArgIn>(name)->Push(t);
}

std::shared_ptr<MLIRFuture<Token>> MLIRRunner::PopArgOut(const std::string& name) {
  return getOnly<MLIRHostArgOut>(name)->Pop();
}

void MLIRRunner::SetSymConst(const std::string& name, int val) {
  return getOnly<MLIRSymConst>(name)->Set(val);
}

void MLIRRunner::ApplyCommand(const std::string& cmd) {
  main.Command(cmd);
}

std::vector<std::string> MLIRRunner::ProcessArgs(int argc, const char **argv, int expect_positional) {
  cxxopts::Options options("MLIRRunner", "A class to interface between C++ host code and MLIR accelerators.");
  options.add_options()
    ("dram_config","Which DRAM configuration file to use",cxxopts::value<string>())
    ("dram_bandwidth","Total DRAM bandwidth in GB/s",cxxopts::value<string>())
    ("net_config","Which network configuration file to use",cxxopts::value<string>())
    ("perf_traces","Where traces should be dumped after execution",cxxopts::value<string>())
    ("save_binlog","Whether in-pipeline values should be logged",cxxopts::value<bool>()->default_value("false"))
    ("sram_mode","Which model to use for on-chip SRAM",cxxopts::value<string>());
    ("h,help","Print this help message");

  options.allow_unrecognised_options();

  auto result = options.parse(argc, argv);

  if (result.count("help")) {
    std::cout << options.help() << std::endl;
    exit(0);
  }

  if (result.count("dram_config")) {
    assert(!result.count("dram_bandwidth"));
    ApplyCommand(fmt::format("apply dram config {}", result["dram_config"].as<string>()));
  }

  if (result.count("dram_bandwidth")) {
    assert(!result.count("dram_config"));
    ApplyCommand(fmt::format("apply dram bandwidth {}", result["dram_bandwidth"].as<string>()));
  }

  ApplyCommand(fmt::format("source {}", result["net_config"].as<string>()));

  if (result.count("perf_traces"))
    perf_traces_path = result["perf_traces"].as<string>();
  Module::SetBinlog(result["save_binlog"].as<bool>());
  if (!result.count("perf_traces") && !result["save_binlog"].as<bool>())
    Module::SetMemoryConserve();
  if (result.count("sram_mode")) {
    auto mode = result["sram_mode"].as<string>();
    set<string> valid_modes({"limit_bw", "limit", "if"});
    assert(valid_modes.count(mode));

    std::vector<Module*> all_pmus;
    for (auto p : getAll<SparsePMU<int,4096,16>>(false))
      all_pmus.emplace_back(p);
    for (auto p : getAll<SparsePMU<float,4096,16>>(false))
      all_pmus.emplace_back(p);
    fmt::print("Applying mode {} to {} PMUs.\n", mode, all_pmus.size());
    for (auto p : all_pmus) {
      p->SetParameter("alloc_type", mode);
    }
  }

  for (auto a : result.unmatched())
    fmt::print("Positional arg: {}\n", a);

  if (expect_positional >= 0)
    assert(result.unmatched().size() == expect_positional);
  return result.unmatched();
}

long MLIRRunner::RunAll() {
  auto cyc = main.RunAll(30000000);
  std::cout << "Ran for " << cyc << " cycles" << std::endl;
  main.Command("stat");
  if (perf_traces_path.size())
    main.dump_line_traces(perf_traces_path);
  return cyc;
}

void MLIRRunner::DumpTracesTo(const std::string& dir) {
  main.dump_line_traces(dir);
}


int MLIRVerify(std::shared_ptr<MLIRFuture<Token>> t, int exp) {
  if (!t->IsResolved()) {
    std::cout << "FAIL" << std::endl;
    return 1;
  }

  int ret = t->Get().int_;
  std::cout << "Got output " << ret << " expected " << exp << std::endl;
  if (ret != exp) {
    std::cout << "FAIL" << std::endl;
    return 1;
  }
  std::cout << "SUCCESS" << std::endl;
  return 0;
}

int MLIRVerify(std::shared_ptr<MLIRFuture<Token>> t, float exp, float tol) {
  if (!t->IsResolved()) {
    std::cout << "FAIL" << std::endl;
    return 1;
  }

  float ret = t->Get().float_;
  std::cout << "Got output " << ret << " expected " << exp << " tol " << tol << std::endl;
  if (abs(ret - exp) >= tol) {
    std::cout << "FAIL" << std::endl;
    return 1;
  }
  std::cout << "SUCCESS" << std::endl;
  return 0;
}
