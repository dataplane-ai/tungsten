#ifndef _MLIR_TEMPLATES_HOST_H
#define _MLIR_TEMPLATES_HOST_H

#include "dram.h"
#include "interface.h"
#include "module.h"
#include "repl.h"
#include "token.h"
#include "network.h"
#include "statnet2.h"
#include "idealnetwork.h"

#include "fmt/format.h"

class MLIRContext;

// These are helper functions to generate tokens for testing from vectors
template<class T, class OutputIt>
void GenTokens(const std::vector<T>& data, OutputIt out, int last_level=2) {
  for (int ii=0; ii < data.size(); ii += 16) {
    int this_size = data.size() - ii;
    if (this_size > 16)
      this_size = 16;
    Token t;
    t.type = TokVecType<T>();
    t.nvalid = this_size;
    bool last = ii + 16 >= data.size();
    for (int i=0; i < this_size; i++) {
      (T&)t[i] = data[ii+i];
    }
    if (last)
      t.done_vec = last_level;

    *out++ = t;
  }
}
template<class T, class OutputIt>
void GenTokens(const std::vector<std::vector<T>>& data, OutputIt out) {
  for (int i=0; i<data.size(); i++) {
    int last_level = (i+1 == data.size()) ? 3 : 2;
    GenTokens(data[i], out, last_level);
}
}
template<class T, class OutputIt>
void GenTokens(T dat, OutputIt out) {
  Token t;
  t.type = TokVecType<T>();
  t.nvalid = 1;
  (T&)t[0] = dat;
  t.done_vec = 0;
  *out++ = t;
}

// This is the return type for HostBufs.
template<class T>
class MLIRFuture {
  T data;
  bool resolved{false};
public:
  MLIRFuture() {}
  // Make futures pointer-only (non-copyable). This lets them be updated later
  MLIRFuture(MLIRFuture&& f)                 =delete;
  MLIRFuture(const MLIRFuture& f)            =delete;
  MLIRFuture& operator=(MLIRFuture&& f)      =delete;
  MLIRFuture& operator=(const MLIRFuture& f) =delete;

  // Access the future from the creator and recipient
  T Get() const {
    assert(resolved);
    return data;
  }
  bool IsResolved() const {
    return resolved;
  }
  void Resolve(T t) {
    assert(!resolved);
    data = t;
    resolved = true;
  }
};

// The goal of this module is to provide a unified interface for accessing
// on-chip registers from host C++ code. 
class MLIRHostArgIn : public Module {
  using NI = NetworkInterface<Token, int, int>;
  std::deque<Token> pending;
  NetworkInput ni;
  // CheckedSend<Token> *i;
public:
  MLIRHostArgIn(const std::string& name, NI* dynnet, NI* statnet, NI* idealnet) 
    : Module(name), ni("out0", dynnet, statnet, idealnet) {
      AddChild(&ni);
    }
  void Eval();
  void Push(const Token& t) {
    pending.push_back(t);
  }
};

class MLIRHostArgOut : public Module {
  using NI = NetworkInterface<Token, int, int>;
  std::deque<std::shared_ptr<MLIRFuture<Token>>> pending;
  NetworkOutput no;
  // CheckedReceive<Token> *i;
public:
  MLIRHostArgOut(const std::string& name, NI* dynnet, NI* statnet, NI* idealnet)
    : Module(name), no("in0", dynnet, statnet, idealnet) {
      AddChild(&no);
    }
  void Eval();
  // Async pop that can be called at any point, and resolves as the tungsten
  // program executes.
  std::shared_ptr<MLIRFuture<Token>> Pop();
};

// A set-once symbolic constant. Models constants being linked into the
// bitstream at the last possible minute before loading.
class MLIRSymConst : public Module {
  bool frozen{false};
  bool set{false};
  std::vector<std::pair<MLIRContext*,int>> users;
public:
  MLIRSymConst(const std::string& name) : Module(name) {}
  void Eval() { assert(set); frozen = true; }
  void Set(int val);
  void Register(MLIRContext* ctx, int idx);
};

template<typename T>
struct MLIRWrapper : public Module {
  DRAMController *dram;
  MLIRWrapper() : Module("DUT") {
    DynamicNetwork<4,4,1> *dynnet = new DynamicNetwork<4,4,1>({22, 22}, "net");
    StaticNetwork2 *statnet = new StaticNetwork2("statnet", 1, 3);
    IdealNetwork *idealnet = new IdealNetwork("idealnet");
    auto W_dir = std::getenv("TUNGSTEN_HOME");
    assert(W_dir && "must set Tungsten home directory.");
    dram = new DRAMController("dram", fmt::format("{}/configs/HBM-2-config.cfg", W_dir), {}, {});
    T* accel = new T("MLIRTop", 0, dynnet, statnet, idealnet, dram);
    AddChild(dynnet);
    AddChild(statnet);
    AddChild(idealnet);
    AddChild(dram);
    AddChild(accel);
  }
  ~MLIRWrapper() {
    fmt::print("Delete MLIRWrapper\n");
    delete dram;
  }
};

// This is the main top class for interfacing with MLIR-defined programs.
// For simplicity, this just wraps existing REPL with a few programmatic (i.e.,
// typed) methods for doing things like accessing ArgIns and ArgOuts.
//
// By wrapping the REPL, we retain the ability to do more in-depth changes for
// now. Ideally, the REPL will eventually be rewritten to support typed access
// to module data and operations instead of string inputs and outputs.
class MLIRRunner {
  REPL main;
  std::string padname(const std::string& name) {
    return "/DUT/MLIRTop/" + name;
  }
  template<typename T>
  T *getOnly(const std::string& name);
  template<typename T>
  std::vector<T*> getAll(bool check=true);
  std::string perf_traces_path = "";
  Module *DUT;
public:
  // MLIRRunner(Module *DUT) : main(DUT, std::cout) {}
  MLIRRunner(Module *DUT, int argc=0, char **argv=NULL); 
  ~MLIRRunner() {
    fmt::print("Delete MLIRRunner\n");
    delete DUT;
  };

  // Helper functions to traverse the module hierarchy
  void SetDRAM(const std::string& name, const std::vector<bool>& vals) {
    auto dram = getOnly<MLIRDRAM>(name);
    if (dram->size()/32 < vals.size())
      dram->realloc(vals.size());
    assert(dram->size()/32 >= vals.size());
    auto buf = (int*)dram->Get();
    for (int i=0; i<vals.size(); i++) {
      int ii = i/32;
      int shift =(i%32);
      buf[ii] &= 0x1<<shift;
      buf[ii] |= (vals[i]?1:0)<<shift;
    }
  }
  template<class T>
  void SetDRAM(const std::string& name, const std::vector<T>& vals) {
    auto dram = getOnly<MLIRDRAM>(name);
    if (dram->size() < vals.size())
      dram->realloc(vals.size());
    assert(dram->size() >= vals.size());
    auto buf = (T*)dram->Get();
    for (int i=0; i<vals.size(); i++) {
      // std::cout << "Set " << name << " " << i << ": " << vals[i] << std::endl;
      buf[i] = vals[i];
    }
  }

  void SetFloatLUT(const std::string& name, const std::vector<float>& vals);
  void SetIntLUT(const std::string& name, const std::vector<int>& vals);
  void SetShortLUT(const std::string& name, const std::vector<short>& vals);
  void SetCharLUT(const std::string& name, const std::vector<char>& vals);

  void AllocDRAM(const std::string& name, int sz);
  template<class T=int>
  std::vector<T> GetDRAM(const std::string& name, int sz, int off=0) {
    auto buf = (T*)getOnly<MLIRDRAM>(name)->Get();
    std::vector<T> ret;
    for (int i=0; i<sz; i++) {
      // std::cout << "Get " << name << " " << i+off << ": " << buf[i+off] << std::endl;
      ret.emplace_back(buf[i+off]);
    }
    return ret;
  }
  std::vector<bool> GetDRAMBool(const std::string& name, int sz, int off=0) {
    auto buf = (unsigned*)getOnly<MLIRDRAM>(name)->Get();
    std::vector<bool> ret;
    for (int i=0; i<sz; i++) {
      int ii = i + off;
      int base = ii/32;
      int bit = 1 << (ii%32);
      ret.emplace_back(buf[base] & bit);
    }
    return ret;
  }
  void PushArgIn(const std::string& name, int i, int last_level=1);
  void PushArgIn(const std::string& name, float f, int last_level=1);
  void PushArgIn(const std::string& name, const Token& t);
  void SetSymConst(const std::string& name, int val);
  std::shared_ptr<MLIRFuture<Token>> PopArgOut(const std::string& name);
  void ApplyCommand(const std::string& cmd);
  std::vector<std::string> ProcessArgs(int argc, const char **argv, int expect_positional=-1);

  long RunAll();
  void DumpTracesTo(const std::string& dir);
};

int MLIRVerify(std::shared_ptr<MLIRFuture<Token>> t, int exp);

template<class T>
int MLIRVerify(std::vector<T> v, std::vector<T> gold, double tol=0.001) {
  double err{0};
  assert(v.size() >= gold.size());
  for (int i=0; i<gold.size(); i++)
    err += abs(v[i] - gold[i]);
  bool fail{err > tol};
  err = 0;
  for (int i=0; i<gold.size(); i++) {
    err += abs(v[i] - gold[i]);
    if (fail || i < 16 || gold.size() - i < 16) {
      if (std::is_same<T,char>::value)
        fmt::print("{:15} {:15} {:15}\n", (int)v[i], (int)gold[i], (int)(v[i] - gold[i]));
      else
        fmt::print("{:15} {:15} {:15}\n", v[i], gold[i], v[i] - gold[i]);
    }
  }
  if (fail) {
    fmt::print("FAIL err {} tol {}\n", err, tol);
    return 1;
  }
  // std::cout << "SUCCESS" << std::endl;
  fmt::print("SUCCESS err {} tol {}\n", err, tol);
  return 0;
}

template<class T>
int MLIRVerifyNullTerminated(std::vector<T> v, std::vector<T> gold, std::vector<int> offsets) {
  int err{0};
  assert(v.size() >= gold.size());
  for (auto off : offsets) {
    for (int i=off; ; i++) {
      err += abs(v[i] - gold[i]);
      if (std::is_same<T,char>::value)
        fmt::print("{:15} {:15} {:15}\n", (int)v[i], (int)gold[i], (int)(v[i] - gold[i]));
      else
        fmt::print("{:15} {:15} {:15}\n", v[i], gold[i], v[i] - gold[i]);
      if (!gold[i])
        break;
    }
  }
  if (err > 0) {
    fmt::print("FAIL err {}\n", err);
    return 1;
  }
  // std::cout << "SUCCESS" << std::endl;
  fmt::print("SUCCESS\n");
  return 0;
}


int MLIRVerify(std::shared_ptr<MLIRFuture<Token>> t, float exp, float tol=0.001);

#endif  // _MLIR_TEMPLATES_HOST_H
