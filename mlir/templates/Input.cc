#include "mlir/templates/Input.h"

#include "debug.h"

#include <algorithm>
#include <ranges>

using namespace ranges;
using namespace ranges::views;

void MLIRContextInput::TryLoad() {
  // Load the buffer from the global input.
  // fmt::print("storage size {} max {} in {}\n", storage.size(), max_storage, in->Valid());
  if (_storage_slots() < max_storage
      && in && in->Valid() && !load_lockout) {
    Token t = in->ReadPop();
    if (!t.nvalid && t.done_vec == 0)
      return;
    if (!storage.size() || std::get<2>(storage.back())) {
      storage.emplace_back();
      std::get<4>(storage.back()) = cycle;
    }
    if (width == kScalar)
      assert(t.nvalid <= 1);
    for (int i=0; i<t.nvalid; i++) {
      // storage.emplace_back(t.intVec_[i], 0);
      std::get<0>(storage.back()).emplace_back(t.intVec_[i]);
      std::get<1>(storage.back()).emplace_back(fifo ? -1 : t.threadIDs.at(i));
    }
    // fmt::print("Storage emplace scalars!\n");
    std::get<2>(storage.back()) = t.done_vec;
    std::get<3>(storage.back()) = t.endThreadID;
    // Lock out loads over following cycles. We're playing a bit fast and 
    // loose here by permitting over-length storage, but we will lock out 
    // future network accesses so this is equivalent to storing the values 
    // in a skid-buffer register (the hardware implementation).

    load_lockout = _storage_slots() - max_storage;
    if (load_lockout < 0)
      load_lockout = 0;
  }
  if (_storage_slots() < max_storage && load_lockout)
    load_lockout--;
  cycle++;
}

bool MLIRContextInput::Valid() const {
  // No storage: not valid
  if (!storage.size())
    return false;
  // If we're a scalar pipeline, don't try to stall
  if (width == kScalar)
    return true;
  // If we have a full vector already, no need to stall
  if (std::get<0>(storage.front()).size() >= 16)
    return true;
  // stall for up to min_age cycles relative to the front vector, which should
  // help with coalescing.
  return (cycle - std::get<4>(storage.front())) >= min_age;
}

// Used to set the vector length dynamically.
std::optional<int> MLIRContextInput::NumOffered() const {
  assert(Valid());
  auto max = 16ul;
  if (width == kScalar)
    max = 1;
  if (!Valid())
    _offered = 0;
  else
    _offered = std::min(std::get<0>(storage.front()).size(), max);

  return _offered;

#if 0
  int ret{0};
  for (const auto& [v, d] : storage) {
    ret++;
    if (d)
      break;
  }

  // fmt::print("Scalar offered {}\n", ret);
  return ret;
#endif
}

std::vector<std::vector<int>> MLIRContextInput::Read(int n, bool IDs) const {
  std::vector<int> ret;
  for (int i=0; i<n; i++) {
    if (IDs) {
      ret.emplace_back(std::get<1>(storage.front())[i]);
    } else {
      ret.emplace_back(std::get<0>(storage.front())[i]);
    }
  }
  // for (int i=0; i<n; i++) {
    // ret.emplace_back(storage[i].first);
  // }
  return {ret};
}

std::optional<int> MLIRContextInput::GetDoneLevel(int n, bool dbg) const {
  assert(Valid());
  assert(n <= std::get<0>(storage.front()).size());
  if (n == std::get<0>(storage.front()).size())
    return std::get<2>(storage.front());
  else
    return 0;
#if 0
  for (int i=0; i<n; i++) {
    if (storage[i].second)
      return storage[i].second;
  }
  return 0;
#endif
}

std::optional<int> MLIRContextInput::GetDoneID(int n) const {
  assert(Valid());
  assert(n <= std::get<0>(storage.front()).size());
  if (n == std::get<0>(storage.front()).size())
    return std::get<3>(storage.front());
  else
    return nullopt;
#if 0
  for (int i=0; i<n; i++) {
    if (storage[i].second)
      return storage[i].second;
  }
  return 0;
#endif
}

void MLIRContextInput::DequeueAtLevel(int l, int n) {
  assert(Valid());
  for (int i=0; i<n; i++) {
    if (l >= 0) {
      assert(std::get<0>(storage.front()).size());
      std::get<0>(storage.front()).pop_front();
      std::get<1>(storage.front()).pop_front();
    }

    // if (storage.front().second <= l)
      // storage.front().first.pop_front();
  }
  assert(std::get<0>(storage.front()).size() == std::get<1>(storage.front()).size());
  if (!std::get<0>(storage.front()).size() && std::get<2>(storage.front()) <= l)
    storage.pop_front();
}

std::string MLIRContextInput::LineTrace() {
  int sz = !!storage.size() ? std::get<0>(storage.front()).size() : -1;
  int age = -1;
  if (storage.size()) {
    age = cycle - std::get<4>(storage.front());
    if (age > 99)
      age = 99;
  }
  return fmt::format("(buf {} {:3} {:2} a{:2} {:3}/{:2})", 
      SubLineTrace(),
      _storage_slots(), 
      load_lockout,
      age,
      sz,
      !!storage.size() ? std::get<2>(storage.front()) : -1);
}

template<class Range>
std::optional<int> _min_defined(Range&& rng) {
  auto f = rng 
    | ranges::views::filter([](auto i) { return i.has_value(); }) 
    | ranges::views::transform([](auto i) { return *i; });
  if (std::begin(f) == std::end(f))
    return nullopt;
  return  ranges::min(f);
}

template<class Range>
std::optional<int> _min_defined_mod_1000(Range&& rng) {
  std::optional<int> ret;
  for (auto i : rng) {
    if (i.has_value()) {
      if (!ret) {
        ret = *i;
      } else {
        if ((*ret%1000) > (*i%1000)) {
          ret = *i;
        }
      }
    }
  }
  return ret;
}

void MLIRMultiInput::AddIn(MLIRInputInterface *in) {
  ins.emplace_back(in);
}

void MLIRMultiInput::TryLoad()  {
  for (auto i : ins) i->TryLoad();
}

bool MLIRMultiInput::Valid() const {
  _valid = ranges::all_of(ins, [](auto i) { return i->Valid(); });
  return *_valid;
}

bool MLIRMultiInput::HasBuffer() const {
  _valid = ranges::all_of(ins, [](auto i) { return i->HasBuffer(); });
  return *_valid;
}

optional<int> MLIRMultiInput::NumOffered() const {
  // auto rng = ins | ranges::views::transform([](auto i) { return i->NumOffered(); });
  // _offered = _min_defined(rng);
  _offered = nullopt;
  for (auto& i : ins) {
    auto o = i->NumOffered();
    if (o && (!_offered || *_offered >= *o))
      _offered = *o;
  }
  return _offered;
}

std::vector<std::vector<int>> MLIRMultiInput::Read(int n, bool IDs) const {
  std::vector<std::vector<int>> ret;
  for (int i=0; i<ins.size(); i++) {
    auto t = ins[i]->Read(n, IDs);
    for (auto& tt : t) {
      ret.emplace_back(tt);
    }
  }
  return ret;
}

// TODO: flat-map over dests here
std::vector<int> MLIRMultiInput::GetDests() const {
  std::vector<int> ret;
  for (auto i : ins) {
    for (auto d : i->GetDests()) {
      ret.emplace_back(d);
    }
  }
  return ret;
  // auto r = ins | ranges::views::transform([](auto i) { return i->GetDest(); });
  // return vector<int>(std::begin(r), std::end(r));
}

optional<int> MLIRMultiInput::GetDoneLevel(int n, bool dbg) const {
  auto rng = ins | ranges::views::transform([&](auto i) { return i->GetDoneLevel(n); });
  return _min_defined_mod_1000(rng);
  // return _min_defined(rng);
}

optional<int> MLIRMultiInput::GetDoneID(int n) const {
  optional<int> ret = nullopt;
  for (auto i : ins) {
    if (auto v = i->GetDoneID(n)) {
      if (!ret)
        ret = v;
      else
        *ret = Module::unionThreadIDs(*ret, *v);
    }
  }
  return ret;
}

void MLIRMultiInput::DequeueAtLevel(int l, int n) {
  _n_dequeue = n;
  _l_dequeue = l;
  for (auto i : ins)
    i->DequeueAtLevel(l, n);
}

std::string MLIRMultiInput::LineTrace() {
  /* auto off = _offered ? fmt::format("{:3}", *_offered) : "???";
  auto ndq = _n_dequeue ? fmt::format("{:3}", *_n_dequeue) : "???";
  auto ldq = _l_dequeue ? fmt::format("{:3}", *_l_dequeue) : "???";

  _offered = _n_dequeue = _l_dequeue = nullopt; */
  string ret =  fmt::format("(mul {}", SubLineTrace());
  for (auto i : ins)
    ret += " " + i->LineTrace();
  return ret + ")";
}

void MLIRMergeInput::AddIns(MLIRInputInterface *l, MLIRInputInterface *r, int dst_reg, bool _is_while) {
  left.AddIn(l);
  right.AddIn(r);
  if (auto inp = dynamic_cast<MLIRContextInput*>(r))
    inp->SetMinAge();
  dests.emplace_back(dst_reg);
  if (while_set)
    assert(is_while == _is_while && "All or no mergers must be while merge.");
  is_while = _is_while;
  while_set = true;
}

void MLIRMergeInput::TryLoad() {
  left.TryLoad();
  right.TryLoad();
}

bool MLIRMergeInput::Valid() const {
  // We are valid if we have an unstalled left input, right input, or both. We
  // are also valid if we have both inputs stalled. This case can occur when
  // using the leader/follower control structure; the leader may dequeue one
  // element without being done which tells the followers to dequeue one
  // element without being done. If the followers dequeue one element with done
  // bits present, then there both stall signals will latch. In this case, we
  // want to present the merge output as valid even though no elements can be
  // dequeued from such a condition.
  bool all_left = left.Valid() && stall_left < 0;
  bool all_right = right.Valid() && stall_right < 0;
  return all_left || all_right 
    || (stall_left >= 0 && stall_right >= 0);
}

std::optional<int> MLIRMergeInput::NumOffered() const {
  _offered = nullopt;
  if (left.size()) {
    _offered = 0;
    if (stall_left < 0 && left.Valid() && left.NumOffered()) {
      int tmp = *left.NumOffered();
      if (from_left >= 0) {
        // fmt::print("Restrict left to {}\n", from_left);
        tmp = std::min(from_left, tmp);
      } else if (is_while && tmp && !right.HasBuffer()) {
        // fmt::print("Restrict left for right-side buffering\n");
        tmp = 0;
      }
      *_offered += tmp;
    }
    if (stall_right < 0 && right.Valid() && right.NumOffered()) {
      int tmp = *right.NumOffered();
      if (from_right >= 0) {
        // fmt::print("Restrict right to {}\n", from_right);
        tmp = std::min(from_right, tmp);
      }
      *_offered += tmp;
    }
  }
  return _offered;
}

std::pair<int,int> MLIRMergeInput::doneLeftRight(int n) const {
  auto [val_l, val_r] = valLeftRight(n);
  // These do not hold for merges with control following. For those, it's
  // possible to have merge operations that have a constant for one input.
  /* if (left.Valid())
    assert(left.GetDoneLevel(val_l));
  if (right.Valid())
    assert(right.GetDoneLevel(val_r)); */
  int done_left = stall_left >= 0 ? stall_left : 
                  (left.Valid() ?  *left.GetDoneLevel(val_l) : 0);
  int done_right = stall_right >= 0 ? stall_right : 
                  (right.Valid() ?  *right.GetDoneLevel(val_r) : 0);
  return make_pair(done_left, done_right);
}

std::pair<int,int> MLIRMergeInput::idsLeftRight(int n) const {
  auto [val_l, val_r] = valLeftRight(n);
  /* if (left.Valid())
    assert(left.GetDoneID(val_l));
  if (right.Valid())
    assert(right.GetDoneID(val_r)); */
  int done_left = left.Valid() ? *left.GetDoneID(val_l) : -1;
  int done_right = right.Valid() ? *right.GetDoneID(val_r) : -1;
  return make_pair(done_left, done_right);
}

std::pair<int,int> MLIRMergeInput::valLeftRight(int n) const {

  int val_left = left.Valid() ? *left.NumOffered() : 0;
  int val_right = right.Valid() ? *right.NumOffered() : 0;

  if (from_left >= 0 && val_left > from_left)
    val_left = from_left;
  if (from_right >= 0 && val_right > from_right)
    val_right = from_right;

  int take_left = 0;
  int take_right = 0;
  assert(!(PRIORITIZE_LEFT && PRIORITIZE_AUTO));

#if PRIORITIZE_AUTO
  if (left.MaxPossible() > right.MaxPossible()) {
    // If the left can send more than the right, prioritize the right
    if (stall_right < 0) {
      take_right = std::min(val_right, n);
      n -= take_right;
    } 
    if (stall_left < 0) {
      take_left = std::min(val_left, n);
      n -= take_left;
    } 
  } else {
    // If the right can send more, prioritize the left
    if (stall_left < 0) {
      take_left = std::min(val_left, n);
      n -= take_left;
    } 
    if (stall_right < 0) {
      take_right = std::min(val_right, n);
      n -= take_right;
    } 
  }

#elif PRIORITIZE_LEFT
  if (stall_left < 0) {
    take_left = std::min(val_left, n);
    n -= take_left;
  } 
  if (stall_right < 0) {
    take_right = std::min(val_right, n);
    n -= take_right;
  } 
#else
  if (stall_right < 0) {
    take_right = std::min(val_right, n);
    n -= take_right;
  } 
  if (stall_left < 0) {
    take_left = std::min(val_left, n);
    n -= take_left;
  } 
#endif
  assert(n == 0);
  return make_pair(take_left, take_right);
}

std::vector<std::vector<int>> MLIRMergeInput::Read(int n, bool IDs) const {
  std::vector<std::vector<int>> ret;
  if (!left.size())
    return ret;

  auto [take_left, take_right] = valLeftRight(n);

  auto l = left.Read(take_left, IDs);
  auto r = right.Read(take_right, IDs);
  assert(l.size() == r.size());
  ret.resize(l.size());

  for (int i=0; i<ret.size(); i++) {
    std::copy(l[i].begin(), l[i].end(), std::back_inserter(ret[i]));
    std::copy(r[i].begin(), r[i].end(), std::back_inserter(ret[i]));
  }
  return ret;
}

std::vector<int> MLIRMergeInput::GetDests() const {
  return dests;
}

std::optional<int> MLIRMergeInput::GetDoneLevel(int n, bool dbg) const {
  if (!left.size())
    return nullopt;
  auto [done_l, done_r] = doneLeftRight(n);
  auto [take_left, take_right] = valLeftRight(n);


  if (is_while) {
    if (dbg)
      fmt::print("DBGWhile Done left {} done right {} stall left {} stall right {}\n",
          done_l, done_r, stall_left, stall_right);
    auto ret = -1;
    if (block_left && (last_done_right == 1) && (take_right == 0) && done_r == 1) {
      // This is the all-done case.
      if (dbg) 
        fmt::print("DBGWhile All done!\n");
      while_all_done = true;
      ret = stall_left;
      // TODO: this should assert that we have no pending done value and send immediately
    } else if (done_l == 0 || block_left) {
      // This just passes through the done level from the right, unless the left input is not done
      if (dbg) 
        fmt::print("DBGWhile pass through (left={}, right={})!\n", done_l, done_r);
      ret = std::min(done_l, done_r);
    } else {
      // Set the pending left block and set the initial done level
      if (dbg) 
        fmt::print("DBGWhile pend left block! (done=1)\n");
      pend_block_left = true;
      // TODO: the done value here should be delayed. We should transition it
      // to a pending-done state with a countdown timer. Any 
      ret = 1;
    }
    // TODO: we need to add a check-pending-done case here, with override for sending.
    
#if 0
    if (!hold_done_token) {
      return ret;
    } else {
      assert(ret == 0 || !pend_done);
      if (pend_done && (cycles_no_data > cycles_to_wait)) {
        ret = *pend_done + 2000;
        pend_done = nullopt;
      } else if (ret > 1) {
        // pass
      } else if (ret > 0) {
        pend_done = ret;
        ret += 1000;
      } else {
        ret = 0;
      }
      if (ret > 0) {
      // if (dbg) 
        fmt::print("DBGWhile return ret {}\n", ret);

      }
#else
      if (ret == 1)
        ret += 1000;
#endif
      if (dbg) 
        fmt::print("DBGWhile return ret {}\n", ret);
      return ret;
    // }

  } else {
    if (!left.Valid() && !right.Valid())
      return nullopt;
    return std::min(done_l, done_r);
  }
}

std::optional<int> MLIRMergeInput::GetDoneID(int n) const {
  if (!left.size())
    return nullopt;
  auto [take_left, take_right] = valLeftRight(n);
  auto [done_l, done_r] = doneLeftRight(n);

  if (is_while) {
    if (block_left && (last_done_right == 1) && (take_right == 0) && done_r == 1)
      return id_left;
    return nullopt;
  } else {
    if (!left.Valid() && !right.Valid())
      return nullopt;
    auto [id_l, id_r] = idsLeftRight(n);
    if (done_l == done_r)
      return Module::unionThreadIDs(id_l, id_r);
    return Module::invThread();
  }
}

void MLIRMergeInput::DequeueAtLevel(int l, int n) {
  if (!left.size())
    return;
  auto [take_left, take_right] = valLeftRight(n);
  auto [done_l, done_r] = doneLeftRight(n);
  auto [id_l, id_r] = idsLeftRight(n);

  if (is_while) {
    if (right.Valid()) {
      last_done_right = done_r;
      // fmt::print("DBGWhile last_done_right={}!\n", last_done_right);
      /* if (done_r == 1) {
        assert(block_left);
        pend_done_right = true;
      } else {
        pend_done_right = false;
      } */
    }

    // TODO: check this over. This probably needs to be rewritten to handle
    // some of the duplicate cases etc. I think this is a bit hacky and may
    // break for the for_for_while test case.
    // fmt::print("DBGWhile dequeue level={}, left={} right={}!\n", l, take_left, take_right);
    if (left.Valid() && !block_left)
      left.DequeueAtLevel(l, take_left);

    dequeued_from_right = 0;
    if (right.Valid()) {
      dequeued_from_right = take_right;
      right.DequeueAtLevel(l, take_right);
    }

    if (while_all_done && stall_left <= l) {
      // When we're all done on a while loop, then there should be no data
      // remaining to take.
      assert(take_left == 0 && take_right == 0);
      pend_block_left = block_left = while_all_done = false;
      last_done_right = -2;
      stall_left = stall_right = -1;
      id_left = Module::invThread();
      // fmt::print("DBGWhile apply all done!\n");
    } else if (pend_block_left && l) {
      block_left = true;
      pend_block_left = false;
      stall_left = done_l + 1;
      id_left = id_l;
      // fmt::print("DBGWhile set block-left (stall_left={})!\n", stall_left);
    }
  } else {
    stall_left = done_l;
    stall_right = done_r;

    if (stall_left <= l)
      stall_left = -1;
    if (stall_right <= l)
      stall_right = -1;

    if (left.Valid())
      left.DequeueAtLevel(l, take_left);

    dequeued_from_right = 0;
    if (right.Valid()) {
      dequeued_from_right = take_right;
      right.DequeueAtLevel(l, take_right);
    }
  }
}

std::string MLIRMergeInput::LineTrace() {
  auto str = is_while ? "while" : "merge";
  return fmt::format("({} {} sl:{:2} sr:{:2} bl:{:5} ldr:{:2} l{} r{})", str, SubLineTrace(), stall_left, stall_right, block_left, last_done_right, left.LineTrace(), right.LineTrace());
}

int MLIRController::getStarveWord() {
  int ret{0};
  for (int i=0; i<max_in; i++) {
    if (starve_map[i] && starve_map[i]->Valid())
      ret |= (1<<i);
  }
  return ret;
}

void MLIRController::_validate() {
  assert(vals.size());
  int sz = vals[0].size();
  for (auto& v : vals)
    assert(v.size() == sz);
}

std::optional<Token> MLIRController::Advance() {
  _validate();
  in_multi.TryLoad();
  cycles_no_data++;

  status = "  GO";
  action = "";

  // TODO: need to check for a valid control word here
  // The control word will have two components: the total number of elements to
  // dequeue and the number from the right side of the merge interface. It will
  // also have a done level, which becomes the done-level for the dequeue.

  lt = in_multi.LineTrace();
  if (pend_done && cycles_no_data > cycles_to_wait) {
    Token t;
    t.nvalid = 0;
    t.done_vec = *pend_done;
    t.endThreadID = -1;
    for (int i=0; i<vals.size(); i++)
      vals.at(i).emplace_back(t);
    fmt::print("{} send delayed token (done={})\n", name, *pend_done);
    pend_done = nullopt;
    return nullopt;
  }

  int max_done = -1;
  int done_thread_id = -1;
  int len = 0;
  int ctrl_out{0};

  if (vals[0].size() > 1) {
    status = "  BP";
    // fmt::print("Fail (backpressure)\n");
    return nullopt;
  }

  if (ctrl_in) {
    ctrl_in->Eval();
    ctrl_in->Clock();
    try_move(ctrl_in_raw, ctrl_in);
    if (!ctrl_in->Valid()) {
      status = "CTRL";
      return nullopt;
    }
    int ctrl_word = ctrl_in->Read();
    int len_from_in = ctrl_word&0xFF;
    int len_from_right = (ctrl_word>>8)&0xFF;
    int len_from_left = len_from_in - len_from_right;

#if DEBUG_PIPE
    fmt::print("{} set expect from right {} (raw={})\n", name, len_from_right, ctrl_word);
    fmt::print("{} set expect from left {}\n", name, len_from_left);
#endif
    in_merge.SetFromCtrl(len_from_left, len_from_right);
  } 

  if (!in_multi.Valid()) {
    status = " INV";
    // fmt::print("Fail (buffer invalid)\n");
    return nullopt;
  }

  if (!in_multi.NumOffered().has_value()) {
    fmt::print("{} could not get a length to run\n", name);
    assert(false);
  }
  if (ctrl_in) {
    max_done = ctrl_in->Read().done_vec;
    int ctrl_word = ctrl_in->Read();
    int len_from_in = ctrl_word&0xFF;
#if DEBUG_PIPE
    fmt::print("{} done from ctrl @ {}\n", name, max_done);
#endif
#if 1
    // If we do not have enough done tokens here to match the controller
    // done-token, then stall until we match. This avoid the case where the
    // controller wants to be done-1 and we're done-0 going to done-1 in
    // several cycles and then choking on the second done-1 token.
    int done_loc;
    if ((done_loc = in_multi.GetDoneLevel(len_from_in, true).value_or(1000)) < max_done) {
#if DEBUG_PIPE
      fmt::print("{} done from loc @ {}\n", name, done_loc);
#endif
      status = " DIN";
      // Hack because there's mutable state in the merge logic.
      in_merge.ResetMutable();
      return nullopt;
    }
#else
    // If we have a local done-level, no tokens remaining, and we're operating
    // as a follower to another context, then check if our done level is above
    // that of the other context. In that case, we would have send its
    // done-level earlier without having dequeued our done_level, so flush our
    // done level so that we can continue.
    int done_cur = in_multi.GetDoneLevel(0, false).value_or(0);
    if (in_multi.NumOffered().value() == 0 && done_cur > max_done) {
      status = " PRG";
      in_multi.DequeueAtLevel(done_cur, 0);
      return nullopt;
    }
#endif
  }

  if (ctrl_in) {
    int ctrl_word = ctrl_in->Read();
    int len_from_in = ctrl_word&0xFF;

#if DEBUG_PIPE
    fmt::print("{} get len from ctrl {} offered len {}\n", name, len_from_in, in_multi.NumOffered().value());
#endif
    if (in_multi.NumOffered().value() < len_from_in) {
      status = " LEN";
      return nullopt;
    }

    assert(len_from_in <= max_par);

    len = std::min(len_from_in, in_multi.NumOffered().value());
  } else {
    len = std::min(max_par, in_multi.NumOffered().value());
  }
  if (!ctrl_in) {
    if (!in_multi.GetDoneLevel(len, false).has_value()) {
      fmt::print("{} could not get a done level\n", name);
      assert(false);
    }
    max_done = in_multi.GetDoneLevel(len, false).value();
    done_thread_id = in_multi.GetDoneID(len).value_or(-1);
  }

#if DEBUG_PIPE
  fmt::print("{}: Advancing... {} (raw_done={})\n", name, len, max_done);
#endif
  bool kill_done_for_send{false};
  if (hold_done_token && max_done > 1000) {
    assert(max_done == 1001);
    assert(!pend_done);
    kill_done_for_send = true;
  }
  max_done = max_done % 1000;
  if (kill_done_for_send) {
    assert(!ctrl_in);
    pend_done = max_done;
    fmt::print("{}: Kill done for send={}\n", name, kill_done_for_send);
  }

  if (!len && !max_done) {
    status = " ZRO";
    return nullopt;
  }

  int idx_next{0};
  idx_to_dst.clear();
  auto dsts = in_multi.GetDests();
  for (auto& n : dsts)
    idx_to_dst[n] = idx_next++; 
  auto vv = in_multi.Read(len, false);
  auto ids = in_multi.Read(len, true);
  std::vector<int> ids_out;
  ids_out.resize(ids[0].size(), -1);
  assert(dsts.size() == vals.size());

  if (!ctrl_in) {
    for (int ii=0; ii<ids[0].size(); ii++)
      for (int i=0; i<ids.size(); i++)
        ids_out[ii] = Module::unionThreadIDs(ids_out[ii], ids[i][ii], false);

    for (int ii=0; ii<ids[0].size(); ii++)
      for (int i=0; i<ids.size(); i++)
        ids_out[ii] = Module::unionThreadIDs(ids_out[ii], ids[i][ii], true);
  } else {
    assert(ctrl_in->Read().threadIDs.size() == ids_out.size());
    ids_out = ctrl_in->Read().threadIDs;
    done_thread_id = ctrl_in->Read().endThreadID;
  }

  for (int i=0; i<vv.size(); i++) {
    auto v = vv[i];
    if (v.size())
      cycles_no_data = 0;
    assert(v.size() <= max_par);
    Token t(v);
    if (kill_done_for_send) {
      t.done_vec = 0;
    } else {
      t.done_vec = max_done;
    }
    // TODO: change this to get accurate threadIDs based on the input broadcasts.
    for (int ii=0; ii<t.nvalid; ii++)
      t.threadIDs.emplace_back(ids_out[ii]);
    t.endThreadID = done_thread_id;
    vals.at(i).emplace_back(t);
#if DEBUG_PIPE
    fmt::print("{}: Emplace token to buf {:2} val {}\n", name, i, t.ToString());
    fmt::print("{}:                         ids {}\n", name, t.IDsToString());
#endif
  }
  // fmt::print("\tPop local buffers at level: {}\n", max_done);
  in_multi.DequeueAtLevel(max_done, len);

  ctrl_out = len | (in_merge.DeqFromRight()<<8);
  if (ctrl_in)
    ctrl_in->Pop();

  action = fmt::format("POP {:3}@{:2}", len, max_done);

  Token ctrl_out_tok(ctrl_out);
  ctrl_out_tok.threadIDs = ids_out;
  ctrl_out_tok.endThreadID = done_thread_id;
  ctrl_out_tok.done_vec = max_done;

  // fmt::print("{} write output control {}\n", name, ctrl_out_tok.ToString());
  return ctrl_out_tok;
}

void MLIRController::Pop() {
  _validate();
#if DEBUG_PIPE
  fmt::print("Controller pop!\n");
#endif
  // If there are pending tokens for the current iteration, dequeue the first
  // value. Return if there are still valid values.
  if (vals[0].size()) {
    for (auto& v : vals)
      v.pop_front();
  }
}

bool MLIRController::Valid() {
  _validate();
  // fmt::print("Check valid {}\n", vals[0].size());
  return !!vals[0].size();
}

MLIRCounter::MLIRCounter(
        MLIRInputInterface *min,
        MLIRInputInterface *max,
        MLIRInputInterface *step,
        bool flat
      ) : flat(flat) {
  m.AddIn(min);
  m.AddIn(max);
  m.AddIn(step);
}

void MLIRCounter::TryLoad() {
  // fmt::print("Try to load counter!\n");
  // if (storage.size() || _level_in >= 0)
  if (storage.size() || _level_in.size())
    return;
  // fmt::print("\tpass internal\n");
  m.TryLoad();
  if (!m.Valid())
    return;
  // fmt::print("\tpass inputs\n");
  
  auto offered = m.NumOffered();
  assert(offered);
  if (*offered == 0) {
    _level_in.emplace_back(m.GetDoneLevel(0).value_or(500));
    _read_empty = true;
  } else {
    _level_in.emplace_back(m.GetDoneLevel(1).value_or(500));
    // _done_id = m.GetDoneID(1).value_or(Module::invThread());

    auto v = m.Read(1, false);
    auto ids = m.Read(1, true);

    assert(v.size() == 3);
    for (int i=0; i<3; i++)
      assert(v[i].size() == 1);

    auto id = Module::unionThreadIDs({ids[0][0], ids[1][0], ids[2][0]});
    _done_id = id;

    for (int i=v[0][0]; i<v[1][0]; i += v[2][0]) {
      storage.emplace_back(i);
      if (flat)
        id_storage.emplace_back(Module::getSiblingThread(id));
      else
        id_storage.emplace_back(Module::getChildThread(id));
    }
    if (storage.size() == 0 && _level_in.front() != 0)
      _level_in.emplace_front(0);
  }
  // fmt::print("\tloaded {} done {}\n", storage.size(), _level_in);
}

bool MLIRCounter::Valid() const {
  _valid = (_level_in.size());
  return *_valid;
}

std::optional<int> MLIRCounter::NumOffered() const {
  // fmt::print("Counter offers: {}!\n", storage.size());
  _offered = storage.size();
  return storage.size();
}

std::vector<std::vector<int>> MLIRCounter::Read(int n, bool IDs) const {
  std::vector<int> ret;
  assert(id_storage.size() == storage.size());
  for (int i=0; i<n; i++) {
    ret.emplace_back(IDs ? id_storage[i] : storage[i]);
  }
  return {ret};
}

std::optional<int> MLIRCounter::GetDoneLevel(int n, bool dbg) const {
  assert(n <= storage.size());
  // TODO: verify this...
  if (n && storage.size() - n)
    return 0;
  else
    return _level_in.front() + 1;
}

std::optional<int> MLIRCounter::GetDoneID(int n) const {
  assert(n <= storage.size());
  // TODO: verify this...
  if (n && storage.size() - n)
    return nullopt;
  // if (_level_in.size() > 1)
    // return nullopt;
  else
    return _done_id;
}

void MLIRCounter::DequeueAtLevel(int l, int n) {
  if (l >= 0) {
    for (int i=0; i<n; i++) {
      storage.pop_front();
      id_storage.pop_front();
    }
    if (l > 0 && (n != 0 || !storage.size())) {
      assert(!storage.size());
      _level_in.pop_front();
      if (_level_in.size() == 0) {
        m.DequeueAtLevel(l - 1, _read_empty ? 0 : 1);
        _done_id = Module::invThread();
        _read_empty = false;
      }
    }
  }
  _n_dequeue = n;
  _l_dequeue = l;
  assert(id_storage.size() == storage.size());
}

string MLIRCounter::LineTrace() {
  return fmt::format("(cnt {} li{:4} {})", SubLineTrace(), _level_in.size() ? _level_in.front() : -10, m.LineTrace());
}

