#ifndef _MLIR_TEMPLATES_INPUT_H
#define _MLIR_TEMPLATES_INPUT_H

#include "interface.h"
#include "module.h"
#include "state.h"
#include "token.h"
#include "util.h"

#include "debug.h"

#include "fmt/format.h"

class MLIRInputInterface {
  int dst_reg{-1};

protected:
  mutable optional<int> _offered;
  mutable optional<int> _n_dequeue;
  mutable optional<int> _l_dequeue;
  mutable optional<bool> _valid;

public:
  void SetDest(int dst) {
    dst_reg = dst;
  }
  virtual std::vector<int> GetDests() const {
    return {dst_reg};
  }

  virtual void TryLoad() {};

  virtual int MaxPossible() const { return 16; }

  virtual bool Valid() const = 0;
  virtual bool HasBuffer() const { return true; }
  // Get the number of elements offered in the current dimension.
  virtual std::optional<int> NumOffered() const { return std::nullopt; }
  // Dequeue a token with the given number of valid elements.
  virtual std::vector<std::vector<int>> Read(int n, bool IDs) const = 0;

  // Get the done-level of this counter.
  virtual std::optional<int> GetDoneLevel(int n, bool dbg=false) const { return std::nullopt; }
  virtual std::optional<int> GetDoneID(int n) const { return std::nullopt; }
  // Dequeue based on the context done-level.
  virtual void DequeueAtLevel(int l, int n) {};

  std::string SubLineTrace() {
    auto val = _valid ? fmt::format("{:1}", *_valid ? "V" : "i") : "#";
    auto off = _offered ? fmt::format("{:3}", *_offered) : "???";
    auto ndq = _n_dequeue ? fmt::format("{:3}", *_n_dequeue) : "???";
    auto ldq = _l_dequeue ? fmt::format("{:3}", *_l_dequeue) : "???";

    // _offered = _n_dequeue = _l_dequeue = nullopt;
    _valid = nullopt;
    return fmt::format("v{}o{}n{}l{}", val, off, ndq, ldq);
  }
  virtual std::string LineTrace() {
    return "???";
  }
};

class MLIRInputBroadcast : public MLIRInputInterface {
  MLIRInputInterface *i;
  int levels;
  bool _empty() const {
    auto off = i->NumOffered(); 
    if (!off || !(*off))
      return true;
    else
      return false;
  }
public:
  MLIRInputBroadcast(MLIRInputInterface *i, int l) : i(i), levels(l) {}
  void TryLoad() override { i->TryLoad(); }
  bool Valid() const override { return i->Valid(); }
  bool HasBuffer() const override { return i->HasBuffer(); }
  virtual std::optional<int> NumOffered() const { 
    return _empty() ? std::optional<int>(0) : std::nullopt;
  }
  std::vector<std::vector<int>> Read(int n, bool IDs) const {
    if (n > 0) {
      auto v = i->Read(1, IDs)[0][0];
      return {std::vector<int>(n, v)};
    } else {
      return {{}};
    }
  }
  std::optional<int> GetDoneLevel(int n, bool dbg=false) const override { 
    return levels + i->GetDoneLevel(_empty()?0:1).value_or(1000);
  }
  std::optional<int> GetDoneID(int n) const { 
    return i->GetDoneID(_empty()?0:1);
  }
  void DequeueAtLevel(int l, int n) {
    i->DequeueAtLevel(l - levels, _empty()?0:1);
  };
  virtual std::string LineTrace() {
    return fmt::format("(+{}{} {})", levels, i->Valid()?(_empty()?"e":"f"):"x", i->LineTrace());
  }
};

// Input buffers represent a key part of the MLIR context abstraction. Although
// their logic is physically small, it has a large number of configuration
// options.
class MLIRContextInput : public MLIRInputInterface {
  friend class MLIRCounter;
protected:
  int min_depth;
  int control_depth;
private:
  // The goal of these variables is to enforce minimum aging on values that sit in the input buffer.
  // Effectively, we don't want to take in a partially-valid vector (e.g., 4
  // elements) and send it into the pipeline if we can coalesce it with the
  // following vector. This should have the effect of building up vector
  // parallelism for requests that get split over time, which would otherwise
  // diminish the amount of vector parallelism.
  int min_age{INPUT_MIN_AGE_GLOBAL};

  // Extra slots to preserve for the back-edge buffered case
  int buf_extra_slots{INPUT_BUF_EXTRA};
  int cycle{0};

  bool fifo{false};
  CheckedReceive<Token>* in;

  // For simplicity, just keep our storage local here.
  int max_storage;
  // std::deque<std::tuple<bool,int,int>> storage;
  // std::deque<std::pair<int,int>> storage;
  // Input values, input IDs, done level, done ID, enqueue cycle
  std::deque<std::tuple<std::deque<int>,std::deque<int>,int,int,int>> storage;
  int load_lockout{0};

  // Whether to limit our inputs to only scalar values.
  enum InputType { kToken, kScalar, kVector };
  InputType width;

  int _storage_width() const {
    if (width == kScalar)
      return 1;
    return 16;
  }
  int _storage_slots() const {
    int ret{0};
    for (auto& s : storage) {
      assert(std::get<0>(s).size() == std::get<1>(s).size());
      ret += (std::get<0>(s).size() + _storage_width() - 1) / _storage_width();
    }
    return ret;
  }

public:

  int MaxPossible() const override  {
    return _storage_width();
  }
  bool HasBuffer() const override {
    return _storage_slots() + buf_extra_slots < max_storage;
  }

  void SetMinAge() {
    min_age = INPUT_MIN_AGE;
    fmt::print("Set minimum age: {}\n", min_age);
  }
  MLIRContextInput(CheckedReceive<Token>* in, int depth, int control_depth, bool fifo, bool scalar) : min_depth(0), in(in), max_storage(depth), control_depth(control_depth), fifo(fifo) {
    if (scalar)
      width = kScalar;
    else
      width = kVector;
  }

  MLIRContextInput(int min_depth, int control_depth) : min_depth(min_depth), control_depth(control_depth) {
  }
  virtual void TryLoad() override;

  virtual bool Valid() const override;
  // Get the number of elements offered in the current dimension.
  virtual std::optional<int> NumOffered() const override;
  // Dequeue a token with the given number of valid elements.
  virtual std::vector<std::vector<int>> Read(int n, bool IDs) const override;

  // Get the done-level of this counter.
  virtual std::optional<int> GetDoneLevel(int n, bool dbg=false) const override;
  virtual std::optional<int> GetDoneID(int n) const override;
  // Dequeue based on the context done-level.
  virtual void DequeueAtLevel(int l, int n) override;
  virtual std::string LineTrace();
};

class MLIRConstantInput : public MLIRInputInterface {
  int c;
public:
  MLIRConstantInput(int c) : c(c) {}
  bool Valid() const override { return true;} ;
  std::optional<int> NumOffered() const override { return 1000; }
  std::vector<std::vector<int>> Read(int n, bool ids) const override {
    if (ids) {
      return {vector<int>(n, -1)};
    } else {
      return {vector<int>(n, c)};
    }
  }
  string LineTrace() {
    return "c";
  }
};

class MLIRInfiniteCounter : public MLIRInputInterface {
  int max;
  int count{0};
public:
  MLIRInfiniteCounter(int max) : max(max) {}
  bool Valid() const override { return true;} ;
  std::optional<int> NumOffered() const override { return 1000; }
  std::vector<std::vector<int>> Read(int n, bool IDs) const override {
    if (IDs) {
      return {vector<int>(n, -1)};
    } else {
      vector<int> ret;
      for (int i=0; i<n; i++)
        ret.emplace_back((count+i)%max);
      return {ret};
    }
  }
  virtual void DequeueAtLevel(int l, int n) {
    count = (count+l)%max;
  }
  string LineTrace() {
    return "c";
  }
};

template<class Range>
std::optional<int> _min_defined(Range&& rng);

class MLIRMultiInput : public MLIRInputInterface {
  std::vector<MLIRInputInterface*> ins;

public:
  int MaxPossible() const override {
    int ret = 16;
    for (auto i : ins) {
      if (i->MaxPossible() < ret)
        ret = i->MaxPossible();
    }
    return ret;
  }
  int size() const {
    return ins.size();
  }
  void AddIn(MLIRInputInterface *in);
  void TryLoad() override;
  bool Valid() const override;
  bool HasBuffer() const override;

  std::optional<int> NumOffered() const override;
  std::vector<std::vector<int>> Read(int n, bool IDs) const override;
  std::vector<int> GetDests() const override;

  std::optional<int> GetDoneLevel(int n, bool dbg=false) const override;
  std::optional<int> GetDoneID(int n) const override;
  void DequeueAtLevel(int l, int n) override;
  virtual std::string LineTrace();
};

class MLIRMergeInput : public MLIRInputInterface {
  MLIRMultiInput left;
  MLIRMultiInput right;
  std::vector<int> dests;

  bool while_set{false};
  bool is_while{false};

  int from_left{-1}, from_right{-1};
  int dequeued_from_right{0};

  // These two variables are used for while loop signalling on the left input
  // branch. The pending block is set when reading the done level, and is then
  // updated to an actual block once dequeued. The block_left variable ensures
  // that exactly one `2' is sent for the tile.
  mutable bool pend_block_left{false};
  bool block_left{false};
  // This variable is set whenever a `2' is dequeued from the right branch.
  // When pend_done_right is set and a `2' is dequeued with zero elements, that
  // indicates the completion of the list. In this case, pend_done_right and
  // block_left are reset, stall_left is reset, and stall_left+1 is output as
  // the done level.
  int last_done_right{-1};

  // This variable is set when the next dequeue should reset all the while loop statuses.
  mutable bool while_all_done{false};

  // When we get a done value from the left or from the right, stall that side
  // of the merge until we get an equivalent (or greater) value from the other
  // side.
  int stall_left{-1}, stall_right{-1};

  // The ID to be passed as the done-ID from the final loop iteration.
  int id_left{-1};

  std::pair<int,int> doneLeftRight(int n) const;
  std::pair<int,int> idsLeftRight(int n) const;
  std::pair<int,int> valLeftRight(int n) const;

public:
  void ResetMutable() {
    while_all_done = false;
    pend_block_left = false;
  }
  int DeqFromRight() const {
    return dequeued_from_right;
  }
  void AddIns(MLIRInputInterface *l, MLIRInputInterface *r, int dst_reg, bool _is_while);
  void TryLoad() override;
  bool Valid() const override;
  void SetFromCtrl(int _from_left, int _from_right) {
    from_left = _from_left;
    from_right = _from_right;
  }

  std::optional<int> NumOffered() const override;
  std::vector<std::vector<int>> Read(int n, bool IDs) const override;
  std::vector<int> GetDests() const override;

  std::optional<int> GetDoneLevel(int n, bool dbg=false) const override;
  std::optional<int> GetDoneID(int n) const override;
  void DequeueAtLevel(int l, int n) override;
  virtual std::string LineTrace();
};

// An `input-like' counter abstraction.
class MLIRCounter : public MLIRInputInterface {
  MLIRInputInterface *min;
  MLIRInputInterface *max;
  MLIRInputInterface *step;

  MLIRMultiInput m; 

  // int _level_in{-10};
  int _done_id{-1};
  std::deque<int> _level_in;
  // std::deque<int> _done_id;
  bool _read_empty{false};
  bool flat;

  std::deque<int> storage;
  std::deque<int> id_storage;

public:
  MLIRCounter(
      MLIRInputInterface *min,
      MLIRInputInterface *max,
      MLIRInputInterface *step,
      bool flat
      );
  virtual void TryLoad();

  virtual bool Valid() const;
  virtual std::optional<int> NumOffered() const;
  virtual std::vector<std::vector<int>> Read(int n, bool IDs) const;

  virtual std::optional<int> GetDoneLevel(int n, bool dbg=false) const;
  std::optional<int> GetDoneID(int n) const override;
  virtual void DequeueAtLevel(int l, int n);
  virtual std::string LineTrace();
};

// This will be the main control manager for the MLIR template infrastructure.
// Forcing the control into different input buffers is too cumbersome and hard
// to reason about. Instead, this control logic will handle the broadcasting,
// etc. in one central place.
class MLIRController {
  string status{""};
  string name{""};
  string lt{""};
  string action{""};
  // There are several key types of values:
  //  - Constants
  //  - Scalar input buffers
  //  - Vector input buffers
  //  - Counters
  //
  // If using a counter, the counter must be present at the lowest input level.
  //
  // For simplicity, the current controller abstraction should probably be re-defined so that each controller only handles one level in the controller hierarchy (PIR-style).
  // The new abstraction will define the innermost controller, along with a centralized PCU controller.
  //
  // Everything should be defined in terms of tensor dimensions
  // Two-dimensional counter = two tensor dimensions
  //
  // For now, require that counters exist at the lowest tensor dimensions, and 
  // also require the existence of a one-level-higher dimensional input.

  int cycles_no_data{0};
  int cycles_to_wait{HOLD_DONE_CYCLES};
  mutable optional<int> pend_done{nullopt};
  bool hold_done_token{true};

  map<int,int> idx_to_dst;

  map<int,int> idx_to_loc;

  CheckedReceive<Token> *ctrl_in_raw{NULL};
  FIFO<Token,16> *ctrl_in{NULL};

  std::vector<MLIRInputInterface*> bufs_raw;
  MLIRMultiInput in_multi;
  MLIRMergeInput in_merge;

  std::vector<std::deque<Token>> vals;
  void _validate();

  int _get_idx(int idx) {
    auto loc = bufs_raw.size() - 1;
    auto ret_idx = idx >= 0 ? idx : -loc-1;
    idx_to_loc[ret_idx] = loc;
    return ret_idx;
  }
  MLIRInputInterface *get_in(int idx) {
    return bufs_raw.at(idx_to_loc.at(idx));
  }
  int max_in{0};
  std::map<int, MLIRInputInterface*> starve_map;

  int max_par{16};

  void regStarve(int idx, MLIRInputInterface *intf) {
    if (idx + 1 > max_in)
      max_in = idx + 1;
    if (idx >= 0)
      starve_map[idx] = intf;
  }

public:
  int DeqFromRight() {
    return in_merge.DeqFromRight();
  }
  void SetNoHold() {
    hold_done_token = false;
  }

  int getMaxIn() { return max_in; }
  int getStarveWord();
  void SetName(const std::string& _name) { 
    name = _name; 
  }
  MLIRController() {
    in_multi.AddIn(&in_merge);
  }
  void SetPar(int p) {
    assert(p == 1 || p == 16);
    max_par = p;
  }
  void SetCtrlInput(CheckedReceive<Token>* in) {
    assert(!ctrl_in && !ctrl_in_raw);
    ctrl_in_raw = in;
    ctrl_in = new FIFO<Token,16>("ctrl_in_fifo");
  }
  int AddInput(CheckedReceive<Token>* in, int depth, int idx, int dst_reg, bool fifo, bool scalar, bool is_short, bool is_token) {
    MLIRInputInterface *intf = new MLIRContextInput(in, is_token ? 65536 : 
                                                        is_short ? 3 : 
                                                        scalar ? 64 : 16, 0, fifo, scalar);
    regStarve(idx, intf);
    if (depth)
      intf = new MLIRInputBroadcast(intf, depth);
    bufs_raw.emplace_back(intf);
    intf->SetDest(dst_reg);
    if (dst_reg >= 0) {
      in_multi.AddIn(intf);
      vals.emplace_back();
    }
    return _get_idx(idx);
  } 
  int AddConstant(int c, int idx, int dst_reg=-1) {
    auto intf = new MLIRConstantInput(c);
    regStarve(idx, intf);
    intf->SetDest(dst_reg);
    bufs_raw.emplace_back(intf);
    if (dst_reg >= 0) {
      in_multi.AddIn(intf);
      vals.emplace_back(); 
    }
    return _get_idx(idx);
  } 
  int AddInfCounter(int max, int idx, int dst_reg=-1) {
    auto intf = new MLIRInfiniteCounter(max);
    regStarve(idx, intf);
    intf->SetDest(dst_reg);
    bufs_raw.emplace_back(intf);
    if (dst_reg >= 0) {
      in_multi.AddIn(intf);
      vals.emplace_back(); 
    }
    return _get_idx(idx);
  } 
  int AddFloatConstant(float c, int idx, int dst_reg=-1) {
    Token tmp;
    tmp.float_ = c;
    auto intf = new MLIRConstantInput(tmp.int_);
    regStarve(idx, intf);
    intf->SetDest(dst_reg);
    bufs_raw.emplace_back(intf);
    if (dst_reg >= 0) {
      in_multi.AddIn(intf);
      vals.emplace_back(); 
    }
    return _get_idx(idx);
  } 
  void AddCounter(int idx_min, int idx_max, int idx_step, int idx, int dst_reg, int depth, bool flat) {
    MLIRInputInterface *intf = new MLIRCounter(get_in(idx_min), get_in(idx_max), get_in(idx_step), flat);
    regStarve(idx, intf);
    if (depth)
      intf = new MLIRInputBroadcast(intf, depth);
    intf->SetDest(dst_reg);
    if (dst_reg >= 0) {
      in_multi.AddIn(intf);
      vals.emplace_back(); 
    }
  }
  void AddMerge(int left, int right, int dst_reg, bool is_while) {
    in_merge.AddIns(get_in(left), get_in(right), dst_reg, is_while);
    if (dst_reg >= 0) {
      vals.emplace_back(); 
    }
  }
  std::optional<Token> Advance();
  void Pop();
  bool Valid();
  Token Read(int dst_reg) { 
    assert(idx_to_dst.count(dst_reg));
    auto idx = idx_to_dst[dst_reg];
    assert(vals[idx].size());
#if DEBUG_PIPE
    fmt::print("Return: {}\n", vals[idx].front().ToString());
#endif
    return vals[idx].front();
  }
  std::string LineTrace() {
    // return fmt::format("{} {} {}", status, in_multi.LineTrace(), action);
    return fmt::format("{} {} {}", status, lt, action);
  }
};

#endif  //  _MLIR_TEMPLATES_INPUT_H
