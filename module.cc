#include "module.h"

std::atomic<unsigned long int> Module::remaining;
bool Module::any_active{false};
bool Module::enable_binlog{true};
int Module::next_thread{1};
bool Module::constant_mem_mode{false};
unsigned long int Module::cycle{0};
std::map<int,int> Module::thread_next_child;
std::map<int,std::pair<int,int>> Module::thread_parent_records;
std::map<int,std::string> Module::thread_names;

binlog::SessionWriter Module::static_writer{GetSession()};

Module::Module(std::initializer_list<Module*> _children,
    const std::string& _name) :
  name(_name), path("/") {
    for (auto c : _children)
      AddChild(c);
  }

Module::Module(const std::string& _name) :
  name(_name), path("/") {
  }

void Module::AddChild(Module *c, bool clock) {
  c->path = path+name+"/";
  if (clock)
    children.push_back(c);
  else
    name_children.push_back(c);
}

void Module::AddChildren(std::initializer_list<Module*> _children, bool clock) {
  for (auto* c : _children)
    AddChild(c, clock);
}

void Module::SetType(int _type) {
  type = _type;
}

// void Module::SetSession(binlog::Session& session) {
  // writer::~SessionWriter();
  // new (&writer) binlog::SessionWriter(session);
  // writer = binlog::SessionWriter(REPL::GetSession());
// }

void Module::Finalize() {
  if (type >= 0) {
    //NetworkLinkManager::AddNode(path+name, type);
    // AddNode(path+name, type);
  }
  // writer.setName(path+name);
  static_writer.setName("global");
}
void Module::SetParameter(const std::string& key, const std::string& val) {
  throw std::runtime_error(this->name + " has no key " + key);
}
void Module::AppendParameter(const std::string& key, const std::string& val) {
  throw std::runtime_error(this->name + " has no key " + key);
}
void Module::Apply(const std::string& cmd, const std::vector<std::string>& vals) {
  throw std::runtime_error(this->name + " has no command " + cmd);
}
std::string Module::GetParameter(const std::string& key) {
  throw std::runtime_error(this->name + " has no key " + key);
}
std::vector<Module*> Module::FindChildren(const std::deque<std::string>& search_path) {
  std::vector<Module*> ret;
  if (search_path.size() == 0) {
    // std::cout << "Leaf module, returning: " << this->path << name << std::endl;
    ret.push_back(this);
  } else {
    std::deque<std::string> rem;
    for (int i=1; i < search_path.size(); i++)
      rem.push_back(search_path[i]);
    // Enforce full matching
    std::regex r("^"+search_path[0]+"$");
    // std::cout << this->path << name << "search children for: " << ("^"+search_path[0]+"$") << std::endl;
    std::vector<Module*> to_search;
    std::copy(children.begin(), children.end(), std::back_inserter(to_search));
    std::copy(name_children.begin(), name_children.end(), std::back_inserter(to_search));
    for (auto c : to_search) {
      if (std::regex_search(c->name, r)) {
        auto tmp = c->FindChildren(rem);
        std::copy(tmp.begin(), tmp.end(), std::back_inserter(ret));
      }
    }
  }
  return ret;
}

Module* Module::FindChild(const std::string& ID) {
  for (Module *c : children) {
    if (c->name == ID)
      return c;
  }
  for (Module *c : name_children) {
    if (c->name == ID)
      return c;
  }
  throw std::runtime_error("Module " + name + " has no child named " + ID);
}
Module* Module::FindDescendent(const std::string& ID) {
  for (Module *c : children) {
    if (c->name == ID)
      return c;
    auto* d = c->FindDescendent(ID);
    if (d != NULL) return d;
  }
  for (Module *c : name_children) {
    if (c->name == ID)
      return c;
    auto* d = c->FindDescendent(ID);
    if (d != NULL) return d;
  }
  return NULL;
}
void Module::ClearChildren() {
  children.clear();
  name_children.clear();
}
std::vector<Module*> Module::BuildAll() {
  std::vector<Module*> ret;
  RefreshChildren();
  for (Module *c : children) {
    c->path = path+name+"/";
    for (Module *m : c->BuildAll()) {
      ret.push_back(m);
    }
  }
  for (Module *c : name_children) {
    c->path = path+name+"/";
    for (Module *m : c->BuildAll()) {
      ret.push_back(m);
    }
  }
  ret.push_back(this);
  return ret;
}
std::vector<Module*> Module::Build() {
  std::vector<Module*> ret;
  RefreshChildren();
  for (Module *c : children) {
    c->path = path+name+"/";
    for (Module *m : c->Build()) {
      ret.push_back(m);
    }
  }
  //for (Module *c : name_children) {
  //c->path = path+name+"/";
  // Continue to build name children: even if a module A is a name-child of
  // module B, module B may have true children [but probably shouldn't, for
  // performance purposes].
  //for (Module *m : c->Build()) {
  //std::cout << "Warning: module " << c->name << " is a name child of " << name << " but has child " << m->name << std::endl;
  //ret.push_back(m);
  //}
  //}
  ret.push_back(this);
  return ret;
}

int Module::getSiblingThread(int thread) {
  if (constant_mem_mode)
    return thread;
  if (thread < 0) {
    // Reserve -20 as invalid, pending sibling
    assert(thread == -1);
    return -20;
  }
  assert(threadHasParent(thread));
  return getChildThread(getThreadParent(thread));
}

int Module::getChildThread(int parent) {
  if (constant_mem_mode)
    return -1;
  if (parent < 0) {
    // Reserve -10 as invalid, pending child
    assert(parent == -1);
    return -10;
  }
  int raw_id = next_thread++;
  auto loc_id = thread_next_child[parent]++;
  assert(!thread_next_child.count(raw_id));
  assert(!thread_parent_records.count(raw_id));
  thread_next_child[raw_id] = 0;
  thread_parent_records[raw_id] = std::make_pair(parent, loc_id);
  auto name = fmt::format("{}.{}", getThreadName(parent), loc_id);
  thread_names[raw_id] = name;
  // fmt::print("Create thread id {} ({}) parent {} ({})\n", name, raw_id, getThreadName(parent), parent);
  if (enable_binlog)
    BINLOG_INFO_W(static_writer, "Create thread id {} ({}) parent {} ({})\n", name, raw_id, getThreadName(parent), parent);
  return raw_id;
}

std::string Module::getThreadName(int thread) {
  if (thread == 0)
    return "root";
  if (thread == -1)
    return "inv";
  if (thread == -10)
    return "inv.pend";
  if (thread == -20)
    return "inv.pend_sibling";
  if (thread_names.count(thread))
    return thread_names[thread];
  return "UNKTHREAD";
}

int Module::getAncestorThread(int thread, int n) {
  for (int i=0; i<n; i++)
    thread = getThreadParent(thread);
  return thread;
}

bool Module::threadIsAncestor(int parent, int child) {
  // Child threads must be spawned strictly after their parents. Therefore,
  // they must have higher thread IDs. This should avoid searching back for
  // parents of root threads.
  if (child < parent)
    return false;
  return child == parent || threadIsAncestor(parent, getThreadParent(child));
}

bool Module::threadBroadcastCompatible(int A, int B) {
  if (A < 0 || B < 0)
    return true;
  return (A == B) 
    || threadIsAncestor(A, B)
    || threadIsAncestor(B, A);
}

int Module::unionThreadIDs(int A, int B, bool rename) {
  if (A == -10) {
    return rename ? getChildThread(B) : B;
  } else if (B == -10) {
    return rename ? getChildThread(A) : A;
  }
  if (A == -20) {
    return rename ? getSiblingThread(B) : B;
  } else if (B == -20) {
    return rename ? getSiblingThread(A) : A;
  }
  /* if (A == -10 || B == -10) {
    assert(false && "static union cannot allocate thread");
  } */
  if (A != B 
      && threadHasParent(A)
      && threadHasParent(B)
      && getThreadParent(A) == getThreadParent(B)) {
    int ret = std::min(A, B);
    if (ret == A && enable_binlog)
      BINLOG_INFO_W(static_writer, "WARNING: Merge thread {} into {}\n", getThreadName(B), getThreadName(A));
      // fmt::print("WARNING: Merge thread {} into {}\n", getThreadName(B), getThreadName(A));
    else if (ret == B && enable_binlog)
      BINLOG_INFO_W(static_writer, "WARNING: Merge thread {} into {}\n", getThreadName(A), getThreadName(B));
      // fmt::print("Merge thread {} into {}\n", getThreadName(A), getThreadName(B));
    return ret;
  } else {
    assert(threadBroadcastCompatible(A, B));
    return std::max(A, B);
  } 
}

int Module::unionThreadIDs(const std::set<int>& threads) {
  if (constant_mem_mode)
    return 0;
  assert(threads.size());
  assert(threadBroadcastCompatible(threads));
  int max{-1};
  for (auto t : threads)
    if (t > max)
      max = t;
  return max;
}

bool Module::threadBroadcastCompatible(const std::set<int>& threads) {
  if (constant_mem_mode)
    return true;
  for (const auto a : threads) {
    for (const auto b : threads) {
      if (!threadBroadcastCompatible(a, b))
        return false;
    }
  }
  return true;
}

bool Module::threadHasParent(int thread) {
  if (constant_mem_mode)
    return true;
  return thread_parent_records.count(thread);
}

int Module::getThreadParent(int thread) {
  if (thread < 0 || constant_mem_mode)
    return thread;
  return thread_parent_records.at(thread).first;
}

void Module::Eval() {}
void Module::Log() {}
void Module::Log(std::ostream& log) { enLog = false; } // turn off logging unless overwritten
void Module::Clock() {}
void Module::RefreshChildren() {}
void Module::Expect(unsigned long int count) {
  remaining += count;
  local_remaining += count;
}
void Module::ResetExpect() {
  remaining -= local_remaining;
  local_remaining = 0;
}
void Module::Complete(unsigned long int count, bool force) {
  if (local_remaining < count && !force) {
    // assert(false);
    throw module_error(this, "completed more than expected");
  }
  remaining -= count;
  local_remaining -= count;
}
bool Module::Finished() {
  return !remaining;
}
void Module::SetInstrumentation() {
  instrumentation = true;
}
void Module::Active() {
  active = true;
  any_active = true;
}
void Module::Count() { // Called by repl every cycle
  if (instrumentation) {
    if (active) {
      active_cnt +=1;
      continue_inactive_cnt = 0;
    } else {
      inactive_cnt += 1;
      continue_inactive_cnt += 1;
    }
    active = false;
  }
}
void Module::DumpState(std::ostream& log) { // called by repl and override by submodules
  log << "\"active\":" << active_cnt;
  log << ",\"inactive\":" << inactive_cnt;
  log << ",\"cont_inactive\":" << continue_inactive_cnt;
}
