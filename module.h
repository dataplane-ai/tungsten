#ifndef MODULE_H_
#define MODULE_H_

#include <cassert>
#include <deque>
#include <string>
#include <stdexcept>
#include <regex>
#include <vector>
#include <iostream>
#include <fstream>
#include <atomic>
#include <set>

#include "session.h"

#include "binlog/Session.hpp"
#include "binlog/SessionWriter.hpp"

#include "binlog/binlog.hpp"

#include "fmt/format.h"

//#include "VirtNet.h"

//class NetworkLinkManager;
//void NetworkLinkManager::AddNode(std::string id, int type);
void AddNode(const std::string& id, int type);

using std::string_literals::operator""s;

class Module {
  int type {-1};
  static bool enable_binlog;
  // When true, do not perform anything that could increase runtime memory
  static bool constant_mem_mode;

  using LineTraceType = std::tuple<int,int,int,int,int,int,int>;
  LineTraceType trace_max;
  std::vector<LineTraceType> traces;

  // binlog::Session dummySession;
 public:
  bool trace_enabled{false};
  static void SetMemoryConserve() {
    constant_mem_mode = true;
  }
  void EnableLineTrace(int max_in, int max_out, int max_read, int max_write, int max_buffer, int max_vector, int max_special=0) {
    assert(!trace_enabled);
    trace_enabled = true;
    trace_max = std::make_tuple(max_in, max_out, max_read, max_write, max_buffer, max_vector, max_special);
  }
  void LineTrace(int in, int out, int read, int write, int buffer, int vector, int special=0) {
    if (!constant_mem_mode)
      traces.emplace_back(in, out, read, write, buffer, vector, special);
  }
  void DumpLineTrace(const std::string& dir) {
    auto dump_to = path + name;
    std::replace(dump_to.begin(), dump_to.end(), '/', '_');
    auto out = std::ofstream(dir + "/" + dump_to);
    out << "starve,stall,read,write,buffer,vector,special\n";
    auto dumpLine = [&](const LineTraceType& t) {
      out << fmt::format("{},{},{},{},{},{},{}\n",
          std::get<0>(t), 
          std::get<1>(t), 
          std::get<2>(t), 
          std::get<3>(t), 
          std::get<4>(t),
          std::get<5>(t),
          std::get<6>(t));
    };
    dumpLine(trace_max);
    for (const auto& t : traces)
      dumpLine(t);
  }

  // This is the ``nested module'' constructor
  explicit Module(std::initializer_list<Module*> _children,
      const std::string& _name);

  // This is the leaf module constructor
  explicit Module(const std::string& _name);
  virtual ~Module() {};

  // void SetSession(binlog::Session& session);
  // binlog::SessionWriter writer;
  static binlog::SessionWriter static_writer;

  // Modules are non-assignable, non-copyable, and non-moveable. When building
  // modules, use ``emplace_back'' to construct in place. This is necessary to
  // ensure that references taken to child modules are maintained.
  Module(Module&& m)                 =delete;
  Module(const Module& m)            =delete;
  Module& operator=(Module&& m)      =delete;
  Module& operator=(const Module& m) =delete;

  struct module_error : public std::exception {
    std::string exp;
    module_error(const Module *m, const std::string& _what) {
      exp = m->path + m->name + ": " + _what;
    }
    const char *what() const noexcept {
      return exp.c_str();
    }
  };
  void AddChild(Module *c, bool clock=true);

  void AddChildren(std::initializer_list<Module*> _children, bool clock=true);

  void SetType(int _type);
  const int pmu_type {2};
  const int pmu_type_dense {5};
  const int pcu_type {4};
  const int dram_ag_type {3};
  const int onchip_ag_type {6};
  const int dram_type {1};
  const int host_type {0};
  int vec_stall   {0};
  int vec_starve  {0};
  int scal_stall  {0};
  int scal_starve {0};
  int run         {0};
  std::string name;
  std::string path;
  virtual void LogOn() {}
  virtual void LogOff() {}
  bool enLog = false;
  bool enable = true;
  std::ostream* log = &std::cout;
#ifdef MODULE_FILE_LOGS
  std::ofstream flog;
#endif
  virtual void Eval();
  virtual void Log();
  // Add post-static-initialization for modules.
  virtual void Finalize();
  virtual void Log(std::ostream& log); // turn off logging unless overwritten
  virtual void Clock();
  virtual void RefreshChildren();
  virtual void SetParameter(const std::string& key, const std::string& val);
  virtual void AppendParameter(const std::string& key, const std::string& val);
  virtual void Apply(const std::string& cmd, const std::vector<std::string>& vals);
  virtual std::string GetParameter(const std::string& key);
  std::vector<Module*> FindChildren(const std::deque<std::string>& path);
  Module* FindChild(const std::string& ID);
  Module* FindDescendent(const std::string& ID);
  void ClearChildren();
  std::vector<Module*> BuildAll();
  std::vector<Module*> Build();

  static void tickCycle() {
    cycle++;
  }
  static int invThread() { return -1; }

  static int getSiblingThread(int parent); 
  static int getChildThread(int parent); 

  static std::string getThreadName(int thread);
  static int getAncestorThread(int thread, int n);
  static bool threadIsAncestor(int parent, int child);
  static bool threadBroadcastCompatible(int A, int B);
  static int  unionThreadIDs(int A, int B, bool rename=false);
  static int  unionThreadIDs(const std::set<int>& threads);
  static bool threadBroadcastCompatible(const std::set<int>& threads);
  template<class T>
  void logValueAtThread(int threadID, T val, const std::string& loc, const std::string& op) {
    if (enable_binlog)
      BINLOG_INFO_W(static_writer, "LOG {} @ {} cyc={} val={} loc={} op={}\n", 
    // fmt::print("LOG {} @ {} cyc={} val={} loc={}\n", 
        getThreadName(threadID),
        path+name,
        cycle,
        val,
        loc, 
        op);
  }

 private:
  static std::atomic<unsigned long int> remaining;
  unsigned long int local_remaining {0};
 protected:
  void Expect(unsigned long int count);
  void ResetExpect();
  void Complete(unsigned long int count, bool force=false);
 public:
  std::vector<Module*> children;
  std::vector<Module*> name_children;
  // This tracks the amount of work remaining to be done.
  bool Finished();

 
 private:
  bool active = false;
  static bool any_active;
 public:
  static void SetBinlog(bool v) {
    enable_binlog = v;
  }
  static bool ShouldBinlog() {
    return enable_binlog;
  }
  bool instrumentation = false;
  unsigned long int active_cnt {0};
  unsigned long int inactive_cnt {0};
  unsigned long int continue_inactive_cnt {0};
  static unsigned long int cycle;

  static bool threadHasParent(int thread);
  static int getThreadParent(int thread);
  // int getThreadLocIndex(int thread);

  // The next raw thread ID
  static int next_thread;
  // A mapping of thread ID and the next local index of a child thread
  static std::map<int,int> thread_next_child;
  // A mapping of thread IDs to <parentID,locIdx>
  static std::map<int,std::pair<int,int>> thread_parent_records;
  static std::map<int,std::string> thread_names;

  static void ResetAnyActive() {
    any_active = false;
  }
  static bool AnyActive() {
    return any_active;
  }

  void SetInstrumentation();
  void Active();
  void Count(); // Called by repl every cycle
  virtual void DumpState(std::ostream& log); // called by repl and override by submodules

};

#endif  // MODULE_H_
