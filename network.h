#ifndef NETWORK_H_
#define NETWORK_H_

#include <optional>
#include <string>
#include <istream>
#include <vector>
#include <regex>
#include <stdexcept>
#include <cassert>
#include <list>

#include "interface.h"
#include "module.h"
#include "state.h"
#include "token.h"
#include "router.h"
#include "statistics.h"

//#define DEBUG_NETWORK

class NetworkInput : public Module,
                     public virtual CheckedSend<Token> {
  NetworkInterface<Token, int, int> *dynnet;
  NetworkInterface<Token, int, int> *statnet;
  NetworkInterface<Token, int, int> *idealnet;
  NetworkInterface<Token, int, int> *net {NULL};
  CounterStatistic sent;
  int addr {-1};
  int flow {-1};
  int vc   {0};
 public:
  explicit NetworkInput(const std::string& _name,
                        NetworkInterface<Token, int, int> *_dynnet,
                        NetworkInterface<Token, int, int> *_statnet,
                        NetworkInterface<Token, int, int> *_idealnet
                        );
  mutable bool this_cyc_starve {false};
  explicit NetworkInput(const std::string& _name,
                        NetworkInterface<Token, int, int> *_dynnet,
                        NetworkInterface<Token, int, int> *_statnet
                        );
  void SetParameter(const std::string& key, const std::string& val);
  void RefreshChildren();
  void Eval() override;
  void Clock() override;
  void Log(std::ostream& log) override;
  void DumpState(std::ostream& log) override;
  bool Ready() const;
  void Push(shared_ptr<Token> v);
  void Push(const Token& v);
};

template<class T>
class Bridge : public Module {
  CheckedReceive<T> *in;
  CheckedSend<T> *out;
 public:
  explicit Bridge(const std::string& _name,
                  CheckedReceive<T> *_in,
                  CheckedSend<T> *_out) :
    Module(_name), in(_in), out(_out) {
  }
  void Eval() {
    if (in->Valid() && out->Ready()) {
      //std::cout << name << " bridge go" << std::endl;
      out->Push(in->Read());
      in->Pop();
    }
    //if (!in->Valid() && out->Ready()) {
     // std::cout << "\t" << name << " not valid" << std::endl;
    //}
    //if (!out->Ready() && in->Valid()) {
      //std::cout << "\t" << name << " not ready" << std::endl;
    //}
    //if (!out->Ready() && !in->Valid()) {
      //std::cout << "\t" << name << " neither valid nor ready" << std::endl;
    //}
  }
};

class NetworkOutput : public Module,
                     public virtual CheckedReceive<Token> {
  NetworkInterface<Token, int, int> *dynnet;
  NetworkInterface<Token, int, int> *statnet;
  NetworkInterface<Token, int, int> *idealnet;
  NetworkInterface<Token, int, int> *net {0}; 
  mutable CounterStatistic received;
  int addr {-1};
  int flow {-1};
  int vc   {-1};
  mutable std::optional<Token> dat {std::nullopt};
 public:
  explicit NetworkOutput(const std::string& _name,
                        NetworkInterface<Token, int, int> *_dynnet,
                        NetworkInterface<Token, int, int> *_statnet,
                        NetworkInterface<Token, int, int> *_idealnet
                        );
  explicit NetworkOutput(const std::string& _name,
                        NetworkInterface<Token, int, int> *_dynnet,
                        NetworkInterface<Token, int, int> *_statnet);

  void SetParameter(const std::string& key, const std::string& val);
  void RefreshChildren();
  void Eval() override;
  void Clock() override;
  void Log(std::ostream& log) override;
  void DumpState(std::ostream& log) override;
  bool Valid() const;
  void Pop();
  const Token& Read() const;

};

template<int D, int V, int L>
class DynamicNetwork : public Module,
                       public virtual NetworkInterface<Token, int, int> {
  std::vector<int>                    dims;
  int                                 max_addr;

  // Channels within the network and routers
  std::vector<ValPipeline<Packet, L>*> channels;
  std::vector<ValPipeline<Credit, L>*> channels_cr;
  std::vector<Router<D, V>*>           routers;

  // Injection ports (one per router)
  std::vector<ValPipeline<Packet, L>*> inj_ports;
  std::vector<ValPipeline<Credit, L>*> inj_ports_cr;

  // Ejection ports
  std::vector<ValPipeline<Packet, L>*> ej_ports;
  std::vector<ValPipeline<Credit, L>*> ej_ports_cr;

  // Ejection and injection buffers (one per VC)
  std::vector<FIFO<Packet, D>*>        ej_buf;
  std::vector<FIFO<Packet, D>*>        inj_buf;

  // Credits available per injection port
  std::vector<int>                    inj_credits;
  // Next VC for injection, for arbitration
  std::vector<int>                    inj_next_vc;

  // Support for returning multiple ejection port credits in a single cycle
  std::vector<Credit>                 ej_credits_pending;

  // The following functions used to linearize nodes and channels are adapted
  // from the booksim2 network simulator (github.com/booksim/booksim2).
  int _LeftNode(int node, int dim);
  int _RightNode(int node, int dim);
  int _LeftChannel(int node, int dim);
  int _RightChannel(int node, int dim);

  bool is_pruned {false};
  vector<int> pruned_channels;
  vector<int> pruned_ej_ports;
  mutable vector<int> pruned_inj_ports;
  vector<int> pruned_routers;
  // Track the list of injection ports that we've added, so that we don't 
  // multiply clock them. Adding this vector makes the check O(1), which is 
  // better for dynamic network usage. Mutable to allow it to be updated when
  // Ready() is called
  mutable vector<bool> inj_port_added_to_list;

  void EvalEj(int i) {
    if (ej_ports[i]->Valid()) {
      int v = ej_ports[i]->Read().vc;
      assert(v < V && v >= 0);
      ej_buf[i*V+v]->Push(ej_ports[i]->Read());
    }
    if (ej_credits_pending[i]) {
      ej_ports_cr[i]->Push(ej_credits_pending[i]);
      ej_credits_pending[i] = Credit();
      //std::cout << "Ejection port credit return at " << i << endl;
    }
  }

  void EvalInj(int i) {
    if (inj_ports_cr[i]->Valid()) {
      for (int v: inj_ports_cr[i]->Read().vcs) {
        inj_credits[i*V+v]++;
        //std::cout << "Injection port credit return at " << i 
                  //<< " vc:" << v << endl;
      }
    }
    for (int j=0; j < V; j++) {
      int v = (inj_next_vc[i]+j)%V;
      // Buffer for the injection port
      int b = i*V+v;
      if (inj_credits[b] && inj_buf[b]->Valid()) {
        inj_credits[b]--;

        // Remove the token from the (src, vc) buffer and push it to the port
        inj_ports[i]->Push(inj_buf[b]->Read());
        inj_buf[b]->Pop();

        // Give the priority to the next VC in sequence.
        inj_next_vc[i]++;
        inj_next_vc[i] %= V;
        break;
      }
    }
  }

  void Prune() {
    is_pruned = true;
    cout << "Pruning!" << endl;
    inj_port_added_to_list.resize(inj_ports.size());
    for (int i=0; i<routers.size(); i++) {
      Router<D,V> *r = routers[i];
      vector<int> out_used = r->OutputsUsed();
      if (out_used.size()) {
        cout << "Router " << i << endl;
        pruned_routers.push_back(i);
      }
      for (int out : out_used) {
        cout << "\tOutput: " << out;
        if (out == 2*dims.size()) {
          cout << " ej port " << i;
          pruned_ej_ports.push_back(i);
        } else {
          int d = out/2;
          int left_node   = _LeftNode(i,      d);
          int right_node  = _RightNode(i,     d);
          int output;
          if (out&1) {
            // left = true;
            output  = _LeftChannel(i,  d);
          } else {
            // left = false;
            output = _RightChannel(i, d);
          }
          cout << " chan " << output;
          if (find(pruned_channels.begin(), 
                   pruned_channels.end(), output) == pruned_channels.end())
            pruned_channels.push_back(output);
        }
        cout << endl;
      }
    }
  }

 public:
  explicit DynamicNetwork(std::vector<int> _dims,
      const std::string& _name);

  void SetParameter(const std::string& key, const std::string& val) {
    if (key == "prune") {
      Prune();
    } else {
      throw std::runtime_error("Unknown key "+key+" at "+name);
    }
  }
  void RefreshChildren();
  // Network Interface
  bool Ready(int src, int vc, int flow) const {
    if (is_pruned && !inj_port_added_to_list[src]) {
      inj_port_added_to_list[src] = true;
      pruned_inj_ports.push_back(src);
    }
    return inj_buf[src*V+vc]->Ready();
  }
  int GetCredit(int src, int vc, int flow) const {
    return inj_credits.at(src*V+vc);
  }
  void Push(shared_ptr<Token> v, int src, int vc, int flow) {
    //std::cout << "Push to inj_buf " << src << " vc" << vc << endl;
    if (is_pruned) { 
      assert(inj_port_added_to_list[src]);
    }
    inj_buf[src*V+vc]->Push(Packet(*v, flow, vc));
  }
  void Push(const Token& v, int src, int vc, int flow) {
    //std::cout << "Push to inj_buf " << src << " vc" << vc << endl;
    if (is_pruned) { 
      assert(inj_port_added_to_list[src]);
    }
    inj_buf[src*V+vc]->Push(Packet(v, flow, vc));
  }
  bool Valid(int dst, int vc) const {
    int p = dst*V+vc;
    return ej_buf[p]->Valid();
  }
  Token Read(int dst, int vc) {
    int p = dst*V+vc;
    Token tmp = ej_buf[p]->Read().data;
    ej_buf[p]->Pop();
    ej_credits_pending[dst].Add(vc);
    return tmp;
  }

  void Clock() {
    //cout << "Clock network modules" << endl;
    if (is_pruned) {
      //cout << "Pruned clock of " << pruned_channels.size()*2 << " modules" << endl;
      //cout << "\tUnpruned " << channels.size()*2 <<endl;
      for (int i : pruned_channels) {
        channels[i]->Clock();
        channels_cr[i]->Clock();
      }
      for (int i : pruned_ej_ports) {
        ej_ports[i]->Clock();
        ej_ports_cr[i]->Clock();
        for (int v = 0; v<V; v++) {
          ej_buf[i*V+v]->Clock();
        }
      }
      for (int i : pruned_inj_ports) {
        inj_ports[i]->Clock();
        inj_ports_cr[i]->Clock();
        for (int v = 0; v<V; v++) {
          inj_buf[i*V+v]->Clock();
        }
      }
      for (int i : pruned_routers) {
        routers[i]->Clock();
      }
    } else {
      for (auto& m : channels)
        m->Clock();
      for (auto& m : channels_cr)
        m->Clock();
      for (auto& m : ej_ports)
        m->Clock();
      for (auto& m : ej_ports_cr)
        m->Clock();
      for (auto& m : ej_buf)
        m->Clock();
      for (auto& m : inj_ports)
        m->Clock();
      for (auto& m : inj_ports_cr)
        m->Clock();
      for (auto& m : inj_buf)
        m->Clock();
      for (auto& m : routers)
        m->Clock();
    }
  }

  // Module interface
  void Eval() {
    //TODO: DEBUG===========
    if (is_pruned) {
      for (int i : pruned_channels) {
        channels[i]->Eval();
        channels_cr[i]->Eval();
      }
      for (int i : pruned_ej_ports) {
        ej_ports[i]->Eval();
        ej_ports_cr[i]->Eval();
        for (int v = 0; v<V; v++) {
          ej_buf[i*V+v]->Eval();
        }
        EvalEj(i);
      }
      for (int i : pruned_inj_ports) {
        inj_ports[i]->Eval();
        inj_ports_cr[i]->Eval();
        for (int v = 0; v<V; v++) {
          inj_buf[i*V+v]->Eval();
        }
        EvalInj(i);
      }
      for (int i : pruned_routers) {
        routers[i]->Eval();
      }
    } else {
      vector<bool> readys;
      for (auto& m : channels)
        m->Eval();
      for (auto& m : channels_cr)
        m->Eval();
      for (auto& m : ej_ports)
        m->Eval();
      for (auto& m : ej_ports_cr)
        m->Eval();
      for (auto& m : ej_buf)
        m->Eval();
      for (auto& m : inj_ports)
        m->Eval();
      for (auto& m : inj_ports_cr)
        m->Eval();
      for (auto& m : inj_buf)
        m->Eval();
      for (auto& m : routers)
        m->Eval();

      for (int i=0; i < ej_ports.size(); i++)
        EvalEj(i);
      for (auto& in: inj_buf)
        readys.push_back(in->Ready());
      for (int i=0; i < inj_ports.size(); i++)
        EvalInj(i);
      //TODO: DEBUG===========
      for (int i = 0; i < inj_buf.size(); i++) {
        assert(inj_buf[i]->Ready() == readys[i]);
      }
    }
  }

  void LogSrc(std::ostream& log, int src, int flow) {
    inj_ports[src]->Log(log);
  }
  void LogDst(std::ostream& log, int dst, int vc) {
    int p = dst*V+vc;
    if (ej_buf[p]->Valid()) {
      //Token tmp = ej_buf[p].Read().data;
      log << ej_buf[p]->Read();
    }
  }
};
extern template class DynamicNetwork<4, 8, 1>;

template<int D, int V, int L>
DynamicNetwork<D, V, L>::DynamicNetwork(std::vector<int> _dims,
    const std::string& _name) :
  Module(_name), dims(_dims), max_addr(1) {
  if (!dims.size())
    throw std::runtime_error("Invalid network dimension");

  if (dims.size() != 2)
    throw std::logic_error("Non-flat network not yet supported");

  for (int d : dims) {
    max_addr *= d;
  }

  // Add just enough network channels to connect all the routers between
  // dimensions.
  int n_ch = dims.size()*2*max_addr;

  for (int i=0; i < n_ch; i++) {
    channels.emplace_back(
        new ValPipeline<Packet,L>("ch" + std::to_string(i)));
    AddChild(channels.back(), false);
    channels_cr.emplace_back(
        new ValPipeline<Credit,L>("ch_c" + std::to_string(i)));
    AddChild(channels_cr.back(), false);
  }
  assert(inj_credits.size()==0);

  ej_credits_pending.resize(max_addr);

  for (int i=0; i < max_addr; i++) {
    inj_ports.emplace_back(
        new ValPipeline<Packet,L>("inj" + std::to_string(i)));
    AddChild(inj_ports.back(), false);

    ej_ports.emplace_back(
        new ValPipeline<Packet,L>("ej" + std::to_string(i)));
    AddChild(ej_ports.back(), false);
    ej_ports_cr.emplace_back(
        new ValPipeline<Credit,L>("ej_c" + std::to_string(i)));
    AddChild(ej_ports_cr.back(), false);
    
    for (int j=0; j < V; j++) {
      ej_buf.emplace_back(
          new FIFO<Packet,D>("ej_buf" + std::to_string(i)+"v"+std::to_string(j)));
      AddChild(ej_buf.back(), false);

      inj_buf.emplace_back(
          new FIFO<Packet,D>("inj_buf" + std::to_string(i) 
                         + "v" + std::to_string(j)));
      AddChild(inj_buf.back(), false);
      inj_credits.push_back(D);
    }

    inj_ports_cr.emplace_back(
        new ValPipeline<Credit,L>("inj_c" + std::to_string(i)));
    AddChild(inj_ports_cr.back(), false);

    inj_next_vc.push_back(0);
  }

  for (int a=0; a < max_addr; a++) {
    routers.emplace_back(
        new Router<D,V>(a, "router"s+std::to_string(a)));
    // This code is adapted from booksim2 (github.com/booksim/booksim2)
    for (size_t d=0; d < dims.size(); d++) {
      int left_node   = _LeftNode(a,             d);
      int right_node  = _RightNode(a,            d);
      int right_input = _LeftChannel(right_node, d);
      int left_input  = _RightChannel(left_node, d);

      int right_output = _RightChannel(a, d);
      int left_output  = _LeftChannel(a,  d);
      routers.back()->AddOutput(channels[right_output],
          channels_cr[right_output]);
      routers.back()->AddOutput(channels[left_output],
          channels_cr[left_output]);
      routers.back()->AddInput(channels[right_input],
          channels_cr[right_input]);
      routers.back()->AddInput(channels[left_input],
          channels_cr[left_input]);
    }
    routers.back()->AddOutput(ej_ports[a], ej_ports_cr[a]);
    routers.back()->AddInput(inj_ports[a], inj_ports_cr[a]);
  }
  //std::cout << "Build network with " << children.size() << " children"<<std::endl;
}

template<int D, int V, int L>
void DynamicNetwork<D, V, L>::RefreshChildren() {
  children.clear();
  name_children.clear();
  for (auto& m : channels)
    AddChild(m, false);
  for (auto& m : channels_cr)
    AddChild(m, false);
  for (auto& m : inj_ports)
    AddChild(m, false);
  for (auto& m : ej_ports)
    AddChild(m, false);
  for (auto& m : inj_ports_cr)
    AddChild(m, false);
  for (auto& m : ej_ports_cr)
    AddChild(m, false);
  for (auto& m : ej_buf)
    AddChild(m, false);
  for (auto& m : inj_buf)
    AddChild(m, false);
  for (auto& m : routers)
    AddChild(m, false);
}

// The following code is adapted from booksim2 (github.com/booksim/booksim2)
template<int D, int V, int L>
int DynamicNetwork<D, V, L>::_LeftChannel(int node, int dim) {
  // The base channel for a node is 2*_n*node
  int base = 2*dims.size()*node;
  // The offset for a left channel is 2*dim + 1
  int off  = 2*dim + 1;

  return ( base + off );
}

// The following code is adapted from booksim2 (github.com/booksim/booksim2)
template<int D, int V, int L>
int DynamicNetwork<D, V, L>::_RightChannel(int node, int dim) {
  // The base channel for a node is 2*_n*node
  int base = 2*dims.size()*node;
  // The offset for a right channel is 2*dim
  int off  = 2*dim;
  return ( base + off );
}

// The following code is adapted from booksim2 (github.com/booksim/booksim2)
template<int D, int V, int L>
int DynamicNetwork<D, V, L>::_LeftNode(int node, int dim) {
  int k_to_dim = 1;
  for (int i=0; i < dim; i++)
    k_to_dim *= dims[i];
  int _k = dims[dim];
  int loc_in_dim = (node / k_to_dim) % _k;
  int left_node;
  // if at the left edge of the dimension, wraparound
  if ( loc_in_dim == 0 ) {
    left_node = node + (_k-1)*k_to_dim;
  } else {
    left_node = node - k_to_dim;
  }

  return left_node;
}

// The following code is adapted from booksim2 (github.com/booksim/booksim2)
template<int D, int V, int L>
int DynamicNetwork<D, V, L>::_RightNode(int node, int dim) {
  int k_to_dim = 1;
  for (int i=0; i < dim; i++)
    k_to_dim *= dims[i];
  int _k = dims[dim];
  int loc_in_dim = (node / k_to_dim) % _k;
  int right_node;
  // if at the right edge of the dimension, wraparound
  if (loc_in_dim == (_k-1)) {
    right_node = node - (_k-1)*k_to_dim;
  } else {
    right_node = node + k_to_dim;
  }

  return right_node;
}

#endif  // NETWORK_H_
