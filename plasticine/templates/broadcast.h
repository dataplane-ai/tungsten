#ifndef BRODCAST_H_
#define BRODCAST_H_

#include "interface.h"
#include "module.h"
#include <array>

using namespace std;

template<class T>
class Broadcast : public Module{
  int active{0};
 public:

  CheckedReceive<T>* from{NULL};
  vector<CheckedSend<T>*> tos;

  explicit Broadcast(const std::string& _name) :
    Module(_name) {
  }

  explicit Broadcast(
    const std::string& _name,
    CheckedReceive<T>* _from,
    initializer_list<CheckedSend<T>*> _tos
  ) :
    Module(_name) {
      from = _from;
      tos = _tos;
  }

  void SetIn(CheckedReceive<T>* _from) {
    from = _from;
  }

  void AddTo(std::initializer_list<CheckedSend<T>*> _tos) {
    for (auto t : _tos)
      tos.push_back(t);
  }

  void AddTo(CheckedSend<T>* to) {
    tos.push_back(to);
  }

  void Eval() {
    if (from->Valid()) {
      bool toAllReady = true;
      for (auto to : tos)
        toAllReady &= to->Ready();
      if (toAllReady) {
        auto data = from->Read();
        for (auto to : tos)
          to->Push(data);
        from->Pop();
        active++;
      }
    }
  }

  void Apply(const string& cmd, const vector<string>& data) {
    if (cmd == "get_active") {
      cout << path << name << " active cycles: " << active << endl;
    } else {
      throw module_error(this, "unknown command");
    }
  }
};
extern template class Broadcast<Token>;

#endif // BRODCAST_H_
