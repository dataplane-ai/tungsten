#ifndef CPPGENUTIL_H 
#define CPPGENUTIL_H

#include <cassert>
#include <cstring>
#include <string>
#include <cstdlib>
#include <iostream>
#include <tuple>
#include <cmath>
#include <vector>
#include <optional>
#include <algorithm>
#include "token.h"
#include <unordered_map>
#include <fstream>
#include <stdexcept>
#include <typeinfo>
#include <map>

using namespace std;

int roundUpDiv(int a, int b);

std::ostream& operator<<(std::ostream& stream, std::tuple<uint64_t, bool, bool>& reqst);

template<typename T>
std::ostream& operator<<(std::ostream& stream, std::optional<T>& opt) {
  if (opt.has_value())
    stream << opt.value();
  else
    stream << "null";
  return stream;
}

template <typename T, int D>
std::ostream& operator<<(std::ostream& stream, std::array<T,D>& arr) {
  stream << "[";
  for (auto& d : arr)
    stream << d << " ";
  stream << "]";
  return stream;
}
std::ostream& operator<<(std::ostream& stream, std::array<uint64_t,16>& arr);
std::ostream& operator<<(std::ostream& stream, std::array<uint64_t,1>& arr);

string string_plus(const string &str1, const string &str2);

void ASSERT(bool test, const char* msg);
void ASSERT(bool test, const string msg);

/*
 * Return first found index of value in list. If value is not in list, return -1.
 * */
template<typename T, int N>
int find(T (&list)[N], T value) {
  for (int i = 0; i < N; i ++) {
    if (list[i] == value) return i;
  }
  return -1;
}

template<typename A, int F, int T>
void shuffleData(int (&from)[F], int (&to)[T], A (&base)[F], A (&shuffled)[T]) {
  for (int i = 0; i < T; i++) {
    int idx = find<int, F>(from, to[i]);
    shuffled[i] = (idx == -1) ? 0 : base[idx];
  }
}

template<int F, int T>
void shuffleAddr(int (&from)[F], int (&to)[T], int (&base)[F], int (&shuffled)[T]) {
  for (int i = 0; i < T; i++) {
    int idx = find<int, F>(from, to[i]);
    shuffled[i] = (idx == -1) ? -1 : base[idx];
  }
}

template<typename T>
T toT(const Token& token, int i) {
  switch(token.type) {
    case TT_FLOATVEC:
      return (T) token.floatVec_[i];
    case TT_FLOAT:
      return (T) token.float_;
    case TT_INTVEC:
      return (T) token.intVec_[i];
    case TT_INT8VEC:
      return (T) token.int8Vec_[i];
    case TT_INT16VEC:
      return (T) token.int16Vec_[i];
    case TT_INT:
      return (T) token.int_;
    case TT_LONGVEC:
      return (T) token.longVec_[i];
    case TT_LONG:
      return (T) token.long_;
    case TT_UINT:
      return (T) token.uint_;
    case TT_UINTVEC:
      return (T) token.uintVec_[i];
    case TT_UINT64:
      return (T) token.uint64_;
    case TT_UINT64VEC:
      return (T) token.uint64Vec_[i];
    case TT_BOOL:
      return (T) token.bool_;
    case TT_BOOLVEC:
      return (T) token.boolVec_[i];
    default:
      throw runtime_error("Unspported type " + to_string(token.type));
  }
}

void cpAt(Token& token, const Token& from, int i);

template<int N>
void set_token_en(Token& token, bool en[N]) {
  int nvalid = 0;
  int j = 0;
  for (int i = 0; i < N; i++) {
    bool e = en[i];
    nvalid += e ? 1 : 0;
    if (e) {
      if (i!=j) {
        switch(token.type) {
          case TT_FLOATVEC:
            token.floatVec_[j] = token.floatVec_[i];
            break;
          case TT_FLOAT:
            ASSERT(false, "Coalesce scalar i:" + to_string(i) + " j:" + to_string(j));
            break;
          case TT_INTVEC:
            token.intVec_[j] = token.intVec_[i];
            break;
          case TT_UINTVEC:
            token.uintVec_[j] = token.uintVec_[i];
            break;
          case TT_INT8VEC:
            token.int8Vec_[j] = token.int8Vec_[i];
            break;
          case TT_INT16VEC:
            token.int16Vec_[j] = token.int16Vec_[i];
            break;
          case TT_INT:
            ASSERT(false, "Coalesce scalar i:" + to_string(i) + " j:" + to_string(j));
            break;
          case TT_UINT:
            ASSERT(false, "Coalesce scalar i:" + to_string(i) + " j:" + to_string(j));
            break;
          case TT_LONGVEC:
            token.longVec_[j] = token.longVec_[i];
            break;
          case TT_LONG:
            ASSERT(false, "Coalesce scalar i:" + to_string(i) + " j:" + to_string(j));
            break;
          case TT_UINT64:
            ASSERT(false, "Coalesce scalar i:" + to_string(i) + " j:" + to_string(j));
            break;
          case TT_UINT64VEC:
            token.uint64Vec_[j] = token.uint64Vec_[i];
            break;
          case TT_BOOL:
            ASSERT(false, "Coalesce scalar i:" + to_string(i) + " j:" + to_string(j));
            break;
          case TT_BOOLVEC:
            token.boolVec_[j] = token.boolVec_[i];
            break;
          default:
            throw runtime_error("Unspported type ");
        }
      }
      j += 1;
    }
  }
  token.nvalid = nvalid;
}

void set_token_en(Token& token, bool en);

Token make_token(bool value);

Token make_token(int value);

Token make_token(uint32_t value);

Token make_token(long value);

Token make_token(float value);

Token make_token(uint64_t value);

template<int N>
Token make_token(int (&value)[N]) {
  Token token;
  if (N==1) {
    token.type = TT_INT;
    token.int_ = value[0];
  } else {
    token.type = TT_INTVEC;
    for (int i = 0; i < N; i ++)
      token.intVec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.intVec_[i] = 0;
  }
  return token;
}

template<int N>
Token make_token(uint32_t (&value)[N]) {
  Token token;
  if (N==1) {
    token.type = TT_UINT;
    token.uint_ = value[0];
  } else {
    token.type = TT_UINTVEC;
    for (int i = 0; i < N; i ++)
      token.uintVec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.uintVec_[i] = 0;
  }
  return token;
}

template<int N>
Token make_token(int8_t (&value)[N]) {
  Token token;
  if (N==1) {
    token.type = TT_INT;
    token.int_ = value[0];
  } else {
    token.type = TT_INT8VEC;
    for (int i = 0; i < N; i ++)
      token.int8Vec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.intVec_[i] = 0;
  }
  token.nvalid = N;
  return token;
}

template<int N>
Token make_token(int16_t (&value)[N]) {
  Token token;
  if (N==1) {
    token.type = TT_INT;
    token.int_ = value[0];
  } else {
    token.type = TT_INT16VEC;
    for (int i = 0; i < N; i ++)
      token.int16Vec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.int16Vec_[i] = 0;
  }
  token.nvalid = N;
  return token;
}

template<int N>
Token make_token(uint64_t (&value)[N]) {
  Token token;
  if (N==1) {
    token.type = TT_UINT64;
    token.uint64_ = value[0];
  } else {
    token.type = TT_UINT64VEC;
    for (int i = 0; i < N; i ++)
      token.uint64Vec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.uint64Vec_[i] = 0;
  }
  token.nvalid = N;
  return token;
}

template<int N>
Token make_token(array<uint64_t,N> value) {
  Token token;
  if (N==1) {
    token.type = TT_UINT64;
    token.uint64_ = value[0];
  } else {
    token.type = TT_UINT64VEC;
    for (int i = 0; i < N; i ++)
      token.uint64Vec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.uint64Vec_[i] = 0;
  }
  token.nvalid = N;
  return token;
}

template<int N>
Token make_token(long (&value)[N]) {
  Token token;
  if (N==1) {
    token.type = TT_LONG;
    token.long_ = value[0];
  } else {
    token.type = TT_LONGVEC;
    for (int i = 0; i < N; i ++)
      token.longVec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.longVec_[i] = 0;
  }
  token.nvalid = N;
  return token;
}

template<int N>
Token make_token(float (&value)[N]) {
  Token token;
  if (N == 1) {
    token.type = TT_FLOAT;
    token.float_ = value[0];
  } else {
    token.type = TT_FLOATVEC;
    for (int i = 0; i < N; i ++)
      token.floatVec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.floatVec_[i] = 0;
  }
  token.nvalid = N;
  return token;
}

template<int N>
Token make_token(bool (&value)[N]) {
  Token token;
  if (N == 1) {
    token.type = TT_BOOL;
    token.bool_ = value[0];
  } else {
    token.type = TT_BOOLVEC;
    for (int i = 0; i < N; i ++)
      token.boolVec_[i] = value[i];
    for (int i = N; i < TOKEN_WIDTH; i ++)
      token.boolVec_[i] = false;
  }
  token.nvalid = N;
  return token;
}

template <class T, int N>
vector<T> make_vector(T (&value)[N]) {
  vector<T> v(N);
  for (int i = N; i < N; i++)
    v.push_back(value[i]);
  return v;
}

union Bits {
  int int_;
  float float_;
};

float FloatOr(float x, float y); // deprecated

template<typename T>
T SafeDiv(T& a, T& b, string ctx) {
  if (b == 0) throw runtime_error("Divide by 0 at " + ctx);
  return a / b;
}

template<typename T, typename F>
T asT(F a) {
  return *(T*) &a;
}

static unordered_map<string, ofstream> traces;

ofstream& get_trace(string path);

void trace(int data, string path);

template<typename T, int V>
void trace(T (&data)[V], string path) {
  auto& stream = get_trace(path);
  stream << "[";
  for (int i = 0; i < V; i++)
    stream << data[i] << " ";
  stream << "]" << endl;
  stream.flush();
}

template <typename T>
T parse(string& token) {
  string tpname(typeid(T).name());
  throw runtime_error("Don't know how to parse type " + tpname);
}
template <>
int parse(string& token);
template <>
bool parse(string& token);
template <>
float parse(string& token);
template <>
double parse(string& token);

template <int V>
bool Any(bool ens[V]) {
  bool e = false;
  for (int i = 0; i < V; i ++) {
    e |= ens[i];
  }
  return e;
}

template <typename T, int V>
void Coalesce(T data[V], bool ens[V]) {
  int ptr = 0;
  for (int i = 0; i < V; i++) {
    if (ens[i]) {
      data[ptr] = data[i];
      ptr += 1;
    }
  }
}

string replace(std::string data, std::string toSearch, std::string replaceStr);

template<typename T>
T from_string(string& token) {
	throw runtime_error("TODO: not supported type to parse from string");
}
template<>
int from_string<int>(string& token);

template<>
float from_string<float>(string& token);

template<>
bool from_string<bool>(string& token);

template <typename T> 
T flattenND(const vector<T> &inds, const vector<T> &dims, int start = 0) {
    
    //SN_ASSERT(inds.size() == dims.size(), "inds size" << inds.size() << " must equals to dims size " << dims.size());
    int size = inds.size() - start;
    //SN_ASSERT(size > 0, "inds size" << size << " must > 0");
    
    if (size == 1)
        return inds[start];
    
    T dim_prod = dims[0];
    
    for (int i = 0; i < dims.size(); i++) {
        if (i == start + 1)
            dim_prod = dims[i];
        else if (i > start + 1)
            dim_prod = dim_prod * dims[i];
    }
    
    return inds[start] * dim_prod + flattenND(inds, dims, start + 1);
}

template<typename T>
void load_lut_from_file(T* data, vector<size_t> dims, string path) {
  std::ifstream myFile(path);
  // Make sure the file is open
  if(!myFile.is_open()) throw std::runtime_error("Could not open file " + path);

  if (dims.size()==1) {
    
    // Handle 1-D array
    std::string line;
    
    // Read data, line by line
	int i = 0;
    while(std::getline(myFile, line) && i < dims[0]) {
			data[i] = from_string<T>(line);
			i++;
    }
  }
  else if (dims.size()==2){
    
    int i = 0;
    std::string line;
    T val;

    // Get a new line from the CSV
    while(std::getline(myFile, line))
    {
        // Create a stringstream of the current line
        std::stringstream ss(line);
        std::string field;
        
        // Read values from row
        int j = 0;
        while(std::getline((std::istream&) ss, field, ',') && j < dims[1]){
            
            // Get new index
            std::vector<size_t> inds = {i,j};
            int idx = flattenND(inds, dims);

            //Write values
            data[idx] = from_string<T>(field);
            j++;
        }
        i++;
    }
  }
  else {
    throw runtime_error("TODO: handle CSV of dimension > 2");
  }
  // Close file
  myFile.close();
}

#endif /* CPPGENUTIL_H */
