#ifndef IDRAM_H_
#define IDRAM_H_

#include <array>
#include <list>
#include <unordered_set>
#include "interface.h"
#include "module.h"
#include "token.h"
#include "dram.h"
#include "state.h"
#include "binlog/binlog.hpp"

using namespace std;

class DRAMAddrGen : public Module {
  public:
  FIFO<DRAMCommand, 16> burstcmd;
  FIFO<DRAMCommand, 16> burstrsp;

  DRAMAddrGen(
    const string& _name
  ) : Module(_name),
      burstcmd(_name + "_burstcmd"),
      burstrsp(_name + "_burstrsp") 
  {
    AddChild(&burstcmd);
    AddChild(&burstrsp);
  }

  void SetupDRAM(DRAMController* dram) {
    dram->Add(&burstcmd, &burstrsp);
  }
};

class DualDRAMAddrGen : public Module {
  public:
  FIFO<DRAMCommand, 16> burstcmd;
  FIFO<DRAMCommand, 16> burstrsp;
  FIFO<DRAMCommand, 16> burstcmd2;
  FIFO<DRAMCommand, 16> burstrsp2;

  DualDRAMAddrGen(
    const string& _name
  ) : Module(_name),
      burstcmd(_name + "_burstcmd"),
      burstrsp(_name + "_burstrsp"), 
      burstcmd2(_name + "_burstcmd2"),
      burstrsp2(_name + "_burstrsp2") 
  {
    AddChild(&burstcmd);
    AddChild(&burstrsp);
    AddChild(&burstcmd2);
    AddChild(&burstrsp2);
  }

  void SetupDRAM(DRAMController* dram) {
    dram->Add(&burstcmd, &burstrsp);
    dram->Add(&burstcmd2, &burstrsp2);
  }
};

// V: vectorization of data
// burstSize: in byte
// T: data type for dram data
template<int V, int burstSize, typename T, bool Compressed, int pad=-1000>
class StreamLoadAG: public DualDRAMAddrGen {
  CheckedReceive<Token>* offset;
  CheckedReceive<Token>* size;
  CheckedSend<Token> *data;

  vector<FIFO<DRAMCommand,16>*> burstcmds;
  vector<FIFO<DRAMCommand,16>*> burstrsps;

  uint64_t base{0};
  uint64_t last_burst{0};
  int resp_depth{16};

  // This is the ``fetch'' vectorization, which is different from the output vectorization
  static const int burst_slots{burstSize/sizeof(T)};

  static_assert(V == 16);
  static_assert(sizeof(T) == 4 || Compressed);
  // static_assert(!Compressed);

  struct cmd {
    uint64_t burst{0};
    array<bool,burst_slots> pend{0};
    bool empty{false};
    bool free{false};
    bool done{false};
    int done_vec{1};
    cmd(uint64_t _burst) : burst(_burst) {}
  };

  deque<cmd>     pend;
  deque<int>     next_from;
  deque<cmd>     inflight;
  deque<T>       resp;
  deque<int>     resp_decomp;
  deque<Token>   resp_tok;

  int _take_big_endian(deque<uint8_t>& d, int b) {
    int ret{0};
    while (b--) {
      ret <<= 8;
      ret |= d.front();
      d.pop_front();
    }
    return ret;
  }

  int _off_bytes(uint8_t header) {
    switch (header&0x40) {
      case 0x00: return 2; break;
      case 0x40: return 4; break;
      default: throw module_error(this, "unknown header");
    }
  }

  int _diff_bytes(uint8_t header) {
    switch (header&0x30) {
      case 0x10: return 1; break;
      case 0x20: return 2; break;
      default: throw module_error(this, "unknown header");
    }
  }

  int _length_decode(uint8_t header) {
    if (header == 0xF0)
      return 1;
    // Start with one byte for the header
    if ((header&0xF0) == 0x30) {
      // This is the special case for incompressible data
      return 1+((header&0xF)+1)*4;
    } else {
      int len{1};
      int f_off{header&0x40};
      int f_diff{header&0x30};
      // The number of *differences* stored. Will be zero for length 1
      int f_n_diff{(header&0xF)+1};
      // Bytes for the offset
      switch (f_off) {
        case 0x00: len += 2; break;
        case 0x40: len += 4; break;
        default: throw module_error(this, "unknown header");
      }
      switch (f_diff) {
        case 0x10: len += f_n_diff*1; break;
        case 0x20: len += f_n_diff*2; break;
        default: throw module_error(this, "unknown header");
      }
      return len;
    }
  }

  bool _decompress(void) {
    //cout << "Decompress called! Resp: ";
    //for (auto& r : resp)
      //cout << " " << (int)r;
    //cout << endl;
    if constexpr (Compressed) {
      if (!resp.size())
        return false;
      int exp_bytes = _length_decode(resp.front());
      int orig_size = resp.size();
      //cout << "Len: " << exp_bytes << endl;
      if (resp.size() < exp_bytes) {
        // if (enLog) BINLOG_INFO_W(writer, "Insufficient bytes for header {} ({}/{})", resp.front(), resp.size(), exp_bytes); 
        return false;
      }
      // Committed to decoding
      vector<int> tmp;
      uint8_t header = resp.front(); resp.pop_front();
      int n_out{(header&0xF)+1};
      if (header == 0xF0) {
        // Pass: empty case
      } else if ((header&0xF0) == 0x30) {
        // Special case for take everything
        for (int i=0; i<n_out; i++) {
          tmp.push_back(_take_big_endian(resp, 4));
        }
      } else {
        //int current = _take_big_endian(resp, _off_bytes(header));
        int base = _take_big_endian(resp, _off_bytes(header));
        //tmp.push_back(current);
        //for (int i=0; i < n_out - 1; i++) {
        for (int i=0; i < n_out; i++) {
          //current += _take_big_endian(resp, _diff_bytes(header));
          int offset = _take_big_endian(resp, _diff_bytes(header));
          tmp.push_back(base+offset);
        }
      }
      // if (enLog) BINLOG_INFO_W(writer, "Decoded output vector: {}", tmp);
      if (resp.size() + exp_bytes != orig_size)
        throw module_error(this, "Incorrect decode length.");
      for (auto t : tmp)
        resp_decomp.push_back(t);
      //cout << "\tRemaining: ";
      //for (auto& r : resp)
        //cout << " " << (int)r;
      //cout << endl;
      return true;
    } else {
      assert(false);
    }
  }

  void _flush_tok(int done_vec) {
    Token ret;
    int orig_size;
    if constexpr (Compressed) { 
      orig_size = resp_decomp.size();
      array<int,V> vec;
      vec.fill(pad);
      for (int i=0; i < V && i < orig_size; i++) {
        vec[i] = resp_decomp.front();
        resp_decomp.pop_front();
      }
      ret = Token(vec);
      if (!resp_decomp.size())
        ret.done_vec = done_vec;
    } else {
      orig_size = resp.size();
      array<T,V> vec;
      vec.fill(pad);
      for (int i=0; i < V && i < orig_size; i++) {
        vec[i] = resp.front();
        resp.pop_front();
      }
      ret = Token(vec);
      if (!resp.size())
        ret.done_vec = done_vec;
    }
    if (pad == -1000)
      ret.nvalid = min(V, orig_size);
    // if (enLog)
      // BINLOG_INFO_W(writer, "Stage token: {}", ret); 
    
    resp_tok.emplace_back(ret);
  }

  void _gen_cmds(uint64_t base, int len, int done_vec) {
    uint64_t burst_base{base/burstSize};
    cmd pend_cmd(burst_base*burstSize);
    // Loop over every address in the requested burst. `a' is a byte address.
    for (uint64_t a = base; a < base+len*sizeof(T); a+=sizeof(T)) {
      if (a/burstSize != burst_base) {
        burst_base = a/burstSize;
        if (pend_cmd.burst == last_burst)
          pend_cmd.free = true;
        pend.push_back(pend_cmd);
        // if (enLog)
          // BINLOG_INFO_W(writer, "Pending base {} offsets {} done:{} free:{}", pend_cmd.burst, pend_cmd.pend, pend_cmd.done, pend_cmd.free); 
        pend_cmd = cmd(burst_base*burstSize);
      }
      auto off = (a/sizeof(T))%burst_slots;
      pend_cmd.pend[off] = true;
    }
    if (len == 0)
      pend_cmd.empty = true;
    pend_cmd.done = true;
    if (pend_cmd.burst == last_burst)
      pend_cmd.free = true;
    last_burst = pend_cmd.burst;
    pend_cmd.done_vec = done_vec+1;
    // if (enLog)
      // BINLOG_INFO_W(writer, "Pending base {} offsets {} done:{} free:{} done_vec:{}", pend_cmd.burst, pend_cmd.pend, pend_cmd.done, pend_cmd.free, pend_cmd.done_vec); 
    pend.push_back(pend_cmd);
  }

 public:
  void SetBase(uint64_t _base) {
    base = _base;
  }

  explicit StreamLoadAG(
      const string& _name, 
      CheckedReceive<Token>* _offset, 
      CheckedReceive<Token>* _size, 
      CheckedSend<Token>* _data,
      DRAMController* _dram
  ) : DualDRAMAddrGen(_name), offset(_offset), size(_size), data(_data) {
    SetupDRAM(_dram);
    burstcmds.push_back(&burstcmd);
    burstcmds.push_back(&burstcmd2);
    burstrsps.push_back(&burstrsp);
    burstrsps.push_back(&burstrsp2);
  }

  void Eval() {
    char c_toDRAM[2]{0};
    for (int i=0; i < 2; i++) {
      auto b = burstcmds[i];
      c_toDRAM[i] = burstcmds[i]->Ready() ? '.' : 'x';
      if (b->Ready() && pend.size()) {
        DRAMCommand c;
        // if (enLog)
          // BINLOG_INFO_W(writer, "Command to DRAM {}", pend.front().burst); 
        c.addr = pend.front().burst;
        c.write = false;
        if (!pend.front().free) 
          b->Push(c);
        c_toDRAM[i] = '#';
        inflight.push_back(pend.front());
        pend.pop_front();
        next_from.push_back(i);
      }
    }
    if (!pend.size() && offset->Valid() && size->Valid()) {
      uint64_t o;
      int done_vec;
      // Add a new base mode for MLIR backend.
      auto off = offset->ReadPop();
      auto sz = size->ReadPop();
      if (base) {
        assert(off.nvalid == 1);
        o = base + (int)off;
      } else {
        o = (uint64_t)off;
      }
      auto s = (int)sz;
      assert(off.done_vec == sz.done_vec);
      // if (enLog)
        // BINLOG_INFO_W(writer, "Register new command {},{}", o, s); 
      _gen_cmds(o, s, off.done_vec);
    }
    char c_fromDRAM[2]{0};
    for (int i=0; i < 2; i++) {
      c_fromDRAM[i] = '?';
      if (!burstrsps[i]->Valid() && resp_tok.size() >= resp_depth)
        c_fromDRAM[i] = ' ';
      else if (!burstrsps[i]->Valid())
        c_fromDRAM[i] = '.';
      else if (resp_tok.size() >= resp_depth)
        c_fromDRAM[i] = 'x';
    }

    bitset<2> already_tried{0};
    deque<int> try_ret;
    for (auto& f : next_from) {
      if (!already_tried[f]) {
        already_tried[f] = true;
        try_ret.push_back(f);
      } else {
        break;
      }
    }
    if (next_from.size())
      assert(try_ret.size());
    // for (int i=0; i < try_ret.size(); i++) {
      // next_from.pop_front();
    // }

    for (auto& i : try_ret) {
      auto& b = burstrsps[i];
      if ((inflight.front().free || b->Valid()) && resp_tok.size() < resp_depth) {
        if (!inflight.front().free)
          b->Pop();
        assert(inflight.size());
        auto& c = inflight.front();
        array<T,burst_slots> raw_data;
        // if (enLog)
          // BINLOG_INFO_W(writer, "Process return with base {} valids {} free {}", c.burst, c.pend, c.free); 
        for (int i=0; i<burst_slots; i++) {
          raw_data[i] = *((T*)(c.burst+i*sizeof(T)));
          if (c.pend[i]) {
            resp.push_back(*((T*) (c.burst+i*sizeof(T))));
            // cout << "Push to resp: " << (int)resp.back() << endl;
          }
        }
        // if (enLog)
          // BINLOG_INFO_W(writer, "Raw return {}", raw_data); 
        // if (enLog)
          // BINLOG_INFO_W(writer, "New resp data {}", resp); 

        if (Compressed) {
          while (_decompress())
            ;
        }

        // Flush as many commands as possible---may be 0, 1, or 2. Only flush for c.done if there's
        // data in the queue---avoid adding an extra zero-length token
        if (Compressed) {
          while (resp_decomp.size() >= V || (resp_decomp.size() && c.done))
            _flush_tok(c.done_vec);
        } else {
          while (resp.size() >= V || (resp.size() && c.done))
            _flush_tok(c.done_vec);
        }
        // If the token is actually zero-length, send a single flush command
        if (c.empty && c.done)
          _flush_tok(c.done_vec);
        c_fromDRAM[i] = '#';
        inflight.pop_front();
        next_from.pop_front();
      } else {
        break;
      }
    }
    assert(inflight.size() == next_from.size());
    if (resp_tok.size() && data->Ready()) {
      //if (enLog)
        //BINLOG_INFO_W(writer, "Output token {}", resp_tok.front()); 
      data->Push(resp_tok.front());
      resp_tok.pop_front();
    }
    // if (enLog)
      // BINLOG_INFO_W(writer, "LT Pend: {} Inflight: {} O:{} I:{}", pend.size(), inflight.size(), c_toDRAM, c_fromDRAM); 
  }
  virtual void Log(std::ostream& log) {
  }
};

// V: vectorization of data
// burstSize: in byte
// T: data type for dram data
template<int V, int burstSize, typename T>
class DenseLoadAG: public DRAMAddrGen {
  uint64_t issueOffset;
  int remainIssueSize = 0;
  CheckedReceive<Token>* offset;
  CheckedReceive<Token>* size;
  CheckedSend<Token> *data;
  deque<int> ens; // <bstAddr, addr, data> Not real hardware. For data handling only
 public:
  RateMatchingFIFO<uint64_t, burstSize / sizeof(T), V> rspMatchFIFO;

  explicit DenseLoadAG(
      const string& _name, 
      CheckedReceive<Token>* _offset, 
      CheckedReceive<Token>* _size, 
      CheckedSend<Token>* _data
  ):
    DRAMAddrGen(_name), 
    data(_data), 
    offset(_offset),
    size(_size),
    rspMatchFIFO(_name + "_rspMatchFIFO", 32)
  {
    AddChild(&rspMatchFIFO);
  }

  explicit DenseLoadAG(
      const string& _name, 
      CheckedReceive<Token>* _offset, 
      CheckedReceive<Token>* _size, 
      CheckedSend<Token>* _data,
      DRAMController* _dram
  ):DenseLoadAG(_name, _offset, _size, _data) {
    SetupDRAM(_dram);
  }

  void Eval() {
    if (burstcmd.Ready()) {
      if (remainIssueSize==0 && offset->Valid() && size->Valid()) {
        issueOffset = offset->Read().long_;
        remainIssueSize = size->Read().int_;
        if (enLog) *log << "Schedule offset:" << issueOffset << " size:" << remainIssueSize << " en: ";
#if BUMP_UP_ZERO_LENGTH_DRAM_LOADS
        if (remainIssueSize == 0) {
          cout << "WARNING: increase zero-length DRAM request to length-one" << endl;
          remainIssueSize = 1;
        }
#endif
        uint64_t addr = 0;
        uint64_t word = remainIssueSize / sizeof(T);
        for (int i = 0; i < roundUpDiv(word, V); i++) {
          addr += V;
          int en;
          if (addr < word) en = V;
          else en = word - (addr - V);
          if (enLog) *log << en << " ";
          ens.push_back(en);
        }
        if (enLog) *log << endl;
        offset->Pop();
        size->Pop();
      }
      if (remainIssueSize > 0) {
        DRAMCommand c;
        c.addr = issueOffset;
        c.write = false;
        burstcmd.Push(c);
        issueOffset += burstSize;
        remainIssueSize -= burstSize;
        if (remainIssueSize < 0) remainIssueSize = 0;
      }
    }

    if (burstrsp.Valid() && rspMatchFIFO.Ready()) {
      auto burstAddr = burstrsp.Read().addr;
      array<uint64_t, burstSize / sizeof(T)> addrs;
      for (int i = 0; i < burstSize / sizeof(T); i++)
        addrs[i] = burstAddr + i * sizeof(T);
      rspMatchFIFO.Push(addrs);
      burstrsp.Pop();
    }

    if (!ens.empty()) {
      int en = ens.front();
      if (rspMatchFIFO.Valid(en) && data->Ready()) {
        // Handle data load
        auto addr = rspMatchFIFO.Read(en);
        ens.pop_front();
        Token t;
        if (V == 1) {
          t = make_token(*((T*) addr[0]));
        } else {
          T vec[V]{0};
          for (int i = 0; i < V; i ++) {
            if (i < en) vec[i] = *((T*) addr[i]);
          }
          t = make_token<V>(vec);
        }
        if (enLog) *log << "Read addr " << addr[0] << " en:" << en << " d:" << t << endl;
        data->Push(t);
        rspMatchFIFO.Pop(en);
      }
    }
  }
  void Log(ostream& log) {
    log << endl;
    log << "cofst:" << issueOffset << " remain:" << remainIssueSize;
  }
};

struct DRAMStoreCommand {
  bool                     write {false};
  uint64_t                 addr {0};
  bool                     returned {false};
  bool                     ack {false};
};
template<int V, int burstSize, typename T>
class DenseStoreAG: public DRAMAddrGen {
  uint64_t issueOffset;
  int remainIssueSize = 0;
  CheckedReceive<Token> *offset;
  CheckedReceive<Token> *size;
  CheckedReceive<Token> *data;
  CheckedReceive<Token> *valid;
  CheckedSend<Token> *ack;
 public:
  RateMatchingFIFO<tuple<uint64_t, bool, bool>, V, burstSize / sizeof(T)> rqstMatchFIFO;
  std::list<DRAMStoreCommand> rob; 
  // This rob is somewhat redundant with the one in dram.h. However, this is required to make sure
  // write bursts 
  //  1. that can be directly issued, 
  //  2. the ones that require read followed by write due to partial invalid writes, 
  //  3. doesn't need to be served due to complete invalid
  //  are acked in the correct order

  explicit DenseStoreAG(
      const string& _name, 
      CheckedReceive<Token>* _offset,
      CheckedReceive<Token>* _size,
      CheckedReceive<Token>* _data,
      CheckedReceive<Token>* _valid,
      CheckedSend<Token>* _ack
  ):
    DRAMAddrGen(_name), 
    offset(_offset),
    size(_size),
    data(_data),
    valid(_valid),
    ack(_ack), 
    rqstMatchFIFO(_name + "_rqstMatchFIFO", 64)
  {
    AddChild(&rqstMatchFIFO);
  }

  explicit DenseStoreAG(
      const string& _name, 
      CheckedReceive<Token>* _offset,
      CheckedReceive<Token>* _size,
      CheckedReceive<Token>* _data,
      CheckedReceive<Token>* _valid,
      CheckedSend<Token>* _ack,
      DRAMController* _dram
  ):DenseStoreAG(_name, _offset,_size,_data,_valid,_ack) {
    SetupDRAM(_dram);
  }

  void Eval() {
    bool do_pop_burst_rsp = false;
    if (rqstMatchFIFO.Ready()) {
      if (remainIssueSize==0 && offset->Valid() && size->Valid()) {
        issueOffset = offset->Read().long_;
        remainIssueSize = size->Read().int_;
        offset->Pop();
        size->Pop();
        if (enLog) *log << "Schedule offset:" << issueOffset << " size:" << remainIssueSize << endl;
      }
      if (remainIssueSize > 0 && data->Valid() && valid->Valid()) {
        Token d = data->Read();
        Token v = valid->Read();
        array<tuple<uint64_t, bool, bool>, V> reqst; // addr, valid, last
        array<bool, V> en;
        for (int i = 0; i < V; i ++) {
          auto addr = issueOffset + i * sizeof(T);
          bool validBit = toT<bool>(v, i);
          // Handle data store
          if (validBit) {
            *((T*) addr) = toT<T>(d, i);
          }
          en[i] = remainIssueSize > 0;
          remainIssueSize -= sizeof(T);
          bool last = remainIssueSize <= 0;
          reqst[i] = make_tuple(addr, validBit, last);
        }
        rqstMatchFIFO.Push(reqst, en);
        data->Pop();
        valid->Pop();
        issueOffset += V * sizeof(T);
        if (remainIssueSize < 0) remainIssueSize = 0;
      }
    }
    
    bool pushed = false;
    // Returned read. Send write now.
    if (burstrsp.Valid() && burstcmd.Ready()) {
      auto rsp = burstrsp.Read();
      if (!rsp.write) {
        for (DRAMStoreCommand& sc : rob) {
          if (!sc.write && sc.addr == rsp.addr) {
            sc.returned = false;
            sc.write = true;
            if (enLog) *log << "Receive read: " << sc.addr << endl;
          }
        }
        DRAMCommand c;
        c.addr = rsp.addr;
        c.write = true;
        if (enLog) {
          *log << "Issue write after read " << c.addr << endl;
        }
        burstcmd.Push(c);
        do_pop_burst_rsp = true;
        //burstrsp.Pop();
        pushed = true;
      }
    }

    // Send read request first if exists invalid data in the burst, otherwise send the write request
    if (rqstMatchFIFO.Valid() && burstcmd.Ready() && !pushed) {
      auto reqst = rqstMatchFIFO.Read();
      auto& burstAddr = get<0>(reqst[0]);
      bool existInvalidWrite = false;
      bool existLastBit = false;
      bool allInvalidWrite = true;
      for (auto& [addr, v, last] : reqst) {
        existInvalidWrite |= !v;
        existLastBit |= last;
        allInvalidWrite &= !v;
      }
      DRAMStoreCommand sc;
      sc.addr = burstAddr;
      sc.ack = existLastBit;
      DRAMCommand c;
      c.addr = burstAddr;
      if (allInvalidWrite) {
        sc.write = true;
        sc.returned = true;
      } else if (existInvalidWrite) {
        c.write = false;
        sc.write = false;
        sc.returned = false;
        if (enLog) *log << "Issue read " << c.addr << endl;
        burstcmd.Push(c);
      } else {
        c.write = true;
        sc.write = true;
        sc.returned = false;
        if (enLog) *log << "Issue write " << c.addr << endl;
        burstcmd.Push(c);
      }
      rob.push_back(sc);
      rqstMatchFIFO.Pop();
    }

    // Receive returned write request
    if (burstrsp.Valid()) {
      auto rsp = burstrsp.Read();
      if (rsp.write) {
        for (DRAMStoreCommand& sc : rob) {
          if (sc.write && sc.addr == rsp.addr) {
            sc.returned = true;
            if (enLog) *log << "Receive write: " << sc.addr << endl;
          }
        }
        do_pop_burst_rsp = true;
        //burstrsp.Pop();
      }
    }
    const auto& sc = rob.front();
    if (rob.size() > 0 && sc.write && sc.returned) {
      if (sc.ack) {
        if (ack->Ready()) {
          if (enLog) *log << "Ack: " << sc.addr << endl;
          Token t;
          t.type = TT_BOOL;
          t.bool_ = true;
          ack->Push(t);
          rob.pop_front();
        }
      } else {
        rob.pop_front();
      }
    }
    if (do_pop_burst_rsp)
      burstrsp.Pop();
  }

  void Log(ostream& log) {
    log << endl;
    log << "offset:" << issueOffset << " remain:" << remainIssueSize;
    log << " dv:" << data->Valid() << " vv:" << valid->Valid();
    log << " mfifo.v:" << rqstMatchFIFO.Valid();
    log << " mfifo.r:" << rqstMatchFIFO.Ready() << " burstcmd.r:" << burstcmd.Ready() ;
  }
};

template<int V, int burstSize, typename T>
class SparseLoadAG: public DRAMAddrGen {
  CheckedReceive<Token>* addr;
  CheckedSend<Token> *data;
 public:
  RateMatchingFIFO<uint64_t, V, 1> addrMatchFIFO;
  RateMatchingFIFO<uint64_t, 1, V> rspMatchFIFO;
  FIFO<array<uint64_t,V>, 16> bstOfstFIFO;

  explicit SparseLoadAG(
      const string& _name, 
      CheckedReceive<Token>* _addr, 
      CheckedSend<Token>* _data
  ):
    DRAMAddrGen(_name), 
    data(_data), 
    addr(_addr),
    addrMatchFIFO(_name + "_addrMatchFIFO", 32),
    rspMatchFIFO(_name + "_rspMatchFIFO", 32),
    bstOfstFIFO(_name + "_bstOfstFIFO")
  {
    AddChild(&addrMatchFIFO);
    AddChild(&rspMatchFIFO);
    AddChild(&bstOfstFIFO);
  }

  explicit SparseLoadAG(
      const string& _name, 
      CheckedReceive<Token>* _addr, 
      CheckedSend<Token>* _data,
      DRAMController* _dram
  ):SparseLoadAG(_name, _addr, _data) {
    SetupDRAM(_dram);
  }

  void Eval() {
    if (addrMatchFIFO.Ready() && bstOfstFIFO.Ready() && addr->Valid()) {
      Token tokenAddr = addr->Read();
      addr->Pop();
      array<uint64_t, V> addrVec;
      array<uint64_t, V> ofstVec;
      for (int i = 0; i < V; i++) {
        uint64_t ad = toT<long>(tokenAddr, i);
        // burst aligned address
        auto bstAddr = ad / burstSize * burstSize;
        addrVec[i] = bstAddr;
        // offset of address w.r.t. burst address
        ofstVec[i] = ad - bstAddr;
      }
      if (enLog) {
        *log << "Receive addr: " << tokenAddr << endl;
        *log << "bstAddr: " << make_token<V>(addrVec) << endl;
        *log << "bstofst: " << make_token<V>(ofstVec) << endl;
      }
      addrMatchFIFO.Push(addrVec);
      bstOfstFIFO.Push(ofstVec);
    }
    if (burstcmd.Ready() && addrMatchFIFO.Valid()) {
      DRAMCommand c;
      c.addr = addrMatchFIFO.Read()[0];
      c.write = false;
      burstcmd.Push(c);
      addrMatchFIFO.Pop();
    }
    if (burstrsp.Valid() && rspMatchFIFO.Ready()) {
      auto burstAddr = burstrsp.Read().addr;
      array<uint64_t, 1> addrs;
      addrs[0] = burstAddr;
      rspMatchFIFO.Push(addrs);
      burstrsp.Pop();
    }
    if (rspMatchFIFO.Valid() && bstOfstFIFO.Valid() && data->Ready()) {
      auto ofstVec = bstOfstFIFO.Read(); // offset from burst address
      auto bstAddrs = rspMatchFIFO.Read(); // Returned burst cmds
      T dv[V];
      uint64_t adVec[V];
      for (int i = 0; i < V; i++) {
        adVec[i] = bstAddrs[i] + ofstVec[i];
        dv[i] = *((T*) adVec[i]);
      }
      // Handle data load
      Token dataToken = make_token<V>(dv);
      if (enLog) {
        *log << "Returned bstAddr:" << make_token<V>(bstAddrs) << endl;
        *log << "Ofst:" << make_token<V>(ofstVec) << endl; 
        *log << "Addr: " << make_token<V>(adVec) << endl;
        *log << "Data: " << dataToken << endl;
      }
      data->Push(dataToken);
      rspMatchFIFO.Pop();
      bstOfstFIFO.Pop();
    }
  }
  void Log(ostream& log) {
    log << "addr v:" << addr->Valid();
    log << " addrMatchFIFO v:" << addrMatchFIFO.Valid() << " r:" << addrMatchFIFO.Ready();
    log << " bstOfstFIFO v:" << bstOfstFIFO.Valid() << " r:" << bstOfstFIFO.Ready();
    log << " burstcmd v:" << burstcmd.Valid() << " r:" << burstcmd.Ready();
    log << " burstrsp v:" << burstrsp.Valid() << " r:" << burstrsp.Ready();
    log << " rspMatchFIFO v:" << rspMatchFIFO.Valid() << " r:" << rspMatchFIFO.Ready();
    log << " data r:" << data->Ready();
  }

};

//TODO: add rob
template<int V, int burstSize, typename T>
class SparseStoreAG: public DRAMAddrGen {
  CheckedReceive<Token>* addr;
  CheckedReceive<Token>* data;
  CheckedSend<Token>* ack;
  deque<tuple<uint64_t, uint64_t, T>> cmdQueue; // <bstAddr, addr, data> Not real hardware. For data handling only
 public:
  RateMatchingFIFO<uint64_t, V, 1> addrMatchFIFO;
  RateMatchingFIFO<uint64_t, 1, V> ackMatchFIFO;
  std::unordered_set<uint64_t> uncommited;

  explicit SparseStoreAG(
      const string& _name, 
      CheckedReceive<Token>* _addr, 
      CheckedReceive<Token>* _data,
      CheckedSend<Token>* _ack
  ):
    DRAMAddrGen(_name), 
    data(_data), 
    addr(_addr),
    ack(_ack),
    addrMatchFIFO(_name + "_addrMatchFIFO", 32),
    ackMatchFIFO(_name + "_ackMatchFIFO", 32)
  {
    AddChild(&addrMatchFIFO);
    AddChild(&ackMatchFIFO);
  }

  explicit SparseStoreAG(
      const string& _name, 
      CheckedReceive<Token>* _addr, 
      CheckedReceive<Token>* _data,
      CheckedSend<Token>* _ack,
      DRAMController* _dram
  ):SparseStoreAG(_name, _addr, _data, _ack) {
    SetupDRAM(_dram);
  }

  void Eval() {
    if (addrMatchFIFO.Ready() && addr->Valid() && data->Valid()) {
      Token tokenAddr = addr->Read();
      Token tokenData = data->Read();
      addr->Pop();
      data->Pop();
      if (enLog)
        *log << "Receive: ";
      array<uint64_t, V> addrVec;
      for (int i = 0; i < V; i++) {
        uint64_t ad = toT<long>(tokenAddr, i);
        // burst aligned address
        auto bstAddr = ad / burstSize * burstSize;
        addrVec[i] = bstAddr;
        // offset of address w.r.t. burst address
        auto d = toT<T>(tokenData, i);
        cmdQueue.push_back(make_tuple(bstAddr, ad, d));
        if (enLog) {
          *log << " [a: " << ad; 
          *log << " b: " << bstAddr;
          *log << " d: " << d << "], ";
        }
      }
      addrMatchFIFO.Push(addrVec);
    }
    bool pushedWrite = false;
    // Issue write request after read comes back
    DRAMCommand rsp;
    if (burstrsp.Valid()) rsp = burstrsp.Read();
    if (burstcmd.Ready() && burstrsp.Valid()) {
      if (!rsp.write) {
        rsp.write = true;
        burstrsp.Pop();
        burstcmd.Push(rsp);
        pushedWrite = true;
        rsp.write = false;
      }
    }
    // Issue read request first. If there's outstanding write on the same burst, wait until the
    // burst is commited
    if (!pushedWrite && burstcmd.Ready() && addrMatchFIFO.Valid()) {
      auto bstAddrs = addrMatchFIFO.Read();
      auto bstAddr = bstAddrs[0];
      if (uncommited.find(bstAddr) == uncommited.end()) {
        DRAMCommand c;
        c.addr = bstAddr;
        c.write = false;
        burstcmd.Push(c);
        addrMatchFIFO.Pop();
        uncommited.insert(bstAddr);
      }
    }
    // Returned write request.
    if (burstrsp.Valid()) {
      if (rsp.write) {
        burstrsp.Pop();
        uncommited.erase(rsp.addr);

        // Hanlde data write
        uint64_t bstAddr, addr;
        T data;
        tie(bstAddr, addr, data) = cmdQueue.front();
        cmdQueue.pop_front();
        *((T*) addr) = data;
        array<uint64_t, 1> ackVec;
        ackVec[0] = true;
        ackMatchFIFO.Push(ackVec);
        if (enLog) {
          *log << "Returned a: " << addr; 
          *log << " b: " << bstAddr;
          *log << " d: " << data ;
          *log << " rsp.addr: " << rsp.addr << ", ";
        }
        if (rsp.addr != bstAddr) throw module_error(this, "Mismatch on returned address");
      }
    }
    if (ackMatchFIFO.Valid() && ack->Ready()) {
      auto ackToken = ackMatchFIFO.Read();
      ackMatchFIFO.Pop();
      ack->Push(make_token<V>(ackToken));
    }
  }

  void Log(ostream& log) {
    log << "addr v:" << addr->Valid();
    log << " data v:" << data->Valid();
    log << " addrMatchFIFO v:" << addrMatchFIFO.Valid() << " r:" << addrMatchFIFO.Ready();
    log << " ackMatchFIFO v:" << ackMatchFIFO.Valid() << " r:" << ackMatchFIFO.Ready();
    log << " burstcmd v:" << burstcmd.Valid() << " r:" << burstcmd.Ready();
    log << " burstrsp v:" << burstrsp.Valid() << " r:" << burstrsp.Ready();
  }

};

template<int V, int burstSize, typename T>
class CoalStoreAG: public DRAMAddrGen {
  CheckedReceive<Token>* addr;
  CheckedReceive<Token>* size;
  CheckedReceive<Token>* data;
  CheckedReceive<Token>* valid;
  CheckedSend<Token>* ack;

  // The next DRAM offset to store to
  int64_t off;
  // The remaining number of input elements to look at from the input queue
  int     rem{0};
  // The number of bursts currently pending at the DRAM
  int     burst_outstanding{0};
  // The number of acks pending for input elements
  int     ack_pend{0};
  // Pending store elements 
  deque<T> pend_burst;

 public:
  explicit CoalStoreAG(const string& _name, 
      CheckedReceive<Token>* _addr,
      CheckedReceive<Token>* _size,
      CheckedReceive<Token>* _data,
      CheckedReceive<Token>* _valid,
      CheckedSend<Token>* _ack,
      DRAMController* _dram) :
    DRAMAddrGen(_name), addr(_addr), size(_size), data(_data), valid(_valid), 
    ack(_ack) {
      SetupDRAM(_dram);
    }
  void Eval() {
    // Read from the setup queue
    if (rem == 0 && ack_pend == 0 && addr->Valid() && size->Valid()) {
      Token a = addr->ReadPop();
      Token s = size->ReadPop();
      assert(!a.isVec() && !s.isVec());
      off = (int64_t)a;
      rem = (int32_t)s;
      if (enLog) *log << "Setup access at address: " << a << " raw size: " << s << endl;
      // cout << "Setup access at address: " << a << " raw size: " << s << endl;
    }
    // Read from the data queue
    if (rem && data->Valid() && valid->Valid() && pend_burst.size() < 16) {
      Token d = data->ReadPop();
      Token v = valid->ReadPop();

      if (V == 1) {
        rem = max(rem-V, 0);
      } else {
        rem = max(rem-d.nvalid, 0);
      }
      ack_pend++;

      assert(V == 1 || V == 16);
      if (V == 1) {
        if ((bool)v) {
          // cout << "push scal" << endl;
          pend_burst.push_back((T)d);
        }
      } else {
        assert(d.nvalid <= V);
        for (int i=0; i<d.nvalid; i++) {
          if ((bool)v[i]) {
            // cout << "push vec" << endl;
            pend_burst.push_back((T)d[i]);
          }
        }
      }
    }

    // Push burst to DRAM if we're at the end of the input data with unpushed
    // outputs, or if we have a full burst to push
    if (((rem == 0 && pend_burst.size()) || pend_burst.size() >= 16) && burstcmd.Ready()) {
      int to_deq = min(pend_burst.size(), 16ul);
      DRAMCommand c;
      c.write = true;
      c.addr = off;
      if (enLog) {
        *log << "Flush " << min(pend_burst.size(), 16ul) << " addresses (base: " << off << ")" << endl;
      }
      cout << path << name << " Flush " << min(pend_burst.size(), 16ul) << " addresses (base: " << off << ")" << endl;
      // cout << "Flush " << to_deq << " addresses (base: " << off << ")" << endl;
      cout << path << name << "\t";
      for (int i=0; i<to_deq; i++) {
        cout << "\t" << pend_burst.front();
        *((T*) (off+i*4)) = pend_burst.front();
        pend_burst.pop_front();
      }
      cout << endl;
      cout << path << name << " Remaining buffered: " << pend_burst.size() << " rem in: " << rem << endl;
      off += to_deq*4;
      burst_outstanding++;
      burstcmd.Push(c);
    }

    // Handle burst returns
    if (burstrsp.Valid()) {
      burstrsp.Pop();
      assert(burst_outstanding);
      burst_outstanding--;
    }

    // Return acks. Hold at least one ack to prevent program progress until
    // the everything is done
    if ((ack_pend && !burst_outstanding && !rem && !pend_burst.size())) {
      if (ack->Ready()) {
        if (enLog) *log << "Ack" << endl;
        Token t;
        t.type = TT_BOOL;
        t.bool_ = true;
        ack->Push(t);
        // ack_pend--;
        ack_pend = 0;
        if (!ack_pend) cout << path << name << " return final ACK" << endl;
      }
    }
  }
};

// This is a slightly hacky controller, and doesn't play nicely with the 
// original Spatial syntax. Effectively, everything must be done in terms of
// full-vector writes, because the controller does not buffer data between
// transactions or handle split vectors
template<int V, int burstSize, typename T>
class DynStoreAG : public DRAMAddrGen {
  // For now
  // static_assert(V == 16);
  CheckedReceive<Token>* addr;
  CheckedReceive<Token>* data;
  CheckedReceive<Token>* done;
  CheckedSend<Token>*    ack;

  // The next DRAM offset to store to
  int64_t off{-1};
  int bursts_pend{0};
  deque<T> pend_dat;
  // The number of bursts for completed sequences, before an ack is sent
  deque<int> bursts_for_ack;
  // The number of bursts currently pending at the DRAM
  int     burst_outstanding{0};

 public:
  explicit DynStoreAG(const string& _name, 
      CheckedReceive<Token>* _addr,
      CheckedReceive<Token>* _data,
      CheckedReceive<Token>* _done,
      CheckedSend<Token>* _ack,
      DRAMController* _dram) :
    DRAMAddrGen(_name), addr(_addr), data(_data), done(_done), ack(_ack) {
      SetupDRAM(_dram);
    }
  void Eval() {
    // Read from the setup queue
    if (off < 0 && addr->Valid()) {
      Token a = addr->ReadPop();
      assert(!a.isVec());
      off = (int64_t)a;
      if (enLog) *log << "Setup access at address: " << a << endl;
      cout << "Setup access at address: " << a << endl;
      assert((off % 64) == 0 && "Offset must be aligned");
    }
    bool is_last{false};
    // Read from the data queue
    if (off >= 0 && data->Valid() && done->Valid() && burstcmd.Ready() && pend_dat.size() < 16) {
      Token dat = data->ReadPop();
      Token last = done->ReadPop();

      for (int i=0; i<last.nvalid; i++) {
        is_last |= (bool)last[i];
      } 
      for (int i=0; i<dat.nvalid; i++) {
        pend_dat.push_back(dat[i]);
        if (enLog) {
          *log << "\tPush: " << (T)dat[i] << endl;
        }
        cout << "\tPush: " << (T)dat[i] << endl;
      }
    }
    if (is_last || pend_dat.size() >= 16) {
      DRAMCommand c;
      c.write = true;
      c.addr = off;
      if (enLog) {
        *log << "Flush " << 16 << " addresses (base: " << off << ")" << endl;
      }
      cout << "Flush " << 16 << " addresses (base: " << off << ")" << endl;
      for (int i=0; i<16 && pend_dat.size(); i++) {
        *((T*) (off+i*4)) = pend_dat.front();
        pend_dat.pop_front();
      }
      off += 64;
      bursts_pend++;
      burstcmd.Push(c);
      if (is_last) {
        bursts_for_ack.push_back(bursts_pend);
        bursts_pend = 0;
        off = -1;
      }
    }

    // Handle burst returns
    if (burstrsp.Valid()) {
      burstrsp.Pop();
      if (bursts_for_ack.size() == 0) {
        bursts_pend--;
      } else {
        for(int i=0; i<bursts_for_ack.size(); i++) {
          if (bursts_for_ack[i]) {
            bursts_for_ack[i]--;
            break;
          }
        }
      }
    }

    // Return the ack for each transaction stream.
    if (ack->Ready() && bursts_for_ack.size() && !bursts_for_ack[0]) {
      if (enLog) *log << "Ack" << endl;
      cout << "Ack" << endl;
      ack->Push(true);
      bursts_for_ack.pop_front();
    }
  }
};

#endif // IDRAM_H_
