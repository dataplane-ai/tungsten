#ifndef REPL_H_
#define REPL_H_

#include <chrono>
#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <thread>

#include "module.h"
#include "commit.h"

#include "binlog/Session.hpp"

class REPL;
using std::string_literals::operator""s;

class REPL {
  Module       *DUT;
  std::chrono::time_point<std::chrono::steady_clock> init;
  std::chrono::time_point<std::chrono::steady_clock> last_tick;
  std::ostream& out;
  std::vector<Module*> all;
  std::vector<Module*> logged_all;
  std::vector<Module*> clocked_all;
  std::set<Module*> disabled;
  std::vector<std::vector<Module*>> chunked;
  long printfreq = 1000;
  bool do_log {false};
  long logfreq = 1; // Log every cycle
  long logstart = 0; // Log from cycle
  bool instrumentation = false;
  bool deadlock = false;
  bool quiet = false;
  int done;

  uint64_t cycle{0};
  // static binlog::Session session;
  std::ofstream logfile; // ("hello.blog", std::ofstream::out|std::ofstream::binary);

  using Command_func = void (REPL::*)(const std::vector<std::string>&);
  struct Command_spec {
    const char* name;
    const Command_func function;
    constexpr Command_spec(const char* _name, Command_func _function) :
      name(_name), function(_function) {
    }
  };
  std::vector<Command_spec> cmds {
    Command_spec("step",          &REPL::Cmd_tick),
    Command_spec("stepall",       &REPL::Cmd_tick),
    Command_spec("set",           &REPL::Cmd_set),
    Command_spec("push",          &REPL::Cmd_set),
    Command_spec("build",         &REPL::Cmd_build),
    Command_spec("get",           &REPL::Cmd_get),
    Command_spec("exit",          &REPL::Cmd_exit),
    Command_spec("quit",          &REPL::Cmd_exit),
    Command_spec("source",        &REPL::Cmd_source),
    Command_spec("quiet",         &REPL::Cmd_quiet),
    Command_spec("verbose",       &REPL::Cmd_verbose),
    Command_spec("dumpstate",     &REPL::Cmd_dumpstate),
    Command_spec("apply",         &REPL::Cmd_set),
    Command_spec("stat",          &REPL::Cmd_printstat),
    Command_spec("log2files",     &REPL::Cmd_log2files),
    Command_spec("logon",         &REPL::Cmd_logon),
    Command_spec("logoff",        &REPL::Cmd_logoff),
    Command_spec("lognet",        &REPL::Cmd_lognet),
    Command_spec("logmem",        &REPL::Cmd_logmem),
    Command_spec("logfile",       &REPL::Cmd_logfile),
    Command_spec("logevery",      &REPL::Cmd_logevery),
    Command_spec("printevery",    &REPL::Cmd_printevery),
    Command_spec("stallstarve",   &REPL::Cmd_stallstarve),
    Command_spec("disable",       &REPL::Cmd_disable),
    Command_spec("logfrom",       &REPL::Cmd_logfrom),
    Command_spec("printchildren", &REPL::Cmd_printchildren)
  };
  bool parallel {false};
  void Chunk(int size = 1);
  void Tick();
  void Cmd_tick(const std::vector<std::string>& args);
  void Cmd_set(const std::vector<std::string>& args);
  void Cmd_get(const std::vector<std::string>& args);
  void Cmd_build(const std::vector<std::string>& args);
  void Cmd_exit(const std::vector<std::string>& args);
  void Cmd_source(const std::vector<std::string>& args);
  void Cmd_quiet(const std::vector<std::string>& args);
  void Cmd_verbose(const std::vector<std::string>& args);
  void Cmd_dumpstate(const std::vector<std::string>& args);
  void Cmd_printstat(const std::vector<std::string>& args);
  void Cmd_log2files(const std::vector<std::string>& args);
  void Cmd_logon(const std::vector<std::string>& args);
  void Cmd_logoff(const std::vector<std::string>& args);
  void Cmd_lognet(const std::vector<std::string>& args);
  void Cmd_logmem(const std::vector<std::string>& args);
  void Cmd_logfile(const std::vector<std::string>& args);
  void Cmd_logevery(const std::vector<std::string>& args);
  void Cmd_printevery(const std::vector<std::string>& args);
  void Cmd_logfrom(const std::vector<std::string>& args);
  void Cmd_disable(const std::vector<std::string>& args);
  void Cmd_printchildren(const std::vector<std::string>& args);
  void Cmd_stallstarve(const std::vector<std::string>& args);

  // void do_consume(std::ostream& _l, volatile int& done);
  void do_consume();
  void regen_log();

 public:
  REPL(Module *_DUT, std::ostream& _out);
  void Command(const std::string& buf);
  void Run(std::istream& in, bool file = false,
      const std::string& prompt = "> "s);
  void dump_line_traces(const std::string& dir);
  long RunAll(unsigned long int timeout);
  void LogAll();
  // static binlog::Session& GetSession() {
    // return session;
  // }
  Module* FindModule(const char* pattern);
  std::vector<Module*> FindModules(const std::string& path);
  std::vector<Module*> AllChildren();
};

#endif  // REPL_H_
