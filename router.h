#ifndef ROUTER_H_
#define ROUTER_H_

#include <optional>
#include <string>
#include <istream>
#include <iostream>
#include <vector>
#include <regex>
#include <stdexcept>
#include <utility>
#include <map>

#include "interface.h"
#include "module.h"
#include "state.h"
#include "token.h"

using namespace std;

template<int D, int V>
struct VCBuffer : public Module {
  typedef enum {
    kActive,
    kIdle
  } state_t;
  std::vector<state_t>         states;
  std::vector<FIFO<Packet, D>*> bufs;
  std::vector<int>             remaining;
  VCBuffer(const std::string& _name) : Module(_name) {
    for (int i=0; i < V; i++) {
      states.push_back(kIdle);
      bufs.emplace_back(new FIFO<Packet,D>("vc"s+std::to_string(i)));
      AddChild(bufs.back(), false);
      remaining.push_back(0);
    }
  }
  void AddPacket(const Packet& p) {
    if (p.vc < 0)
      throw std::runtime_error("Invalid packet VC");
    if (p.vc >= V)
      throw std::runtime_error("Packet VC out of range: " + std::to_string(p.vc) + " limit:" + std::to_string(V));
    bufs[p.vc]->Push(p);
  }

  void RefreshChildren() {
    children.clear();
    name_children.clear();
    for (auto& m : bufs)
      AddChild(m, false);
  }
  void Eval() {
    for (auto& m : bufs)
      m->Eval();
  }
  void Clock() {
    for (auto& m : bufs)
      m->Clock();
  }
};

template<int D, int V>
class Router : public Module {
  std::vector<ValReceive<Packet>*>              inputs;
  std::vector<ValSend<Credit>*>                 input_credits;
  std::vector<ValSend<Packet>*>                 outputs;
  std::vector<ValReceive<Credit>*>              output_credits;
  std::vector<VCBuffer<D, V>*>                  input_vcs;
  std::vector<Register<std::optional<Packet>>*> output_buf;
  std::vector<int>                              credits;

  // priority_in tracks the output with request priority for each input
  std::vector<int>                     priority_out;
  std::vector<int>                     priority_in;

  // Combined VC/input to combined VC/output
  std::vector<std::pair<int, int>>      alloc_reqs;
  std::vector<std::pair<int, int>>      alloc_grants;
  // Flow to combined VC/output
  std::map<int, std::vector<int>>       routes;
  int                                  addr;
  void AddRoute(const std::string& rt);
  void Allocate();
  void RefreshChildren();

  // Helper functions for combining VCs and ports
  inline int CombinePortVC(int port, int VC) { return port*V+VC; }
  inline int GetVC(int combined)             { return combined % V; }
  inline int GetPort(int combined)           { return combined / V; }

 public:
  vector<int> OutputsUsed() {
    vector<int> ret;
    for (const auto &r : routes)
      for (int o : r.second)
        if (find(ret.begin(), ret.end(), GetPort(o)) == ret.end())
          ret.push_back(GetPort(o));
    return ret;
  }

  void Eval();
  void Clock();
  void AddInput(ValReceive<Packet>* i, ValSend<Credit>* c);
  void AddOutput(ValSend<Packet>* o, ValReceive<Credit>* c);
  Router(int _addr, const std::string& _name) : addr(_addr), Module(_name) {
  }
  void SetParameter(const std::string& key, const std::string& val);
  void AppendParameter(const std::string& key, const std::string& val);
};

template<int D, int V>
void Router<D, V>::AddRoute(const std::string& rt) {
  int flow         {-1};
  int port_out     {-1};
  int vc_out       {-1};
  // std::cout << "Attempting to match " << rt << std::endl;
  std::regex route_regex("([fdv][0-9]+)");
  auto matches_begin = std::sregex_iterator(rt.begin(), rt.end(), route_regex);
  auto matches_end = std::sregex_iterator();

  for (auto i = matches_begin; i != matches_end; i++) {
    std::smatch match = *i;
    std::string match_str = match.str();
    // std::cout << "\t substr " << match_str << std::endl;
    int match_int = std::stoi(std::string(++match_str.begin(),
                                          match_str.end()));
    char match_char = match_str[0];
    if (match_char == 'v') {
      if (vc_out != -1)
        throw std::runtime_error("Attempt to set vc twice ("+rt+")");
      vc_out = match_int;
    } else if (match_char == 'f') {
      if (flow != -1)
        throw std::runtime_error("Attempt to set flow twice ("+rt+")");
      flow = match_int;
    } else if (match_str[0] == 'd') {
      if (port_out != -1)
        throw std::runtime_error("Attempt to set output port twice ("+rt+")");
      port_out = match_int;
    }
  }
  if (flow < 0 || vc_out < 0 || port_out < 0)
    throw std::runtime_error("Underspecified route ("+rt+")");
  if (vc_out >= V)
    throw std::runtime_error("VC "+std::to_string(vc_out)+" is greater than maximum: "+std::to_string(V-1));
  routes[flow].push_back(CombinePortVC(port_out, vc_out));
}

// Input-first for now
template<int D, int V>
void Router<D, V>::Allocate() {
  assert(inputs.size() == outputs.size());
  alloc_grants.clear();
  if (alloc_reqs.size() == 0)
    return;
  // This is a vector, where out_reqs_from_in[input] = {input/VC, output/VC}
  std::vector<std::pair<int, int>> out_reqs_from_in;
  // This is a vector, where in_grants_from_out[output] = {input/VC, output/VC}
  std::vector<std::pair<int, int>> in_grants_from_out;
  out_reqs_from_in.resize(inputs.size(), std::make_pair(-1, -1));
  in_grants_from_out.resize(outputs.size(), std::make_pair(-1, -1));

  // Try to generate 1:1 matchings between input to output/VC
  for (const auto& r : alloc_reqs) {
    int port_in = GetPort(r.first);
    // There is no output port/VC requested, so just assign this one
    if (out_reqs_from_in[port_in].first < 0) {
      out_reqs_from_in[port_in] = r;
      //std::cout << "\t" << name << " req0 " << r.second << " from in " << port_in << std::endl;
    }
    if (priority_in[port_in] > out_reqs_from_in[port_in].second) {
      // For this case, the mapped VC is less than the priority VC, so anything
      // less than the mapped VC gets higher priority, or anything greater
      // than the priority VC
      if (r.second < out_reqs_from_in[port_in].second
          || r.second >= priority_in[port_in]) {
        out_reqs_from_in[port_in] = r;
        //std::cout << "\t" << name << " req1 " << r.second << " from in " << port_in << std::endl;
      }
    } else {
      // For this case, the mapped VC is greater than the priority VC, so a
      // higher priority VC would be between the two.
      if (r.second < out_reqs_from_in[port_in].second
          && r.second >= priority_in[port_in]) {
        out_reqs_from_in[port_in] = r;
        //std::cout << "\t" << name << " req2 " << r.second << " from in " << port_in << std::endl;
      }
    }
  }

  for (const auto& r : out_reqs_from_in) {
    int port_out = GetPort(r.second);
    if (r.first < 0)
      continue;

    assert(port_out >= 0);
    assert(port_out < outputs.size());
    if (in_grants_from_out[port_out].first < 0) {
      in_grants_from_out[port_out] = r;
      //std::cout << "\t" << name << " accept0 " << r.second << " from out " << port_out << std::endl;
    }
    if (priority_out[port_out] > in_grants_from_out[port_out].first) {
      // For this case, the mapped VC is less than the priority VC, so anything
      // less than the mapped VC gets higher priority, or anything greater
      // than the priority VC
      if (r.first < in_grants_from_out[port_out].first
          || r.first >= priority_out[port_out]) {
        in_grants_from_out[port_out] = r;
        //std::cout << "\t" << name << " accept1 " << r.second << " from out " << port_out << std::endl;
      }
    } else {
      // For this case, the mapped VC is greater than the priority VC, so a
      // higher priority VC would be between the two.
      if (r.first < in_grants_from_out[port_out].first
          && r.first >= priority_out[port_out]) {
        in_grants_from_out[port_out] = r;
        //std::cout << "\t" << name << " accept2 " << r.second << " from out " << port_out << std::endl;
      }
    }
  }

  // We now have the desired 1:1 mapping, need to move the valid reqs to grants
  std::vector<std::pair<int, int>> remaining_reqs;
  alloc_grants.clear();
  for (const auto& r : alloc_reqs) {
    if (in_grants_from_out[GetPort(r.second)] == r) {
      alloc_grants.push_back(r);
      //std::cout << "\t"<< name << " " << "approve grant <" << r.first << "," << r.second << ">" << std::endl;
      priority_in[GetPort(r.first)] =
        (r.second + 1) % CombinePortVC(outputs.size(), V);
      priority_out[GetPort(r.second)] =
        (r.first + 1) % CombinePortVC(inputs.size(), V);
    } else {
      remaining_reqs.push_back(r);
      //std::cout << "\t"<< name << " " << "reject grant <" << r.first << "," << r.second << ">" << std::endl;
    }
  }
  alloc_reqs = remaining_reqs;
}

template<int D, int V>
void Router<D, V>::RefreshChildren() {
  children.clear();
  name_children.clear();
  for (auto& m : output_buf)
    AddChild(m, false);
  for (auto& m : input_vcs)
    AddChild(m, false);
}

template<int D, int V>
void Router<D, V>::Clock() {
  if (!routes.size())
    return;

  for (auto& m : output_buf)
    m->Clock();
  for (auto& m : input_vcs)
    m->Clock();
}

template<int D, int V>
void Router<D, V>::Eval() {
  // Fast-path optimization for no routes.
  if (!routes.size())
    return;

  for (auto& m : output_buf)
    m->Eval();
  for (auto& m : input_vcs)
    m->Eval();

  // Start by copying inputs to VCs
  for (size_t i=0; i < inputs.size(); i++) {
    if (inputs[i]->Valid()) {
      //std::cout << name << " read packet from input buffer " << i << std::endl;
      input_vcs[i]->AddPacket(inputs[i]->Read());
    }
  }

  // Now handle incoming credits
  for (size_t i=0; i < outputs.size(); i++) {
    if (output_credits[i]->Valid()) {
      Credit c = output_credits[i]->Read();
      for (int v : c.vcs) {
        assert(i < outputs.size() && v < V);
        credits[CombinePortVC(i, v)]++;
      }
    }
  }

  //alloc_reqs.clear();

  // Update state of allocator requests from input VCs based on output
  // availability
  for (size_t i=0; i < input_vcs.size(); i++) {
    assert(i < outputs.size());
    for (size_t v=0; v < V; v++) {
      if (input_vcs[i]->bufs[v]->Valid()) {
        if (input_vcs[i]->states[v] == VCBuffer<D, V>::kIdle) {
          const Packet& pkt = input_vcs[i]->bufs[v]->Read();
          //std::cout << name << " add allocator request from buffer " << i 
                            //<< " vc " << v 
                            //<< " flow " << pkt.flow << std::endl;
          input_vcs[i]->remaining[v] += routes[pkt.flow].size();
          if (routes[pkt.flow].size() == 0)
            throw module_error(this, "no route for packet " + std::to_string(pkt.flow));
          for (const auto& rt : routes[pkt.flow]) {
            alloc_reqs.emplace_back(CombinePortVC(i, v), rt);
            //std::cout << "\t" << name << ": " << rt << std::endl;
          }
          input_vcs[i]->states[v] = VCBuffer<D, V>::kActive;
        }
      }
    }
  }

  Allocate();

  // Traverse switch to output buffers
  for (auto& o : output_buf)
    o->Write(std::nullopt);

  for (const auto& g : alloc_grants) {
    int outPort = GetPort(g.second);
    int outVC   = GetVC(g.second);
    int inPort  = GetPort(g.first);
    int inVC    = GetVC(g.first);

    Packet p = input_vcs[inPort]->bufs[inVC]->Read();
    p.vc = outVC;
    // If the grant can't make progress, retry it.
    if (!credits[g.second]) {
      alloc_reqs.push_back(g);
      //std::cout << name << " not enough credits for grant to output "  << 
        //g.second << std::endl;
      //throw std::runtime_error("Packet overwritten in output buffer!");
    } else {
      credits[g.second]--;
      //std::cout << name << " write packet to output buffer " <<
                   //outPort << " flow " << p.flow << std::endl;
      output_buf[outPort]->Write(p);
      assert(input_vcs[inPort]->remaining[inVC]);
      input_vcs[inPort]->remaining[inVC]--;
      if (input_vcs[inPort]->remaining[inVC] == 0) {
        input_vcs[inPort]->states[inVC] = VCBuffer<D, V>::kIdle;
        input_vcs[inPort]->bufs[inVC]->Pop();
        input_credits[inPort]->Push(Credit(inVC));
      }
    }
  }

  // Write outputs
  for (int i=0; i < output_buf.size(); i++) {
    if (output_buf[i]->Read().has_value()) {
      outputs[i]->Push(output_buf[i]->Read().value());
    }
  }
}

template<int D, int V>
void Router<D, V>::AddInput(ValReceive<Packet>* i, ValSend<Credit>* c) {
  inputs.push_back(i);
  input_credits.push_back(c);
  input_vcs.emplace_back(new VCBuffer<D,V>("input"+std::to_string(input_vcs.size())));
  //AddChild(&input_vcs.back());
  for (int i=0; i < V; i++) {
    credits.push_back(D);
    priority_out.push_back(0);
  }
}

template<int D, int V>
void Router<D, V>::AddOutput(ValSend<Packet>* o, ValReceive<Credit>* c) {
  outputs.push_back(o);
  output_credits.push_back(c);
  for (int i=0; i< V; i++) {
    priority_in.push_back(0);
  }
  output_buf.emplace_back(new Register<optional<Packet>>("outReg"+std::to_string(outputs.size()-1)));
  //AddChild(&output_buf.back());
}

template<int D, int V>
void Router<D, V>::SetParameter(const std::string& key,
                                const std::string& val) {
  throw std::runtime_error("Unknown key "+key+" at "+name);
}

template<int D, int V>
void Router<D, V>::AppendParameter(const std::string& key,
                                   const std::string& val) {
  if (key == "route") {
    AddRoute(val);
  } else {
    throw std::runtime_error("Unknown key "+key+" at "+name);
  }
}

#endif  // ROUTER_H_
