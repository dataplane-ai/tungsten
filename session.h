#ifndef SESSION_H_
#define SESSION_H_

#include "binlog/Session.hpp"

binlog::Session& GetSession();

#endif  // SESSION_H_
