#ifndef SIMTIME_H_
#define SIMTIME_H_

#include "stdint.h"

class SimTime {
  static uint64_t cycle;
 public:
  static uint64_t& Cycle() {
    return cycle;
  }
};

#endif // SIMTIME_H_
