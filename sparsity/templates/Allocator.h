#ifndef ALLOCATOR_H_
#define ALLOCATOR_H_

#include <random>
#include <array>
#include <cassert>
#include <optional>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <climits>

using namespace std;

enum alloc_prio_t {kRand, kStatic, kLonely};
enum alloc_type_t {kIF, kOF, kAugmenting, kNone, kLimit};

template<int R, int C, int P=1>
class Allocator {
  // Simplify the type of the request matrix
  using ReqArr = array<bool,R*C>;

  bool req = false;
  int it {0};

  array<ReqArr,P> reqs      {false};
  ReqArr grants    {false};

  array<bool,R> grants_row {false};
  array<bool,C> grants_col {false};

  int _idx(int r, int c) const {
    assert(0 <= r && r < R);
    assert(0 <= c && c < C);
    return c+C*r;
  }

  // Get the row, column pair
  pair<int,int> _de_idx(int i) {
    assert(0 <= i && i < R*C);
    return make_pair(i/C, i%C);
  }

  void Reset() {
    for (auto& r : reqs)
      r.fill(false);
  }

  int _count_mat(const ReqArr& m) {
    return accumulate(m.begin(),
                      m.end(), 
                      0,
                      [](int x, int y) {return x + y;});
  }
    

  bool CheckGrantState() const {
    bool val = true;
    for (int r=0; r<R; r++) {
      bool granted=false;
      for (int c=0; c<C; c++) {
        val &= !(granted && grants[_idx(r,c)]);
        granted |= grants[_idx(r,c)];
      }
      val &= (granted == grants_row[r]);
    }
    for (int c=0; c<C; c++) {
      bool granted=false;
      for (int r=0; r<R; r++) {
        val &= !(granted && grants[_idx(r,c)]);
        granted |= grants[_idx(r,c)];
      }
      val &= (granted == grants_col[c]);
    }
    return val;
  }

  bool CheckNoGrants() const {
    auto f = [](bool x) { return x; };
    return !any_of(grants.begin(), grants.end(), f) 
        && !any_of(grants_row.begin(), grants_row.end(), f) 
        && !any_of(grants_col.begin(), grants_col.end(), f);
  }


  mt19937 rng;

  array<int,R> col_prio {0};
  array<int,C> row_prio {0};

  void ArbCols(ReqArr& m, array<int,R>& prio) {
    for (int r=0; r<R; r++) {
      bool g = false;
      for (int c=0; c<C; c++) {
        int cc = (c+prio[r])%C;
        if (m[_idx(r,cc)]) {
          if (g) {
            m[_idx(r,cc)] = false;
          }
          g = true;
        }
      }      
    }
  }

  void ArbRows(ReqArr& m, array<int,C>& prio) {
    for (int c=0; c<C; c++) {
      bool g = false;
      for (int r=0; r<R; r++) {
        int rr = (r+prio[c])%R;
        if (m[_idx(rr,c)]) {
          if (g) {
            m[_idx(rr,c)] = false;
          }
          g = true;
        }
      }      
    }
  }

  void Augment(ReqArr& rq, ReqArr &gr) {
    // first is the distance, second is the back-pointer
    array<pair<int,int>,R> rq_dists;// {make_pair(false,-1)};
    array<pair<int,int>,C> gr_dists;// {make_pair(false,-1)};

    //array<bool,R> rq_front {false};
    //array<bool,C> gr_front {false};

    // Check for forward edge that is reachable and improves the matching
    auto fe = [&, this](int r, int c) -> bool {
      return rq[_idx(r,c)] 
         && !gr[_idx(r,c)] 
         &&  rq_dists[r].first < INT_MAX
         &&  gr_dists[c].first > rq_dists[r].first+1;
    };
    // The same, but for back-edges
    auto re = [&, this](int r, int c) -> bool {
      return gr[_idx(r,c)]
         &&  gr_dists[c].first < INT_MAX
         &&  rq_dists[r].first > gr_dists[c].first+1;
    };
    // Check and see if row/columns are granted
    auto rg = [&, this](int r) -> bool {
      for (int c=0; c<C; c++)
        if (gr[_idx(r,c)])
          return true;
      return false;
    };
    auto cg = [&, this](int c) -> bool {
      for (int r=0; r<R; r++)
        if (gr[_idx(r,c)])
          return true;
      return false;
    };
    // Try to update a row, column pair
    auto upd = [&, this](int r, int c) -> bool {
      if (fe(r,c)) {
        //cout << "Forward edge " << "r" << r << " -> " << "c" << c << endl;
        gr_dists[c].first  = rq_dists[r].first+1;
        gr_dists[c].second = r;
        return true;
      } else if (re(r,c)) {
        //cout << "Reverse edge " << "r" << r << " <- " << "c" << c << endl;
        rq_dists[r].first  = gr_dists[c].first+1;
        rq_dists[r].second = c;
        return true;
      } else {
        return false;
      }
    };
    // Toggle a bit in the grant matrix
    auto tog = [&, this](int r, int c) { gr[_idx(r,c)] = !gr[_idx(r,c)]; };

    bool aug_conv;
    do {
      rq_dists.fill(make_pair(INT_MAX, -1));
      gr_dists.fill(make_pair(INT_MAX, -1));
      aug_conv = true;
      // Update the distances to granted rows
      for (int r=0; r<R; r++)
        if (!rg(r))
          rq_dists[r].first = 0;

      bool converged;
      do {
        converged = true;
        for (int r=0; r<R; r++)
          for (int c=0; c<C; c++)
            converged &= !upd(r,c);
      } while (!converged);

      for (int c=0; c<C; c++) {
        if (gr_dists[c].first < INT_MAX && !cg(c)) {
          aug_conv = false;

          //cout << "Reachable output " << c << endl;
          bool back_edge = true;
          int next_idx = c;
          while (true) {
            //assert(next_idx >= 0);
            //if (next_idx < 0)
              //break;
            if (back_edge) {
              //if (!gr_dists[next_idx].first) 
                //break;
              if (gr_dists[next_idx].second < 0 ) 
                break;
              //cout << "col " << setw(2) << next_idx << " -> row " << setw(2) << gr_dists[next_idx].second << endl;
              tog(gr_dists[next_idx].second, next_idx);
              next_idx = gr_dists[next_idx].second;
            } else {
              //if (!rq_dists[next_idx].first)
                //break;
              if (rq_dists[next_idx].second < 0 ) 
                break;
              //cout << "row " << setw(2) << next_idx << " -> col " << setw(2) << rq_dists[next_idx].second << endl;
              tog(next_idx, rq_dists[next_idx].second);
              next_idx = rq_dists[next_idx].second;
            }
            back_edge = !back_edge;
          }
          break;
        }
      }
    } while (!aug_conv);

  }


  void StoreGrants(ReqArr& m, int prio) {
    for (int c=0; c<C; c++) {
        for (int r=0; r<R; r++) {
        if (m[_idx(r,c)]) {
          assert(!grants_row[r]);
          assert(!grants_col[c]);
          grants_row[r] = true;
          grants_col[c] = true;
          grants[_idx(r,c)] = true;
          assert(reqs[prio][_idx(r,c)]);
          reqs[prio][_idx(r,c)] = false;
        }
      }      
    }
  }

  void PrintMatrix(ostream &str, const ReqArr& m) {
    str << "\t";
    for (int r=0; r<R; r++) {
      for (int c=0; c<C; c++) {
        if (m[_idx(r,c)])
          str << "(" << r << "," << c << ")" << " ";
      }
    }

    str << endl;
    for (int r=0; r<R; r++) {
      str << "\t";
      for (int c=0; c<C; c++) {
        str << (m[_idx(r,c)] ? "#" : ".");
      }
      str << endl;
    }
  }

 public:

  int NumReqs() const {
    return accumulate(reqs[P-1].begin(),
                      reqs[P-1].end(), 
                      0,
                      [](int x, int y) {return x + y;});
  }

  int NumGrants() const {
    return accumulate(grants.begin(),
                      grants.end(), 
                      0,
                      [](int x, int y) {return x + y;});

  }

  void Request(int r, int c, int prio=0) {
    // Request for this priority and all lower priorities as well
    for (int i=prio; i<P; i++) {
      reqs[i][_idx(r,c)] = true;
    }
    req = true;
  }

  // This function is destructive to the request matrix
  void Allocate(int iter=1, alloc_type_t typ=kIF, alloc_prio_t prio=kRand, vector<int> prio_iters={}) {
    assert(typ != kNone);
    if (!req) {
      it++;
      return;
    } else {
      //cout << endl << endl << "Allocator iteration " << it++ << endl;
    }
    req = false;
    assert(CheckNoGrants());
    if (!prio_iters.size())
      prio_iters.push_back(iter);

    for (int i=0; i<prio_iters.size(); i++) {
      for (int j=0; j<prio_iters[i]; j++) {
        ReqArr int_reqs;
        int_reqs = reqs[i];

        //cout << "Received allocator requests" << endl;
        //PrintMatrix(cout, int_reqs);

        // Start by removing requests that are invalidated by current grants
        for (int r=0; r<R; r++) {
          for (int c=0; c<C; c++) {
            if (grants_row[r] || grants_col[c]) {
              int_reqs[_idx(r,c)] = false;
            }
          }
        }

        if (prio==kRand) {
          uniform_int_distribution<int> col_dist (0,C-1);
          uniform_int_distribution<int> row_dist (0,R-1);
          for (auto& x : col_prio) 
            x = col_dist(rng);
          for (auto& x : row_prio) 
            x = row_dist(rng);
        } else if (typ==kIF) {
          for (int i=0; i<col_prio.size(); i++)
            col_prio[i] = i;
          for (int i=0; i<row_prio.size(); i++)
            row_prio[i] = 0;
        } else if (typ==kOF) {
          for (int i=0; i<col_prio.size(); i++)
            col_prio[i] = 0;
          for (int i=0; i<row_prio.size(); i++)
            row_prio[i] = i%C;
        }

        // Get new grants from the intermediate requests
        if (typ == kIF) {
          ArbCols(int_reqs, col_prio);
          //cout << "After column-wise arb" << endl;
          //PrintMatrix(cout, int_reqs);
          ArbRows(int_reqs, row_prio);
          //cout << "After row-wise arb" << endl;
          //PrintMatrix(cout, int_reqs);
        } else if (typ == kOF) {
          ArbRows(int_reqs, row_prio);
          //cout << "After row-wise arb" << endl;
          //PrintMatrix(cout, int_reqs);
          ArbCols(int_reqs, col_prio);
          //cout << "After column-wise arb" << endl;
          //PrintMatrix(cout, int_reqs);
        } else if (typ == kAugmenting) {

          //cout <<endl<<endl<< "Request matrix (sum=" << _count_mat(reqs) << ")" << endl;
          //PrintMatrix(cout, int_reqs);
          
          // Take the requests for this stage---pruned by the grants from previous stages---and generate an initial matching
          auto init_iter_grants = int_reqs;

          ArbRows(init_iter_grants, row_prio);
          //cout <<endl<<endl<< "After row-wise arb (sum=" << _count_mat(int_reqs) << ")" << endl;
          //PrintMatrix(cout, int_reqs);

          ArbCols(init_iter_grants, col_prio);
          //cout <<endl<<endl<< "After column-wise arb (sum=" << _count_mat(int_reqs) << ")" << endl;
          //PrintMatrix(cout, int_reqs);

          // When augmenting, only consider the grants that are still possible---don't do the full priority arbitration
          // Instead, respect strict priorities
          Augment(int_reqs, init_iter_grants);
          //cout <<endl<<endl<< "After augmenting (sum=" << _count_mat(int_reqs) << ")" << endl;
          //PrintMatrix(cout, int_reqs);

          int_reqs = init_iter_grants;
        }


        // Add these grants to the output grants
        StoreGrants(int_reqs, i);

        //cout << "Granted allocator requests" << endl;
        //PrintMatrix(cout, grants);

        // Check that this iteration did not introduce any new errors
        assert(CheckGrantState());
      }
      reqs[i].fill(false);
    }
  }

  optional<pair<int,int>> GetGrant() {
    assert(CheckGrantState());
    for (int i=0; i<R*C; i++) {
      if (grants[i]) {
        grants[i] = false;
        auto ret = _de_idx(i);
        grants_row[ret.first] = false;
        grants_col[ret.second] = false;
        return ret;
      }
    }
    return nullopt;
  }

};

#endif
