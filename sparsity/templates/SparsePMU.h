#ifndef SPARSE_PMU_H_
#define SPARSE_PMU_H_

#include <algorithm>
#include <deque>

#include "sparsity/templates/SparseReorder.h"
#include "state.h"
#include "token.h"

#include "debug.h"

#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

using namespace std;

template<typename T, size_t N, size_t B>
class SparsePMU : public SparseOuter, public RMWShared<T> {

  std::set<int> inflight;
  std::deque<int> alloc_bufs;

  int tile{0};
  int base_align{32};
  int drop_warn_rem{10};
  int max_alloc{1};
  int io_max{1};
  map<int,int> in_queue_for_port;
  int words_written{0};
  int words_read{0};

  int next_dense_read{0};
  int next_dense_write{0};

  int next_in_read{0};
  int next_in_write{0};

  bool order_set{false};
  bool split_read_write{false};

  int next_output_idx{0};
  uint64_t cycle {0};
  uint64_t last_pass {0};
  CounterStatistic stats {"stats"};

  // These are a sanity check on RMW-dense conflicts
  bool any_rmw_read{false};
  bool any_rmw_write{false};

  enum tx_type {k_txR,k_txW,k_txRMW};
  bool rmw_en{true};

  enum order_type {k_UnOrdered=0,k_FullyOrdered=1};

  order_type _get_order(const string& _name) {
    if (_name == "") {
      return k_UnOrdered;
    } else if (_name == "unordered") {
      return k_UnOrdered;
    } else if (_name == "ordered") {
      return k_FullyOrdered;
    } else {
      throw module_error(this, "unknown ordering: " + _name);
    }
  }

  order_type _comb_order(order_type a, order_type b) {
    if (b > a)
      return b;
    else
      return a;
  }

  void OrderUpdateHelper() {
    order_set = true;
    for (int i=0; i<in_ports.size(); i++) {

      bool fully_order = ro_modes[i] == k_FullyOrdered;

      int op = op_indices[i];
      bool op_coal = (op == op_name_to_idx["read"])
                  || (op == op_name_to_idx["inc"]);
      bool coal_inc = op == op_name_to_idx["inc"];

      if (op >= 0) {
        cout << path << name << ": Add context " << i << endl;
        reorder_rmw.AddCTX(i, fully_order, false, fully_order, op_coal, coal_inc, alignments[i]);
        reorder_read.AddCTX(i, fully_order, false, fully_order, op_coal, coal_inc, alignments[i]);
        reorder_write.AddCTX(i, fully_order, false, fully_order, op_coal, coal_inc, alignments[i]);
      }
    }
  }

  using RMWShared<T>::ops;
  using RMWShared<T>::op_names;
  using RMWShared<T>::op_name_to_idx;
  using RMWShared<T>::_add_op;

  struct tx_data {
    tx_type type{k_txRMW};
    bool is_write{false};
    int32_t addr{-1};
    int op{-1};
    T data{0};
    string str() const {
      return is_write?"W":"R";
    }
    tx_data() : is_write(false), addr(-1), op(-1) { }
    tx_data(bool _is_write, int32_t _addr, T _data, tx_type _type, int _op) : 
      is_write(_is_write), addr(_addr), data(_data), type(_type), op(_op) {
    }
  };

  using ROBuf = SparseReorder<tx_data,N,B>;

  ROBuf reorder_rmw {"reorder_rmw"};
  ROBuf reorder_read {"reorder_read"};
  ROBuf reorder_write {"reorder_write"};

  array<T,N*B> banks;
  void _add_tx(ROBuf& buf, int idx, const Token* addr, const Token* data, int op, int ro_mode, bool force_coal, bool force_coal_inc, int align) {
    Token tmp;
    assert(TOKEN_WIDTH == B);
    assert(addr);

    array<int,B> a{-1};
    array<tx_data,B> t;
    array<int,B>     b{-1};
    array<bool,B>    v{false};

    tx_type typ = k_txRMW;

    if (!addr->isVec()) {
      T dat{0};
      if (data && data->isVec())
        dat = (T)(*data)[0];
      else if (data)
        dat = (T)(*data);
      t[0] = tx_data(!!data, _trans_addr((int)(*addr)), dat, typ, op);
      for (int i=1; i<B; i++) {
        t[i] = tx_data(!!data, -1, 0, typ, op); 
      }
    } else if (addr->isVec()) {
      if (data && !data->isVec()) {
        tmp.type = data->vecType();
        for (int i=0; i<addr->nvalid; i++) {
          (T&)tmp[i] = (T)(*data);
        }
        data = &tmp;
      }
      for (int i=0; i<addr->nvalid; i++)
        t[i] = tx_data(!!data, _trans_addr((int)(*addr)[i]), data?(T)(*data)[i]:0, typ, op);
    } else {
      assert(false);
    }
    for (int i=0; i<B; i++) {
      t[i].addr = _to_loc(t[i].addr);
      v[i] = i<addr->nvalid && t[i].addr >= 0 ;
      if (v[i] && lut_loaded && max_alloc > 1) {
        int off = 65536/max_alloc;
        // t[i].addr += off*(i%max_alloc);
      }
      if (!v[i])
        t[i].data = 0;
      b[i] = _to_bank(t[i].addr);
      a[i] = t[i].addr;
    }
#if DBG_PMU
    cout << path << name << " Align: " << align << endl;
    cout << path << name << " Add transaction: ";
    for (int i=0; i<addr->nvalid; i++) {
      cout << "(" << t[i].addr << ":" << t[i].data << ")  ";
    }
    cout << endl;
#endif
    buf.AddTX(addr->done_vec, t, v, b, idx, a, force_coal, force_coal_inc, addr->nvalid);
  }
  int _to_loc(int addr) {
    if (addr < 0)
      return addr;
    int raw_addr = (addr/(32/base_align));
    int ret = addr&((65536*(32/base_align))-1);
    if ((raw_addr>>16) != tile)
      ret = -10;
    // fmt::print("Translate address {} to {} (base_align={}, tile={})\n", addr, ret, base_align, tile);
    return ret;
  }
  int _to_bank(int addr) {
    auto raw_addr = (addr/(32/base_align));
    if (lut_loaded) {
      auto hashed_addr = (raw_addr ^ (raw_addr>>3) ^ (raw_addr>>7)) % B;
      return hashed_addr % B;
    } else {
      auto hashed_addr = (raw_addr + ((raw_addr>>4) ^ (raw_addr>>8))) % B;
      return hashed_addr % B;
    }
  }
  void ProcessOutput(ROBuf& b, tx_type t) {
    int output_used{0};
    int last_output_idx{next_output_idx};
    for (int i=0; i<in_ports.size(); i++) {
      int idx = (i+next_output_idx)%in_ports.size();
      if (op_indices[idx] < 0)
        continue;
      if (output_used < io_max) {
        output_used += b.AccumPartialTX(idx);
      }
      if ((output_used < io_max && b.VecTXDone(idx) && !b.IsAccumTX(idx))
          || b.AccumTXDone(idx)) {
        if (out_ports[idx]->Ready()) {
          last_output_idx = idx+1; 
          output_used++;
          in_queue_for_port[idx]--;
          assert(in_queue_for_port[idx] >= 0);
          tuple<int,array<tx_data,B>,int,int> tmp;
          if (b.IsAccumTX(idx)) {
            tmp = b.GetDoneAccumTX(idx);
          } else {
            tmp = b.GetDoneVecTX(idx);
          }
          auto dat = get<1>(tmp);
          assert(get<0>(tmp) == idx);
          Token tok;
          tok.type = TokVecType<T>();
          for (int i=0; i<B; i++) {
            if (port_types[idx] == kRd) 
              (T&)tok[i] = dat[i].data;
            else if (port_types[idx] == kWrA) 
              (uint32_t&)tok[i] = dat[i].addr | (bank_id<<bank_shift);
            else if (port_types[idx] == kRMWA)
              (T&)tok[i] = dat[i].data;
          }
          tok.done_vec = get<2>(tmp);
          tok.nvalid = get<3>(tmp);
          out_ports[idx]->Push(tok);
          //if (!b.SlotsTaken(idx))
            //bubbles_res[idx] = 1;
#if DBG_PMU
          if (port_types[idx] == kRd) {
            cout << path << name << " Push output to idx " << idx << " (read)" << endl;
          } else if (port_types[idx] == kWrA) {
            cout << path << name << " Push output to idx " << idx << " (write)" << endl;
          } else if (port_types[idx] == kRMWA) {
            cout << path << name << " Push output to idx " << idx << " (rmw) Accum: ";
            if (b.IsAccumTX(idx))
              cout << "yes";
            else
              cout << "no";
            cout << endl;
          }
          cout << path << name << " " << tok << endl;
#endif
        }
      }
    }
    next_output_idx = last_output_idx;
  }
  void ProcessScalar(ROBuf& b, tx_type t) {
    while (b.ScalTXReady()) {
      auto s = b.GetNextScalTX();

      if (s.tx_data.op == op_name_to_idx["read"]) {
        words_read++;
        any_rmw_read = true;
      } else if (s.tx_data.op == op_name_to_idx["write"]) {
        words_written++;
        any_rmw_write = true;
        if (s.align != 32 && s.align != -1)
          any_rmw_read = true;
      } else {
        any_rmw_read = true;
        any_rmw_write = true;
        words_read++; words_written++;
      }

      auto word_addr = s.tx_data.addr;
      auto subword_addr = 0;

      subword_addr = word_addr % (32/base_align);
      word_addr /= (32/base_align);

      auto data_in = banks.at(word_addr);

      if constexpr (std::is_integral<T>::value) {
        if (s.align > 0 && s.align != 32) {
          data_in >>= base_align*subword_addr;
          data_in &= (1<<s.align)-1;
        }
      } else {
        assert(s.align == -1 || s.align == 32);
      }

      auto [mem, ret] = ops.at(s.tx_data.op)(data_in, s.tx_data.data);

#if DBG_PMU
      if (t == k_txR)  {
        cout << path << name
             << " Process " 
             << setw(6) << "read" << " "
             << setw(8) << s.tx_data.addr << " at cycle " 
             << setw(8) << cycle << " " 
             << setw(6) << s.tx_data.addr << "->" << setw(6) << s.tx_data.data << endl;
      } else if (t == k_txW) {
        cout << path << name
             << "Process " 
             << setw(6) << "write" << " "
             << setw(8) << s.tx_data.addr << " at cycle " 
             << setw(8) << cycle << " " 
             << setw(6) << s.tx_data.addr << "<-" << setw(6) << s.tx_data.data << endl;
      } else if (t == k_txRMW) {
        fmt::print("{}/{:10} Process modify at cycle {:6}: a{:6}={:6}/{:1} ({:2}/{:2})<- [{:6}({:8},{:8}) -> {:8}, {:8}]\n",
            path, name, cycle,
            s.tx_data.addr,
            word_addr,
            subword_addr,
            base_align, s.align,
            op_names[s.tx_data.op], 
            data_in, s.tx_data.data, mem, ret);
      }
#endif
      s.tx_data.data = ret;

      auto to_mem = mem;
      if constexpr (std::is_integral<T>::value) {
        if (s.align > 0 && s.align < 32) {
          to_mem &= (1<<s.align)-1;
          auto mask = ~(((1<<s.align)-1)<<(base_align*subword_addr));
          to_mem = (banks.at(word_addr)&mask) | (to_mem << (base_align*subword_addr));
        }
      }
      banks.at(word_addr) = to_mem;
      b.CompleteTX(s);
    }
  }
  // Returns true if a cycle should be stolen for allocation
  bool TryAlloc() {
    bool did_alloc{false};
    bool did_dealloc{false};
    for (int i=0; i<in_ports.size(); i++) {
      if (in_ports[i]->Valid() && ((!did_alloc && (port_types[i] == kAlloc && (alloc_bufs.size() >= in_ports[i]->Read().nvalid))) || (!did_dealloc && port_types[i] == kFree)) && out_ports[i]->Ready()) {
        assert(in_ports[i]->Valid());
        if (port_types[i] == kAlloc) {
          did_alloc = true;
          std::vector<int> alloc;
          for (int j=0; j<in_ports[i]->Read().nvalid; j++) {
            int ret = alloc_bufs.front();
#if DBG_PMU || DEBUG_ALLOC
            cout << path << name
                 << " Allocate " 
                 << setw(8) << ret <<  " done: " << in_ports[i]->Read().done_vec
                 // << " Inflight: " << inflight.size() 
                 // << " alloc_bufs.size() before: " << alloc_bufs.size() 
                 << endl;
#endif
            assert(!inflight.count(ret));
            inflight.insert(ret);
            alloc.emplace_back(ret);
            alloc_bufs.pop_front();
          }
          Token t = in_ports[i]->ReadPop();
          for (int j=0; j<alloc.size(); j++)
            t.intVec_[j] = alloc[j];
          out_ports[i]->Push(t);
        } else {
          assert(port_types[i] == kFree);
          did_dealloc = true;
          auto tok = in_ports[i]->ReadPop();
          for (int j=0; j<tok.nvalid; j++) {
            int to_free = (int)tok[j];
            if (to_free >= 0) {
#if DBG_PMU || DEBUG_ALLOC
              cout << path << name
                   << " Free " 
                   << setw(8) << to_free
                   // << " Inflight: " << inflight.size() 
                   // << " count= " << inflight.count(to_free) 
                   // << " alloc_bufs.size() before: " << alloc_bufs.size() 
                   << endl;
              // if (inflight.size() < 128) {
                // cout << path << name
                     // << " Inflight: ";
                // for (auto b : inflight)
                  // cout << " " << b;
                // cout << endl;
              // }
#endif
              assert((inflight.count(to_free) == 1) && "double free");
              inflight.erase(to_free);
              assert((inflight.count(to_free) == 0) && "check that we actually did free this value");
              alloc_bufs.push_front(to_free);
            }
          }
          Token t = tok;
          for (int j=0; j<16; j++)
            (int&)t[j] = 0;
          out_ports[i]->Push(t);
        }
      }
    }
    return did_alloc || did_dealloc;
  }
  void TryExecuteDense(int& next, bool write) {
    int next_tmp{next};
    for (int i=0; i<in_ports.size(); i++) {
      int ii = (i+next)%in_ports.size();
      if (in_ports[ii]->Valid() 
          && (port_types[ii] != kWrD)
          && (port_types[ii] != kWrA || write)
          && (port_types[ii] != kRd || !write)
          && (port_types[ii] != kRMWD)
          && (port_types[ii] != kAlloc)
          && (port_types[ii] != kFree)
          && dense_status[ii]
          && (port_types[ii] == kRd || in_ports[ii+1]->Valid())) {
        if (lut_name != "" && !lut_loaded) {
          fmt::print(stderr, "Read from unloaded LUT: {}\n", lut_name);
          assert(false);
        }
        next_tmp = ii+1;
        in_queue_for_port[ii]++;

        int align = alignments[ii];
        assert(align >= base_align);
        assert(align % base_align == 0);
        int div = align/base_align;

#if DBG_PMU
        fmt::print("{}/{}: Executing a dense operation (write={},align={},div={}) from port {}\n", path, name, write, align, div, ii);
#endif

        if (write)
          assert(port_types[ii] == kWrA && port_types[ii+1] == kWrD);
        else
          assert(port_types[ii] == kRd);

        Token addr_tok = in_ports[ii]->ReadPop();
        Token data_tok;
        if (write)
          data_tok = in_ports[ii+1]->ReadPop();

        Token ret_tok = addr_tok;
        if (write)
          ret_tok.type = data_tok.type;
        
        std::set<int> banks_touched;
        for (int i=0; i<addr_tok.nvalid; i++) {
          int addr_loc = _to_loc((int)addr_tok[i]);
          int subword_addr = addr_loc % div;
          int word_addr = addr_loc/div;
          int bank = _to_bank(addr_loc);
          if (word_addr >= 0) {

            if (write) {
              words_written++;
              assert(!any_rmw_write);
#if DBG_PMU
              fmt::print("{}/{}: \tWrite [{}] <- {}\n", path, name, word_addr, (T)data_tok[i]);
#endif
              banks.at(word_addr) = (T)data_tok[i];
              assert(align == 32);
              (int&)ret_tok[i] = 1;
            } else {
              words_read++;
              assert(!any_rmw_read);
              auto data_in = banks.at(word_addr);

              if constexpr (std::is_integral<T>::value) {
                if (align > 0 && align != 32) {
                  data_in >>= base_align*subword_addr;
                  data_in &= (1<<align)-1;
                }
              } else {
                assert(align == -1 || align == 32);
              }

              (T&)ret_tok[i] = data_in;
#if DBG_PMU
              fmt::print("{}/{}: \tRead {} <- [{}]\n", path, name, data_in, word_addr);
#endif
            }
            assert(!banks_touched.count(bank));
            banks_touched.insert(bank);
          }
        }
        out_ports[ii]->Push(ret_tok);
      }
    }
    next = next_tmp;
  }
  void TryAddPipe(ROBuf& b, tx_type t, int& next, bool restrict_read, bool restrict_write) {
    assert(t == k_txRMW);
    int next_tmp{next};
    int in_added{0};
    for (int i=0; i<in_ports.size(); i++) {
      int ii = (i+next)%in_ports.size();
      if (in_added < io_max
          && in_ports[ii]->Valid() 
          && (port_types[ii] != kWrD)
          && (!restrict_write || (port_types[ii] == kWrA))
          && (!restrict_read || (port_types[ii] == kRd))
          && (port_types[ii] != kRMWD)
          && (port_types[ii] != kAlloc)
          && (port_types[ii] != kFree)
          && !dense_status[ii]
          && (port_types[ii] == kRd || in_ports[ii+1]->Valid()) 
          && b.CanAddTX(ii)) {
        if (lut_name != "" && !lut_loaded) {
          fmt::print(stderr, "Read from unloaded LUT: {}\n", lut_name);
          assert(false);
        }
        next_tmp = ii+1;
        in_added++;
        in_queue_for_port[ii]++;
#if DBG_PMU
        cout << path << name << " Add input " << ii << " " << in_ports[ii]->Read() << " type: ";
        if (port_types[ii] == kRd) 
          cout << "read";
        else if (port_types[ii] == kWrA)
          cout << "write";
        else if (port_types[ii] == kRMWA) 
          cout << "modify";
        cout << endl;
        if (port_types[ii] != kRd)
          cout << path << name << " Data: " << in_ports[ii+1]->Read() << endl;
#endif
        int op_idx = op_indices[ii];
        bool force_coal{false};
        bool force_coal_inc{false};

        int ro_mode = ro_modes[ii];
        int align = alignments[ii];

        const auto& addr_tok = in_ports[ii]->Read();
        if (port_types[ii] == kRd) {
          Token tok;
          tok.type=in_ports[ii]->Read().type;
          for (int j=0; j<16; j++) {
            (T&)tok[j] = 0;
          }
          _add_tx(b, ii, &in_ports[ii]->Read(), &tok, op_idx, ro_mode, force_coal, force_coal_inc, align); 
        } else {
          _add_tx(b, ii, &in_ports[ii]->Read(), &in_ports[ii+1]->Read(), op_idx, ro_mode, force_coal, force_coal_inc, align); 
        }

        in_ports[ii]->Pop();
        if (port_types[ii] != kRd)
          in_ports[ii+1]->Pop();
      }
    }
    next = next_tmp;
  }
  bool lut_loaded{false};
 public:
  string lut_name;
  explicit SparsePMU(const std::string& _name, int max_alloc=1, int base_align=32, int tile=0, bool split_read_write=false, const std::string& lut_name="") :
    SparseOuter(_name), base_align(base_align), lut_name(lut_name), split_read_write(split_read_write), max_alloc(max_alloc), tile(tile) {
      assert((base_align & (base_align-1)) == 0);
      AddChild(&reorder_rmw);
      AddChild(&reorder_read);
      AddChild(&reorder_write);
      reorder_rmw.SetParameter("stages",to_string(RO_STAGE));
      reorder_read.SetParameter("stages",to_string(RO_STAGE/2));
      reorder_write.SetParameter("stages",to_string(RO_STAGE/2));
      AddChild(&stats);
      stats.CreateCounter("reads");
      stats.CreateCounter("writes");
    for (int i=0; i<N*B; i++)
      banks[i] = T(-1);

    for (int i=0; i<max_alloc; i ++)
      alloc_bufs.emplace_back(i);
    std::reverse(alloc_bufs.begin(), alloc_bufs.end());

    if (split_read_write)
      EnableLineTrace(0, 0, 16, 16, 8, 0, 8);
    else
      EnableLineTrace(0, 0, 16, 16, 16, 0, alloc_bufs.size());

  }
  void Set(T v, int a, int align) {
    assert(align == 32);
    lut_loaded = true;
    banks.at(a) = v;
    // for (int i=0; i<65536; i += 65536/max_alloc) 
      // banks.at(a+i) = v;
  }
  void Fill(int v=0) {
    for (auto& a : banks)
      a = T(v);
  }

  // Returns one port (token input, addr output)
  SparsePMUPort *GetAllocPort(int k=-1) {
    auto ret = GetPort("al_in_A", "al_out_A", kAlloc, 3*k, -1, _get_order("ordered"), 1);
    OrderUpdateHelper();
    return ret;
  }
  SparsePMUPort *GetFreePort(int k=-1) {
    auto ret = GetPort("free_in_A", "free_out_A", kFree, 3*k, -1, _get_order("ordered"), 1);
    OrderUpdateHelper();
    return ret;
  }

  // Returns one port (addr input, data output)
  SparsePMUPort *GetReadPort(int k=-1, const string& order="unordered", int align=32, bool dense=false) {
    auto ret = GetPort("rd_in_A", "rd_out_D", kRd, 3*k, op_name_to_idx["read"], _get_order(order), 1, align, dense);
    OrderUpdateHelper();
    return ret;
  }
  // Returns two ports (addr input, done output) and (data input)
  pair<SparsePMUPort*,SparsePMUPort*> GetWritePorts(int k=-1, const string& order="unordered", int align=32, bool dense=false) {
    auto a = GetPort("wr_in_A", "wr_out", kWrA, 3*k, op_name_to_idx["write"], _get_order(order), 1, align, dense);
    auto b = GetPort("wr_in_D", "wr_invalid", kWrD, 3*k+1, -1, -1, 0);
    OrderUpdateHelper();
    return make_pair(a,b);
  }
  pair<SparsePMUPort*,SparsePMUPort*> GetRMWPorts(const string& op, const string& order="unordered", int k=-1, int align=32) {
    if (!op_name_to_idx.count(op))
      throw module_error(this, string("unknown op: ")+op);
    rmw_en = true;
    int op_idx = op_name_to_idx[op];
    auto a = GetPort("rmw_in_A", "rmw_out", kRMWA, 3*k, op_idx, _get_order(order), 1, align);
    auto b = GetPort("rmw_in_D", "rmw_invalid", kRMWD, 3*k+1, -1, -1, 0);
    OrderUpdateHelper();
    return make_pair(a,b);
  }
  void UpdateROMode(int k, const string& order) {
    int ii = LookupPort(k);
    ro_modes.at(ii) = _get_order(order);
    OrderUpdateHelper();
  }

  void Eval() {
    // Do all steps in reverse pipeline order, to avoid incorrect bypassing 
    // First, dequeue from the pending tx pipeline (if possible)

    if (split_read_write) {
      ProcessOutput(reorder_read, k_txRMW);
      ProcessOutput(reorder_write, k_txRMW);
      ProcessScalar(reorder_read, k_txRMW);
      ProcessScalar(reorder_write, k_txRMW);
    } else {
      ProcessOutput(reorder_rmw, k_txRMW);
      ProcessScalar(reorder_rmw, k_txRMW);
    }

    bool any_alloc = TryAlloc();

    if (split_read_write) {
      TryAddPipe(reorder_read, k_txRMW, next_in_read, true, false);
      TryAddPipe(reorder_write, k_txRMW, next_in_write, false, true);
    } else {
      TryAddPipe(reorder_rmw, k_txRMW, next_in_rmw, false, false);
    }
    TryExecuteDense(next_dense_read, false);
    TryExecuteDense(next_dense_write, true);

    if (!any_alloc) {
      if (split_read_write) {
        reorder_read.Tick();
        reorder_write.Tick();
      } else {
        reorder_rmw.Tick();
      }
    }

    if (split_read_write) {
      LineTrace(0, 0, words_read, words_written, reorder_read.BufDepth(), 0, reorder_write.BufDepth());
    } else {
      LineTrace(0, 0, words_read, words_written, reorder_rmw.BufDepth(), 0, alloc_bufs.size());
    }
    words_read = words_written = 0;

    // Finally, process inputs to the pending tx pipeline
    cycle++;
  }

  void Apply(const string& cmd, const vector<string>& data) {
    if (cmd == "dump") {
      cout << "PMU RMW reordering pipeline state" << endl;
      reorder_rmw.Dump(cout);
    } else {
      throw module_error(this, "unknown command");
    }
  }

  void SetParameter(const std::string& key, const std::string& val) override {
    if (key == "alloc_type" || key == "alloc_splits" || key == "alloc_iter" || key == "dualbank" || key == "BE_stages" || key == "BE_max_val") {
      reorder_rmw.SetParameter(key, val);
      reorder_read.SetParameter(key, val);
      reorder_write.SetParameter(key, val);
    } else if (key == "stages") {
      reorder_rmw.SetParameter("stages", to_string(stoi(val)));
      reorder_read.SetParameter("stages", to_string(stoi(val)/2));
      reorder_write.SetParameter("stages", to_string(stoi(val)/2));
    } else if (key == "override_romode") {
      cout << path << name << "WARNING! This may cause incorrect behavior." << endl;
      auto t = _get_order(val);
      for (auto& [k, p] : ports) {
        int idx = p->index;
        cout << path << name << "\tupdate port: " << idx << endl;
        ro_modes.at(idx) = t;
      }
      OrderUpdateHelper();
    } else {
      SparseOuter::SetParameter(key, val);
    }
  }
};

#endif
