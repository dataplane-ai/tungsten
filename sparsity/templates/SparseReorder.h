#ifndef SPARSE_REORDER_H_
#define SPARSE_REORDER_H_

#include <array>
#include <deque>
#include <optional>
#include <functional>
#include <vector>
#include <tuple>
#include <algorithm>
#include <string>

#include "token.h"
#include "state.h"
#include "interface.h"
#include "statistics.h"

#include "sparsity/templates/Allocator.h"

#include "debug.h"

#ifdef NDEBUG
#undef NDEBUG
#endif
#include <cassert>

using namespace std;

template <class To, class From>
typename std::enable_if<
    (sizeof(To) == sizeof(From)) &&
    std::is_trivially_copyable<From>::value &&
    std::is_trivial<To>::value,
    // this implementation requires that To is trivially default constructible
    To>::type
// constexpr support needs compiler magic
bit_cast(const From &src) noexcept
{
    To dst;
    std::memcpy(&dst, &src, sizeof(To));
    return dst;
}

template<typename T>
struct RMWShared {
  // Takes memory, data as input and returns memory, data, success
  using AtomOp = function<pair<T,T>(T,T)>;
  map<string,int> op_name_to_idx;
  vector<string> op_names;;
  vector<AtomOp> ops;
  void _add_op(const string& name, AtomOp op) {
    int id = ops.size();
    op_names.push_back(name);
    ops.push_back(op);
    op_name_to_idx[name] = id;
  }
  RMWShared() {
    _add_op("noop",  [](T m, T d) { return make_pair(m,   m); });
    _add_op("write", [](T m, T d) { return make_pair(d,   d); });
    _add_op("read",  [](T m, T d) { return make_pair(m,   m); });
    _add_op("add",   [](T m, T d) { return make_pair(d+m, d+m); });

    _add_op("addi",  [](T m, T d) { return make_pair(d+m, d+m); });
    _add_op("addf",  [](T m, T d) { return make_pair(d+m, d+m); });

    _add_op("maxi",  [](T m, T d) { return make_pair(d+m, d+m); });
    _add_op("maxf",  [](T m, T d) { return make_pair(d+m, d+m); });

    _add_op("mini",  [](T m, T d) { return make_pair(d+m, d+m); });
    _add_op("minf",  [](T m, T d) { return make_pair(d+m, d+m); });

    for (int i=1; i<32; i*=2) {
      _add_op("subwr"+std::to_string(i), [=](T m, T d) { 
          auto mm = (unsigned)m;
          auto dd = (unsigned)d;
          auto off = (dd>>16)*i;
          auto mask = ~(((1<<i)-1)<<off);
          auto ret = (mm&mask) | ((dd&0xFFFF)<<off);
          return make_pair((T)ret, (T)ret); });
    }

    _add_op("inc",   [](T m, T d) { return make_pair(m+d, m+1); });
    _add_op("dec",   [](T m, T d) { return make_pair(m-1, m-1); });
    _add_op("or",    [](T m, T d) { 
        auto ret = bit_cast<uint32_t>(d) | bit_cast<uint32_t>(m);
        return make_pair(ret, ret); 
    });
    _add_op("and",    [](T m, T d) { 
        auto ret = bit_cast<uint32_t>(d) & bit_cast<uint32_t>(m);
        return make_pair(ret, ret); 
    });
    _add_op("and_old",    [](T m, T d) { 
        auto ret = bit_cast<uint32_t>(d) & bit_cast<uint32_t>(m);
        return make_pair(ret, m); 
    });
    _add_op("read_clear_nz",    [](T m, T d) { 
        if (d)
          return make_pair(T(), m);
        else
          return make_pair(m, m);
    });
    _add_op("swap",  [](T m, T d) { return make_pair(d,   m); });
    _add_op("or_changed",    [](T m, T d) { 
        auto ret = bit_cast<uint32_t>(d) | bit_cast<uint32_t>(m);
        return make_pair(ret, ret != m); 
    });
    _add_op("write_neg", [](T m, T d) { 
        if (m < 0) {
          return make_pair(d,   1);
        } else {
          return make_pair(m,   0);
        }
    });
    _add_op("min_changed",    [](T m, T d) { 
        if (d < m ) {
          return make_pair(d, true); 
        } else {
          return make_pair(m, false); 
          }
    });
    _add_op("kill_zero", [](T m, T d) {assert(false); return make_pair(d, d);});
  }
};

class SparseOuter : public Module {
 protected:

  enum port_type {kRd,kWrA,kWrD,kRMWA,kRMWD,kAlloc,kFree};

  int next_in {0};
  int next_in_read {0};
  int next_in_write {0};
  int next_in_rmw {0};

  int bank_shift {32};
  int bank_id {0};


  inline int _trans_addr(int addr) {
    if (addr < 0)  {
#ifdef DEBUG_REORDER
      cout << path << name << " addr invalid" << endl;
#endif
      return addr;
    } else if (bank_shift == 32) {
#ifdef DEBUG_REORDER
      cout << path << name << " addr " << addr << " -> " << addr << endl;
#endif
      return addr;
    } else if (((addr >> bank_shift) == bank_id)) {
      int ret = addr - ((addr>>bank_shift)<<bank_shift);
#ifdef DEBUG_REORDER
      cout << path << name << " addr " << addr << " -> " << ret << endl;
#endif
      return ret;
    } else {
#ifdef DEBUG_REORDER
      cout << path << name << " addr " << addr << " no match" << endl;
      cout << path << name << " cmp " <<  (addr >> bank_shift) << endl;
#endif
      return -1;
    }
  }

  vector<port_type>      port_types;
  vector<int>            op_indices;
  vector<int>            ro_modes;
  vector<int>            alignments;
  vector<int>            dense_status;
  vector<FIFO<Token,2>*> in_ports;
  vector<FIFO<Token,2>*> out_ports;
  bool _ready(int idx) const { return in_ports[idx]->Ready(); }
  void _push(int idx, const Token& v) { in_ports[idx]->Push(v); }
  bool _valid(int idx) const { return out_ports[idx]->Valid(); }
  void _pop(int idx) { out_ports[idx]->Pop(); }
  const Token& _read(int idx) { return out_ports[idx]->Read(); }
 public:

  void SetBank(int _bank_id, int _bank_shift=16) {
    cout << name << " set bank " << _bank_id << " shift " << _bank_shift << endl;
    bank_id = _bank_id;
    bank_shift = _bank_shift;
  }

  SparseOuter(const std::string& _name) : Module(_name) {
  }
  class SparsePMUPort : public CheckedSend<Token>, public CheckedReceive<Token> {
    SparseOuter* parent;
   public:
    int index;
    SparsePMUPort(SparseOuter *_parent, int _index) : parent(_parent), index(_index) {
    }
    bool Ready() const { return parent->_ready(index); }
    void Push(const Token& v) { parent->_push(index, v); }
    bool Valid() const {return parent->_valid(index); }
    void Pop() {parent->_pop(index); }
    const Token& Read() const {return parent->_read(index); }
  };
  virtual void SetPreDrop(int n) {
    throw module_error(this, "call this on DRAM!");
  }

 protected:
  map<int,SparsePMUPort*>           ports;
  int LookupPort(int k) {
    assert(ports.count(k));
    auto p = ports[k];
    return p->index;
  }
  SparsePMUPort *GetPort(const string& in, const string& out, port_type type, int k, int op=-1, int ro_mode=-1, int bubble=0, int align=-1, bool dense=false) {
    assert(in_ports.size() == out_ports.size());
    if (k >= 0 && ports[k])
      return ports[k];
    in_ports.push_back(new FIFO<Token,2>(in+to_string(in_ports.size())));
    out_ports.push_back(new FIFO<Token,2>(out+to_string(out_ports.size())));
    port_types.push_back(type);
    op_indices.push_back(op);
    ro_modes.push_back(ro_mode);
    alignments.push_back(align);
    dense_status.push_back(dense);
    AddChild(in_ports.back());
    AddChild(out_ports.back());
    if (k >= 0) {
      ports[k] = new SparsePMUPort(this, in_ports.size()-1);
      return ports[k];
    } else {
      return new SparsePMUPort(this, in_ports.size()-1);
    }
  }
};

// This implements a generic sparse reordering pipeline, used for both the PMU
// and the locking unit.
template<typename T, size_t N, size_t B>
class SparseReorder : public Module {
  Allocator<B,B,4> alloc;
  vector<int> alloc_splits=ALLOC_SPLITS_MULTI;
  vector<int> alloc_iter=ALLOC_ITER_MULTI;
  CounterStatistic stats {"stats"};
  CounterStatistic iss_hist {"iss_hist"};
  CounterStatistic stage_occ_hist {"stage_occ_hist"};
  CounterStatistic req_hist {"req_hist"};

  int tput{1};
 public:
  struct sTX {
    T        tx_data;
    uint32_t addr;
    uint32_t align;
    int      TX_num;
    int      TX_lane;
    uint64_t in_cycle;
  };
 private:
  struct TX {
    array<bool,B>     valid;
    array<bool,B>     issued {false};
    array<bool,B>     complete {false};
    array<int,B>      banks;
    int               id;
    int               num;
    int               stats_key {0};
    int               nvalid {0};

    int               align;

    array<int,B>      addr;
    array<int,B>      coal_from{-1};
    array<int,B>      coal_data{0};
    array<T,B>        tx_data;
    int               done;
    bool              fully_order{false};
    bool              seq_last{false};
  };

  enum accum_state_t {k_empty, k_filling, k_full};

  struct InputCTX {
    int bubble_res{1};
    int in_pipe{0};
    int align;

    deque<TX> pend_split;
    bool fully_order{false};
    bool coal{false};
    bool coal_inc{false};
    int id{-1};
    accum_state_t accum_state;
    array<T,B> accum;
    int done_vec{0};
    int nvalid{0};

    bool SplitHeldOver() {
      return pend_split.size() && (bubble_res || pending_tx.size() + fully_order_tx.size() + _tot_bubbles() < max_tx);
    }
  };

  map<int,InputCTX> ctxs;
  vector<int>	ctx_indices;
  int next_ctx{0};

  int _tot_bubbles() const {
    int ret{0};
    for (auto& [p,c] : ctxs) {
      assert(c.bubble_res >= 0);
      ret += c.bubble_res;
      //cout << path << name << " Context: " << c.id << " bubbles: " << c.bubble_res << endl;
    }
    //cout << path << name << " Total bubbles: " << ret << endl;
    return ret;
  }


  uint64_t cycle {0};
  int next_TX_num {0};

  //deque<TX> pending_split_tx;
  deque<TX> pending_tx;
  deque<TX> fully_order_tx;
  deque<sTX> scheduled_tx;

  //set<int> fully_order_ports;

  int max_tx      {RO_STAGE};
  //int alloc_iter {ALLOC_ITER};
  //int alloc_iter_high {ALLOC_ITER_HIGH};
  alloc_type_t alloc_type {ALLOC_TYPE};
  alloc_prio_t alloc_prio {kStatic};
  //alloc_prio_t alloc_prio {kRand};

  void _add_tx(TX& t, int lane, int addr=-1) {
    assert(!t.complete[lane]);
    assert(t.valid[lane]);
    sTX s;
    s.addr = addr;
    s.align = t.align;
    s.tx_data = t.tx_data[lane];
    // if (t.coal_from[lane] < 0) {
      // s.tx_data.data += t.coal_data[lane];
      // s.tx_data.data = t.coal_data[lane];
    // }
    s.TX_num = t.num;
    s.TX_lane = lane;
    s.in_cycle = cycle + pipe_delay;
    scheduled_tx.push_back(s);
    t.issued[lane] = true;
#if DBG_PMU
    cout << "\tSchedule tx (l: " << lane << ") (a: " << t.addr[lane] << ") (d: " << s.tx_data.data << ")" << endl;
#endif
  }

  array<T,B> default_accum() {
    array<T,B> ret;
    for (int i=0; i<B; i++) {
      ret[i] = T();
    }
    return ret;
  }

  void AccumTX(TX& t) {
    assert(IsAccumTX(t.id));
    assert(!AccumTXDone(t.id));
    auto& c = ctxs[t.id];
    //cout << "Accum vecTX" << endl;
    for (int i=0; i<B; i++) {
      if (t.valid[i]) {
        c.accum[i] = t.tx_data[i];
      }
      if (t.coal_from[i] >= 0) {
        assert(false); // for testing
        c.accum[i] = c.accum[t.coal_from[i]];
        c.accum[i].data += t.coal_data[i];
      }
    }
    if (t.seq_last) {
      //cout << "\t\tDONE" << endl;
      c.accum_state = k_full;
      c.nvalid = t.nvalid;
      c.done_vec = t.done;
    }
  }

 public:
  void SetVecTput(int x) {
    tput = x;
  }
  int Size() {
    return max_tx;
  }
  int BufDepth() {
    return pending_tx.size();
  }

  bool AccumTXDone(int ID) {
    assert(ID >= 0);
    auto& c = ctxs.at(ID);
    if (!IsAccumTX(ID))
      return false;
    return c.accum_state == k_full;
  }

  bool IsAccumTX(int ID) {
    assert(ID >= 0);
    auto& c = ctxs.at(ID);
    return c.fully_order;
  }

  SparseReorder(const string& name) : Module(name) {
    AddChild(&stats);
    AddChild(&req_hist);
    AddChild(&iss_hist);
    AddChild(&stage_occ_hist);
  }
  void SetDepth(int d) {
    if (d < 0) {
      throw std::runtime_error("invalid depth");
    }
    max_tx = d;
  }

  bool next_cyc_fully_order{false};

  int be_stages{2};
  int be_max_val{5};

  // This is designed to be called at the end of the parent's Eval() method
  void Tick() {
    // Calculate histograms about the various entries currently in the pipeline
    stage_occ_hist.AddToDenseHist(pending_tx.size());
    map<int,int> stats_keys;
    for (const auto& p : pending_tx)
      stats_keys[p.stats_key]++;
    for (const auto& [d,c] : stats_keys)
      stage_occ_hist.AddTo2DHist(d,c);

    if ((next_cyc_fully_order && fully_order_tx.size()) || !pending_tx.size()) {
      // TODO: may want to support retriable transactions with full ordering.
      // However, this will require waiting for each transaction to complete, 
      // which probably makes it a relatively undesirable case. Can elaborate
      // on that in the paper.
      next_cyc_fully_order = false;
      for (auto& t : fully_order_tx) {
        bool any_rem{false};
        for (int i=0; i<B; i++)
          any_rem |= (t.valid[i] && !t.issued[i]);
        if (!any_rem)
          continue;

        array<bool,B> bank_sched{false};

        for (int i=0; i<B; i++) {
          if (!t.valid[i] || t.issued[i])
            continue;
          if (bank_sched[t.banks[i]])
            break;
          bank_sched[t.banks[i]] = true;
          _add_tx(t, i, t.addr[i]);
        }
        break;
      }
    } else {
      next_cyc_fully_order = true;
      int stage = 0;
      if (alloc_type != kLimit) {
        for (auto& t : pending_tx) {
          int prio;
          for (prio=0; prio<alloc_splits.size(); prio++) {
            if (stage < alloc_splits[prio]) {
              break;
            }
          }

          bool req_this_stage{false};
          for (int i=0; i<B; i++) {
            if (t.valid[i] && !t.issued[i]) {
              assert(!t.complete[i]);
              req_this_stage = true;
              alloc.Request(i,t.banks[i], prio);
            }
          }
          stage++;
          if (alloc_type == kNone && req_this_stage)
            break;
        }
      }

      req_hist.AddToDenseHist(alloc.NumReqs());
      if (alloc_type == kNone) 
        alloc.Allocate(1, kAugmenting, kStatic);
      else
        alloc.Allocate(-1, alloc_type, alloc_prio, alloc_iter);
      iss_hist.AddToDenseHist(alloc.NumGrants());

      // For debugging
      array<bool,B> bank_sched {false};
      if (alloc_type == kLimit) {
        for (auto &t : pending_tx) {
          for (int lane=0; lane<B; lane++) {
            if (!t.valid[lane] || t.complete[lane] || t.issued[lane])
              continue;
            t.issued[lane] = true;
            _add_tx(t, lane);
          }
        }
      } else {
        while (auto g = alloc.GetGrant()) {
          int lane = g->first;
          int bank = g->second;
          stage = 0;
          for (auto& t : pending_tx) {
            if (!t.valid[lane] 
              || t.issued[lane] 
              || t.banks[lane] != bank) {
              stage++;
              continue;
            }
            assert(!bank_sched[t.banks[lane]]);
            bank_sched[t.banks[lane]] = true;
            _add_tx(t, lane, t.addr[lane]);
            break;
          }
        }
      }
    }

    assert((pending_tx.size()+fully_order_tx.size()+_tot_bubbles()) <= max_tx);
    int n_added{0};
    int next_ctx_pend{next_ctx};
    for (int i=0; i<ctx_indices.size(); i++) {
      int ii = (i+next_ctx)%ctx_indices.size();
      auto& c = ctxs[ctx_indices[ii]];
      if (c.pend_split.size() && !c.fully_order) {
	bool can_add{(pending_tx.size()+fully_order_tx.size()+_tot_bubbles() < max_tx) || c.bubble_res};
	auto& t = c.pend_split.front();
	if (c.bubble_res) {
	  assert(pending_tx.size() + fully_order_tx.size() < max_tx);
	}
	if (can_add) {
	  pending_tx.push_back(t);
	  c.pend_split.pop_front();
	  next_ctx_pend=ii+1;
	  if (c.bubble_res)
	    c.bubble_res--;
          if (n_added++ >= tput)
            break;
	}
      } else if (c.pend_split.size() && c.fully_order) {
	bool can_add{(fully_order_tx.size()+pending_tx.size()+_tot_bubbles() < max_tx) || c.bubble_res};
	auto& t = c.pend_split.front();
	if (c.bubble_res) {
	  assert(fully_order_tx.size() + pending_tx.size() < max_tx);
	}
        if (can_add) {
          fully_order_tx.push_back(t);
          c.pend_split.pop_front();
          next_ctx_pend=ii+1;
          if (c.bubble_res)
            c.bubble_res--;
          if (n_added++ >= tput)
            break;
        }
      }
    }
    next_ctx = next_ctx_pend;
    assert((pending_tx.size()+fully_order_tx.size()+_tot_bubbles()) <= max_tx);
    cycle++;
  }

  void Dump(ostream& str) {
    str << "\tPending transactions" << endl;
    for (const auto& t : pending_tx) {
      str << "\t" << setw(8) << t.id << "/" << setw(8) << t.num << ": ";
      for (int i=0; i<B; i++) {
        if (t.valid[i]) {
          str << "[" << (t.issued[i] ? "I" : "-") 
              << (t.complete[i] ? "C" : "-")
              << setw(5) << t.addr[i] << "/"
              << setw(2) << t.banks[i] << " " << t.tx_data[i].str() << "] ";
        } else {
          str << setw(15) << " ";
        }
      }
      str << endl;
    }
    str << "\tScheduled transactions\t" << endl;
    for (const auto& t : scheduled_tx) {
      str << "(" << t.TX_num << "/" << t.TX_lane << ")";
    }
    str << endl;
  }

  int pipe_delay  {2};
  bool ScalTXReady() {
    return scheduled_tx.size() && scheduled_tx.front().in_cycle <= cycle;
  }
  sTX GetNextScalTX() {
    assert(ScalTXReady());
    sTX ret = scheduled_tx.front();
    scheduled_tx.pop_front();
    return ret;
  }
  void CompleteTX(const sTX& s) {
    for (auto& t : fully_order_tx) {
      if (t.num != s.TX_num || !t.valid[s.TX_lane])
        continue;
//#if DBG_PMU
      //cout << "Complete against fully ordered pipe" << endl;
//#endif
      t.complete[s.TX_lane] = true;
      t.tx_data[s.TX_lane] = s.tx_data;
      return;
    }
    for (auto& t : pending_tx) {
      if (t.num != s.TX_num || !t.valid[s.TX_lane])
        continue;
//#if DBG_PMU
      //cout << "Complete against reordering pipe" << endl;
//#endif
      t.complete[s.TX_lane] = true;
      t.tx_data[s.TX_lane] = s.tx_data;
      return;
    }
    assert(false);
  }
  // Put this transaction back at the head of the queue, and stall the pipeline
  // by pushing all scheduled transactions back by one cycle.
  void RefuseTX(const sTX& s) {
    //int idx = s.TX_num - pending_tx.front().num;
    //assert(idx >= 0);
    //assert(idx < pending_tx.size());
    for (auto& t : pending_tx) {
      if (t.num != s.TX_num)
	continue;
      t.issued[s.TX_lane] = false;
    }
    //TX& t = pending_tx[idx];
    //t.issued[s.TX_lane] = false;
  }
  bool AccumPartialTX(int ID) {
    assert(ID >= 0);
    auto& c = ctxs.at(ID);
    if (!c.fully_order)
      return false;
    if (c.accum_state == k_full)
      return false;
    if (!VecTXDone(ID))
      return false;
    // At this point, we know that we have an accumulation port,
    // that the accumulation port is not full, and that we have a done vector
    // headed to that accumulation port.
    for (auto& t : pending_tx) {
      // Find the vector
      if (t.id == ID) {
        AccumTX(t);
        break;
      }
    }
    // After we've accumulated the done vector, throw it away.
    GetDoneVecTX(ID);
    return true;
  }
  bool VecTXDone(int ID=-1) const {
    if (!pending_tx.size() && !fully_order_tx.size())
      return false;
    for (const TX& t : pending_tx) {
      // Skip forward to the first transaction for this ID
      if (ID >= 0 && t.id != ID)
        continue;
      // We've found a transaction
      for (int i=0; i<B; i++) {
        if (!t.valid[i])
          continue;
        // Return false if the first transaction for the output port is not complete
        if (!t.complete[i])
          return false;
      }
      // First transaction for the output port is complete
      return true;
    }

    for (const TX& t : fully_order_tx) {
      // Skip forward to the first transaction for this ID
      if (ID >= 0 && t.id != ID)
        continue;
      // We've found a transaction
      for (int i=0; i<B; i++) {
        if (!t.valid[i])
          continue;
        // Return false if the first transaction for the output port is not complete
        if (!t.complete[i])
          return false;
      }
      // First transaction for the output port is complete
      return true;
    }
    // No transactions found for this port
    return false;
  }
  int GetNextVecID() {
    return pending_tx.front().id;
  }

  tuple<int,array<T,B>,int,int> GetDoneAccumTX(int ID) {
    assert(AccumTXDone(ID));
    auto& c = ctxs.at(ID);

    auto ret = make_tuple(ID, c.accum, c.done_vec, c.nvalid);
    c.accum = default_accum();
    c.accum_state = k_empty;
    return ret;
  }

  tuple<int,array<T,B>,int,int> GetDoneVecTX(int ID) {
    assert(VecTXDone(ID));
    for (auto it=pending_tx.begin(); it != pending_tx.end(); it++) {
      if (ID < 0 || it->id == ID) {
        auto ret = make_tuple(it->id, it->tx_data, it->done, it->nvalid);
	for (int i=0; i<B; i++) {
	  if (it->coal_from[i] >= 0) {
	    get<1>(ret)[i]  = get<1>(ret)[it->coal_from[i]];
#if DBG_PMU
            cout << path << name << " Coalesce " << it->coal_from[i] << "->" << i << endl;
            cout << path << name << " " << get<1>(ret)[i].data << "+=" << it->coal_data[i] << endl;
#endif
            get<1>(ret)[i].data += it->coal_data[i];
	  }
	}
        pending_tx.erase(it);
        if (ID >= 0)
          assert(get<0>(ret) == ID);
	assert(ctxs.count(ID));
	assert(!ctxs.at(ID).bubble_res);
	if (!QueueContains(ID, pending_tx)) {
#if DBG_PMU
	  cout << path << name << " emptied queue of " << ID << "; restoring bubble" << endl;
#endif
	  ctxs.at(ID).bubble_res = 1;
          assert((pending_tx.size()+fully_order_tx.size()+_tot_bubbles()) <= max_tx);
	}
        return ret;
      }
    }
    for (auto it=fully_order_tx.begin(); it != fully_order_tx.end(); it++) {
      if (ID < 0 || it->id == ID) {
        auto ret = make_tuple(it->id, it->tx_data, it->done, it->nvalid);
	for (int i=0; i<B; i++) {
	  assert(it->coal_from[i]<0);
	}
        fully_order_tx.erase(it);
        if (ID >= 0)
          assert(get<0>(ret) == ID);
	assert(ctxs.count(ID));
	assert(!ctxs.at(ID).bubble_res);
	if (!QueueContains(ID, fully_order_tx)) {
#if DBG_PMU
	  cout << path << name << " emptied queue of " << ID << "; restoring bubble" << endl;
#endif
	  ctxs.at(ID).bubble_res = 1;
          assert((pending_tx.size()+fully_order_tx.size()+_tot_bubbles()) <= max_tx);
	}
        return ret;
      }
    }
    // We should always return something
    assert(false);
  }
  //int SlotsRem() {
    //return max_tx - pending_tx.size();
  //}
  //int SlotsTaken(int port) {
    //int count=0;
    //for (auto& t : pending_tx) {
      //if (t.id == port)
	//count++;
    //}
    //return count;
  //}
  int QueueContains(int id, const deque<TX>& q) const {
    int ret{0};
    for (const auto& t : q)
      if (t.id == id)
	ret++;
    return ret;
  }
  bool CanAddTX(int id) const {
    const auto& c = ctxs.at(id);
    return c.pend_split.size() == 0;
  }

  void AddCTX(int id, bool addr_order=false, bool best_effort=false, bool fully_order=false, bool coal=false, bool coal_inc = false, int align=-1) {
    bool is_new{true};
    if (ctxs.count(id))
      is_new = false;
    assert(id >= 0);
    cout << "Add ID: " << id << endl;
    auto& c = ctxs[id];
    c.fully_order = fully_order;
    c.coal = coal && !fully_order;
    c.coal_inc = c.coal && coal_inc;
    c.align = align;
    c.id = id;
    if (is_new)
      ctx_indices.push_back(id);
  }

  void AddTX(int last_level, const array<T,B>& data, const array<bool,B>& valid, const array<int,B>& banks, int id, const array<int,B>& addr, bool force_coal, bool force_coal_inc, int nvalid) {
    assert(CanAddTX(id));

    auto& c = ctxs.at(id);
    c.bubble_res--;
    if (c.bubble_res < 0)
      c.bubble_res = 0;

    TX t;
    t.valid = valid;
    t.banks = banks;
    t.addr = addr;
    t.id = id;
    t.stats_key = id;
    t.num = next_TX_num++;
    t.tx_data = data;
    t.done = last_level;
    t.nvalid = nvalid;
    t.align = c.align;
    t.fully_order = c.fully_order;
    for (int i=0; i<B; i++) {
      t.coal_from[i] = -1;
    }

    if (c.coal || force_coal) {
      assert(!c.fully_order);
      map<int,pair<int,int>> coal_map;
      for (int i=0; i<16; i++) {
	auto a = t.addr[i];
        if (a < 0)
          continue;
	if (!coal_map.count(a)) {
	  coal_map[a] = make_pair(i, 1);
	} else {
	   t.coal_from[i] = coal_map.at(a).first;
           if (c.coal_inc || force_coal_inc)
             t.coal_data[i] = coal_map.at(a).second++;
           else
             t.coal_data[i] = 0;
	   t.valid[i] = false;
	}
      }
      for (auto& [addr, count] : coal_map) {
        auto idx = count.first;
        auto data = count.second;
        assert(idx >= 0);
        assert(idx < 16);
#if DBG_PMU
        cout << "Process coalesce map addr: " << addr 
          << " idx: " << idx
          << " data: " << data << endl;
#endif
        if (c.coal_inc || force_coal_inc)
          t.tx_data[idx].data = count.second;
      }
    }
    for (int i=0; i<16; i++) {
      if (t.addr[i] < 0)
        assert(!t.valid[i]);
    }
    if (c.fully_order) {
      // TODO: add this back in
      assert(c.pend_split.size() <= 1);
      // If we're enforcing per-address ordering, split the input vector
      // by address and place input vectors into the pending queue
      array<bool,B> rem_valid = t.valid;
      int num_valid = accumulate(rem_valid.begin(), rem_valid.end(), 0);
      do {
        TX tmp = t;
        tmp.valid = {false};
        set<int> pend_addrs;
        array<bool,B> this_vec_valid{false};
        array<bool,B> bank_alloc{false};
        // Replicate the splitter logic here
        for (int i=0; i<B; i++) {
          if (!rem_valid[i])
            continue;
          assert(tmp.addr[i] >= 0);
          // Stop looking for new addresses to insert into the vector
#if DBG_PMU
          cout << "Check for key " << tmp.addr[i]%LOCK_PROT_SIZE << " addr " << tmp.addr[i] << " count " << pend_addrs.count(tmp.addr[i]%LOCK_PROT_SIZE) << endl;
#endif
          if (c.fully_order && bank_alloc[tmp.addr[i]%B])
            break;
          //pend_addrs.insert(tmp.addr[i]);
          pend_addrs.insert(tmp.addr[i]%LOCK_PROT_SIZE);
          bank_alloc[tmp.addr[i]%B] = true;
          tmp.valid[i] = true;
          rem_valid[i] = false; num_valid--;
        }
        c.pend_split.push_back(tmp);
      } while (num_valid);

      c.pend_split.back().seq_last = true;
    } else {
      c.pend_split.push_back(t);
    }
  }
  void SetParameter(const std::string& key, const std::string& val) {
    if (key == "tx" || key == "stages") {
      SetDepth(stoi(val));
    } else if (key == "alloc_splits") {
      alloc_splits.clear();
      stringstream ss(val);
      string item;
      cout << "Adding splits: " << val << endl;
      while (getline(ss, item, ',')) {
        cout << "\tAdd split: " << stoi(item) << endl;
        alloc_splits.push_back(stoi(item));
      }
    } else if (key == "alloc_iter") {
      alloc_iter.clear();
      stringstream ss(val);
      string item;
      while (getline(ss, item, ',')) {
        alloc_iter.push_back(stoi(item));
      }
    } else if (key == "alloc_type") {
      if (val == "if") {
        alloc_type = kIF;
      } else if (val == "of")  {
        alloc_type = kOF;
      } else if (val == "none") {
        alloc_type = kNone;
      } else if (val == "limit") {
        alloc_type = kLimit;
      } else if (val == "augmenting") {
        alloc_type = kAugmenting;
      } else {
        throw module_error(this, "invalid iteration count");
      }
    }
  }
};

#endif
