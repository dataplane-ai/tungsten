#ifndef RANDOM_H_
#define RANDOM_H_

#include <random>
#include <deque>

#include "sparsity/templates/SparsePMU.h"

#include "token.h"

using namespace std;

// Test harness for reading from the sparse PMU
template<int N, int B>
class RandomTest : public Module {
  int to_test {1000};

  const string op{"add"};

  float read_ratio {0.0};
  float valid_ratio {0.1};
  float banked_ratio {1.0};

  SparsePMU<int32_t,N,B> *PMU;
  SparsePMU<float,N,B> *PMU_float_dummy;
  typename SparsePMU<int32_t,N,B>::SparsePMUPort *read;
  typename SparsePMU<int32_t,N,B>::SparsePMUPort *modA;
  typename SparsePMU<int32_t,N,B>::SparsePMUPort *modD;

  //enum {kFill, kWarm, kExec, kDrain} state {kFill};

  mt19937_64 rd;
  //uniform_int_distribution<> dis; // {0, 16*16-1};
  uniform_int_distribution<> dis; // {0, 16*16-1};
  uniform_int_distribution<> data_dis; // {0, 16*16-1};

  deque<Token> write_issued;
  deque<Token> read_issued;

  deque<pair<Token,Token>> write_next;
  deque<Token> read_next;

  bool write_stall {false};
  bool read_stall {false};

  vector<int> write_outstanding;
  vector<int> read_outstanding;
  vector<int> mem_loc_val;

  void gen_read() {
    Token addr;
    bernoulli_distribution coin(valid_ratio);
    bernoulli_distribution is_banked(banked_ratio);
    addr.type = TT_INTVEC;
    for (int i=0; i<B; i++) {
      addr.intVec_[i] = dis(rd);
      addr.valid[i] = coin(rd);
      if (is_banked(rd))
        addr.intVec_[i] = i;
      if (!addr.valid[i])
        addr.intVec_[i] = -1;
    }
    read_next.push_back(addr);
  }

  void gen_write() {
    Token addr, data;
    bernoulli_distribution coin(valid_ratio);
    addr.type = TT_INTVEC;
    data.type = TT_INTVEC;
    for (int i=0; i<B; i++) {
      addr.intVec_[i] = 0;
      addr.valid[i] = coin(rd);
      data.intVec_[i] = data_dis(rd);
      if (!addr.valid[i])
        addr.intVec_[i] = -1;
    }
    for (int i=0; i<B; i++) {
      addr.intVec_[i] = dis(rd);
      for (int j=0; j<i; j++) {
        if (addr.intVec_[i] == addr.intVec_[j]) {
          addr.intVec_[i]++;
          addr.intVec_[i] %= (N*B);
          j=-1; 
        }
      }
    }
    write_next.push_back(make_pair(addr,data));
  }

  bool can_go(const Token& t, bool is_write) {
    bool stall = false;
    for (const auto& a : t.uintVec_) {
      if (is_write) {
        stall |= !!read_outstanding[a];
      }
      stall |= !!write_outstanding[a];
    }
    return !stall;
  }

  void scoreboard(const Token& t, bool wr, bool iss) {
    for (const auto& a : t.uintVec_) {
      if (wr)
        write_outstanding[a] += iss ? 1 : -1;
      else
        read_outstanding[a] += iss ? 1 : -1;
      assert(write_outstanding[a] >= 0);
      assert(write_outstanding[a] <= 1);
      assert(read_outstanding[a] >= 0);
    }
  }

  
 public:
  RandomTest(const std::string& _name) : Module(_name), dis(0, N*B-1), data_dis(0, 1000) {
    PMU = new SparsePMU<int32_t,N,B>("PMU");
    PMU_float_dummy = new SparsePMU<float,N,B>("XXXX");
    PMU->Fill(0);
    AddChild(PMU);
    read = PMU->GetReadPort();
    auto tmp = PMU->GetRMWPorts(op, "ordered");
    //auto tmp = PMU->GetWritePorts();
    modA = tmp.first;
    modD = tmp.second;
    Expect(to_test);
    for (int i=0; i<N*B; i++) {
      write_outstanding.push_back(0);
      read_outstanding.push_back(0);
      mem_loc_val.push_back(0);
    }
  }
  
  void Eval() {
    if (read->Valid()) {
      //cout << "Read valid" << endl;
      const Token& ret = read->Read();
      const Token& ref = read_issued.front();
      for (int i=0; i<B; i++) {
        if (!ref.valid[i])
          continue;
        assert(ret.intVec_[i] == mem_loc_val[ref.uintVec_[i]]);
      }
      scoreboard(ref, false, false);

      read->Pop();
      read_issued.pop_front();
      Complete(1);
      //cout << "Returned token." << endl;
    }

    if (modA->Valid()) {
      //cout << "Write valid" << endl;
      Complete(1);
      scoreboard(write_issued.front(), true, false);
      write_issued.pop_front();
      //assert(modA->Read().bool_);
      modA->Pop();
    }

    // Get a transaction ready to execute for write and read
    if (write_next.empty()) gen_write();
    if (read_next.empty()) gen_read();

    // No transaction is currently stalled on address
    if (!write_stall && !read_stall && to_test) {
      to_test--;
      bernoulli_distribution coin(read_ratio);
      if (coin(rd))
        read_stall = true;
      else
        write_stall = true;
    }

    //if ((read_stall && !read->Ready()) || (write_stall && !modA->Ready()))
      //return;

    if (read_stall) {
      if (!read->Ready() || !can_go(read_next.front(), false))
        return;

      scoreboard(read_next.front(), false, true);
      read_issued.push_back(read_next.front());

      read->Push(read_next.front());
      //cout << "Issue read" << endl;
      read_next.pop_front();

      read_stall = false;

    } else if (write_stall) {
      if (!modA->Ready() || !can_go(write_next.front().first, true))
        return;

      auto next = write_next.front();

      scoreboard(next.first, true, true);
      write_issued.push_back(next.first);
      for (int i=0; i<B; i++)
        if (next.first.valid[i])
          mem_loc_val[next.first.uintVec_[i]] += next.second.intVec_[i];

      modA->Push(write_next.front().first);
      assert(modD->Ready());
      modD->Push(write_next.front().second);
      write_next.pop_front();
      //cout << "Issue write" << endl;

      write_stall = false;
    }
  }
  void SetParameter(const string& key, const string& val) {
    if (key == "banked_frac") {
      banked_ratio = stof(val);
    } else {
      throw module_error(this, "no key");
    }
  }

};

#endif
