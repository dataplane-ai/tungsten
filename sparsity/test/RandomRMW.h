#ifndef RANDOM_RMW_H_
#define RANDOM_RMW_H_

#include <random>
#include <deque>

#include "sparsity/templates/SparsePMU.h"

#include "token.h"

using namespace std;

// Test harness for reading from the sparse PMU
template<int N, int B, typename T=uint32_t>
class RandomTestRMW : public Module {
  int to_test {100000};
  //int to_test {1};
  int to_check {N};

  const string op{"add"};
  string experiment{"XXX"};

  bool check_mod_out{true};
  bool do_verify_read{false};
  bool approx_verify_read{false};

  int upd_iss{0};
  int perf_iss{0};
  int rmw_cyc{0};

  float valid_ratio {1.0};
  float banked_ratio {0.0};

  SparsePMU<T,N,B> *PMU;
  typename SparsePMU<T,N,B>::SparsePMUPort *read;
  typename SparsePMU<T,N,B>::SparsePMUPort *modA;
  typename SparsePMU<T,N,B>::SparsePMUPort *modD;

  enum {kFill, kWarm, kExec, kDrain} state {kExec};

  mt19937_64 rd;
  //uniform_int_distribution<> dis; // {0, 16*16-1};
  uniform_int_distribution<> dis; // {0, 16*16-1};
  uniform_int_distribution<> data_dis; // {0, 16*16-1};

  deque<Token> write_expected;
  deque<Token> read_issued;

  deque<pair<Token,Token>> write_next;
  deque<Token> read_next;

  bool write_stall {false};
  bool read_stall {false};

  vector<T> mem_loc_val;

  void gen_read(int base) {
    Token addr;
    addr.type = TokVecType<int>();
    for (int i=0; i<B; i++) {
      //addr.intVec_[i] = base+i;
      (int&)addr[i] = base+i/4;
      addr.valid[i] = true;
      if (!addr.valid[i])
        (int&)addr[i] = -1;
    }
    read_next.push_back(addr);
  }

  void gen_write() {
    Token addr, data;
    bernoulli_distribution coin(valid_ratio);
    addr.type = TokVecType<int>();
    data.type = TokVecType<T>();
    for (int i=0; i<B; i++) {
      (int&)addr[i] = dis(rd);
      addr.valid[i] = coin(rd);
      if (!addr.valid[i])
        (int&)addr[i] = -1;
      (T&)data[i] = T(1);
    }
    write_next.push_back(make_pair(addr,data));
  }
  
 public:
  RandomTestRMW(const std::string& _name) : Module(_name), dis(0, N*B-1), data_dis(0, 1000) {
    PMU = new SparsePMU<T,N,B>("PMU");
    PMU->Fill(0);
    AddChild(PMU);
    read = PMU->GetReadPort();
    auto tmp = PMU->GetRMWPorts(op, "addr_ordered");
    modA = tmp.first;
    modD = tmp.second;
    Expect(to_test);
    if (do_verify_read || approx_verify_read)
      Expect(to_check);
    for (int i=0; i<N*B; i++) {
      mem_loc_val.push_back(0);
    }

    for (int i=0; i<to_test; i++) {
      gen_write();
    }
    for (int i=0; i<N*B; i+=B) {
      gen_read(i);
    }
  }
  
  void Eval() {
    if (read->Valid()) {
      const Token& ret = read->Read();
      const Token& ref = read_issued.front();
      for (int i=0; i<B; i++) {
        if (!ref.valid[i])
          continue;
        // Count the number of performed accesses 
        perf_iss += (T)ret[i];
        // Check an exact match (not for best effort)
        if (do_verify_read) {
          assert((T)ret[i] == mem_loc_val[(int)ref[i]]);
        }
      }

      read->Pop();
      read_issued.pop_front();
      Complete(1);
      //cout << "Returned token." << endl;
    }

    if (modA->Valid()) {
      assert(state == kExec);
      //cout << "Write valid" << endl;
      const Token& ret = modA->Read();
      const Token& ref = write_expected.front();
      Complete(1);
      for (int i=0; i<B; i++) {
        if (!ref.valid[i])
          continue;
        if (check_mod_out) 
          assert((T)ret[i] == (T)ref[i]);
      }
      write_expected.pop_front();
      to_test--;
      if (to_test == 0) {
        state = kDrain;
      }
      modA->Pop();
    }

    if (state == kExec) {
      rmw_cyc++;
      if (modA->Ready() && write_next.size()) {
        assert(modD->Ready());
        auto next = write_next.front();
        Token ref;
        ref.type = TokVecType<T>();
        ref.valid = next.first.valid;
        for (int i=0; i<B; i++) {
          if (next.first.valid[i]) {
            upd_iss++;
            //cout << "Enqueue access to addr " << next.first.intVec_[i] << " +=" << next.second.intVec_[i];
            assert((int)next.first[i] >= 0);
            mem_loc_val[(int)next.first[i]] += (T)next.second[i];
            //cout << " new value: " << mem_loc_val[next.first.intVec_[i]] << endl;
            (T&)ref[i] = mem_loc_val[(int)next.first[i]];
          }
        }
        write_expected.push_back(ref);
        modA->Push(write_next.front().first);
        modD->Push(write_next.front().second);

        write_next.pop_front();
      }
    } else if (state == kDrain) {
      if (read_next.size() && read->Ready()) {
        read->Push(read_next.front());
        read_issued.push_back(read_next.front());
        read_next.pop_front();
      }
    } else {
      assert(false);
    }
  }
  void SetParameter(const string& key, const string& val) {
    if (key == "banked_frac") {
      banked_ratio = stof(val);
    } else if (key == "simname") {
      experiment = val;
    //} else if (key == "order") {
      //PMU->UpdateRMWOrder(val, true);
    } else {
      throw module_error(this, "unknown key");
    }
  }
  string GetParameter(const string& key) {
    stringstream out;
    if (key == "summary") {
      out << "Execution completed in " << rmw_cyc << " cycles with " << upd_iss << " accesses." << endl;
      out << "Access rate: " << upd_iss*1.0/rmw_cyc << endl;
      out << "Access rate: " << upd_iss*1.0/rmw_cyc/16 << "%" << endl;
      out << "Performed access rate: " << perf_iss*1.0/rmw_cyc << endl;
      out << "Performed access rate: " << perf_iss*1.0/rmw_cyc/16 << "%" << endl;
    } else if (key == "csv") {
      out << experiment << "," << upd_iss*1.0/rmw_cyc/16 << "," << perf_iss*1.0/rmw_cyc/16 << endl;
    }
    return out.str();

  }

};

#endif
