#ifndef READ_H_
#define READ_H_

#include <random>
#include <deque>

#include "sparsity/templates/SparsePMU.h"

#include "token.h"

using namespace std;

// Test harness for reading from the sparse PMU
class ReadTest : public Module {
  int to_test {10000};
  SparsePMU<int32_t,16,16> *PMU;
  SparsePMU<int32_t,16,16>::SparsePMUPort *read;

  mt19937_64 rd;
  uniform_int_distribution<> dis; // {0, 16*16-1};

  deque<Token> issued;
  
 public:
  ReadTest(const std::string& _name) : Module(_name), dis(0, 16*16-1) {
    PMU = new SparsePMU<int32_t,16,16>("PMU");
    AddChild(PMU);
    read = PMU->GetReadPort();
    Expect(to_test);
  }
  
  void Eval() {
    if (read->Valid()) {
      const Token& ret = read->Read();
      for (int i=0; i<16; i++) {
        assert(ret.intVec_[i] == issued.front().intVec_[i]);
      }
      read->Pop();
      issued.pop_front();
      Complete(1);
      //cout << "Returned token." << endl;
    }

    if (!read->Ready() || !to_test)
      return;
    Token tmp;
    tmp.type = TT_UINTVEC;
    for (int i=0; i<16; i++) {
      tmp.uintVec_[i] = dis(rd);
    }
    read->Push(tmp);
    issued.push_back(tmp);
    //cout << "Issue token: " << tmp << endl;
  }


};

ReadTest DUT("DUT");

#endif
