#ifndef STATE_H_
#define STATE_H_

#include <optional>
#include <string>
#include <stdexcept>
#include <deque>
#include <array>
#include <cassert>
#include <iostream>

#include "module.h"
#include "interface.h"
#include "plasticine/templates/cppgenutil.h"

// This is the simplest clockable state element. It has one element (with an
// optional reset value) that can be written to and read from. Updates take
// effect at the clock edge.
template<class T>
class Register : public Module {
  T next;
  T val;

 public:
  explicit Register(const std::string& _name="reg") :
    Module(_name),
    next(),
    val() {
  }
  explicit Register(T _init, const std::string& _name) :
    Module(_name),
    next(_init),
    val(_init) {
  }
  void Eval() {
  }
  void Clock() {
    val = next;
  }
  void Write(const T& v) {
    next = v;
  }
  const T& Read() const {
    return val;
  }
};

template<class T, int COMPOUND=0>
class VariableFIFO : public Module,
                     public virtual CheckedSend<T>,
                     public virtual CheckedReceive<T> {
  bool          dequeue {false};
  std::optional<T> next;
  int max {1};
 public:
  std::deque<T> data; // Debug purpose

  explicit VariableFIFO(const std::string& _name, int _max) : Module(_name), data(0), max(_max) {
    SetInstrumentation();
  }
  void SetMax(int _max) {
    max = _max;
    if (data.size() > max) {
      throw module_error(this, "Max size below VariableFIFO size");
    }
  }
  void Eval() {
  }
  void Clock() {
    //if (dequeue || next) Active();
    if (next) Active();
    if (dequeue) {
      if (data.size() == 0)
        throw module_error(this, "FIFO underflow");
      data.pop_front();
    }
    if (next)
      data.push_back(*next);
    dequeue = false;
    next = std::nullopt;
    if (data.size() > max)
      throw module_error(this, "Variable FIFO overflow");
  }
  bool Ready() const {
    return data.size() < max;
  }
  bool Valid() const {
    return data.size();
  }
  void Push(const T& v) {
    if (next)
      throw module_error(this, " attempt to push twice in same cycle");
    next = v;
  }
  void Pop() {
    if (dequeue)
      throw module_error(this, " attempt to pop twice in same cycle");
    dequeue = true;
  }
  const T& Read() const {
    if (data.empty() )
      throw module_error(this, "Variable FIFO underflow");
    return data.front();
  }
  int size() const {
    return data.size();
  }
  void Log(std::ostream& log) {
    log << " v:" << Valid() << " r:" << Ready() << " ";
    if constexpr(!COMPOUND) {
      log << " size:" << data.size() << " [";
      for (auto& d: data) {
        log << d << " ";
      }
      log << "] ";
    }
  }
  void DumpState(std::ostream& log) {
    Module::DumpState(log);
    log << ",\"valid\":" << Valid();
    log << ",\"ready\":" << Ready();
    log << ",\"nelem\":" << data.size();
    if  constexpr(!COMPOUND) {
      if (next)
        log << ",\"next\":\"" << next << "\"";
    }
  }


};

// This is a fifo with variable depth and maximum size. Entries are dequeued in
// order, like in a traditional FIFO, and slots are reserved for new entries in
// order as well. However, slots are filled out of order.
template<class T, int N>
class ReorderFIFO : public Module,
                    public CheckedReceive<T> {
  bool             dequeue {false};
  bool             inc_write {false};
  std::optional<std::pair<T,int>> next;
  const int Sz {N+1};
  int              count     {0};
  int              read_ptr  {0};
  int              write_ptr {0};
  int size_lim {N};

  long             starve_cycle {0};
  long             stall_cycle {0};
 public:
  //std::deque<T> data_old; // Debug purpose
  std::array<std::optional<T>,N+1> data;

  int size() {
    return count;
  }

  explicit ReorderFIFO(const std::string& _name="ReorderFIFO") : Module(_name) {
    SetInstrumentation();
  }
  void Eval() {
    if (count == N) stall_cycle += 1;
    if (!count) starve_cycle += 1;
  }
  void Clock() {
    if (next) Active();
    if (dequeue) {
      if (count == 0)
        throw module_error(this, "FIFO underflow");
      data[read_ptr] = std::nullopt;
      read_ptr = (read_ptr+1)%Sz;
      count--;
    }
    if (next) {
      //cout << "Process write to slot: " << next->second << " val: " << next->first << endl;
      data[next->second] = next->first;
    }
    if (inc_write) {
      data[write_ptr] = std::nullopt;
      write_ptr = (write_ptr+1)%Sz;
      if (count > N) {
        throw module_error(this, "FIFO overflow");
      }
      count++;
    }
    dequeue = false;
    inc_write = false;
    next = std::nullopt;
  }
  bool Ready() const {
    return count < size_lim;
  }
  bool Valid() const {
    //if (!count)
      //cout << "No entries" << endl;
    //else if (!data[read_ptr])
      //cout << "Not ready for slot: " << read_ptr << endl;
    return (!!count) && data[read_ptr];
  }
  int Alloc() {
    assert(!inc_write);
    inc_write = true;
    return write_ptr;
  }
  void Push(const T& v, int idx) {
    //cout << "Setup write to slot: " << idx << " val: " << v << endl;
    if (next)
      throw module_error(this, " attempt to push twice in same cycle");
    if (idx < 0)
      throw module_error(this, " invalid index");
    next = make_pair(v,idx);
  }
  void Init(const T& v) {
    data[write_ptr] = v;
    write_ptr = (write_ptr+1)%Sz;
    count++;
    if (count > N)
      throw module_error(this, "FIFO overflow");
  }
  void SetReadEn(bool en) {
    //readEn = en;
  }
  void LimitSize(int sz) {
    size_lim = sz;
  }
  void Pop() {
    if (dequeue)
      throw module_error(this, " attempt to pop twice in same cycle");
    dequeue = true;
  }
  const T& Read() const {
    if (!data[read_ptr])
      throw module_error(this, "Read from unfilled FIFO slot");
    if (count == 0)
      throw module_error(this, "FIFO underflow");
    if (dequeue)
      throw module_error(this, "Read after pop");
    return *data[read_ptr];
  }
  void Log(std::ostream& log) {
    log << " v:" << Valid() << " r:" << Ready() << " " << " d:" << dequeue << " e:" << next.has_value();
    log << " size:" << count;
    if (next) {
      log << " n:" << next->first<<"@"<<next->second;
    }
    if (dequeue == 1) {
      log << " r:" << data[read_ptr];
    }
  }
  void DumpState(std::ostream& log) {
    Module::DumpState(log);
    log << ",\"valid\":" << Valid();
    log << ",\"ready\":" << Ready();
    log << ",\"nelem\":" << count;
    log << ",\"stall\":" << stall_cycle;
    log << ",\"starve\":" << starve_cycle;
    if (next)
      log << ",\"next\":\"" << next->first<<"@"<<next->second << "\"";
    else
      log << ",\"next\":\"null\"";
  }


};

// This is a FIFO with variable depth, and a maximum size.
template<class T, int N, int COMPOUND=0>
class FIFO : public Module,
             public virtual CheckedSend<T>,
             public virtual CheckedReceive<T> {
  bool             dequeue {false};
  bool             readEn {true};
  std::optional<T> next;
  const int Sz {N+1};
  int              count     {0};
  int              read_ptr  {0};
  int              write_ptr {0};

  long             starve_cycle {0};
  long             stall_cycle {0};
 public:
  //std::deque<T> data_old; // Debug purpose
  std::array<T,N+1> data;

  int size() {
    return count;
  }

  explicit FIFO(const std::string& _name="FIFO") : Module(_name) {
    SetInstrumentation();
  }
  void Eval() {
    if (count == N) stall_cycle += 1;
    if (readEn && !count) starve_cycle += 1;
  }
  void Clock() {
    //if (dequeue && next) Active();
    if (next) Active();
    if (dequeue) {
      //if (read_ptr == write_ptr)
      if (count == 0)
        throw module_error(this, "FIFO underflow (pop)");
      read_ptr = (read_ptr+1)%Sz;
      //data_old.pop_front();
      count--;
      //assert(count == data_old.size());
    }
    if (next) {
      data[write_ptr] = *next;
      //data_old.push_back(*next);
      write_ptr = (write_ptr+1)%Sz;
      if (count > N) {
        throw module_error(this, "FIFO overflow");
      }
      count++;
      //assert(count == data_old.size());
    }
    dequeue = false;
    readEn = true;
    next = std::nullopt;
    //if (data.size() > N)
      //throw module_error(this, "FIFO overflow");
  }
  bool Ready() const {
    //assert(count == data_old.size());
    return count < N;
    //return read_ptr != (write_ptr+1)%Sz;
    //return data.size() < N;
  }
  bool Valid() const {
    return !readEn || !!count;
    //return data.size();
  }
  void Push(const T& v) {
    if (next)
      throw module_error(this, " attempt to push twice in same cycle");
    next = v;
  }
  void Init(const T& v) {
    //data_old.push_back(v);
    data[write_ptr] = v;
    write_ptr = (write_ptr+1)%Sz;
    count++;
    //if (write_ptr == read_ptr)
    //assert(count == data_old.size());
    if (count > N)
    //if (data.size() > N)
      throw module_error(this, "FIFO overflow");
  }
  void SetReadEn(bool en) {
    readEn = en;
  }
  void Pop() {
    if (dequeue)
      throw module_error(this, " attempt to pop twice in same cycle");
    dequeue = true;
  }
  const T ReadPop() {
    if (count == 0 && readEn)
      throw module_error(this, "FIFO underflow (readPop)");
    if (dequeue)
      throw module_error(this, " attempt to pop twice in same cycle");
    dequeue = true;
    if (readEn)
      return move(data[read_ptr]);
    else
      return data[read_ptr];
  }
  const T& Read() const {
    if (!readEn) return data[read_ptr];
    if (count == 0)
    //if (read_ptr == write_ptr)
    //if (data.empty() )
      throw module_error(this, "FIFO underflow (read)");
    //assert(data[read_ptr] == data_old.front());
    if (dequeue)
      throw module_error(this, "Read after pop");
    return data[read_ptr];
  }
  void Log(std::ostream& log) {
    log << " v:" << Valid() << " r:" << Ready() << " " << " d:" << dequeue << " e:" << next.has_value();
    log << " size:" << count;
    //log << " [";
    //for (auto& d: data) {
      //log << d << " ";
    //}
    //log << "] ";
    if constexpr(!COMPOUND) {
      if (next) {
        log << " n:" << *next;
      }
      if (dequeue == 1) {
        log << " r:" << data[read_ptr];
      }
    }
  }
  void DumpState(std::ostream& log) {
    Module::DumpState(log);
    log << ",\"valid\":" << Valid();
    log << ",\"ready\":" << Ready();
    log << ",\"nelem\":" << count;
    log << ",\"stall\":" << stall_cycle;
    log << ",\"starve\":" << starve_cycle;
    if constexpr(!COMPOUND) {
      if (next)
        log << ",\"next\":\"" << next << "\"";
      else
        log << ",\"next\":\"null\"";
    }
  }

};
extern template class FIFO<Token,16>;
extern template class FIFO<Token,1>;
extern template class FIFO<Token,2>;

template<class T>
class NBlockBufferReg : public Module,
             public virtual CheckedSend<T> {
  std::optional<T> next;

 public:
  T data;

  explicit NBlockBufferReg(const std::string& _name) : Module(_name) {
    SetInstrumentation();
  }
  void Clock() {
    if (next) Active();
    else data = *next;
    next = std::nullopt;
  }
  bool Ready() const { return true; }
  //bool Valid() const {
  //}
  void Push(const T& v) {
    if (next)
      throw module_error(this, " attempt to push twice in same cycle");
    next = v;
  }
  void Pop() {} // do nothing
  const T& Read() const {
    return data;
  }
  void Log(std::ostream& log) {
    if (next) {
      log << *next;
    }
  }
  void DumpState(std::ostream& log) {
    Module::DumpState(log);
    if (next)
      log << ",\"next\":\"" << *next << "\"";
    else
      log << ",\"next\":\"null\"";
    log << ",\"data\":\"" << data << "\"";
  }

};

template<int V, int N>
class BufferReg : public Module,
             public virtual CheckedReceive<Token> {
  std::optional<Token> next;
  bool          dequeue {false};
  bool          enqueue {false};

 public:
  std::array<Token,N> buf; // use one more buffer to avoid handling pointer overlap after wrapped around
  int head = 0;
  int tail = 0;
  int size = 0;
  FIFO<Token, 2> data;
  FIFO<Token, 2> writeEn;
  FIFO<Token, 2> writeDone;

  explicit BufferReg(const std::string& _name) : Module(_name), 
    data(_name + "_data"), writeEn(_name + "_we"), writeDone(_name + "_wd") {
    SetInstrumentation();
    AddChildren({&data, &writeEn, &writeDone}, false);
  }
  void Eval() {
    data.Eval();
    writeEn.Eval();
    writeDone.Eval();
    if (data.Valid() && writeEn.Valid() && writeDone.Valid() && Ready()) {
      Token d = data.Read();
      const Token& curr = buf[tail];
      const Token& e = writeEn.Read();
      for (int i = 0; i < V; i ++) {
        bool en = toT<bool>(e, i);
        if (!en) {
          cpAt(d, curr, i);
        }
      }
      if (next)
        throw module_error(this, " attempt to push twice in same cycle");
      next = d;
      enqueue = toT<bool>(writeDone.Read(), 0);
      data.Pop();
      writeEn.Pop();
      writeDone.Pop();
    }
  }
  void Clock() {
    data.Clock();
    writeEn.Clock();
    writeDone.Clock();
    if (next) Active();
    if (dequeue) {
      size -= 1;
      if (size < 0) throw module_error(this, "BufferReg underflow");
      head += 1;
      if (head == N) head = 0;
    }
    if (next) {
      buf[tail] = *next;
      next = std::nullopt;
    }
    if (enqueue) {
      size += 1;
      if (size > N) throw module_error(this, "BufferReg overflow");
      tail += 1;
      if (tail == N) tail = 0;
    }
    dequeue = false;
    enqueue = false;
  }
  bool Ready() const {
    return size < N;
  }
  bool Valid() const {
    return size;
  }
  void Push(const Token& v) { // Should only be used during initialization
    writeEn.Push(make_token(true));
    writeDone.Push(make_token(true));
    data.Push(v);
  }
  void Pop() {
    if (dequeue)
      throw module_error(this, " attempt to pop twice in same cycle");
    dequeue = true;
  }
  const Token& Read() const {
    if (size==0)
      throw module_error(this, "BufferReg underflow");
    return buf[head];
  }
  void Log(std::ostream& log) {
    log << " size: " << size;
    log << " v:" << Valid() << " deq:" << dequeue;
    if (next) log << " next: " << *next;
    log << " data: ";
    data.Log(log);
    log << " we: ";
    writeEn.Log(log);
    log << " v: " << writeEn.Valid();
    log << " wd: ";
    writeDone.Log(log);
    log << " v: " << writeDone.Valid();
  }
  void DumpState(std::ostream& log) {
    Module::DumpState(log);
    log << ",\"valid\":" << Valid();
    log << ",\"ready\":" << Ready();
    log << ",\"nelem\":" << size;
    if (next)
      log << ",\"next\":\"" << next << "\"";
    else
      log << ",\"next\":\"null\"";
  }
};

template<class T, int I, int O>
class RateMatchingFIFO : public Module,
             public virtual CheckedReady,
             public virtual CheckedValid {
  int          dequeue {0};
  int N;
  std::optional<std::array<T, I>> next{std::nullopt};
  std::optional<std::array<bool, I>> writeEn{std::nullopt};
  std::optional<std::array<bool, O>> readEn{std::nullopt};

 public:
  std::deque<T> data; // Debug purpose

  explicit RateMatchingFIFO(const std::string& _name, int _N) : Module(_name), N(_N), data(0) {
    if (N < I) throw module_error(this, "Depth less than input width");
    if (N < O) throw module_error(this, "Depth less than output width");
    SetInstrumentation();
  }
  void Eval() {
  }
  void Clock() {
    if (data.size() < dequeue)
      throw module_error(this, "RateMatchingFIFO underflow");
    for (int i = 0; i < dequeue; i++)
      data.pop_front();
    if (next) {
      Active();
      for (int i = 0; i < I; i ++) {
        if ((*writeEn)[i])
          data.push_back((*next)[i]);
      }
    }
    dequeue = 0;
    next = std::nullopt;
    writeEn = std::nullopt;
    readEn = std::nullopt;
    if (data.size() > N)
      throw module_error(this, "RateMatchingFIFO overflow");
  }
  bool Ready() const {
    return N - data.size() >= I;
  }
  int NumRead() const {
    int readSum = O;
    if (readEn) {
      readSum = 0;
      for (bool en: *readEn)
        readSum += en ? 1 : 0;
    }
    return readSum;
  }
  void SetReadEn(array<bool,O> ens) {
    readEn = ens;
  }
  bool Valid() const {
    int num = NumRead();
    return Valid(num);
  }
  bool Valid(int en) const {
    return data.size() >= en;
  }
  void Push(const std::array<T, I>& v) {
    std::array<bool, I> e;
    e.fill(true);
    Push(v, e);
  }
  void Push(const std::array<T, I>& v, const std::array<bool,I>& e) {
    if (next)
      throw module_error(this, "Attempt to push twice in same cycle " + name);
    next = v;
    writeEn = e;
  }
  void Pop(int en) {
    if (dequeue > 0)
      throw module_error(this, "Attempt to pop twice in same cycle " + name);
    dequeue = en;
  }
  void Pop() {
    Pop(NumRead());
  }
  std::array<T,O> Read() const {
    if (data.size() < NumRead())
      throw module_error(this, "RateMatchingFIFO underflow");
    std::array<T,O> out;
    int j = 0;
    if (readEn) {
      for (int i = 0; i < O; i++) {
        if ((*readEn)[i]) {
          out[i] = data.at(j);
          j++;
        } else {
          out[i] = T();
        }
        // if ((*readEn)[i]) j += 1;
      }
    } else {
      for (int i = 0; i < O; i++) {
        out[i] = data.at(i);
      }
    }
    return out;
  }
  std::array<T,O> Read(int en) const {
    if (data.size() < en)
      throw module_error(this, "RateMatchingFIFO underflow");
    std::array<T,O> out;
    for (int i = 0; i < en; i++)
      out[i] = data.at(i);
    return out;
  }
  void Log(std::ostream& log) {
    log << "  " << name << " v:" << Valid() << " r:" << Ready() << " d:" << dequeue << " e:" << next.has_value();
    if (readEn) {
      log << " re: ";
      for (bool en: *readEn)
        log << en;
      log << " nr:" << NumRead();
    }
    log << " size:" << data.size() << " [";
    for (auto& d: data)
      log << d << " ";
    log << "] ";
  }
  void DumpState(std::ostream& log) {
    Module::DumpState(log);
    log << ",\"valid\":" << Valid();
    log << ",\"ready\":" << Ready();
    log << ",\"nelem\":" << data.size();
    log << ",\"next\":\"";
    if (!next)
      log << "null";
    else {
      log <<"[";
      for (auto d: *next)
        log << d << " ";
      log << "]";
    }
    log << "\"";
  }
};

template<class T, int N, int I, int O>
class RateMatchingTokenFIFO : public Module,
             public virtual CheckedSend<Token>,
             public virtual CheckedReceive<Token> {

  long             starve_cycle {0};
  long             stall_cycle {0};
 public:
  RateMatchingFIFO<T, I, O> fifo;
  Token* ret;

  explicit RateMatchingTokenFIFO(const std::string& _name) : Module(_name), fifo(_name + "_fifo", N) {
    AddChild(&fifo);
    ret = new Token();
    SetInstrumentation();
  }
  bool Ready() const {
    return fifo.Ready();
  }
  void SetReadEn(bool ens[O]) {
    std::array<bool,O> readEn;
    for (int i = 0; i < O; i++)
      readEn[i] = ens[i];
    fifo.SetReadEn(readEn);
  }
  void SetReadEn(bool& en) {
    if (O != 1)
      throw module_error(this, "RateMatchingTokenFIFO with O = " + to_string(O) + " use scalar readEn");
    bool ens[O];
    ens[0] = en;
    SetReadEn(ens);
  }
  bool Valid() const {
    return fifo.Valid();
  }
  void Push(const Token& v) {
    std::array<T,I> in;
    std::array<bool, I> e;
    e.fill(false);
    for (int i = 0; i < I; i ++) {
      in[i] = toT<T>(v, i);
      e[i] = i < v.nvalid;
    }
    fifo.Push(in,e);
  }
  void Pop() {
    fifo.Pop();
  }
  const Token& Read() const {
    std::array<T,O> out = fifo.Read();
    if (O == 1) {
      T& d = out[0];
      *ret = make_token(d);
    } else {
      T vec[O];
      for (int i = 0; i < O; i++) {
        vec[i] = out[i];
      }
      *ret = make_token<O>(vec);
    }
    return *ret;
  }
  void Log(std::ostream& log) {
    fifo.Log(log);
  }
  void DumpState(std::ostream& log) {
    fifo.DumpState(log);
    log << ",\"stall\":" << stall_cycle;
    log << ",\"starve\":" << starve_cycle;
  }
  void Eval() {
    if (!fifo.Ready()) stall_cycle += 1;
    if (!fifo.Valid()) starve_cycle += 1;
  }

};

// This is a fixed-latency pipeline, without backpressure but with an optional
// stall signal
template<class T, int N>
class Pipeline : public Module {
  //std::deque<T> data;
  std::array<T, N+1> data;
  T                next;
  int              write {0};
  int              read  {1};
  bool             stall {false};
  bool             pushed {false};

 public:
  explicit Pipeline(const std::string& _name) :
    Module(_name),
    data(),
    next() {
    for (int i=0; i<N; i++) {
      data[i] = T();
    }
  }
  void Eval() {
  }
  void Clock() {
    if (!stall) {
      data[write] = next;
      read = (read+1)%(N+1);
      write = (write+1)%(N+1);
      next = T();
    }
    stall = false;
    pushed = false;
  }
  void Stall() {
    stall = true;
  }
  void Write(const T& v) {
    next = v;
    //std::cout << path << name << " pipeline write" << std::endl;
    if (pushed) {
      throw module_error(this, "double push to pipeline");
    }
    pushed = true;
  }
  const T& Read() const {
    return data[read];
    //std::cout << path << name << " pipeline read" << std::endl;
    //return data.front();
  }
  const T ReadMove() {
    return move(data[read]);
    //std::cout << path << name << " pipeline read" << std::endl;
    //return data.front();
  }
  void Log(std::ostream& log) {
    //log << "n:" << next << " s:" << stall << " wa:" << write << " ra:" << read << " r:" << "" << data[read] << " d:";
    //log << "n:" << next << " s:" << stall << " d:";
    //log << "[";
    //for (auto& d:data) {
      //log << d << " ";
    //}
    //log << "]";
  }
};

template<class T, int N>
class ValCompactPipeline : public Module, public CheckedSend<T>, public CheckedReceive<T> {
  int64_t cycle{0};
  bool popped{false};
  // Pair of data, ready time
  deque<pair<T,int64_t>> data;
 public:
  explicit ValCompactPipeline(const std::string& _name) :
    Module(_name) {
  }
  void Clock() {
    cycle++;
    popped = false;
  }
  void Push(const T& v) {
    assert(Ready());
    data.emplace_back(v, cycle+N);
  }
  bool Ready() const {
    return data.size() < N;
  }
  bool Valid() const {
    assert(!popped);
    return data.size() && data.front().second <= cycle;
  }
  const T& Read() const {
    assert(Valid());
    return data.front().first;
  }
  void Pop() {
    assert(Valid());
    popped = true;
    data.pop_front();
  }
};

// A pipeline specialization implementing validity tracking
template<class T, int N>
class ValPipeline : public Module, 
                    public ValReceive<T>, 
                    public ValSend<T>{
 protected:
  Pipeline<std::optional<T>, N> pipe;
 public:
  explicit ValPipeline(const std::string& _name) :
    Module(_name),
    pipe(_name) {
  }
  void RefreshChildren() {
    children.clear();
    AddChild(&pipe, false);
  }
  void Stall() {
    pipe.Stall();
  }
  void Eval() {
    pipe.Eval();
  }
  void Clock() {
    pipe.Clock();
  }
  void Push(const T& v) {
    pipe.Write(v);
  }
  void Push(const T&& v) {
    pipe.Write(v);
  }
  bool Valid() const {
    return pipe.Read().has_value();
  }
  T Read() const {
    return pipe.Read().value();
  }
  T ReadMove() {
    return move(pipe.Read().value());
  }
  void Log(std::ostream& log) {
    pipe.Log(log);
  }
};

template<class T>
class ValReadyPipeline : public Module, 
                         public ValReceive<T>, 
                         public ValSend<T>, 
                         public CheckedReady  {
  struct Entry {
    T value;
    int idx;
  };

  std::deque<Entry> data;
  optional<T>      next;
  bool             stall {false};
  int              N;

  public:
  explicit ValReadyPipeline(const std::string& _name, int _N) :
    Module(_name),
    data(),
    next(), 
    N(_N){
  }
  void Stall() {
    stall = true;
  }
  void Eval() {
  }
  void Clock() {
    if (!data.empty() && data.front().idx == N && !stall) {
      data.pop_front();
    }
    int prevIdx = -1;
    for (auto& e: data) {
      if (prevIdx == -1) {
        if (e.idx != N) e.idx += 1;
      } else {
        if (prevIdx > e.idx + 1) e.idx += 1;
      }
      prevIdx = e.idx;
    }
    if (next.has_value()) {
      Entry e;
      e.value = next.value();
      e.idx = 1;
      data.push_back(e);
      if (data.size()>N+1) {
        throw module_error(this, "Overflow ValReadyPipeline");
      }
    }
    next = std::nullopt;
    stall = false;
  }
  void Push(const T& v) {
    if (next.has_value()) {
      throw module_error(this, "double push to ValReadyPipeline");
    }
    next = v;
  }
  bool Valid() const {
    return !data.empty() && data.front().idx==N;
  }
  bool Ready() const {
    return data.size() <= N;
  }
  T Read() const {
    return data.front().value;
  }
  void Log(std::ostream& log) {
    log << "r:" << Ready() << " v:" << Valid();
    log << " s:" << stall << " d:";
    log << "[";
    for (auto& e:data) {
      log << e.value << "[" << e.idx << "]";
    }
    log << "]";
    log << " n:" << next;
  }
};
extern template class ValReadyPipeline<Token>;

// This is a pipeline that has backpressure. The sender must be aware of
// the backpressure generated, and take care not to enqueue unless there is a
// corresponding dequeue. This is because adding support for backpressure from
// the input to the output would create a feedthrough path for the module,
// which is not allowed.
template<class T, int N>
class BackpressurePipeline : public Module,
                             public virtual CheckedReceive<T> {
  std::deque<std::optional<T>> data;
  std::optional<T>             next;
  bool                         dequeue {false};

 public:
  explicit BackpressurePipeline(const std::string& _name) :
    Module(_name),
    data(N),
    next() {
  }
  void Eval() {
  }
  void Clock() {
    if (data.front() || !dequeue) {
      if (next)
        throw module_error(this, "Write to a backpressured pipeline");
      return;
    }
    dequeue = false;
    data.pop_front();
    data.push_back(next);
    next = std::nullopt;
  }
  void Write(const T& v) {
    next = v;
  }
  void Pop() {
    dequeue = true;
  }
  bool Valid() {
    return data.front();
  }
  const T& Read() const {
    if (!data.front())
      throw module_error(this, "Attempt to read from an empty pipeline " + name);
    return data.front().value;
  }
};

// This is a SRAM with a single read/write port and registered reads.
template<class T, int N>
class SRAM : public Module {
  bool             do_write   {false};
  bool             do_read    {false};
 public:
  std::array<T, N> data;
  size_t           read_addr {0};
  size_t           write_addr {0};
  bool             read_valid {false};
  T                read_data;
  T                write_data;

 public:
  explicit SRAM(const std::string& _name) : Module(_name) {
  }
  void Eval() {
  }
  void Clock() {
    if (do_write) {
      if (write_addr >= N)
        throw module_error(this, "SRAM write out of range write_addr:" + to_string(write_addr) + " N:" + to_string(N));
      data[write_addr] = write_data;
    }
    do_write = read_valid = false;
    if (do_read) {
      if (read_addr >= N) {
        throw module_error(this,
            "SRAM read out of range in " + name + 
            " read_addr:" + std::to_string(read_addr) + 
            " valid range 0 - " + std::to_string(N)
            );
      } else {
        read_data = data[read_addr];
        read_valid = true;
        do_read = false;
      }
    }
  }
  void Write(const T& d, size_t a) {
    do_write = true;
    write_addr = a;
    write_data = d;
  }
  void SetupRead(size_t a) {
    if (do_read && read_addr != a) {
      throw module_error(this, 
          "SetupRead SRAM twice with different addr in the same cycle. Original addr " + std::to_string(read_addr) + " new addr " + std::to_string(a));
    }
    read_addr = a;
    do_read = true;
  }
  const T& ReadData() const {
    if (!read_valid)
      throw module_error(this, "Attempt to read invalid data in " + name);
    return read_data;
  }
  bool ReadValid() {
    return read_valid;
  }
};


#endif  // STATE_H_
