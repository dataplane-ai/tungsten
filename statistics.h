#ifndef STATISTICS_H_
#define STATISTICS_H_

#include <map>

#include "module.h"

using namespace std;

class CounterStatistic : public Module {
  uint64_t counts {0};
  uint64_t cycles {0};
  int64_t clip_hist {-1};
  std::map<std::string, uint64_t> custom_counts;
  std::map<uint64_t, uint64_t> sparse_hist;
  std::map<pair<int,int>, uint64_t> hist_2d;
  std::vector<uint64_t> dense_hist;
  string stat_name {"stat"};

 public:
  explicit CounterStatistic(const std::string& _name) : Module(_name) {
    dense_hist.resize(1);
  }

  virtual void SetParameter(const std::string& key, const string& val) {
    if (key == "clip_hist") {
      clip_hist = stoi(val);
      if (dense_hist.size() <= clip_hist) {
        dense_hist.resize(clip_hist+1);
      }
    } else if (key == "name") {
      stat_name = val;
    } else {
      throw module_error(this, "Unknown parameter");
    }
  }


  virtual std::string GetParameter(const std::string& key) {
    if (key == "count") {
      return std::to_string(counts);
    } else if (key == "all") {
      return "Count: " + std::to_string(counts) + " (" + 
        std::to_string(100.0*counts/cycles) + "%)";
    } else if (key == "sparse") {
      std::string output = "";
      for (auto const& [bin, val] : sparse_hist) {
        output.append(std::to_string(bin) + ", " + std::to_string(val) + "\n");
      }
      return output;
    } else if (key == "dense") {
      std::string output = "";
      int i = 0;
      output.append("Counts," + stat_name + "\n");
      for (auto const& val : dense_hist) {
        output.append(std::to_string(i) + ", " + std::to_string(val) + "\n");
        i++;
      }
      return output;
    } else if (key == "2d") {
      std::string output = "";
      int i = 0;
      output.append("Dim,Counts," + stat_name + "\n");
      for (auto const& [x, val] : hist_2d) {
        auto const& [dim, counts] = x;
        output.append(std::to_string(dim) + ", " + to_string(counts) + ", " + std::to_string(val) + "\n");
        i++;
      }
      return output;
    } else if (key == "cycles") {
      //return std::to_string(cycles);
      return (string(",") + stat_name + "\n" + key + "," + to_string(cycles));
    } else {
      if (custom_counts.find(key) != custom_counts.end()) {
        return (string(",") + stat_name + "\n" + key + "," + to_string(custom_counts[key]));
      }
    }
    throw module_error(this, "Unknown parameter to get");
  }

  void Clock() {
    cycles++;
  }

  void Inc() {
    counts++;
  }

  void AddTo2DHist(int d, int v) {
    if (clip_hist > 0 && v > clip_hist)
      v = clip_hist;
    hist_2d[make_pair(d,v)]++;
  }

  void AddToSparseHist(uint64_t v) {
    if (clip_hist > 0 && v > clip_hist)
      v = clip_hist;
    sparse_hist[v]++;
  }

  void AddToDenseHist(uint64_t v) {
    if (clip_hist > 0 && v > clip_hist)
      v = clip_hist;
    if (v >= dense_hist.size()) {
      while (dense_hist.size() <= v) {
        dense_hist.resize(dense_hist.size() * 2);
      }
    }

    dense_hist[v]++;
  }

  void CreateCounter(std::string name) {
    if (custom_counts.find(name) == custom_counts.end()) {
      custom_counts[name] = 0;
    }
  }

  void CustomInc(std::string name) {
    if (custom_counts.find(name) != custom_counts.end()) {
      custom_counts[name]++;
    }
  }
};

class BandwidthStats {
public:
  BandwidthStats() {
    cycleCount = 0;
    cycleBegin = 0;
    cycleEnd = 0;
    tokenCount = 0;
    active = false;
  }

  void Accum() {
    if (!active) {
      active = true;
      cycleBegin = cycleCount;
    }

    cycleEnd = cycleCount;
    tokenCount++;
    auto &bucket = histogram.back();
    bucket++;
  }

  void Step() {
    if ((cycleCount % sampleResolution) == 0) {
      histogram.push_back(0);
    }
    cycleCount++;
  }

  uint64_t CycleBegin() {
    return cycleBegin;
  }

  uint64_t CycleEnd() {
    return cycleEnd;
  }

  uint64_t TokenCount() {
    return tokenCount;
  }

  uint64_t CyclesActive() {
    return cycleEnd - cycleBegin + 1;
  }

  float DutyCycle() {
    return (float)tokenCount / (float)CyclesActive();
  }

  const std::vector<int> & Histogram() const {
    return histogram;
  }

  const static int sampleResolution = 1000;
private:
  // keep track of the cycle count
  uint64_t cycleCount;
  // first cycle this link received a token
  uint64_t cycleBegin;
  // last cycle this link received a token
  uint64_t cycleEnd;
  // total number of tokens that traversed this link
  uint64_t tokenCount;
  // keep track of whether this link
  bool active;
  // how many cycles to sample in a single bucket
  // a list of sampled buckets in order
  std::vector<int> histogram;
};


#endif  // STATISTICS_H_
