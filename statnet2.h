#ifndef STATNET2_H_
#define STATNET2_H_

#include "interface.h"
#include "module.h"
#include "token.h"

#include <deque>
#include <memory>
#include <utility>
#include <vector>

using namespace std;

class StaticNetwork2;

class BufferPoint {
  // Input buffer/VC and downstream buffer/VC
  pair<BufferPoint*, int> in;
  // Output buffer with credits
  vector<pair<BufferPoint*, int>> out;

  deque<shared_ptr<Token>> buf;

  int max_depth;
  StaticNetwork2 *parent;

  bool _ready(void) const;
  void _send(void);

  const string name;

 public:
  BufferPoint(const string& _name, StaticNetwork2 *_parent, int _max_depth); 

  void SetIn(BufferPoint *p, int idx);
  int  AddOut(BufferPoint *p);
  void Credit(int idx);
  // void Packet(shared_ptr<Token>&& t);
  void Packet(shared_ptr<Token> t);

  inline bool Ready(void) const {
    return buf.size() < max_depth;
  }
  inline bool Valid(void) const {
    // cout << "Check valid: " << buf.size() << endl;
    return buf.size();
  }
  shared_ptr<Token> ReadPop(void);
};

class StaticNetwork2 : public Module,
                       public virtual NetworkInterface<Token, int, int> {

  // The original links, used to build the final graph.
  vector<tuple<int, int, int, bool>> links_orig;

  // Store this as a map to permit easy proute mapping
  map<pair<int, int>, BufferPoint*> bufs;
  map<pair<int, int>, BufferPoint*> outputs;

  void AddLink(const string& l, bool output);
  void BuildAll();


  int delay;
  int depth;
  int64_t cycle{0};
  deque<tuple<int64_t, BufferPoint*, int>>               creds_pend;
  deque<tuple<int64_t, BufferPoint*, shared_ptr<Token>>> packets_pend;

 public:
  StaticNetwork2(const string& _name, int _delay, int _depth);

  // Interface for BP callbacks
  void Credit(BufferPoint *p, int idx);
  void Packet(BufferPoint *p, shared_ptr<Token>& t);

  // Module interfaces
  void Eval(void);
  void Clock(void);
  void AppendParameter(const string& key, const string& val);
  void Apply(const string& key, const vector<string>& vals);

  // Network interface
  bool Ready(int src, int vc, int flow) const;
  void Push(const Token& v, int src, int vc, int flow);
  bool Valid(int dst, int vc) const;
  Token Read(int dst, int vc);

  // Debug API MISSING
  int GetCredit(int src, int vc, int flow) const        { return -1; }
  void LogSrc(std::ostream& log, int src, int flow) {}
  void LogDst(std::ostream& log, int dst, int vc)   {}
  
};

#endif  // STATNET2_H_
