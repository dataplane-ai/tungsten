#ifndef TESTS_BINLOG_BASIC_H_
#define TESTS_BINLOG_BASIC_H_

#include "binlog/binlog.hpp"
#include <iostream>

class TestBinlog : public Module { 
  deque<Token> toks;
 public:
  TestBinlog(const std::string& _name) : Module(_name) {
    array<int,16> ints;
    array<float,16> floats;
    array<bool,16> bools;
    for (int i=0; i<16; i++) {
      ints[i] = i;
      floats[i] = i*0.5;
      bools[i] = i%3 == 2;
    }
    toks.emplace_back(42);
    toks.emplace_back(ints);
    toks.emplace_back(floats);
    toks.emplace_back(bools);
    Expect(toks.size());
  }
  void Eval() {
    BINLOG_INFO_W(writer, "{}", toks.front());
    toks.pop_front();
    Complete(1);
  }
};

#endif  // TESTS_BINLOG_BASIC_H_
