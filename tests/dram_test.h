#ifndef TESTS_DRAM_H_
#define TESTS_DRAM_H_

#include "example.h"
#include "state.h"
#include "module.h"
#include "network.h"
#include "dram.h"
#include "io.h"

FIFO<Token,4>           DataFifo("DataFIFO");
FIFO<Token,4>           AddrFifo("AddrFIFO");
FIFO<Token,4>           ReadAddrFifo("ReadAddrFIFO");
FIFO<Token,4>           ReadDataFifo("ReadDataFIFO");
FIFO<Token,4>           RetFifo("RetFIFO");
FIFO<DRAMCommand,4>     CmdQueue("CmdQueue");
FIFO<DRAMCommand,4>     ReadCmdQueue("ReadCmdQueue");
FileReader<int32_t>     TestGenerator("FileIn"s, "data/primes"s, &DataFifo);
FileWriter<int32_t>     TestConsumer("FileOut"s, "/dev/stdout"s, &ReadDataFifo);
SequenceGenerator       AddrGenerator("AddrGen", &AddrFifo);
SequenceGenerator       ReadAddrGenerator("ReadAddrGen", &ReadAddrFifo);
DRAMWriteAG             DRAMWriter("DRAMWriteAG", &CmdQueue, &AddrFifo, &DataFifo);
DRAMReadAG              DRAMReader("DRAMReadAG",  &ReadCmdQueue, &ReadAddrFifo, &RetFifo);
DRAMController          DRAM("DRAM", "ini/DRAM.ini", "ini/system.ini", ".", {&CmdQueue, &ReadCmdQueue}, {&RetFifo, &ReadDataFifo});

Module                  DUT({&DataFifo,
                             &AddrFifo,
                             &ReadDataFifo,
                             &ReadAddrFifo,
                             &RetFifo,
                             &CmdQueue,
                             &ReadCmdQueue,
                             &TestGenerator,
                             &TestConsumer,
                             &AddrGenerator,
                             &ReadAddrGenerator,
                             &DRAMWriter,
                             &DRAMReader,
                             &DRAM}, "DUT");


#endif  // TESTS_STREAMAB_H_
