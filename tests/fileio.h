#ifndef TESTS_FILEIO_H
#define TESTS_FILEIO_H

#include "example.h"
#include "state.h"
#include "module.h"
#include "network.h"
#include "dram.h"
#include "io.h"

//DynamicNetwork<20, 2>   MainNet({16, 12}, "network");
DRAMController          DRAM("DRAM", "ini/DRAM.ini", "ini/system.ini", ".", {}, {});
//NetworkInput            TestInput("netIn", &MainNet);
//NetworkOutput           TestOutput("netOut", &MainNet);
FIFO<Token,4>           TestFifo("FIFO");
FileReader<int32_t>     TestGenerator("FileIn"s, "data/primes"s, &TestFifo);
FileWriter<int32_t>     TestConsumer("FileOut"s, "testout/primes"s, &TestFifo);
//SequenceConsumer        TestConsumer("DispOut", &TestFifo);
Module                  DUT({&TestGenerator,
                             &TestConsumer,
                             &TestFifo,
                             &DRAM}, "DUT");


#endif  // TESTS_STREAMAB_H_
