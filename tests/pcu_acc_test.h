#ifndef TEST_PCU_ACC_H_
#define TEST_PCU_ACC_H_

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"

#include "pcu_acc.h"

FIFO<Token, 2> Input("in");
FIFO<Token, 2> Output("out");
SequenceGenerator TestGenerator("TestGen1", &Input);
SequenceConsumer TestConsumer("TestConsumer", &Output);
PCUAcc<6, 2> TestPCU("pcu", {&Input}, {&Output});
Module DUT({&TestGenerator,
            &TestConsumer,
            &TestPCU,
            &Input,
            &Output}, "DUT");

#endif // TEST_PCU_ACC_H_
