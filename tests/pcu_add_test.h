#ifndef TEST_PCU_ADD_H_
#define TEST_PCU_ADD_H_

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"

#include "pcu_add.h" // also set vector = false in example.cc

FIFO<Token, 2> Input1("in1");
FIFO<Token, 2> Input2("in2");
FIFO<Token, 2> Output("out3");
SequenceGenerator TestGenerator1("TestGen1", &Input1);
SequenceGenerator TestGenerator2("TestGen2", &Input2);
SequenceConsumer TestConsumer("TestConsumer", &Output);
PCUAdd<6, 2> TestPCU("pcu", {&Input1, &Input2}, {&Output});
Module DUT({&TestGenerator1,
            &TestGenerator2,
            &TestConsumer,
            &TestPCU,
            &Input1,
            &Input2,
            &Output}, "DUT");

#endif // TEST_PCU_ADD_H_
