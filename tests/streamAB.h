#ifndef TESTS_STREAMAB_H_
#define TESTS_STREAMAB_H_

#include "example.h"
#include "state.h"
#include "module.h"
#include "network.h"
#include "dram.h"

DynamicNetwork<20, 2>   MainNet({16, 12});
DRAMController          DRAM("DRAM", "ini/DRAM.ini", "ini/system.ini", ".", {}, {});
NetworkInput            TestInput("netIn", &MainNet);
NetworkOutput           TestOutput("netOut", &MainNet);
SequenceGenerator       TestGenerator(&TestInput);
SequenceConsumer        TestConsumer(&TestOutput);
Module                  DUT({&MainNet,
                             &TestGenerator,
                             &TestConsumer,
                             &TestInput,
                             &TestOutput,
                             &DRAM});


#endif  // TESTS_STREAMAB_H_
