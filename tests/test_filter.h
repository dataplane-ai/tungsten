#ifndef TEST_FILTER_H_
#define TEST_FILTER_H_

#include <cstdlib>

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"

#include "db_filter.h"
#include "db_test.h"
#include "db_factory.h"
#include "db_stat.h"

using namespace DB;
using namespace Factory;

class TestFilter : public Test {
public:
  explicit TestFilter(
      DRAMController *_dram
    ) : Test("TestFilter", std::string()) {
    dram = _dram;

    enable = new FIFOBroadcast("enable");
    AddChild(enable);

    layout = Record::Layout({ "col0", "col1", "col2", "col3", "col4" });

    for (int i = 0; i < 80000; i++) {
      values.push_back(Record::random(1000));
    }

    tableIn = new MaterializedTable(layout, values.size());
    tableIn->WriteRecords(values.begin(), values.end());
    tableOut = new MaterializedTable(layout, values.size());

    auto load = new Load(this, "load", _dram, tableIn, enable, 4);
    pred = [](const Record& r) -> bool { return r.data[0] < 1; };
    auto filter = new Filter(this, "filter", pred, load->Out());
    auto sort = new Sort(this, "sort", { "col0" }, true, _dram, filter->Out());
    auto store = new Store(this, "store", _dram, sort->Out());

    tableOut = store->Table();
    testDone = store->Done()->AddDst();
  }

  void Run() {
    Token t;
    enable->Src()->Push(t);
  }

  bool Success() {
    DBStats stat;
    stat.Find(this, {dram});
    stat.Write();

    std::vector<Record> gold;
    Gold::Filter(std::begin(values), std::end(values), std::back_inserter(gold), pred);

    std::vector<Record> out;
    tableOut->ReadRecords(std::back_inserter(out));
    tableOut->WriteCSV("out.csv");

    return TestHelper::Equal(out.begin(), out.end(), gold.begin(), gold.end(), layout);
  }

private:
  DRAMController *dram;
  FIFOBroadcast *enable;
  Record::Layout layout;
  std::vector<Record> values;

  std::function<RecordPredicate> pred;

  MaterializedTable *tableIn;
  MaterializedTable *tableOut;
};

DRAMController ddr(
  "DDR4",
  "configs/HBM-config.cfg",
  {},
  {}
);

TestFilter test(&ddr);

Module DUT({
  &ddr,
  &test,
}, "DUT");

#endif
