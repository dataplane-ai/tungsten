#ifndef TEST_GROUUBY_H_
#define TEST_GROUUBY_H_

#include <cstdlib>

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"

#include "db_test.h"
#include "db_factory.h"

using namespace DB;
using namespace Factory;

class TestGroupBy : public Test {
public:
    TestGroupBy(DRAMController *_dram)
      : Test("TestGroupBy", std::string()) {
    enable = new FIFOBroadcast("enable");
    AddChild(enable);

    Record::Layout layout({ "A", "B", "C", "D" });

    std::map<std::string, Record::GroupByOpcode::Opcode> fields = {
      { "C", Record::GroupByOpcode::Opcode::Sum },
      { "D", Record::GroupByOpcode::Opcode::Sum }
    };
    gbOpcode.keys = { "A", "B" };
    gbOpcode.layout = layout;
    gbOpcode.fields = fields;

    std::generate_n(std::back_inserter(input), 542700, []() { return Record::random(5); });

    inTable = new MaterializedTable(layout, input.size());
    outTable = new MaterializedTable(layout, input.size());
    inTable->WriteRecords(input.begin(), input.end());
    inTable->WriteCSV("inTable.csv");

    auto load = new Load(this, "load", _dram, inTable, enable, 4);
    auto groupBy = new SortGroupBy(this, "groupBy", _dram, { "A", "B" }, fields, load->Out());
    auto store = new Store(this, "store", _dram, groupBy->Out());

    outTable = store->Table();
    testDone = store->Done()->AddDst();
  }

  void Run() {
    Token t;
    enable->Src()->Push(t);
  }

  bool Success() {
    auto sortedIn = input;
    Record::SortOpcode sortOpcode;
    sortOpcode.ascending = true;
    sortOpcode.keys = { "A", "B" };
    sortOpcode.layout = inTable->layout;
    Gold::Sort(std::begin(sortedIn), std::end(sortedIn), sortOpcode);

    std::vector<Record> gold;
    Gold::GroupBy(sortedIn.begin(), sortedIn.end(), std::back_inserter(gold), gbOpcode);
    auto goldTable = new MaterializedTable(inTable->layout, input.size());
    goldTable->WriteRecords(gold.begin(), gold.end());
    goldTable->WriteCSV("gold.csv");

    std::vector<Record> out;
    outTable->ReadRecords(std::back_inserter(out));
    outTable->WriteCSV("out.csv");
    return TestHelper::Equal(out.begin(), out.end(), gold.begin(), gold.end(), gbOpcode.layout);
  }

private:
  Record::GroupByOpcode gbOpcode;
  MaterializedTable *inTable;
  MaterializedTable *outTable;
  FIFOBroadcast *enable;
  std::vector<Record> input;
};

DRAMController dram(
  "DDR4",
  "configs/DDR4-config.cfg",
  {},
  {}
);

TestGroupBy test(&dram);

Module DUT({
  &dram,
  &test
}, "DUT");

#endif
