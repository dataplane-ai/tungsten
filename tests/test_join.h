#ifndef TEST_JOIN_H_
#define TEST_JOIN_H_

#include <cstdlib>

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"
#include "dram.h"

#include "db_test.h"
#include "db_factory.h"

using namespace DB;
using namespace Factory;

class TestJoin : public Test {
public:
  explicit TestJoin(
    DRAMController *_dram
  )
    : Test("TestJoin", std::string()) {
    enable = new FIFOBroadcast("enable");
    AddChild(enable);

    Record::Layout layoutA({ "key", "1", "2" });
    Record::Layout layoutB({ "1", "key", "2", "3", "4", "5", "6" });

    auto sizeA = 25544;
    auto sizeB = 27767;
    for (int i = 0; i < sizeA; i++) {
      recordsA.push_back(Record::fill(sizeA - i - 1));
    }
    //for (int i = 0; i < sizeB; i++) {
      //recordsB.push_back(Record::fill(sizeB - i - 1));
    //}
    std::generate_n(std::back_inserter(recordsB), sizeB, []() { return Record::random(45); });

    tableA = new MaterializedTable(layoutA, recordsA.size());
    tableB = new MaterializedTable(layoutB, recordsB.size());

    tableA->WriteRecords(recordsA.begin(), recordsA.end());
    tableB->WriteRecords(recordsB.begin(), recordsB.end());

    tableA->WriteCSV("tableA.csv");
    tableB->WriteCSV("tableB.csv");

    auto loadA = new Load(this, "loadA", _dram, tableA, enable, 1);
    auto loadB = new Load(this, "loadB", _dram, tableB, enable, 1);

    auto sortA = new Sort(this, "sortA", {"key"}, true, _dram, loadA->Out());
    auto sortB = new Sort(this, "sortB", {"key"}, true, _dram, loadB->Out());

    auto storeA = new Store(this, "storeA", _dram, sortA->Out());
    auto storeB = new Store(this, "storeB", _dram, sortB->Out());

    auto join = new Join(this, "join", _dram, {"key"}, {"key"}, storeA->Table(), storeB->Table(), storeA->Done(), storeB->Done(), 4);
    auto merge = new Merge(this, "merge", {"key"}, true, join->Out());
    auto store = new Store(this, "store", _dram, merge->Out());

    tableOut = store->Table();
    testDone = store->Done()->AddDst();
  }

  void Run() {
    Token t;
    enable->Src()->Push(t);
  }

  bool Success() {
    Record::SortOpcode sortAOpcode;
    sortAOpcode.ascending = true;
    sortAOpcode.keys = {"key"};
    sortAOpcode.layout = tableA->layout;

    Record::SortOpcode sortYOpcode;
    sortYOpcode.ascending = true;
    sortYOpcode.keys = {"key"};
    sortYOpcode.layout = tableB->layout;

    auto sortA = recordsA;
    auto sortB = recordsB;
    Gold::Sort(std::begin(sortA), std::end(sortA), sortAOpcode);
    Gold::Sort(std::begin(sortB), std::end(sortB), sortYOpcode);

    std::vector<Record> gold;
    Record::JoinOpcode joinOpcode({"key"}, {"key"}, tableA->layout, tableB->layout);
    Gold::Join(sortA.begin(), sortA.end(), sortB.begin(), sortB.end(), std::back_inserter(gold), joinOpcode);

    std::vector<Record> out;
    tableOut->ReadRecords(std::back_inserter(out));
    tableOut->WriteCSV("out.csv");

    Record::SortOpcode sortOutOpcode;
    sortOutOpcode.ascending = true;
    sortOutOpcode.keys = {"key"};
    sortOutOpcode.layout = joinOpcode.layoutOut;

    return TestHelper::Sorted(out.begin(), out.end(), sortOutOpcode) &&
           TestHelper::Equal(out.begin(), out.end(), gold.begin(), gold.end(), joinOpcode.layoutOut);
  }

private:
  FIFOBroadcast *enable;
  MaterializedTable *tableA;
  MaterializedTable *tableB;
  MaterializedTable *tableOut;

  std::vector<Record> recordsA;
  std::vector<Record> recordsB;
};

DRAMController dram(
  "HBM",
  "configs/HBM-config.cfg",
  {},
  {}
);

TestJoin test(&dram);

Module DUT({
  &dram,
  &test
}, "DUT");

#endif
