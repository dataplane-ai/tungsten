#ifndef QUERY_H
#define QUERY_H

#include <cstdlib>

#include "db_factory.h"
#include "db_route.h"

using namespace DB;

class TestLink : public Test {
public:
  TestLink(
    DRAMController *_dram
  ) : Test("TestLink", std::string()) {
    netConfig.ideal = false;

    testEnable = new FIFOBroadcast("testEnable");
    AddChild(testEnable);

    Record::Layout layout({ "A", "B", "C", "D" });
    std::generate_n(std::back_inserter(input), 127, []() { return Record::random(8); });

    auto table = new MaterializedTable(layout, input.size());
    table->WriteRecords(input.begin(), input.end());

    Factory::Load load(this, "load", _dram, table, testEnable, 1, true);
    Factory::Link link(this, load.Out(), layout, {}, true);
    Factory::Store store(this, "store", _dram, table, link.Out(), true);

    Routing route;
    route.Route(this);
    route.WriteRoutes();
  }

  void Run() {
    Token t;
    testEnable->Src()->Push(t);
  }

  bool Success() {
    return true;
  }

private:
  FIFOBroadcast *testEnable;
  std::vector<Record> input;
};

DRAMController ddr(
  "DDR4",
  "configs/DDR4-config.cfg",
  {},
  {}
);

TestLink test(&ddr);

Module DUT({
  &ddr,
  &test
}, "DUT");



#endif
