#ifndef TESTS_STREAMAB_H_
#define TESTS_STREAMAB_H_

#include "example.h"
#include "state.h"
#include "module.h"
#include "offchiplink.h"

OffChipLink             Link("ocl", {64});
SequenceGenerator       TestGenerator("seqSrc", Link.Channels()[0]);
SequenceConsumer        TestConsumer("seqSnk", Link.Channels()[0]);
Module                  DUT({&Link,
                             &TestGenerator,
                             &TestConsumer}, "DUT");


#endif  // TESTS_STREAMAB_H_
