#ifndef TEST_SORT_H_
#define TEST_SORT_H_

#include <cstdlib>

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"

#include "dram.h"

#include "db_factory.h"
#include "db_stat.h"

using namespace DB;
using namespace Factory;

class TestSort : public Test {
public:
  explicit TestSort(
    DRAMController *_mem,
    DRAMController *_scratchpad
  ) : Test("TestSort", std::string()) {
    mem = _mem;
    scratchpad = _scratchpad;

    enable = new FIFOBroadcast("enable");
    AddChild(enable);

    opcode.layout = Record::Layout({"0", "1", "2", "3", "4", "5", "6"});
    opcode.keys = { "0", "1" };
    opcode.ascending = true;

    for (int i = 0; i < list_size; i++) {
      input.push_back(Record::random(100));
      //input.push_back(Record::fill(list_size - i - 1));
    }

    tableIn = new MaterializedTable(opcode.layout);
    tableIn->WriteRecords(input.begin(), input.end());

    tableOut = new MaterializedTable(opcode.layout);

    auto load = new Load(this, "load", _mem, tableIn, enable, 4);
    auto sort = new Sort(this, "sort", { "0", "1" }, true, _scratchpad, load->Out());
    auto store = new Store(this, "store", _mem, sort->Out());

    tableOut = store->Table();
    testDone = store->Done()->AddDst();

    routing.Route(this);
    routing.WriteRoutes();
  }

  void Run() {
    assert(enable->Src()->Ready());

    Token t;
    enable->Src()->Push(t);
  }

  bool Success() {
    DBStats stat;
    stat.Find(this, {mem, scratchpad});
    stat.Write();

    std::vector<Record> gold = input;
    Gold::Sort(std::begin(gold), std::end(gold), opcode);

    std::vector<Record> out;
    tableOut->ReadRecords(std::back_inserter(out));
    tableOut->WriteCSV("out.csv");

    return TestHelper::Sorted(out.begin(), out.end(), opcode) &&
           TestHelper::Equal(out.begin(), out.end(), gold.begin(), gold.end(), opcode.layout);
  }

private:
  FIFOBroadcast *enable;
  int list_size = 564700;

  MaterializedTable *tableIn;
  MaterializedTable *tableOut;
  Record::SortOpcode opcode;
  std::vector<Record> input;

  DRAMController *mem;
  DRAMController *scratchpad;

  Routing routing;
};

DRAMController ddr(
  "DDR4",
  "configs/DDR4-config.cfg",
  {},
  {}
);

DRAMController hbm(
  "HBM",
  "configs/HBM-config.cfg",
  {},
  {}
);

TestSort test(&ddr, &hbm);

Module DUT({
  &ddr,
  &hbm,
  &test
}, "DUT");

#endif
