#ifndef TEST_TABLE_H_
#define TEST_TABLE_H_

#include <cstdlib>

#include "example.h"
#include "module.h"
#include "state.h"
#include "token.h"

#include "db_test.h"
#include "db_factory.h"

using namespace DB;
using namespace Factory;

class TestTable : public Test {
public:
  TestTable(
    DRAMController *_dram
  ) : Test("TestTable", std::string()) {

    layout = Record::Layout({"col0", "col1", "col2", "col3", "col4"});
    int recordCount = 1000;

    for (int i = 0; i < recordCount; i++) {
      input.push_back(Record::random(200));
    }

    enable = LinkBroadcast::Create("enable");
    AddChild(enable);

    auto linkOut = LinkBroadcast::Create("linkOut");
    AddChild(linkOut);

    auto table = new MaterializedTable(layout, recordCount);
    table->WriteRecords(input.begin(), input.end());
    table->WriteCSV("test_table.csv");

    std::ifstream file("test_table.csv");
    Table inputTable(file);
    auto load = new Load(this, "load", _dram, inputTable.Subtable(layout.fieldNames), enable);
    auto sort = new SortStream(this, "sort", { "col0", "col1" }, true, _dram, load->Out());

    auto store = new Store(this, "store", _dram, sort->Out());
    tableOut = store->Table();
    testDone = store->Done()->AddDst();
  }

  void Run() {
    Token t;
    enable->Src()->Push(t);
  }

  bool Success() {
    std::vector<Record> out;
    tableOut->ReadRecords(std::back_inserter(out));
    tableOut->WriteCSV("tableOut.csv");

    return (input.size() && out.size());
  }

private:
  MaterializedTable *tableOut;
  LinkBroadcast *enable;
  std::vector<Record> input;
  Record::Layout layout;
};

DRAMController dram(
  "DDR4",
  "configs/DDR4-config.cfg",
  {},
  {}
);

TestTable test(&dram);

Module DUT({
  &dram,
  &test
}, "DUT");

#endif
