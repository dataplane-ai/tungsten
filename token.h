#ifndef TOKEN_H_
#define TOKEN_H_

#include <array>
#include <cassert>
#include <set>
#include <iostream>
#include <numeric>
#include <span>

#ifndef TOKEN_WIDTH
  #define TOKEN_WIDTH 16
#endif

#include "mserialize/tag.hpp"
#include "mserialize/detail/Serializer.hpp"

enum TokenType { TT_FLOATVEC, TT_FLOAT, TT_UINTVEC, TT_INTVEC, TT_INT8VEC, TT_INT16VEC, TT_INT, TT_UINT, TT_UINT64VEC, TT_UINT64, TT_LONGVEC, TT_LONG, TT_BOOLVEC, TT_BOOL };

using namespace std;

template<typename T>
TokenType TokType(bool vec) {
  if (is_same<T,int32_t>::value) {
    return vec ? TT_INTVEC : TT_INT;
  } else if (is_same<T,uint32_t>::value) {
    return vec ? TT_UINTVEC : TT_UINT;
  } else if (is_same<T,float>::value) {
    return vec ? TT_FLOATVEC : TT_FLOAT;
  } else if (is_same<T,int8_t>::value) {
    assert(vec);
    return TT_INT8VEC;
  } else if (is_same<T,int16_t>::value) {
    assert(vec);
    return TT_INT16VEC;
  } else if (is_same<T,int64_t>::value) {
    return vec ? TT_LONGVEC : TT_LONG;
  } else if (is_same<T,uint64_t>::value) {
    return vec ? TT_UINT64VEC : TT_UINT64;
  } else if (is_same<T,bool>::value) {
    return vec ? TT_BOOLVEC : TT_BOOL;
  } else {
    assert(false);
  }
}

template<typename T>
TokenType TokVecType(void) {
  return TokType<T>(true);
}

template<typename T>
TokenType TokScalType(void) {
  return TokType<T>(false);
}

class Token;
class ConstTokenSlice;
class TokenSlice {
  friend class ConstTokenSlice;
 protected:
  Token* t;
  int i;
 public:
  TokenSlice(Token *_t, int _i=-1) : t(_t), i(_i) {
  }
  operator uint64_t&();
  operator long&();
  operator int8_t&();
  operator int16_t&();
  operator bool&();
  operator int32_t&();
  operator uint32_t&();
  operator float&();

  int getThreadID();
  void setThreadID(int id);
};

class ConstTokenSlice {
  const Token* t;
  const int i;
 public:
  ConstTokenSlice(const Token *_t, const int _i=-1) : t(_t), i(_i) {
  }
  ConstTokenSlice(const TokenSlice ts) : t(ts.t), i(ts.i) {
  }
  operator const uint64_t&() const;
  operator const long&() const;
  operator const int8_t&() const;
  operator const int16_t&() const;
  operator const bool&() const;
  operator const int32_t&() const;
  operator const uint32_t&() const;
  operator const float&() const;

  int getThreadID() const;
};

// This is the base class for tokens, and allows them to be modeled as all
// being the same. This is important for packets in the network, where the type
// of a packet should not depend on the type of contained token.
class Token {
 public:
  int endThreadID{-1};
  std::vector<int> threadIDs;

  Token(int x) : type(TT_INT), int_(x), nvalid(1) { }
  Token(uint32_t x) : type(TT_UINT), uint_(x), nvalid(1) { } 
  Token(float x) : type(TT_FLOAT), float_(x), nvalid(1) { } 
  Token(bool x) : type(TT_BOOL), bool_(x), nvalid(1) { }
  Token(const vector<int>& x) : type(TT_INTVEC), nvalid(x.size()) { 
    for(int i=0; i<nvalid; i++) {
      intVec_[i] = x[i];
    }
  }
  Token(const array<int32_t,16>& x) : type(TT_INTVEC), intVec_(x) { }
  Token(const array<uint32_t,16>& x) : type(TT_UINTVEC), uintVec_(x) { }
  Token(const array<float,16>& x) : type(TT_FLOATVEC), floatVec_(x) { }
  Token(const array<bool,16>& x) : type(TT_BOOLVEC), boolVec_(x) { }

  void copyUnionThreadIDsAndVal(const std::vector<Token>& from, bool strict=false);

  const char* sender;
  int         seq_num;
  TokenType   type {TT_INTVEC};
  string special;
  union {
    std::array<float, TOKEN_WIDTH>   floatVec_;
    float                   float_;
    std::array<int32_t, TOKEN_WIDTH> intVec_;
    std::array<uint32_t, TOKEN_WIDTH> uintVec_;
    std::array<int8_t, TOKEN_WIDTH> int8Vec_;
    std::array<int16_t, TOKEN_WIDTH> int16Vec_;
    int                     int_;
    uint32_t                uint_;
    std::array<uint64_t, TOKEN_WIDTH> uint64Vec_;
    uint64_t                uint64_;
    std::array<long, TOKEN_WIDTH>     longVec_;
    long                    long_;
    std::array<bool,TOKEN_WIDTH>      boolVec_;
    bool                    bool_;
  };
  std::array<bool,TOKEN_WIDTH> valid;
  int nvalid = TOKEN_WIDTH;
  bool last = false;
  bool prog_last = false;

  // acrucker: this is added for the sparse splitter
  int done_vec {0};

  bool isVec() const {
    return type == TT_FLOATVEC 
      ||   type == TT_UINTVEC 
      ||   type == TT_INTVEC 
      ||   type == TT_INT8VEC 
      ||   type == TT_INT16VEC 
      ||   type == TT_UINT64VEC 
      ||   type == TT_LONGVEC 
      ||   type == TT_BOOLVEC;
  }
  TokenType vecType() const {
    if (isVec()) 
      return type;
    else if (type == TT_FLOAT)
      return TT_FLOATVEC;
    else if (type == TT_INT)
      return TT_INTVEC;
    else if (type == TT_UINT)
      return TT_UINTVEC;
    else if (type == TT_UINT64)
      return TT_UINT64VEC;
    else if (type == TT_BOOL)
      return TT_BOOLVEC;
    else
      assert(false);
  }

  operator long&() {
      return long_;
  }
  operator uint64_t&() {
      return uint64_;
  }
  operator bool&() {
      return bool_;
  }
  operator int32_t&() {
      return int_;
  }
  operator uint32_t&() {
      return uint_;
  }
  operator float&() {
      return float_;
  }

  operator const long&() const {
      return long_;
  }
  operator const uint64_t&() const {
      return uint64_;
  }
  operator const bool&() const {
      return bool_;
  }
  operator const int32_t&() const {
      return int_;
  }
  operator const uint32_t&() const {
      return uint_;
  }
  operator const float&() const {
      return float_;
  }

  TokenSlice operator[](size_t x) {
    return TokenSlice(this, x);
  };
  ConstTokenSlice operator[](size_t x) const {
    return ConstTokenSlice(this, x);
  };

  void copyMeta(const Token& t) {
    nvalid = t.nvalid;
    threadIDs = t.threadIDs;
    done_vec = t.done_vec;
  }

  int getID(int idx) const {
    if (idx >= nvalid)
      assert(false && "trying to get thread ID for invalid thread");
    // if (idx >= threadIDs.size())
      // assert(false && "to access a thread ID that was not set");
    // return threadIDs[idx];
    return threadIDs[idx];
  }

  Token();
  explicit Token(const char* _sender, int _seq_num);
  std::string ToString() const;
  std::string IDsToString() const;
  int GetValid() const;

};

template<typename T>
T& Get(Token& t, int i=-1) {
  if (is_same<T,int32_t>::value) {
    return i<0 ? t.int_ : t.intVec_[i];
  } else if (is_same<T,uint32_t>::value) {
    return i<0 ? t.uint_ : t.uintVec_[i];
  } else if (is_same<T,float>::value) {
    return i<0 ? t.float_ : t.floatVec_[i];
  } else if (is_same<T,bool>::value) {
    return i<0 ? t.bool_ : t.boolVec_[i];
  } else {
    assert(false);
  }
}

std::ostream& operator<<(std::ostream& stream, Token token);

// This is the class that wraps tokens into network packets.
struct Packet {
  Token data;
  int   flow;
  int   vc {0};
  Packet();
  Packet(const Token& _data, int _flow);
  Packet(const Token& _data, int _flow, int _vc);
  std::string ToString() const;
};

std::ostream& operator<<(std::ostream& stream, Packet packet);

struct Credit {
  std::set<int> vcs;
  explicit Credit(int v);
  Credit();
  void Add(int v);
  operator bool() {
    return !vcs.empty();
  }
};

std::ostream& operator<<(std::ostream& stream, Credit token);

namespace mserialize {

template<>
struct CustomTag<Token> {
  static constexpr auto tag_string() {
    // These MUST align to the TokenType definitions!!
    //                                               U  
    //                                  F        I   I  I    
    //                                  L  U   I N   N  N  B
    //                                  O  I I N T   T  T  O
    //                                  A  N N T 1   6 U6  O 
    //                                  T FT T 8 6   4 I4 IL 
    //                                    L            N  N  B
    //                                  V OV V V V I V TV TV O
    //                                  E AE E E E N E 6E 6E O
    //                                  C TC C C C T C 4C 4C L
    return make_cx_string("{Token`data'<[ff[I[i[b[siI[LL[ll[yy>`last'I`nvalid'I}");
  }
};

template<>
struct CustomSerializer<Token, void> {
  template <typename OutputStream>
  static void serialize(const Token& t, OutputStream& ostream)
  {
    // std::span<int> span_int(t.intVec_);
    std::vector<int> ints;
    mserialize::serialize(std::int8_t{t.type}, ostream);
    switch (t.type) {
      case TT_FLOAT:     mserialize::serialize(t.float_,     ostream); break;
      case TT_INT:       mserialize::serialize(t.int_,       ostream); break;
      case TT_UINT:      mserialize::serialize(t.uint_,      ostream); break;
      case TT_UINT64:    mserialize::serialize(t.uint64_,    ostream); break;
      case TT_LONG:      mserialize::serialize(t.long_,      ostream); break;
      case TT_BOOL:      mserialize::serialize(t.bool_,      ostream); break;

      case TT_FLOATVEC:  mserialize::serialize(t.floatVec_,  ostream); break;
      case TT_UINTVEC:   mserialize::serialize(t.uintVec_,   ostream); break;
      case TT_INT8VEC:   mserialize::serialize(t.int8Vec_,   ostream); break;
      case TT_INT16VEC:  mserialize::serialize(t.int16Vec_,  ostream); break;
      case TT_UINT64VEC: mserialize::serialize(t.uint64Vec_, ostream); break;
      case TT_LONGVEC:   mserialize::serialize(t.longVec_,   ostream); break;
      case TT_BOOLVEC:   mserialize::serialize(t.boolVec_,   ostream); break;

      // TODO: extend this to the other cases
      case TT_INTVEC:    
        for (int i=0; i<t.nvalid; i++)
          ints.emplace_back(t.intVec_[i]);
        mserialize::serialize(ints,    ostream); break;

      default: assert(false);
    }
    mserialize::serialize(t.done_vec, ostream);
    mserialize::serialize(t.nvalid, ostream);
  }

  static std::size_t serialized_size(const Token& t)
  {
    bool array{t.isVec()};
    int basesize{1};
    switch (t.type) {
      case TT_FLOATVEC:  basesize = sizeof(float);    break;
      case TT_FLOAT:     basesize = sizeof(float);    break;
      case TT_UINTVEC:   basesize = sizeof(uint32_t); break;
      case TT_INTVEC:    basesize = sizeof(int32_t);  break;
      case TT_INT8VEC:   basesize = sizeof(int8_t);   break;
      case TT_INT16VEC:  basesize = sizeof(int16_t);  break;
      case TT_INT:       basesize = sizeof(int32_t);  break;
      case TT_UINT:      basesize = sizeof(uint32_t); break;
      case TT_UINT64VEC: basesize = sizeof(uint64_t); break;
      case TT_UINT64:    basesize = sizeof(uint64_t); break;
      case TT_LONGVEC:   basesize = sizeof(int64_t);  break;
      case TT_LONG:      basesize = sizeof(int64_t);  break;
      case TT_BOOLVEC:   basesize = sizeof(bool);     break;
      case TT_BOOL:      basesize = sizeof(bool);     break;
      default: assert(false);
    }
    if (array && t.type == TT_INTVEC) 
      return sizeof(int8_t) + sizeof(int32_t) + t.nvalid*basesize + sizeof(uint32_t) + sizeof(uint32_t);
    else if (array) 
      return sizeof(int8_t) + sizeof(int32_t) + 16*basesize + sizeof(uint32_t) + sizeof(uint32_t);
    else
      return sizeof(int8_t) + basesize + sizeof(uint32_t) + sizeof(uint32_t);
  }
};
} // namespace mserialize

#endif  // TOKEN_H_
