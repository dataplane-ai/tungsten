#ifndef UTIL_H_
#define UTIL_H_

#include "module.h"
#include "interface.h"
#include "token.h"

#include <string>

using namespace std;

template<class T>
bool try_move(CheckedReceive<T>* f, CheckedSend<T>* t) {
  if (!f)
    return false;
  if (f->Valid() && t->Ready()) {
    t->Push(f->ReadPop());
    return true;
  }
  return false;
}

string rep_str_loop(string name, int i, int j, int k);

#endif
